#title Manifesto
#author Sitüasyonist Enternasyonal
#SORTtopics sitüasyonist, yabancılaşma, teknoloji, gösteri toplumu, sanat
#date 17.05.1960
#source 03.06.2022 tarihinde şuradan alındı: [[http://www.geocities.ws/anarsistbakis/makaleler/durumcumanifesto.html][geocities.ws/anarsistbakis]]
#lang tr
#pubdate 2022-06-03T18:35:10
#notes Çeviri: Anarşist Bakış <br>İngilizce Aslı: [[http://library.nothingness.org/articles/SI/en/display/9][Manifesto]], [[http://library.nothingness.org/authors.php3?id=13][SI 1960]]


Varolan çerçeve, teknolojinin karşı konulamayan gelişimi ve bunun anlamsız toplumsal yaşamımızdaki olası kullanımlarının [yarattığı] hoşnutsuzlukla birlikte gün be gün artan yeni beşeri kuvveti boyunduruk altına alamaz.

Bu toplum içerisindeki yabancılaşma ve tahakküm, bir dizi başka şekiller arasında dağıtılamaz, ancak bizzat bu toplumun kendisi tarafından toptan reddedilebilir. Bugünkü çok-biçimli krizin devrimci çözümüne kadar tüm gerçek ilerleme açıkça askıya alınmıştır.

Gerçekten de [özgün bir şekilde] "üretimi üreticilerin özgür ve eşit birliği temelinde yeniden örgütleyen" bir toplumdaki yaşamın örgütsel perspektifleri nelerdir? Çalışma, üretimin otomasyonu ve hayati malların toplumsallaştırılması yoluyla dışsal bir gereklilik olarak giderek azalacaktır --ki bu, en sonunda, bireye tam bir hürriyet sağlayacaktır. Böylece, tüm ekonomik sorumluluklardan, geçmişin ve diğer insanların tüm borç ve sorumluluklarından kurtulmuş olan insanoğlu, ücretli çalışma ölçüsüne indirgenmesi imkansız olduğu için paraya çevirilemeyen yeni bir artık değer üretecektir. Her insanın ve tüm insanların hürriyetinin güvencesi, oyunun ve özgürce inşa edilmiş yaşamın değerindedir. Bu civelek eğlencenin gerçekleştirilmesi, insanın insan tarafından sömürülmesinin son bulmasıyla güvence altına alınan yegane eşitlik çerçevesidir. Oyunun özgürleşmesi ve onun yaratıcı otonomisi, dayatılan çalışma ile edilgen boş zaman arasındaki eski işbölümünün yerini alacaktır.


Kilise, halk kutlamalarında korunan ilkel civelek eğilimleri bastırmak amacıyla sözde cadıları zaten yakmıştır. Gayri-katılımın [<em>non-participation</em>] berbat kalp-oyunlarını üreten varolan hakim toplumda, gerçek sanatsal faaliyet zorunlu bir şekilde bir suç olarak sınıflandırılır. Bu kısmen gizlidir. Skandal biçiminde ortaya çıkar.

Peki gerçek durum nedir? Bu, tamamen insan varlığıyla provoke edilen daha iyi bir oyunun gerçekleşmesidir. Tüm ülkelerin devrimci oyuncuları, günlük yaşamın tarihöncesinden ortaya çıkışını başlatmak için S.I.'da [Durumcu Enternasyonal] birleşebilirler.

Şu andan itibaren geçerli olmak üzere, varolan siyasi ve sendikal örgütlenmelerden bağımsız olarak (çünkü, biz, bunların halihazırda varolanın idaresinden daha başka bir şey örgütleme kapasitesine sahip olduklarından şüpheliyiz), yeni bir kültürün üreticilerinin otonom örgütlenmesini öneriyoruz.

Bu örgütlenme ilk kamusal kampanyasına yönelik başlangıçtaki deneysel aşamayı geride bıraktığı andan itibaren, bu örgüte atfettiğimiz en acil amaç UNESCO'nun ele geçirilmesidir. Dünya düzeyinde birleşmiş olan sanatın ve tüm kültürün bürokratikleşmesi, eklektik bir korumacılık ve geçmişin yeniden üretimi temelinde dünyada bir arada varolan toplumsal sistemlerin karşılıklı derin ilişkilerini ifade eden yeni bir fenomendir. Devrimci sanatçıların bu yeni koşullara karşı hamlesi yeni türde bir eylem olmalıdır. Kültür yönetimini tek bir yapı içerisinde toplayan bu yoğunlaşma komplocu [<em>putsch</em>] ele geçirmeyi tercih ettiği için; ve kurum, bizim yıkıcı perspektifimiz dışında herhangi bir anlamlı kullanımdan yoksun olduğu için, bu aygıtın ele geçirilmesini çağdaşlarımız önünde meşru görüyoruz. Ve onu ele geçireceğiz. Uzun bir talepler dizisinin aydınlatılmasında en önemli [iş] olduğu ortaya çıkacak olan bu işi hızla yerine getireceğimizden emin olduğumuz için, yanlızca kısa bir zaman için olsa dahi UNESCO'yu ele geçirmeye kararlıyız.

Yeni kültürün temel özellikleri ne olacaktır ve eski sanatla nasıl karşılaştırılacaktır?

Gösteri [<em>spectacle</em>] karşı, gerçekleştilen durumcu kültür topyekün bir katılımı işin içine sokar.

Korunan sanata karşı, yaşanan anın derhal örgütlenmesidir.

Parçalanmış [<em>particularised</em>, birbirinden tamamen ayrı, her biri kendine özgü ve özel olarak görülen] sanata karşı, bu, kullanılabilir tüm unsurlarla her an ilgili olan, küresel bir pratik olacaktır. Tabiatiyle, bu, hiç şüphesiz ki anonim olacak bir kolektif üretime meyledecektir (en azından işlerin artık metalaşmaması ölçüsünde, bu kültür arkasında izler bırakma gereğinin hakimiyetinde olmayacaktır.) Bu deneyimlerin asgari önerileri, davranışta bir devrim ve --tüm gezegene yayılma, ve bunun da ötesinde yaşanılabilir tüm gezegenlere yayılma yetisine sahip olan-- dinamik üniter bir şehirciliktir.

Tek yönlü [<em>unilateral</em>] sanat karşısında, durumcu kültür bir diyalog sanatı, bir etkileşim sanatı olacaktır. Bugün, --tüm görünür kültürüyle-- sanatçılar, tıpkı rekabet nedeniyle birbirlerinden koparıldıkları gibi, toplumdan tamamen koparılmışlardır. Ancak sanat, kapitalizmin bu açmazıyla karşı karşıya iken, buna tepki olarak temelde tek yönlü kalmıştır. İlkelciliğin bu içine kapalı döneminin üstesinden tam bir iletişimle gelinmelidir.

Daha ileri bir aşamada, herkes bir sanatçı, yani bütün kültür yaratımının ayrılmaz bir üreticisi-tüketici haline gelecektir; bu ise doğrusal yenilik kriterinin hızla dağıtılmasına yardım edecektir. Eğilimlerin, deneyimlerin veya radikal şekilde farklı "okulların" çok-yönlü --birbiri ardına değil, eş zamanlı-- patlamasıyla, tabiri caizse herkes bir durumcu olacaktır.

Tarihsel olarak zanaatların en sonuncusu olacak şeyi resmen başlatacağız. Amatör-profesyonel --uzman karşıtı-- bir durumcunun rolü, yine de --sanatçıların kendi yaşamlarını inşa etmeyi sonlandırmamış olmaları anlamında-- herkesin "sanatçı" olduğu ekonomik ve zihinsel bolluk noktasına gelinceye kadar uzmanlaşmadır. Ancak, tarihin son zanaatı, kalıcı bir işbölümü [uzmanlaşma] olmadan topluma o derece yakındır ki, SI içinde ortaya çıktığında, zanaat olarak statüsü genellikle reddedilir.

Bizi doğru dürüst anlamayanlara, küçümsenemez bir aşağılamayla şunu söylüyoruz: "Belki de yargıçları olduğunuza inandığınız durumcular, bir gün gelecek sizi yargılayacaklar. Tüm biçimleriyle yoksunluk dünyasının kaçınılmaz tasfiyesi olacak o dönüm noktasını bekliyoruz. Bunlar bizim hedeflerimizdir, ve bunlar insanlığın gelecekteki hedefleri olacaktır."

