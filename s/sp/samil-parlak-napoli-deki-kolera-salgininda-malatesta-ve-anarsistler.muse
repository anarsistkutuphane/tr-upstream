#title Napoli’deki Kolera Salgınında Malatesta ve Anarşistler
#author Şamil Parlak
#SORTtopics errico malatesta, koranavirüs, salgın, tarih
#date 02.05.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-05-02T17:07:58




<quote>
“Halk artık yiyecek bir şey bulamadığında zenginleri yiyecek.”

– La Révolte
</quote>


1884 yılında Napoli, tüm Avrupa’da görülen, yüzyılın en etkili salgınıyla karşı karşıyaydı. Avrupa’nın tüm büyükşehirlerinde görülen kolera salgını Napoli’de bu büyükşehirlerin tamamından daha büyük bir etki yaratacak, sonrasında şehrin mimarisini yeniden yapılandırmaya girişip labirentleri yıkarak büyük bulvarlarda kesişen caddelere sahip yeni merkezi bulvarlar yapacak kadar kalıcı etkiler bırakacaktı. Yaklaşık 2 milyon nüfusu bulunan Paris’te 1884 yılı boyunca kolera salgını kaynaklı yaşamını yitirenlerin sayısı 986 iken ortalama 550 bin nüfuslu Napoli’de kolera 14 bin kişiyi etkilemiş, 7 bin kişi salgın nedeniyle yaşamını yitirmişti. [1]

Henüz 14 yaşındayken İtalya Kralı’na bölgesel bir adaletsizlikle ilgili olarak yazdığı şikayet mektubu nedeniyle hapis cezası ile tanışan Errico Malatesta, yaşamı boyunca defalarca tutuklandı veya İtalya dışına çıkmak zorunda kaldı. Tıp öğrencisiyken bir eyleme katıldığı için okuldan atılmıştı. 1884 yılına gelindiğinde, “La Questione Sociale”de yazılan yazılar gerekçe gösterilerek hakkında 3 yıl hapis cezası verilmişti. Cezanın temyiz duruşması 14 Kasım’da görülecek ve Malatesta bu süre boyunca tutuksuz yargılanacaktı. Hapis cezasının kesinleşeceği tahmin edildiği için İtalya’dan ayrılmayı planlayan Malatesta, aynı dosyadan ceza alan yoldaşlarıyla birlikte karantina altında olan Napoli’ye gitmeyi tercih etti. Kolera salgınına yakalanan hastaların tedavi edilebilmesi ve karantina altına alınmış bir şehirde yaşayan ezilenlerle dayanışma göstermek için Napoli’ye giden anarşistler, salgın bittikten sonra “Kolera salgınının nedenleri içerisinde sefillik var, sefil yaşamlar salgının yayılmasını hızlandırdı, bu bir adalet sorunudur.” diyerek şehirden ayrılacaklardı.

Napoli’de devlet kolera salgınına büyük bir baskıyla karşılık verdi; sıkıyönetim altına alınan şehre giriş çıkışlar yasaklandı, sokağa çıkma yasağı uygulandı. Devrimcilerin yaklaşımı ise bambaşka oldu. Errico Malatesta, Galileo Palla, Fransesco Pezzi, Luisa Minguzzi, Fransesco Nata, Cesare Agostinelli, Antonio Valdre, Rocco Lombardo ve Massimiliano Boschi Napoli’deki ezilenlerle dayanışmak için şehre gelen anarşistlerden bazılarıydı. Hızlıca gönüllü grupları oluşturdular. Napoli’ye gelir gelmez uzun yıllardır burada yaşayan anarşist Florentine Lombard ile bir araya geldiler. Lombard aynı zamanda bir hemşireydi, salgın boyunca yürütülen dayanışma faaliyetlerini örgütleyenlerin arasında önemli bir rolü vardı. Kolera, yasaklı Napoli sokaklarında ezilenlerle dayanışmaya koşan devrimciler için de ölümcüldü. Bu süreçte anarşist gazete Proximus Tuns’tan Antonio Valdre, Rocco Lombardo ve Massimiliano Boschi koleraya yakalanarak yaşamını yitirenler arasındaydı. [2]

1884 yılında Fransızca yayınlanmakta olan anarşist gazete La Révolte, Napoli’de yaşananları şöyle aktarıyor:

“Kolera İtalya’da etkisini gösterdiğinde haliyle proleter ailelerden çok fazla kurban vardı çünkü onlar hijyen lüksüne para ayırabilecek durumda değillerdi. Hijyen de diğer bütün ayrıcalıklar gibi sadece burjuvaziye aitti. Halk artık yiyecek bir şey bulamadığında zenginleri yiyecek.” [3]

La Révolte Napoli’de dayanışma için bulunan yoldaşlardan da haber veriyordu:

“Napoli’deki kolera salgını bildiğiniz gibi işçiler arasında büyük bir sıkıntıya yol açtı. Bu süreçte hastaları iyileştirmek üzere Napoli’ye giden yoldaşlarımız döndüklerinde koleranın gerçek sebebini anlatan bir manifestoyu -sefalet ve bunun tek çözümü olan toplumsal devrimin vurgulanacağı- yayınlayacaklar. Bugünlerde gazeteler, doğal olarak dindar gazeteler ‘insanların rahatça ölmesine izin vermeyen acımasız anarşistlerin’ üzerine polis kalabalıklarını göndermeye çalışmadan edemiyor. Ama bunlar bizi durduramayacak.

Anarşistler hasta insanları iyileştirmek pahasına yaşamlarını yitirdiler. Lombardo, Boschi ve diğer yerlerdeki yoldaşlar da ölenler arasındaydı. 2 Kasım tarihinde kaybettiğimiz yoldaşlarımız için bir anma töreni örgütledik. Devlet bu anmayı engellemeye çalıştı fakat biz pes etmedik.” [4]

Errico Malatesta, Napoli’de bulunduğu süre boyunca bir hasta grubunun sorumluluğunu üstlenmişti. Bu hasta grubu, en yüksek iyileşme oranının yakalandığı gruplardan biri olacaktı. Tüm gönüllülerin devlet tarafından ödüllendirildiği Napoli’de, Malatesta ve -bir çoğu çoktan İtalya’dan ayrılmış- yoldaşlarının payına düşen ödül, karantinadan önce verilen hapis cezasının temyiz duruşmasına gitmedikleri gerekçesiyle çıkarılan yakalama kararı olmuştu.

Malatesta da hakkında yakalama kararı çıkmasından sonra Floransa’ya geçti. Floransa’da yoldaşlarıyla kaldığı bina polis tarafından kuşatıldı. Hızlıca bir kaçış planı hazırlandı. Bir dikiş makinası kutusunun içerisine saklanarak yoldaşları tarafından binanın önünde bekleyen yük arabasına taşınacaktı. Bu sırada kapıda bekleyen polislerden biri, Malatesta’nın içinde olduğu kutunun yük arabasına taşınmasına yardım edecek, Malatesta aynı gece Buenos Aires’e doğru yola çıkacaktı. [5]

Yaşamının büyük bölümünü ezilenlerin özgürlük mücadelesi pahasına hapiste ve sürgünde geçiren Malatesta, doğrudan ezilenlerin yaşamlarını tehdit eden bir salgın söz konusu olduğunda, yoldaşlarıyla birlikte kuşkusuz dayanışmaya koştu. Devletin saldırıları, kolerayla boğuşan ezilenlerle dayanışmasına da özgürlük mücadelesini başka coğrafyalara taşımasına da engel olamadı. La Révolte’yle kolera bölgesinden seslenen anarşistler, bölgede yaşadıkları engellemelere rağmen “Bunlar bizi durduramayacak!” demişti. Malatesta’yı, dayanışmayı, anarşizmi durduramadılar.


[1] Frank M. Snowden: Naples İn The Time of Cholera 1884-1911.

[2] Max Nettlau: Errico Malatesta, The Biography of An Anarchist.

[3] La Révolte, Yıl: 6 Sayı: 15, 14 Eylül 1884.

[4] La Révolte, Yıl: 6 Sayı: 21, 7 Aralık 1884.

[5] Luigi Fabbri: Life of Malatesta.



