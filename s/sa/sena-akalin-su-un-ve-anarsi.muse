#title Su, Un ve Anarşi
#author Sena Akalın
#SORTtopics işçi, anarşist hareket, tarih, anarşist, arjantin, güney amerika, errico malatesta
#date 30.11.2015
#source 19.07.2022 tarihinde şuradan alındı: [[https://www.5harfliler.com/su-un-ve-anarsi/][5harfliler.com]]
#lang tr
#pubdate 2022-07-19T11:56:10



Bundan bir kaç sene önce her sabah, Buenos Aires’in Palermo semtinden metroya binip, Recoleta semtinin Ayacucho sokağında bulunan üniversite binasında saat sekizde başlayan dersime yetişmem gerekiyordu.

Her gün evimin bulunduğu sokaktan dümdüz ilerleyip Serrano sokağına sapar sapmaz, köşe başındaki fırından yeni çıkmış kruvasan ve benzeri poğaça kokularına bu sefer karşı gelebilirim diye kendimi motive etmeye çalışsam da, kendimi hep fırında çalışan tezgâhtara günaydın derken buluyordum.



[[s-a-sena-akalin-su-un-ve-anarsi-1.png 60 f]]


Tepsilere özenle yerleştirilmiş çoğu Arjantin ve Uruguay’a özgü karamel (<em>dulce de leche</em>) dolu farklı şekillerdeki kruvasan ve milföy hamurundan yapılmış tatlıları kese kâğıdına dolduran tezgâhtara istediklerimi elimle işaret edip, ücreti öder ödemez birazdan mideme indireceğim sıcacık tatlıları düşünerek fırından mutlu bir şekilde ayrılıyordum.

Arjantin’de yaşadığım yaklaşık üç sene boyunca neredeyse her gün yediğim bu poğaça benzeri tatlıların isimlerini bir türlü ezberleyemediğim için fırına her gittiğimde onlara “bu, şu, o” diye işaret etmeye devam ettim. Bundan bir sene evvel, tekrar Buenos Aires’e gittiğimde bir sabah rutini olan fırın ziyaretimi bu sefer doğma büyüme Buenos Airesli bir arkadaşımla gerçekleştirdim. Arkadaşım Ivana’nın büyük büyük babası 1880’lerin başlında Ukrayna’daki açlıktan ve pogromlardan kaçmak için Odessa’dan Almanya’ya gitmiş ve daha sonra da kendisi gibi bir çok Yahudi’nin bulunduğu Weser gemisiyle Buenos Aires’e göç etmiş.

Ivana’nın ailesi gibi bir çok farklı aile, Avrupa ülkelerindeki yoksulluk ve savaşlardan kaçarak <em>tierra nueva</em> (yeni topraklar) olarak adlandırdıkları Arjantin’e fikirleri ve geleneklerini de getirmişlerdi. Fırına girdiğimizde Ivana benim gibi tatlılara işaret etmek yerine isimlerini sıralamaya başladı. Parayı ödeyip sokağa çıktığımızda, kese kâğıdına sarılı tatlılardan birini eline alarak bana dönüp elindekinin isminin ne anlama geldiğini bilip bilmediğimi sordu. Bilmiyordum. <em>Rahibe iç geçirmesi</em> dedi. Baya şaşırmış bir şekilde nasıl yani diye sorduğumda, bana şimdi sizlerle paylaşacağım hikayeyi anlatmaya başladı:

[[s-a-sena-akalin-su-un-ve-anarsi-2.png f][Weser Buharlı Gemisi]]

Livorno doğumlu İtalyan anarşist Ettore Mattei ve yine onun anarşist teorisyen komünist arkadaşı Errico Malestesta, 19. yüzyılın başlarında bir çok fakir İtalyan köylü ve işçinin hayallerini süsleyen Arjantin’e kaçmışlardı. Buenos Aires’e geldiklerinde siyasi faaliyetlerine ve eylemlerine ara verecek değillerdi. İtalyan fırın işçilerini örgütleyerek 18 Temmuz 1887’de Arjantin’nin ilk anarşist Fırıncılar Sendikasını, “La Sociedad Cosmopolita de Resistencia y Colocación de Obreros Panaderos”ı kurdular. Sendikanın kuruluş prensiplerini doğrudan eylem ve devrimci grevlerin gerçekleştirilmesi olarak belirlediler. Ivana’nın anlattıklarını dinledikten sonra dayanamayıp küçük bir araştırma yaptım. Anarşist fırıncılar sendikasının kuruluş manifestosuna da baktım, tam olarak şöyle yazıyor: “Amaç işçinin entelektüel, ahlaki ve fiziksel olarak gelişimini sağlamak ve onu kapitalizmin pençelerinden kurtarmak.”

Sendika üyesi fırıncılar çalışma şartlarının düzelmesi için 1888’de greve gitmiş ve on gün süren direnişin neticesinde çalışma şartlarında ciddi bir düzelme görülmüş.

[[s-a-sena-akalin-su-un-ve-anarsi-3.png f][Anarşist Fırıncı İşçiler Sendikası üyeleri]]

[[s-a-sena-akalin-su-un-ve-anarsi-4.png f][Fırıncı İşçisi gazetesi]]

[[s-a-sena-akalin-su-un-ve-anarsi-5.png f][Eski bir Buenos Aires Fırını]]

[[s-a-sena-akalin-su-un-ve-anarsi-6.png f][‘Rahip testisi’ olarak adlandırılan karamelli tatlı]]

[[s-a-sena-akalin-su-un-ve-anarsi-7.png f][Libritos: Kutsal Kitapçıklar]]

Anarşist fırıncı sendikasının en büyük başarılarından biri de, daha sonra internette hakkında yazılan araştırmalara rastladığım, 1894-1930 yılları arasında düzenli bir şekilde yayınlanan <em>El Obrero Panadero (</em>Fırın İşçisi) gazetesi olmuş. Fakat Ivana’ya göre anarşist fırıncıları Arjantin günlük hayatının vazgeçilmez bir parçası yapan ve bugün bile hikâyelerini canlı tutan şey, hazırladıkları ürünlere verdikleri isimler: Kiliseye olan tepkilerini “rahibe iç geçirmesi”, “rahip testisi” ve “kutsal kitapçıklar” adlarını verdikleri küçük poğaçalarla dile getirmişler, her yürüyüşlerinde ve eylemlerinde dayak yedikleri polislerin isimlerini sopa şeklindeki tatlılara vermişler, orduya olan nefretlerini ise içi karamel dolu küçük poğaçalara karamelli gülle ve top diyerek ölümsüzleştirmişler.
