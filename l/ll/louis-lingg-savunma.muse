#title Savunma
#author Louis Lingg
#SORTtopics haymarket, mahkeme, konuşma, 1 mayıs, abd
#date 1886
#source 08.05.2022 tarihinde şuradan alındı: [[https://karala.org/dergi/louis-lingg-savunma-ceviri-zehra-garip/][karala.org]]
#lang tr
#pubdate 2022-05-08T20:57:06
#notes Çeviri: Zehra Garip <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/louis-lingg-address-to-the-court][Address to the Court]]



Adalet Mahkemesi! Bu özgürlükler ülkesi Amerika’da, tüm insanlığın insan gibi yaşamasına yetecek bir geçim kaynağı yaratma çabalarımla alay ettiğiniz gibi, şimdi beni ölüme mahkum ettikten sonra bana son bir konuşma yapma özgürlüğü tanıyorsunuz. Sadece üzerime yığılan iftiralar ve hakaretlerin adaletsizliğini ortaya çıkarmak amacıyla bu “ayrıcalığı” kabul ediyorum.

Beni cinayet ile suçlayıp mahkum ettiniz. Hangi kanıt size suçlu olduğumu düşündürdü? Öncelikle, Seliger denilen şahsı bana karşı tanıklık yapması için getirdiniz. Onun ve benim bombaların yapılmasına yardım ettiğimizi ve o bombaları Clybourn Caddesi 58 Numara’ya götürdüğümü başka biri sayesinde kanıtladınız. Ama sizin, olayda bu kadar önemli bir rol oynamış gibi görünen, satın aldığınız “muhbir” Seliger’in yardımlarıyla bile kanıtlayamadığınız şey, bu bombalardan herhangi birinin Haymarket’e götürülüp götürülmediğidir.

Aynı zamanda birkaç kimyager buraya uzman olarak getirildiler, ancak tek bulabildikleri Haymarket bombalarının yapıldığı metal ile benim bombalarım arasındaki belli bir benzerlikti ve Bay Ingham, bombaların oldukça farklı olduğunu inkâr etmek için boşuna çaba sarf etti. Kabuk kısmındaki kalınlıkta çeyrek inçlik bir fark olduğu gerçeğini gizlemesine rağmen, çaplarında yarım inçlik bir fark olduğunu kabul etmek zorunda kaldı.

Ancak, beni mahkum ettiğiniz şey cinayet değil. Yargıç bunu daha bu sabah yazdığı davanın özgeçmişinde belirtti ve Grinnell defalarca bizim cinayetten değil, anarşizmden yargılandığımızı itiraf etti. Yani mahkumiyetimin sebebi, bir anarşist olmamdır!

Anarşizm nedir? Bunu yoldaşlarım yeterince net açıkladılar, benim tekrar üzerinden geçmeme gerek bile yok. Amaçlarımızın neler olduğunu yeteri kadar açık anlattılar fakat eyalet savcısı size bu bilgileri vermedi. O sadece anarşizmin doktrinlerini değil, aynı zamanda pratik yöntemlerini de kınadı ve burada bile bu yöntemlerin bize polisin gaddarlığı tarafından dayatıldığı gerçeğine karşı ihtiyatlı sessizliğini sürdürdü.

Grinnell’in şikayetlerimiz için önerdiği çare oy pusulası ve sendikaların birleşmesidir. Ingham bile altı saat hareketinin arzu edilirliğini kabul etmiştir. Ama gerçek şu ki, oy pusulasını kullanmak için her girişimde, işçilerin çabalarını birleştirmek için giriştiği her çalışmada, polisin acımasız şiddetine maruz kaldık ve ben mücadele için kaba kuvveti bu yüzden tavsiye ettim.

Beni kanun ve düzeni aşağılamak ile suçladınız. Sizin “kanun ve düzen” dediğiniz şey ne işe yarıyor? Onun temsilcileri polistir ve saflarında hırsızlar vardır, işte Yüzbaşı Schaack burada oturuyor. Ofisinde şapkamın ve kitaplarımın polisler tarafından çalındığını kendisi de itiraf etti.

İşte bunlar sizin mülkiyet haklarınızı savunanlar! Beni tutuklayan dedektifler, Burlington Sokağı’ndan bir marangoz olan Lorenz’in adını vererek, sahte beyanlarla, hırsız gibi zorla evime girdiler. Odamda yalnız kaldığıma yemin ederek yalancı şahitlik yaptılar. Hâlihazırda orada bulunan bu kadını (Mrs. Klein), mahkemeye çağırmadınız ve adı geçen dedektiflerin odama sahte beyanlarla girdiklerine ve tanıklıklarının yalan olduğuna şahitlik etmesini engellediniz.

Ama daha da ileri gidelim. Yalan söyleyerek yemin eden bir polis yüzbaşımız var. Pazartesi gecesi toplantıda olduğumu kabul ettiğime yemin etti, oysa katıldığım toplantının Zepf’s Hall’da bir marangoz toplantısı olduğunu ona açıkça belirtmiştim. Johann Most’un kitabından bomba yapmayı öğrendiğimi söylediğime dair bir yemin daha etti, bu da bir iftiradır.

Bu kanun ve düzen temsilcileri arasında birkaç adım daha yukarı çıkalım. Grinnell ve ortakları yalancı şahitliğe izin verdiler ve ben bunu kasıtlı olarak yaptıklarını söylüyorum. Kanıt, avukatım tarafından sunuldu, ayrıca kendi gözlerimle Grinnell’in, kürsüye çıkmadan sekiz gün önce, aleyhime şahitlik yapacak adamların isimlerini Gilmer’a işaret ettiğini gördüm.

Bir yandan ben, yukarıda da belirttiğim gibi, kendim ve iş arkadaşlarım için insanca yaşamaya yetecek bir geçim kaynağının kazanılması uğruna kullanılan güce inanıyorken; öte yandan Grinnel, polisi ve diğer haydutları aracılığıyla, aralarında benim de bulunduğum yedi adamı öldürmek için yalancı şahitlik yaptırıyor. Grinnell burada, kendimi savunamadığım mahkeme salonunda bana korkak diyecek kadar zavallı bir cesarete sahiptir! Alçak!

Beni darağacına götürmek için bir sürü aşağılık düzenbazla birleşen bir adam. Neden peki? Makul bir nedeni yok, aşağılık bir bencillikten başka bir şey yok, tamamen “para için yükselme” arzusu.

Diğer zavallıların yalanları ile yedi adamı öldürecek olan bu zavallı, bana “korkak” diyen adam! Ve buna rağmen, bu tür “hukukun savunucularını”, bu kadar ağza alınmaz ikiyüzlüleri küçümsediğim için beni suçluyorsunuz!

Anarşi, bir kimsenin diğeri üzerinde tahakküm ve otoritesi olmaması anlamına gelir. Ama siz haydutların ve hırsızların hizmetine ihtiyaç duymayacak bu sisteme “düzensizlik” diyorsunuz!

Yargıç, eyalet savcısının bomba atma olayıyla beni bağdaştıramadığını kabul etmek zorunda kaldı. Ancak eyalet savcısı bunu nasıl aşacağını biliyor ve beni “komplocu” olmakla suçluyor. Bunu nasıl kanıtlayacak peki? Basitçe Uluslararası Emekçi Halklar Derneği’ni (International Working People’s Association) bir “komplo” olarak sunarak. Ben de o birliğin bir üyesiydim, bu yüzden bana rahatlıkla saldırdı. Harika! Bir eyalet savcısının dehası için hiçbir şey zor değildir!

Talihsizce birlikte yargılandığım yoldaşlarımla ilgili değerlendirme yapmak bana düşmez. Gerçekten ve açıkça söyleyebilirim ki, mahkum arkadaşlarımla Kaptan Schaack ile olduğum kadar bile yakın değilim. Evrensel sefalet ve kapitalist sırtlanın tahribatı bizi mücadelede bir araya getirdi. Kişiler olarak değil, aynı amaç uğruna savaşan işçiler olarak. Beni mahkum ettiğiniz “komplo” budur.

Mahkemenin kararına karşı, mahkûmiyetimizi protesto ediyorum. Geçmiş yüzyılların hiçkimseleri tarafından yapılmış karmakarışık yasalarınızı ve mahkemenin kararını tanımıyorum.

Kendi avukatım, aynı derecede yüksek mahkemelerin emsal kararlarından, bize yeni bir yargılama yapılması gerektiğini kesin olarak kanıtladı. Eyalet savcısı, aksini kanıtlamak için belki de daha yüksek mahkemelerden üç kat daha fazla karar getiriyor. Ve başka bir davada, eğer yargılanacak olan anarşistlerse ve bu kararların yirmi bir ciltle desteklenmesi gerekiyorsa, değil yirmi bir, yüz yirmi bir cilt yalanla destekleyebileceklerini de biliyorum.

Böyle bir yasayla bile, ki küçük bir çocuk bile buna güler, bu tür yöntemlerle bile, yalan yere ettikleri sayısız yemine rağmen bizi “yasal olarak” mahkûm etmeyi başaramadılar.

Size açıkça söylüyorum ben şiddet yanlısıyım. Eğer bizim üstümüze top atarlarsa, biz de onlara karşı dinamit atarız. Ben bu düzenin düşmanıyım, açıkça söylüyorum, bütün gücümle, son nefesime kadar onunla savaşacağım. Tekrar, açık ve net olarak, güç kullanmaktan yana olduğumu beyan ediyorum. Daha önce yüzbaşı Schaack’a da söyledim, sözlerimin arkasındayım. Bize top atarsanız biz de size dinamit atarız!

Gülüyorsunuz! Belki artık dinamit atamayacağımı düşünüyorsunuz. Ama emin olun ki ölüme mutlu gidiyorum. Çünkü biliyorum ki bugüne kadar konuştuğum binlerce insan sözlerimi hatırlayacaktır. Ve bizi astığınız an asıl bombayı onlar atacak!

Bu umutla size sesleniyorum. Sizi aşağılıyorum. Sizin düzeninizi tanımıyorum, zorba yasalarınızı, iktidarınızı, otoritenizi, tahakkümünüzü aşağılıyorum.

Bu yüzden asın beni!



