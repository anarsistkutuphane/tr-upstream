#title Sitüasyonistler - bir giriş
#author libcom.org
#SORTtopics sitüasyonist, giriş
#date 12.10.2006
#source 01.05.2022 tarihinde şuradan alındı: [[https://www.yeryuzupostasi.org/2021/07/12/situasyonistler-bir-giris/][www.yeryuzupostasi.org]]
#lang tr
#pubdate 2022-05-01T12:04:46
#notes Çeviri: Yeryüzü Postası <br>İngilizce Aslı: [[https://libcom.org/article/situationists-introduction][Situationists - an introduction]]



Sitüasyonist fikirler, 1957’de kurulan Avrupalı örgüt Situationist Enternasyonal’den (SI) geldi. Sadece 15 yıl var olmasına rağmen fikirleri derin etkiye sahipti ve o zamandan beri Batı toplumunun – ve radikal hareketlerin – bir parçası oldu.

Fikirlerini statik bir ideolojiye dönüştürmeye yönelik kalıplaştırma çabalarına direnen sitüasyonizm, yani SI, Troçkizm, Leninizm, Maoizm ve hatta anarşizm benzeri ideolojilerde olduğu gibi mutlak olduğu var sayılan bazı doğruları sürekli tekrarlamak yerine, dikkati sürekli olarak deneyimlenen ve kendisini düzelten gerçek hayatın canlı etkinliğinin öncelikli olduğuna çekti. Statik ideolojiler, ne kadar doğru olurlarsa olsunlar, kapitalist toplumdaki diğer her şey gibi, katılaşma ve fetişleştirilme, sadece pasif bir şekilde tüketilecek bir şey daha haline gelme eğilimlidirler.

Kısmen bunun da sonucu olarak, sitüasyonist fikirler, açıklanması zor olduğu ve ayrıntılı bir izah gerektirdiği şeklinde kötü bir üne sahiptir. Ancak, birkaç noktanın üzerinde durulabilir. Sitüasyonistlerle ilgili yapılan açıklamaların çoğu kültürel fikirlerine, özellikle <em>détournement[1] </em> (popüler kültürün imgelerinin altüst edilmesi) ve Punk’ın gelişimi ile olan ilişkisine odaklanır, ancak Sitüasyonist fikirlerin kökleri Marksizmdedir. Geleneksel Marksizmin otoriter kollarından ziyade, işçilerin kapitalizmde sistematik olarak sömürüldüğü ve onların örgütlenip, üretim araçlarını kontrol altına almaları ve toplumu demokratik işçi konseyleri temelinde örgütlemeleri gerektiği şeklindeki, temel anlayışıyla anarşizme daha yakın olan özgürlükçü Marksizmdedir.

Sitüasyonistler veya Situs, kapitalizmi tüketim toplumu biçiminde analiz eden ilk devrimci gruptu. Bugün olduğu gibi, o zaman da Batı’da çoğu işçi fabrikalarda ve madenlerde günde 12 saat çalışan umutsuz yoksullar değillerdi (işçilerin önceki 150 yıldaki mücadeleleri bunu olmasını sağlamıştı). Ancak günlük yaşamın sefaleti hiç bu kadar büyük olmamıştı. İşçiler vahşi zor önlemleriyle değil, en az bunun kadar, kültür ve pazarlama yoluyla empoze edilen, gerçekten sahip olunamayan karakterde, gereksiz tüketim mallarının yarattığı illüzyonlar veya şatafatlı gösterilerle bastırıldılar. Örneğin, şu ya da bu zımbırtıyı ya da ayakkabı markasını satın almanın hayatınızdaki eksikliği gidereceği ya da hüzünlü yaşamınızı ünlülerin ve modellerin kültürünün bize gösterdiği hale getireceği [empoze edilerek].

Situs, işçilerin artan maddi zenginliğinin sınıf mücadelesini durdurmak ve o sırada soldaki birçok kişinin iddia ettiği gibi kapitalizmin sürekli varlığını sağlamak için yeterli olmadığını, çünkü otantik insan arzularının her zaman yabancılaşan kapitalist toplumla çatışacağını savundu. Sitüasyonist taktikler, insanların metalar aracılığıyla değil, insanlar olarak etkileşime gireceği “durumlar” yaratma girişimini içeriyordu. Gerçek bir toplum haline gelinebildiği anlarında gelecek, neşeli ve yabancılaşmamış bir toplum olasılığını gördüler.

<em>“Açıkça gündelik hayata atıfta bulunmadan, aşkta neyin yıkıcı olduğunu ve baskının reddedilmesinde neyin olumlu olduğunu anlamadan, devrimden ve sınıf mücadelesinden konuşan insanlar, böyle insanların ağızlarında cesetler var.”</em> [2]

Nanterre Üniversitesi’ndeki bir grup sitüasyonist ve anarşist, fikirlerinin geçerliliğinin göz alıcı (karşıtı) bir şekilde gösterilmesinde ve ülkeyi kasıp kavuran ve onu devrime yakın bir duruma getiren, on milyon işçinin genel grevde olduğu, birçoğunun işyerlerini işgal ettiği Mayıs 1968 İsyanı’nın ateşlenmesinde etkili oldular.

SI ‘nin kilit figürü Guy Debord, 1994’te intihar etti, ancak günümüzde çoğu anarşist teorinin temel bir parçası haline gelmiş olan sitüasyonist fikirler ve günümüzde çoğu insan tarafından artık bilinen bir gerçek olarak kabul edilen tüketimcilik hakkındaki düşünceleri yaşamaya devam ediyor.

“Kazanacak keyif dolu bir dünya var ve can sıkıntısından başka kaybedecek bir şeyimiz yok.” [3]


[1] Çn. Fransızca kökenli Détournement kavramı saptırma, ters döndürme, yıkma, yozlaştırma olarak çevrilebilmektedir. Gösteri toplumunun bilinen imgelerinin dönüştürülmesi, bu imgelere farklı, çoğu zaman zıt anlamlar yüklenerek kullanılması olarak özetlenebilir.
[2] The Revolution of Everyday Life – Raoul Vaneigem [ç.n. Türkçe basım: Harry Cleaver, Gençler İçin Hayat Bilgisi El Kitabı, Ayrıntı Yayınları]
[3] a.g.e
