#title İşçi sınıfının laneti: Fazla alakadar olmak
#author David Graeber
#SORTtopics işçi sınıfı, kapitalizm, çalışma, iş, dayanışma, topluluk, kemer sıkma, çeviri
#date 26.03.2014
#source 25.12.2021 tarihinde şuradan alındı: [[https://vesaire.org/isci-sinifinin-laneti-fazla-alakadar-olmak/][vesaire.org]]
#lang tr
#pubdate 2021-12-25T19:22:59
#notes Çeviri: Deniz Dehri, [[https://vesaire.org/isci-sinifinin-laneti-fazla-alakadar-olmak/][vesaire]]


<quote>

Kemer sıkma mantığı neden herkes tarafından kabullenildi? Çünkü dayanışma fikri musibet gibi görülmeye başladı.

</quote>

Para ve mevki sahibi ailelerden gelen kişilerin “İnsanların neden ayaklanıp sokaklara inmediğini aklım almıyor!” gibi sözler ettiklerine zaman zaman tanık oluyorum. Bir tür inanmazlık seziliyor bu sözlerinde. Bunları söylerken alttan alta “Sonuçta vergiyle ilgili ayrıcalıklarımıza dokunulduğunda biz yeri göğü inletiyoruz, biri benim gıda ve barınma hakkıma el uzatmaya cüret edecek olsa kesin gider bankaları yakar, meclisi basarım. Bu insanların nesi var yahu?” diyorlar adeta.

Aslında doğru bir noktaya işaret ediyorlar. Direnecek en az kaynağa sahip insanların üzerinde (ekonomiyi de rayına oturtmaksızın) bu kadar baskı oluşturan bir hükümetin artık siyasi intiharın eşiğine gelmiş olduğunun düşünülmesi gerekir, normalde. Ama öyle olmadı, dümdüz bir kemer sıkma mantığı neredeyse herkes tarafından kabullenildi. Peki neden? İşçi sınıfı nasıl oluyor da aynı eziyetin devam edeceğini vaat eden politikacılara göz yumabiliyor, hatta onları destekleyebiliyor?

Başta değindiğim inanmazlığın bu durumu ancak kısmen açıklayabildiğini düşünüyorum. Emekçiler kanuna ve adabımuaşerete riayet konularında “üstlerindeki” sınıflardan daha özensiz olabilirler (bu sık sık başımıza kakılıyor zaten) ama bencillikleri çok daha azdır. Emekçiler arkadaşlarına, ailelerine ve çevrelerine daha düşkündür. Hiç değilse, bir bütün olarak ele alındıklarında, başkalarına karşı tavırları temelde daha iyidir.

Bu durum bir ölçüye kadar evrensel bir sosyolojik kanunun bir yansıması gibi görünüyor. Feministler uzun zamandır her türlü eşitsiz toplumsal zeminde, alttakilerin üsttekilere yönelik düşünce ve alakasının üsttekilerin alttakiler hakkındaki düşünce ve alakasına kıyasla daha fazla olduğunu ifade ediyor. Dünyanın her yerinde kadınların erkeklerin hayatına dair bildikleri, erkeklerin onlar hakkında bildiklerine kıyasla daha fazladır. Aynı şekilde siyahlar beyazlar hakkında, çalışanlar patronlar hakkında, yoksullar zenginler hakkında daha fazla bilgiye sahiptir.

İnsanlar epeyce empatik yaratıklar olduklarından, başkaları hakkındaki bilgileri beraberinde merhameti de getirir. Zenginler ve güçlüler ise başka insanlar konusunda ilgisiz ve kayıtsız kalabilirler çünkü bundan doğan maliyeti karşılama imkânları vardır. Psikoloji alanında yakın dönemde yapılan pek çok çalışma bu durumu doğrulayan sonuçlara ulaştı. Diğer insanların duygularını kestirmek konusunda yapılan deneylerde işçi sınıfından gelen çocuklar zengin veya meslek sahibi ailelerden gelen beyzadelere göre her zaman daha başarılı oldu. Bir bakıma bu pek de şaşırtıcı bir sonuç değil. Nihayetinde “güçlü” olmak aşağı yukarı böyle bir şeydir, etraftaki diğer kişilerin ne düşündüğüne, ne hissettiğine kafa yormak zorunda olmazsınız. Zira gücü elinde tutanlar bu işi yapsın diye başkalarını çalıştırır.

Peki, bu işlerde kimler çalışır? Çoğunlukla işçi sınıfının çocukları. Bu noktada şunu söylemek isterim: “Gerçek iş” paradigması olarak fabrika işçiliğine (ve bunun romantikleştirilmesine, diye eklesem mi?) o kadar saplantılı bir şekilde sarıldık ki insan emeğinin çoğunlukla nelerden oluştuğunu unuttuk. Karl Marx ve Charles Dickens’ın yaşadığı dönemde bile işçi mahallelerinde kömür madenlerinde, tekstil ve demir döküm atölyelerinde çalışanlardan çok daha fazla sayıda hizmetçi, ayakkabı boyacısı, çöpçü, aşçı, bakıcı, şoför, öğretmen, fahişe ve işportacı yaşıyordu. Bugün bu makas daha da açıldı. Başkalarına bakmak, onların arzu ve ihtiyaçlarını karşılamak, patronun ne istediğini veya ne planladığını izah etmek, desteklemek, öngörmek ve tabii ki bitkilerin, hayvanların, makinelerin ve diğer nesnelerin bakımını yapmak, bunları gözlemlemek ve bunlarla ilgilenmek gibi – arketipik olarak kadın işi diye gördüğümüz– işler işçi sınıfının çekiç, testere, vinç veya orak kullanarak yaptığı işlerden çok daha fazlasına tekabül ediyor.

Bunun böyle olmasının tek sebebi işçi sınıfının (veya genel olarak insanların) çoğunluğunu kadınların oluşturması değil, erkeklerin ne yaptığına dair bilgimizin epey yanlı ve yanıltıcı olması da bunda önemli bir etken. Geçenlerde metrodaki bilet kontrolcülerinin öfkeli yolculara açıklamaya çalıştıkları gibi, “biletçiler” esasında zamanlarının çoğunu bilet kontrolü yaparak değil, başkalarına bir şeyler açıklayarak, bir şeyler tamir ederek, kayıp çocukları arayarak, yaşlılarla, hastalarla ve nereye gideceğini bilemeyenlerle ilgilenerek geçiriyorlar.

Burada durup biraz düşünecek olursak, hayat da özünde bu değil mi zaten? İnsan dediğimiz şey karşılıklı bir yaratımın ürünüdür. Yaptığımız işin çoğu birbirimiz hakkındadır. Fakat işçi sınıfı bu konuda hissesine düşenden fazla yük omuzluyor. “İlgilenen”, “alakadar olan” sınıf onlar. Hep de öyle oldular. Bu gerçeğin –misal bunun gibi bir– kamusal alanda hakkının verilmesini zorlaştıran şey ise yoksulların bu “alakasından” faydalananların onları ardı arkası kesilmeyecek çabalarla şeytanlaştırması.

Bir işçi ailesinin çocuğu olarak diyebilirim ki, bizi asıl gururlandıran şey de tam olarak buydu. Sürekli olarak bize işin kendisinin bir erdem olduğu, insanın karakterini geliştirdiği falan söylenirdi ama kimsenin buna inandığı yoktu. Pek çoğumuza göre iş, başkasına faydası dokunmadığı takdirde kaytarılması gereken bir şeydi. Ama başkasına faydası dokunan bir iş yapınca da (bu iş köprü inşa etmek de olabilir, hasta lazımlığı temizlemek de) göğsünü gere gere gururlanabilirdin. Hiç şüphesiz gururlandığımız bir şey daha vardı: Bizler birbiriyle alakadar olan, birbirini kollayan insanlardık. Bizi zenginlerden ayıran da buydu, çünkü onlar –çözebildiğimiz kadarıyla– kendi çocuklarıyla ilgilenmekte bile zorlanıyorlardı.

En yüksek burjuva değerinin tutumluluk, en yüksek işçi sınıfı değerinin de dayanışma olması boşa değil. Ama şu anda işçi sınıfını kendi bacağından asan urgan da bu gerçekliğin kendisi. Bir zamanlar insanın çevresindekilerle alakadar olması işçi sınıfı için savaşmakla aynı anlama gelebiliyordu. O günlerde “toplumsal ilerleme”den bahsederdik. Bugün ise doğrudan işçi sınıfı siyaseti fikrine, işçi sınıfı birlikteliği fikrine karşı verilen durmak bilmez savaşın etkilerini görüyoruz. Bu sürecin sonunda pek çok emekçi için bu ilgiyi, alakayı ifade edecek pek bir mecra kalmadı. Bu duygu ya saldırgan bir milliyetçiliğe başvurularak ya da kolektif fedakârlık beklentisine yaslanarak “torunlarımız”, “ulusumuz” gibi üretilmiş soyutlamalara yöneltiliyor.

Bütün bunların sonucunda her şey tersine döndü. Kuşaklar boyunca sürdürülen siyasi manipülasyon, bu dayanışma duygusunu bir tür musibete dönüştürdü. Başkalarına karşı duyduğumuz ilgi, alaka bize yönelmiş bir silah oldu. Çalışanların sözcüsü olduğunu iddia eden sol, çoğu çalışma biçiminin nelerden oluştuğunu ve çalışanların en çok neleri erdemli gördüğünü ciddi ve stratejik bir şekilde ele alana kadar da bu durum pek değişecek gibi durmuyor.



