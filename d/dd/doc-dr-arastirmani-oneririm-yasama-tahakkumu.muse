#title Yaşama Tahakkümü
#author Doç. Dr. Araştırmanı Öneririm
#SORTtopics toplum, intihar, birey
#date 11.04.2022
#source 03.06.2022 tarihinde şuradan alındı: [[https://docdrarastirmanioneririm.substack.com/p/yasama-tahakkumu][docdrarastirmanioneririm.substack.com]]
#lang tr
#pubdate 2022-06-03T18:10:02


İntiharın kötü olduğu önkabulü, doğal seçilimden ötürü yaşama içgüdüsüne sahip insanın yaşam ve ölüm seçenekleri arasında her zaman, en azından teoride, yaşam seçeneğini daha kabul edilebilir bulmasıdır. İntihar doğal değildir; doğanın bozuluşudur ve sapkınlıktır.

İntiharın doğal olmadığını ve bundan ötürü intiharın kötü olduğunu önermek, doğal olanın her zaman iyi olduğu aksiyomuna dayanır. Peki gerçekten öyle midir? Marx’ın aktardığı kadarıyla Peuchet, intiharın sürekli görülen bir eylem olduğundan doğaya aykırı olmadığını öne sürer.

Bunu başka şekilde de tartışabiliriz. İnsan doğaya, doğasına hükmetme becerisine sahiptir ve doğadan kopmuştur. Doğal seçilimden öte seleksiyonlar yaratmıştır, yaşama içgüdüsü küçük gruplardan öte kolektif bir hale gelmiştir ve bu yüzden kimin nerede ne şekilde ve ne sebeple olursa olsun ölmesi her zaman bir derece üzücüdür; bu kolektifliği sürdürebilmek adına doğada olmayan ya da doğanın kurallarının algımızda işlenmesi sonucu ortaya çıktığını iddia ettiği normlar, kurallar ortaya atmıştır -tecavüz etmez, öldürmez, çalmaz. Halbuki bu örnekler, doğada, insan dışındaki her hayvanda sürekli rastlanan olaylardır. Doğal olan mutlak iyi ise bu eylemlerden neden kaçınır olduk?

Dediğim gibi, bunun sebebi, kolektifliğin sürdürülebilirliği amacı ve bunun tam aksi gerçekleşirse bu büyük kolektif hâlin içinde bulunanların sonunun geleceği inancıdır. Gerçekten de, birbirimizi öldürüp tecavüz ettiğimiz bir hâl, çoğunluk için sürdürülemezdir. Fark ettiyseniz, burada da bu sürdürülebilirliğin iyi olduğu kabulü vardır. Problem algısının subjektifliğinden defalarca bahsettim; kısaca, algı yoksa problem de yok. Sürdürülebilirliği terk etmek, son derece iyi bir çözümdür.


Yine de öyle değilmiş gibi devam edelim. Sorun şu; öznellik. Bağlı kalacağınız normlar ve gruplar büyük kolektif hâle zarar vermedikçe tamamen özeldir; ancak, intihar sözkonusu olduğunda, buraya tamamen keyfi bir sınır konulur. İntihara izin verilmesi sürdürülebilirliği gerçekten sekteye uğratır ancak bu geri kalan insanların üremeyi terk ettiği anlamına mı gelir? Nüfus artışı her zaman çeşitli programlar ile sağlanabilir.

Belki de buradaki problem bireyler arası ilişkidir. Sonuçta, sevdiğiniz birini kaybetmek istemezsiniz -intihar bunun tam tersidir. Bu istek aslında son derece bencildir ve yine keyfi bir sınır içermektedir; insanlar hayatlarını başkalarına göre şekillendirmek zorunda değildir. Bunun bir kere kabul edilmesi demek, istenilenden çok daha fazlasını serbest kılar. Kölelik bağlamında düşünürsek daha açık olacaktır. Kaldı ki bu sorumluluklar, Hume’un değindiği üzere, karşılıklılık esnasındadır. O halde toplumdan fayda görmediğini düşünen biri, toplumdan gayet vaz geçebilir durumdadır. Hume, bu karşılıklılığın faraziliği üzerine şöyle devam eder; “Hayır işleme zorunluluklarımız sürekli olsaydı, bunların kesinlikle bazı sınırları olurdu. Kendime büyük zararlar vermek pahasına topluma küçük bir hayır işlemek zorunda değilim. O halde sefil bir varoluşu, belki de toplumun benden edinebileceği bazı anlamsız yararlar yüzünden neden sürdürmeliyim? Yaşlılıktan ve hastalıklardan dolayı herhangi bir işten yasal olarak ayrılabiliyor ve zamanımı tamamen bu felaketlere karşı kendimi savunmakla ve gelecek yaşamımın ızdıraplarını mümkün olduğunca dindirmekle geçirebiliyorsam eğer; bu ızdırapları, toplum için sakıncasız bir eylemle, bir seferde, neden dindiremeyeyim? Ancak varsayalım, toplumun çıkarlarını gözetmek artık benim elimde değil: Varsayalım ki, ben toplum için bir yüküm: Varsayalım, yaşamım bazı kişilerin topluma çok daha fazla yararlı olmalarına engel oluyor. Böyle durumlarda yaşamdan vazgeçişim sadece masumane bir eylem olmakla kalmaz, aynı zamanda övgüye de değerdir. Ve varoluşu sonlandırmanın günah işlemek olduğu yalanını söyleyen çoğu insan, bu türden bir durumdadır. Sağlık ya da güç ya da yetke sahibi olan bu kişiler çoğunlukla, dünyanın keyfini çıkarmak için daha iyi bir nedene sahiptirler.”

Hume, bireyin topluma karşı olan görevleri üzerine tartışmaya devam eder; “Yaşamdan elini eteğini çekmiş biri, topluma zarar vermez. O sadece hayırlı şeyler yapmayı bırakır, eğer bir zararı dokunacaksa da, bu en küçük türden olacaktır.” Birini kaybetmek yas ve depresyon hislerini doğurur ancak bu, intiharı yanlışlamak adına kullanılabilir değildir; yukarıda değindiklerimizin yanında, bu argüman doğrudan ölümü hedef alır. Dikkat ederseniz “birini kaybetmek” kelime grubunu bilerek kullanıyorum zira intihar etmeniz veya başka bir sebeple ölmeniz durumunda ortaya çıkacak olan şey aynıdır; yas ve depresyon. O halde ne olursa olsun kimse ölmemelidir ancak henüz bu mümkün değildir.

Sonuç olarak, intihar hiç de olumsuzlanabilir değildir. Bu olumsuzlanma, toplumun sürdürülebilirliği amacı güden bir tahakkümdür. Tabi buradan kendinizi öldürün dediğim anlamı çıkmaz. Yaşayın yani. Bunu sadece yukarıdaki argümanlar ile demiyorum, onu bilmeniz yeter.




