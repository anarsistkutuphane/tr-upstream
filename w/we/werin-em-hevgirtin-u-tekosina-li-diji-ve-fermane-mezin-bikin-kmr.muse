#title Werin em hevgirtin û têkoşîna li dijî vê fermanê mezin bikin
#date 10.02.2023
#source 11.02.2023 tarihinde şuradan alındı: [[https://www.yeryuzupostasi.org/2023/02/10/werin-em-hevgirtin-u-tekosina-li-diji-ve-fermane-mezin-bikin/][yeryuzupostasi.org]]
#lang kmr
#pubdate 2023-02-11T09:04:30
#topics afet, bildiri




Di 6’ê Sibatê de anarşîstên ji bajarên cuda der barê erdhejê de daxuyaniyeke hevpar dan. Daxuyaniya Bêçareseriyê ya li ser hesabê xwe yê medyaya civakî wiha ye: Werin em li dijî vê fermanê hevgirtinê ava bikin û têbikoşin. Di 6ê Sibatê de li navçeya Pazarcika Mereşê di saet 04.17an de 2 erdhejên bi lerza 7.7 û li navçeya Elbîstan a Mereşê jî di saet 1.24an de bi lerza 7.6an erdhejek pêk hat. Li gorî daxuyaniyên fermî jî em bi karesateke ku tê de bi hezaran kesan jiyana xwe ji dest dane, bi hezaran kes birîndar bûne û bi sed hezaran kes jî rasterast mexdûr bûne re rû bi rû ne.

Her çend erdhejîn bûyerên xwezayî ne jî, ya ku pişt re qewimî ji karesatek “xwezayî” ya neçarî wêdetir e. Beriya her tiştî divê were destnîşankirin ku merkezîbûn, bajarîbûna qelş û metropolîtîzma ku ji ber kapîtalîzmê pêk tê, li ser bingeha erdhejên ku ewqas wêranker in, pêk tê. Lê belê encamên pêşketina kapîtalîst a li Tirkiyeyê ya li ser esasê kirêkirina xaniyan û talankirina van salên dawî, bi vê erdhejê careke din derket holê. Her çend sal berê zanyaran hişyarî dabûn, ku di demeke nêzîk de dê erdhejeke mezin li wê herêmê çêbibe û tevî ku tê zanîn çi pêwîste bê kirin, lê tiştek nayê kirin û avadankirin bê kontrol û bê plan berdewam dike. Ji bo vê felaketê qanûnên bi navê erdhejê hatin vexwendin.

Felaket bi “performansa” dewleta piştî erdhejê re li hev hat. Her çend roja ewil pir krîtîk bû jî, dewletê beşeke girîng ji tesîsên xwe bi kar neanîn, dema ku jiyana mirovan pirsgirek bû, amûra mîlîtarîst a mezin tevnegeriya, tîmên lêgerîn û rizgarkirinê û alîkarî piştî saetan jî nekarîn bigihêjin gelek cihan. Dema ku dewletê hêza xwe seferber nekir, hestên hevgirtinê yên xwezayî yên ku em baş dizanin di nav kedkar, bindest û gelan de hene derketin holê û bi hezaran kes bi derfetên xwe seferber bûn ku gel ji bin xirbeyan rizgar bikin û nîşan bidin. Hevgirtina mexdûrên erdhejê. Di demeke kurt de li seranserê welêt seferberiyeke mezin a hevpariya civakî dest pê kir.

Dema ku gel ji bo jiyana xwe şer dikir, hukûmetê dîsa serî li rêbazên xwe yên asayî da ku ji berpirsiyariyê dûr bixe. Li gel axaftina ku Tirkiye kembera erdhejê ye û ev felaket hema bêje çarenûs in, ji eywanên mizgeftan xwendin û performansa “şîna neteweyî” ya bi banga “şehîd” ên kesên jiyana xwe ji dest dane hatin xemilandin. . Berjewendiyên çînên serdest careke din li pişt netewperestiyê hatin veşartin. Li aliyê din kontrayên ku bi malzemeyên xerab li cihên ne guncaw avahiyan çêdikin, oportunîstên ku hewcedariyên bingehîn vedişêrin û bi buhayên giran difroşin û yên ku nikarin vê yekê bibînin. sîstem jiyana me dizîne, mexdûrên erdhejê ji ber ku erzaq û malzemeyên pêwîst kirîn bi dizî û talankirinê sûcdar dikin. Hin faşîstên ku hewl didin tiştên ku dewletê kirine û nekirine veşêrin, ji ber ku “leşkeran Divê ferman bê dayîn ku gule li talankeran bê vekirin.” Di nava vê şermê de, erka sereke ya dewletê ne parastina jiyana mirovan, parastina berjewendiyên sermaye û desthilatdariya xwe bi her awayî ye. rastî jî ev e.

Kapîtalîzmê careke din em bi awayekî hovane û bi girseyî kuştin. Barbariya kapîtalîst û yên ku ji vê fermanê sûd werdigirin, hemû sermayedarên ku rasterast an nerasterast ji avahiyên kirêkirî dixwin, hemû rêveberiyên navendî û herêmî, yanî hemû partî û siyasetmedarên welêt, ku heta niha polîtîkaya kirêkirina avahiyan dimeşînin. bajaran talan dikin. Berpirsiyarê vê komkujiyê ferman e. Em îro bi giranî birîndar in. Mîna bi mîlyonan kedkar û gelên bindest em ê bi avakirina hevgirtinê birînên xwe bipêçin. Lê em bang li aktorên vê polîtîkaya fermanê dikin; Çi kirin û çi nekirin em ê tu carî ji bîr nekin. Me heval, malbat, hezkiriyên me winda kir, lê em ne xemgîn in. Em hêrsê hîs dikin, em hêrsê dijîn.

Di nav hêrsa me de cîhanek azad şîn dibe.
