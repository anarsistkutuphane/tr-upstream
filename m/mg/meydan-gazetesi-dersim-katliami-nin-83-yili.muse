#title Dersim Katliamı’nın 83. Yılı
#author Meydan Gazetesi
#SORTtopics dersim katliamı, katliam, devlet
#date 04.05.2020
#source 04.05.2020 tarihinde şuradan alındı: [[https://meydan.org/2020/05/04/dersim-katliaminin-83-yili/][meydan.org]]
#lang tr
#pubdate 2020-05-04T18:20:27




[[m-g-meydan-gazetesi-dersim-katliami-nin-83-yili-1.jpg 50 l]]


Devletin, 4 Mayıs 1937’de Bakanlar Kurulu kararı ile uygulamaya koyduğu, on binlerce Dersimlinin katledildiği ve yaşam alanlarından sürüldüğü Dersim Katliamı, Dersimlilerin, “Tertelu Viren-İlk Tertele” denilen 1915 Ermeni Soykırımı’na atfen “Tertelu Peen-Son Tertele” ya da “Tertele Kırmancu, Tertele Zazawu” şeklinde de ifade ettiği “Tertele-Soykırım” 81 yıl önce bugün başladı.

25 Aralık 1935’te çıkarılan “Tunceli Kanunu” ile başlatılan katliam, sürgün ve soykırım sürecinde ilk olarak, 4 Ocak 1936’da Dersim’in adı “Tunceli” olarak değiştirildi. Tunceli,devletin yaklaşık bir yıl sonra başlatacağı katliam ve soykırım operasyonuna, “Dersim’e devletin tunç eli değecek” söylemine atfedilerek fiilen verdiği isimdi.


[[m-g-meydan-gazetesi-dersim-katliami-nin-83-yili-2.jpg 50 r]]

Dersim’de katliam,sürgün ve tertele, 1938 sonlarına dek yoğun bir şekilde sürdü.15-18 Kasım 1938 tarihleri arasında Seyyid Rıza ve arkadaşlarının idam edilmesiyle katliam fiilen sonlandırıldı ancak devletin saldırıları 1939 yılı boyunca aralıklarla sürdü. Resmi olmayan rakamlara göre katliamdan 72 bin kişi doğrudan ya da dolaylı olarak etkilendi.

1923’te Cumhuriyetin ilanı ile birlikte “ulus devlet” inşasına girişen TC’nin gerçekleştirdiği Dersim Katliamı, fiziksel soykırımın ötesinde, yer değiştirmeye zorlanan yetişkinler ve ebeveynleri katledilerek,evlatlık verilen kız çocukları gibi gerçekler düşünüldüğünde kültürel bir soykırım amacı da taşıyordu.

Aradan geçen 83 yılda devlet hiçbir geri dönüş yapmadı, sorumlular hesap vermedi. Katliamcı asimilasyon politikaları ise bugün dersim halkına karşı yürütülmeye devam ediyor.



