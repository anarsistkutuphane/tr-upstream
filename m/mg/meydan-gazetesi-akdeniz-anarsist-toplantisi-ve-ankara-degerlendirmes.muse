#title Akdeniz Anarşist Toplantısı ve Ankara Değerlendirmesi
#author Meydan Gazetesi
#SORTtopics yunanistan, akdeniz, anarşist hareket, anarşist dayanışma
#date 03.11.2015
#lang tr
#pubdate 2020-02-15T13:34:04


Şubat 2015’te başlayan (IFA) Uluslararası Anarşist Federasyonları tarafından da desteklenen uluslararası anarşist dayanışma kampanyası “3 Gefires” (3 Köprü) projesi, 9-18 Ekim arasında Yunanistan’ın farklı şehirlerinde düzenlenen bir dizi etkinlik ile sona erdi.

Yunanistan’ın çeşitli kentlerinde mücadele veren anarşist örgütlenmelerin birbirleriyle uyumlu; ortak ya da bağımsız olarak yürüttükleri eylemlikler şu üç başlık altında örgütleniyordu:

 - Güney Avrupa’da dayanışma köprüsü

 - Doğu Akdeniz yayında dayanışma köprüsü

 - Balkanlar’da dayanışma köprüsü

Bu köprüler aracılığıyla proje yürütücüsü örgütlenmeler proje başlangıcından itibaren dünyanın pek çok farklı bölgesindeki anarşist örgütlenme ve federasyonla iletişime geçmişlerdi.

Devrimci Anarşist Faaliyet’i temsilen katıldığımız CRIFA (Anarşist Federasyonlar Uluslararası İlişkiler Komisyonu) son toplantısına katılan “Liberter Komünistler Grubu” içinde bulundukları projeyi detaylarıyla açıklamış ve DAF’ın da dayanışma göstermesini istemişti.



*** Akdeniz Anarşist Toplantısı

Atina, Selanik, Patra, Larissa, Heraklio, Rethimno ve Hanya kentlerinde toplamda 10 gün süren eylemler, açık ve kapalı toplantılar, sunumlar, kitap fuarı, kolektif yemekler ve konserler gerçekleştirildi.

9 Ekim günü Atina Ekonomi Üniversitesi’nde gerçekleştirilen Balkan Yarımadası’nda Uluslararası Dayanışma başlıklı etkinlikte devletlerin milliyetçilik politikaları ve toplumsal yansımaları tartışıldı. Panel ve tartışmaların ardından 17 Kasım direnişinin mekanı olan Politeknik bahçesinde, anarşist tutsaklarla dayanışma konseri düzenlendi.

10 Ekim günü Atina Manastır Meydanı’nda savaşa, milliyetçiliğe ve faşizme karşı bildiri dağıtımı gerçekleşti. Bu esnada Ankara Katliamı’na ilişkin gelen haberler doğrultusunda Yunanistan Parlementosu önündeki Syntagma Meydanı’na eylem çağrısı yapıldı. Syntagma Meydanı’nda başlayan yürüyüş TC Büyükelçiliği önünde son buldu. Yürüyüşte Yunanca ve Türkçe “Unutulamaz Affedilemez” yazılı DAF imzalı pankart taşındı. Gerçekleştirilen protestoların ardından oldukça geç başlayan, “Günümüzde anarşist örgütlenmelerin yapıları ve faaliyet alanları” başlıklı panel gerçekleştirildi. Panelde DAF, CNT, Francophone Anarşist Federasyon, FAI İtalya ve 3 Gefires temsilcisi konuşmacılar kendi örgütlenmelerinin yapısını ve işleyişini anlatarak faaliyet alanlarına değindiler. Sunumların ardından karar alma süreçleri hakkında oldukça verimli tartışmalar gerçekleştirildi.

11 Ekim günü ise Atina’daki son etkinlik olan “Günümüz dünyasına anarşist bakış ve incelemeler” başlıklı panel ve tartışmalar gerçekleştirildi.

11-14 Ekim tarihleri arasında Selanik Aristoteles Üniversitesi fizik bölümünde ve anarşist sosyal merkez SABOT’ta “Balkan Anarşist Toplantısı” kapsamında çeşitli bölgelerden katılımcı grup, örgüt ve federasyonlar bir araya geldi. 12 Ekim günü, Ankara Katliamı’nı protesto etmek için Kamara Meydanından başlayan bir yürüyüş gerçekleştirildi. Yürüyüşte Türkçe ve Yunanca “Unutmak Yok Affetmek Yok” yazılı DAF imzalı pankart taşındı. Binlerce kişi Selanik’teki TC konsolosluğuna yöneldi. Burada polis otobüslerinin tampon tampona yapışarak büyükelçiliği koruma altına aldığı görüldü.

“Antimilitarizm, Antifaşizm, Antinasyonalizm” başlıklı panelde Ioannina’dan “Barefoot Battalion”, Selanik’ten “Ruthless Critique”, Bulgaristan’dan “Antifa Action”, Sırbistan’dan “Antifa Action Nis”, Makedonya’dan “Bitola” gruplarından katılımcılar sunumlar yaptı.

“Sermaye kendi çıkarları için göçmenleri yaratır ve onları kullanır. Balkanlarda dayanışmayı örgütleyelim!” başlıklı panelde ise “No Border Sırbistan”, “No Border Zagreb” ve “No Border Bulgaristan”, Kilkis’ten Otonom Mekan, Selanik’ten No Lager, Lesvos Adası’ndan ise Musaferat grupları konuştu.

“Sosyal direniş ve öz örgütlenme” başlıklı panelde de Romanya’daki “Barınma Hakkı Cephesi” tahliyelere karşı mücadele hakkında deneyim aktardı. Ljubljana “A-infoshop”tan konuşmacılar Slovenya’daki 2013 ayaklanmasını anlattı. Belgrad’dan “Infoshop Furija”, “Koko Lepo” otonom kreş deneyiminden bahsetti. Selanik’teki öz yönetimli VIOME fabrikasından işçiler bilgilendirme yaptılar. Ve son olarak “3 köprü” ittifakından yoldaşlar 2008-2015 sürecindeki sosyal mücadeleleri, seçim kandırmacasını ve öz örgütlülüğü tartıştılar.

14 Ekim günü Heraklio kentinde Ekoloji gündemli bir panel gerçekleştirildi. Evagelismos işgal evi gönüllülerinden bir yoldaşımız petrol boru hatları ve Kıbrıs çevresi kıta sahanlığındaki petrol yataklarının hangi şirketlerce paylaşıldığı, deniz alanlarının nasıl ticarileştirildiği hakkında bir sunum yaptı. Biz de Patika Ekoloji Kolektifi olarak mücadele alanlarımızdan ve deneyimlerimizden bahsettik. Bölgede kurulu olan ve kurulması planlanan rüzgar tribünleri nedeniyle yoğun bir RES karşıtı mücadelelerini aktaran yoldaşlarımız en çok yerellerle kurulan iletişimin öneminden bahsettiler.

15 Ekim günü Rethymno kentinde gündüz saatlerinde Ankara Katliamı’nı protesto yürüyüşü gerçekleştirildi. Yürüyüşte Türkçe ve Yunanca “Unutmak Yok Affetmek Yok” yazılı DAF imzalı pankartın yanı sıra “3 Gefires” den yoldaşlarımız da Türkçe “Bütün Devletler Katildir” yazılı pankart taşıdı. Gerçekleşen eylemin ardından Üniversite Kültür Merkezi’nde “Hareket, örgütlenme ve direniş” başlıklı bir panel gerçekleştirildi. Etkinlikte Rethymno ve Kıbrıs’tan yoldaşlarımız mücadele deneyimlerini aktardılar. Biz de DAF olarak mücadele alanlarımız ve perspektifimiz hakkında kısa bir tanıtım yaptıktan sonra Pirsus ve Kobanê’deki dayanışmanın örgütlenişini konuştuk ve Rojava Devrim sürecini tartıştık.

16-18 Ekim günlerinde Hanya’da Akdeniz Anarşist Toplantısı ve yeni süreçlerin örgütlenmesi hakkında toplantılar gerçekleştirildi. 16 Ekim günü “Savaşa, sınırlara ve Ankara Katliamına Karşı” bir protesto yürüyüşü gerçekleşti.

17 Ekim günü “Kürt Coğrafyasında Demokratik Özerklik” başlıklı bir panel gerçekleştirildi. Demokratik özerklik üzerine çalışmaları bulunan Ercan Ayboğa, demokratik özerklik fikrinin altyapısı ve uygulama alanları hakkında bir konuşma gerçekleştirdi. Ardından DAF adına gerçekleştirdiğim, “Rojava Devrimi ve Kobane Direnişi” başlıklı sunumda; Osmanlı döneminden bugüne Pirsus ve Kobanê’nin ortak geçmişinden başlayarak devletlerin yapay sınırlarıyla parçaladığı bölgenin yapısından bahsettim. 2012 yılından itibaren Rojava Devriminde çeşitli alanlarda ve ölçeklerde gerçekleşen toplumsal dönüşümü özetledim. Kobane Direnişindeki TC ve IŞİD arasındaki organik bağın kanıtlarını ortaya koyarak bu bağın gerekçelerini analiz ettim. Bölgedeki yoğun devlet terörüne karşı yerel halkın özsavunma güçleri ile direnişini ve sınırın her iki tarafındaki dayanışma ilişkisini anlattım. TC, Esad, ÖSO, ABD, Koalisyon Güçleri, Rusya arasındaki anlaşmaları ve pazarlığı, tampon bölge senaryolarını, güncel değişimlerle aktarıp bu eksende bir tartışma zemini oluşturmaya çalıştım. Son olarak da Avukat Deniz Gedik, Nusaybin Ablukasını, son günlerdeki sokağa çıkma yasaklarını anlatarak bölgedeki deneyimlerinden bahsetti. Etkinlik çok sayıda soru ve uzun süreli tartışmalarla son buldu.

18 Ekim günü “Kırım’daki dayanışma ve uluslararası dayanışmanın önemi” başlıklı son panel gerçekleştirildi. Kırım bölgesinde etkinliği giderek artan Neonazilerin ve faşist toplulukların saldırıları ve toplumda yarattığı etkiler konuşuldu.


*** Ankara Katliamı’nın Dünya Gündemine Etkisi

Atina’ya vardığım ilk gün olan cuma günü akşamında yediğimiz yemekte İtalya Anarşist Federasyonu’ndan Bolonya’lı bir yoldaşımın “Türkiye’deki durum şu anda nasıl?” sorusuna verdiğim, vermek zorunda kaldığım cevap ertesi günkü felaketin kehaneti gibiydi: “Seçim dönemi Türkiye’de sadece sandıktan ve oylardan ibaret değil, geçtiğimiz seçimde Amed’de bir katliam yaşandı, yine sonrasında Suruç katliamı gerçekleşti. Rusya’nın füzeleri Esad’ı güçlendiriyor. Şu an çok gergin bir ortam var her an bir yerlerde bir bomba patlayabilir…”

10 Ekim öğle saatlerinde bildiri dağıtırken Liberter Komünistler Grubu’ndan bir yoldaşın yüzünde endişeyle yaklaşıp “Ankara’da ne olmuş?” sorusuna “Ne zaman ne olmuş?” diye cevap verdim şaşkınlıkla. Yoldaşın yüz ifadesinin ürkütücülüğüyle bildiri dağıttığımız meydanın köşesindeki kahve dükkanına girdim internet şifresi alabilmek için. Anarşi Haber’de gördüğüm ilk haber “Bu meydan kanlı meydan videosu” idi.

İçinde bulunduğumuz koşulların aktif savaştan başka bir şey olmadığını bir kez daha hissettim. Ama belki de hissettiğim en çirkin duygu da bu durumun olağanlaşmasıydı. Üzüntüm, öfkem, çaresizliğim ile uzakta ve yapayalnızdım. Yalnızlığım uzun sürmedi. Sağolsun yoldaşlar hemen yanıma geldiler, biraz sonra eyleme son verdiler ve Excarhia mahallesine döndük. Hemen akşamına Syntagma’da bir protestoya hazırlandık.

Sonraki günlerde gittiğim Selanik’te, Rethymno’da, Hanya’da tıpkı Atina’daki gibi Yunanistan sokaklarında “Katil Devlet Hesap Verecek”, “Faşizme Karşı Omuz Omuza”, “Bütün Devletler Katildir” sloganları yankılandı. Sadece Türkler ve Kürtler değil Yunanlılar, İtalyanlar, Slovenyalılar, herkes çat pat söyleyebildiği kadar…

Gün geçtikçe daha fazla maruz kaldığımız devlet terörünün dünyanın başka bölgelerindeki yoldaşlarımızca nasıl tepki bulduğunu gördüm. Yoldaşlarımız sadece pankartlarında değil gözlerinde, yüreklerinde de gösteriyorlardı:


<strong>DAYANIŞMA SİLAHIMIZDIR!</strong>



