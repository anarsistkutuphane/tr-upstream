#title Anarşizmin Düşünce ve Eylem Dünyasından Simone Weil Geçti
#author Meydan Gazetesi
#SORTtopics kadın mücadelesi, kadın, simone weil, anarşist feminizm, anarşist
#date 01.03.2018
#source 25.08.2020 tarihinde şuradan alındı: [[https://meydan.org/2018/03/01/tarihteki-anarsist-kadinlar-4/][meydan.org]]
#lang tr
#pubdate 2020-08-24T21:16:42


Ünlü Fransız felsefecisi ve anarşist Simone Weil, 3 Şubat 1909 tarihinde Paris’te doğdu. Yahudi kökenli olmasına rağmen agnostik bir ailede dini dogmalara maruz kalmadan büyüdü. On iki yaşında Antik Yunanca öğrenerek ileri düzeyde kitapları okuyabiliyordu.

Gençliğinde işçi hareketlerine sempati duymaya başladı. Önceleri kendini sosyalist olarak nitelendiriyordu. 1931 yılında felsefe öğretmeni oldu, işsizler ve grevdeki işçiler arasında da örgütleniyor, yerel eylemlere katılıyordu.

Stalin’e tepki duymasıyla başlayan yolculuğu anarşizme ulaşmasıyla daha da radikalleşmesini sağladı. 1934 yılında kullandığı özgürleştirici yöntemler otoriteler tarafından uygun bulunmadı ve öğretmenlikten uzaklaştırıldı. Fransa banliyölerinde Renault fabrikasında çalıştı. Buradaki yıllarında ağır koşullar altında çalışan Simone, neredeyse kazandığı tüm parayı işçi mücadelesine vermişti.

1936 yılında kötüye giden sağlık durumu ve öğretmenliğe geri dönme çabaları arasındaki umutsuzluk döneminde ortaya çıkan İberya Devrimi, onun yaşam enerjisini yeniden yükseltti. Durruti Birliği’ne katıldı ve özellikle cephe gerisinde yaralıların tedavisinden sosyal-kültürel faaliyetlere, eğitim çalışmalarına, yaşlı ve çocukların bakımına kadar birçok alanda devrimin inşası için çalıştı. Franco’nun iktidarı ele geçirmesinin ardından Fransa’ya dönen Simone, anti-faşist direniş hareketine destek vermiştir.

Dünyada anarşist kimliğinden ziyade felsefeci ve yazar olarak ün salmıştır. Mistik bir felsefeci olan Simon’un felsefesinde Platon, Kant ve Descartes etkisi hissedilir. Spinoza’dan da etkilendiğini, onun bağımsızlığı ve cesaretine hayranlık duyduğunu söylemiştir.

1943’te tüberküloz teşhisi konmasına rağmen Simon, siyasi faaliyetlerine ara vermemiş, direniş hareketi için çalışmaya devam etmiştir. Doktorlar ona çok yemek yemesi gerektiğini söylediğinde “işçiler ne kadar yemek yiyorsa ben de o kadar yiyeceğim” demiştir. Özel bir tedaviyi reddetmiş, 24 Ağustos 1943’te ,henüz 34 yaşındayken kalp yetmezliğinden yaşama veda etmiştir.

Simon Weil’in, yaşamını yitirdikten sonra basılan dört kitabından biri olan “Yerçekimi ve İnayet” M.Mukadder Yakupoğlu çevirisiyle 2019’da Doğu-Batı Yayınları tarafından yayınlandı.



