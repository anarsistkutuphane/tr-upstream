#title Rus Devrimi’nde Anarşistler
#author Meydan Gazetesi
#SORTtopics kitap, rus devrimi, mahnovşçina, devrim, anarşist devrim
#date 10.09.2012
#source 04.05.2020 tarihinde şuradan alındı: [[https://meydan.org/2012/09/10/kitap-tanitimi-rus-devriminde-anarsistler/][meydan.org]]
#lang tr
#pubdate 2020-04-30T22:00:00



<quote>

Anarşistlerin, merkezileştirilmiş iktidarın tehlikelerine karşı 95 yıl öncesine dayanan uyarılarının öngörülü ve düşüncelerinin bugün bile geçerli oluşu, günümüzdeki devrim tartışmalarında toplumsallık vurgusunu savunanların tarihsel arka planını oluşturuyor.
</quote>



1917 önce ve hemen sonrası Rusya’da olup biteni anarşist bir bakış açısıyla yorumlayan; deneyimlenen süreçlerde anarşistlerin neler yaşadığını, neler yapıp neler tartıştığını anlatan belgelerle,“Rus Devriminde Anarşistler“ kitabı, 1995’te Metis Yayınları tarafından basılmıştır. Bülent Somay’ın sunuşunda verdiği özeleştirisiyle şu andan çok farklı bir siyasal gündemde basıldığı çok açık. Bu kitap Sovyetler Birliği’nin çöküşüyle nerede hata yapıldığına bakmak adına 1917’ye dönen sosyalistlerin o dönemi iyi anlayıp hatalardan ders çıkarma veya (1987’de “Mağlup” durumuna düşen sosyalistlerin) “Mağlupların” da tarihte bir yeri olmalı kaygısı içerisinde bir geriye dönüş çabası olarak algılanabilir. Tarihsel süreçleri çok yönlü anlama çabasıyla Türkçe‘ye çevrilen kitap, Paul Avrich tarafından, ilk olarak 1973 yılında İngilizce olarak basıldığında, herhangi bir iktidarın tarihsel anlatımı kaygısı güdülmeden, devrim sürecindeki hareketin ruhunu kavramayı amaç edinmişti. Anarşistlerin, çeşitli makale ve manifestolarla, söylev ve önergelerle, mektup ve günlüklerle, şiir ve marşlarla devrim ve iç savaş tarihleri süresince anlatıldığı kitap, ikincil anlatılardan kaçınmaktadır. Kitap, dönemin çeşitli anarşist birey ve gruplarının (komünistler, sendikalistler, pasifistler, bireyciler gibi) kendi belgeleriyle hazırlanmıştır.

Paul Avrich’in anarşistler açısından süreci kısaca özetleyen bir giriş metniyle başlayan kitapta, Volin, Maksimov, Mahno, Kropotkin gibi dönemin önemli anarşistleri ile çok fazla bilinmeyen Borovoy, Graçev, Sokolov gibi anarşistlerin makaleleri ile Şubat Devrimi’nden Ekim sürecine, İç Savaş’tan Ukrayna’ya ve Rusya’daki anarşistlerin tutsaklık durumuna kadar birçok süreç hakkında değerlendirme ve yorum yazıları yer alıyor. Ayrıca Paul Avrich kitabın bir bölümünü eğitim, gelecek toplum, antientellektüelizm gibi konular hakkında Golos Truda (Emeğin Sesi), Burevestnik (Fırtına Kuşu) gibi dönemin önemli dergilerinde yayımlanan ve Nabat Federasyonu gibi dönemin önemli örgütlerinin yayımladığı tartışma, bildiri ve makalelere ayırmış durumda.

Anarşistlerin, merkezileştirilmiş iktidarın tehlikelerine karşı 95 yıl öncesine dayanan uyarıların öngörülü oluşu ve düşüncelerinin bugün bile geçerli oluşu, günümüzdeki devrim tartışmalarında toplumsallık vurgusunu savunanların tarihsel arka planını oluşturuyor. 95 yıl öncesinde anarşistlerin o günün toplumsal koşullarını yorumlayıp deneyimlenmesi gereken güncel pratikleri ve yazdıkları belgeler, bu coğrafyada bu geleneği devam ettirmeye çalışanların uzun süredir unuttuğu bir şeyi hatırlatıyor o da güncele ilişkin söz üretmek.


Kitabı okumak için: [[https://anarcho-copy.org/copy/rus-devriminde-anarsistler/][https://anarcho-copy.org/copy/rus-devriminde-anarsistler/]]



