#title Barzani'nin KDP'sinden Halkın PYD'sine: Rojava Halkındır
#author Meydan Gazetesi
#SORTtopics kürt özgürlük hareketi, rojava
#date 26.08.2013
#source https://meydan.org/2013/08/26/barzaninin-kdpsinden-halkin-pydsine-rojava-halkindir/
#lang tr
#pubdate 2020-04-28T16:37:02



Yaklaşık bir yıldır gündemde geniş yer tutan, TC’nin “Kürtlerle BarışMa” ve Suriye siyaseti, esas olarak bölgenin durumu; çatışmaların sadece Baas Rejimi’yle “sözde” bu rejime direnen Özgür Suriye Ordusu arasında sürmekte olduğu aldatmacasının, iktidar ve ana akım medya tarafından bile sürdürülemediği günlerde, daha kritik bir hale geldi. Baas Rejimi ve Özgür Suriye Ordusu dışında, üçüncü bir yolun daha varlığının idrakıyla; bu yolun gösterdiği Rojava’nın, iki tarafın da saldırılarının hedefi haline geldiği gözler önüne serildi.

Rojava halkı; Kuzey’de TC’ye, Güney ve Batı’da El Kaide’ye bağlı El Nusra çetelerinin yanı sıra, Irak ve Suriye’de kurulacak bir İslam Devleti için cihat ilan eden El Kaide bağlantılı diğer çetelere direniyor. Zaman zaman bu cihatçı çetelerle ya da TC’yle işbirliği içinde saldıran Azadi Partisi’yle ve KDP çizgisindeki El Parti ile mücadele ediyor. Doğuda ise, Irak Bölgesel Kürt Yönetimi’nin tutarsız siyasetiyle köşeye kıstırılmaya çalışılıyor. Üç tarafı düşmanla çevriliyken, nefes alacağı tek yön olan doğusundaki Federal Kürdistan Bölgesi’nden KDP’nin siyaseti, kafalarda soru işaretleri yaratıyor.

KDP Lideri Mesut Barzani, Rojava’da Temmuz ayından beri süren çatışmaların ve Kürtlere yönelik katliamların yoğunlaşmasının ardından, Ağustos ayı ortasında, bu katliamlara dair yaptığı geç kalmış açıklamada;<em>“Bir süreden beridir, bazı siyasiler ve haber merkezlerinde, teröristler tarafından, Batı Kürdistan’da Kürtler’e karşı cihat ilan edildiği ve katliamlar yapıldığı, El Kaide’ye bağlı teröristlerin sivil ve masum halka saldırılar yaptığı, Kürt kadın ve çocukların başlarını kestiği yönünde haberler yayınlanıyor. Gerçeklerin açığa çıkması ve bu haberlerin doğruluğunu araştırmak için Batı Kürdistan’a gidecek bir özel komite oluşturulmasını istiyorum.”</em> diyerek, belki de, bölgedeki statüsünü kaybetmemek için, Rojava’daki durumu görmezden geliyor.

Barzani; TC yetkililerine <em>“Yeter ki Türkiye’de barış olsun. Ben PKK’lıların silahlı olarak bölgemize gelmesini sorun etmem. Bunları sizin belirlediğiniz koşullarla nereye gitmeleri, nasıl muameleye tabi tutulmaları gerekiyorsa o şekilde karşılarız.”</em> diyerek, TC ile yakınlık siyasetini, yine aynı kaygıyla güdüyor.

Bu beyanı dışında, bayram süresince ve sonrasında, birkaç gün önceye kadar Semalka sınır kapısını kapalı tutarak, yaşanan savaş koşulları sebebiyle Federal Kürdistan Bölgesi’ne göç etmeye çalışan Rojavalılara engel oldu. Güney Kürdistan haber merkezlerinden duyurulan, kapının açıldığına dair yalan haberler, göç hazırlığında olan Kürtlerin sınır kapısına yığılmasına neden oldu ve peşmergeler tarafından geri çevrildiler. Sadece 30 ağır hastanın geçişine izin verildi.

Aylardır süren çatışmaların yanı sıra, sivillere yönelik katliamların da rutinleştiği Rojava’da yaşananlardan, yaşamın yeniden inşasını baltalama girişimlerinden haberdar değilmişçesine söz ve eylem üreten KDP, belki de bölgesel statüsünü koruma stratejisi güderek hareket ediyor. Bölgesel statüsünü, küresel şirketler ve devletlerle geliştirmiş olduğu işbirliği siyasetine dayandıran KDP’nin, gözden kaçırdığı nokta; bu statüsünü, Kürt halkının mücadelesine de borçlu olduğu gerçeğidir.

“Karanlıklar dolu 20. yüzyılın tersine 21. yüzyıl, Kürtlerin umutlarının gerçekleşeceği bir yüzyıl olacak. Bu kritik aşamada halkımızın haklı mücadelesini zafere ulaştırmak için birlik ve ortak çalışmaya ihtiyacımız var.” diyen Barzani’nin çağrıcılığında, 15-16-17 Eylül tarihlerinde, Kürdistan’ın dört parçasından gelecek tüm siyasi bileşenlerin katılımıyla Hewler’de Kürdistan Ulusal Kongresi gerçekleşecek. Bu kongrenin örgütlenmesinde önemli rol oynayan KDP ile PYD ve PKK arasındaki ilişkilerin hangi yönde ilerleyeceği, İran, TC ve diğer küresel faktörlerin yanı sıra, sadece Batı ve Kuzey Kürdistan için değil; Doğu ve Güney için de kritik önem taşıyor.



