#title Otorite
#author Meydan Gazetesi
#SORTtopics otorite
#date 13.11.2019
#source 08.02.2021 tarihinde şuradan alındı: [[https://meydan1.org/2019/11/13/kilavuz-kavramlar-1-otorite/][meydan1.org]]
#lang tr
#pubdate 2021-02-08T08:46:11


<quote>
 <em>
İçerisinde yaşadığımız devletli ve kapitalist sistemin analizini yapmak, düşlediğimiz adil ve özgür dünyayı ve bu özgür dünyanın değerlerini sade bir dille anlatabilmek için gazetemizin bu sayısından itibaren anarşist düşüncenin dikkat çektiği belirli kavramları sizlerle paylaşacağız. Meydan Gazetesi olarak devrimci anarşist perspektif ve eylemlerimiz sonucunda biriktirdiğimiz yazınsal ve eylemsel deneyimlerimizden hareketle yorumladığımız kılavuz kavramlardan ilkini, otorite kavramını sizlerle paylaşıyoruz.</em>
</quote>

*** "Otoritenin olduğu yerde özgürlük yoktur."


<quote>
Pyotr Kropotkin
</quote>


Özgürlüğün olduğu yerde ise bütün otoriteler yıkılacaktır. Otorite, bir bireyin ya da topluluğun başka bireyler ya da topluluklar üzerinde onların iradesini şekillendirmek ve ortadan kaldırmak için bir yetkiye sahip olması olarak tanımlanabilir. Temelde otorite zora dayalı, emir alma-emir verme ve yönetme-yönetilme ilişkisinin belirleyiciliğinde şekillenir. Bu ilişki biçimi, yaşamlarımızı çalan bütün kurumlar ve kişilerde somutlanmıştır. Otorite olgusu, anarşist literatürde, özellikle Bakunin ve Kropotkin’in çalışmalarında incelenmiştir. Otoritenin toplumsal örgütlenmedeki gereksizlikleri bir yana, toplumun yapısını bozmasıyla da tarih boyunca anarşistler tarafından eleştirilmiştir.

*** Yasalar, Ezenlerin Sömürü Aracıdır


<quote>
“Yasa, aylak zenginlerin emekçi kalabalıklar üzerindeki sömürülerinin ve egemenliklerinin amacından başka bir şey değildir.”

Pyotr Kropotkin
</quote>


İnsanlığın, otoriter mekanizmaların baskısı altına alınmadan önceki yaşantısında yüzyıllar boyunca özgürce yaşadığını bilmekteyiz. Kropotkin’in de 1886 yılında kaleme aldığı “Yasa ve Otorite”de başarılı bir şekilde altını çizdiği gibi bugün dahi “insanlığın büyük bir bölümünün yazılı yasası yok”. “Balta girmemiş ormanlarda”, modern dünyanın vahşetiyle karşılaşmamış kabileler, gelenekleri ve ihtiyaçları ölçeğinde kararlaştığı ilkeleriyle, otoriter olmayan bir yaşam biçimini sürdürmeye devam etmektedir.

Diğer yandan iktidarlı ilişkiler herhangi bir kurumsallaşmış ve tanımlanmış mekanizma olmadan da ortaya çıkabilir. Tam olarak tarihlendiremesek de bitkilerin ve hayvanlarn evcilleştirildiği, yerleşik yaşamın ortaya çıkmaya başladığı çağlarda insanlar arasındaki iktidarlı ilişkiler kurumsallaşmaya başlamıştı. İktidarlı ilişkilerin kurumsallaşmasını ve iktidar olabilme yetkisini tanımlayan otorite olgusunun izleri, o yıllara dek sürülebilir.

*** Yaşamlarımızı Çalan Otorite, Özgürlüğün Yadsınmasıdır

<quote>

“Otorite, özgürlüğün yadsınmasıdır.”

Mikhail Bakunin
</quote>


Otorite olma her zaman meşrulaştırılmaya çalışılır. Bu, bazen içinde bulunulan topluluğun inancına bazen otoriteyi elinde bulunduranın kişiliğine bazen de hukuka dayandırılarak yapılmaya çalışılır. Otoritenin en önemli özelliklerinden birisi mutlak olmasıdır. Otoriteler tüm uygulamalarını bireylere bilgi, inanç ya da yasalar aracılığıyla değiştirilemez ve sorgulanamaz olarak dayatır.

Otorite, zaman içerisinde itaat kültürünün içselleştirilmesiyle değiştirilemez, yokluğu tahayyül edilemez bir gerçeklik olarak dayatılmıştır. Otorite, onu tanıyıp kabul edecek bireyler ya da topluluklar var olduğu sürece vardır. Yani otorite sadece zor uygulamakla değil aynı zamanda itaatin kabulüyle de ilişkilidir. Otoritelerin varoluşunu garanti altına alan, itaatin sürekliliğidir. Çünkü iktidarlı ilişkilerin birer iktidar mekanizmasına dönüşümü otorite ile gerçekleşir ve kalıcı hale gelir.

Aileden okula, işyerinden kışlaya, ibadethanelerden sokağa kadar otorite, nerede ve nasıl yaşayacağımızdan nasıl düşüneceğimize kadar tüm irademizi yok sayar. Otoriteler, istek ve çıkarları doğrultusunda bizim adımıza kararlar almayı amaçlar. Yaşamlarımızı belirleyen bu kararlar; ebeveynler, öğretmenler, patronlar, komutanlar, din adamları yani otoriterler tarafından toplumun her alanında kendini gösterir.

*** Otorite Devletin Ahlakıdır, Yani Ahlaksızlıktır


<quote>
“Otoriterler hepimizin temelde kötü olduğumuzu ve eğitimin, gözetimin zorunlu olduğunu düşünür ve de en çok ele geçirmek istedikleri şey olan ‘kontrol’ün kaçınılmazlığını savunurlar.”

Dave Neal
</quote>


Otorite olgusu insanın özü itibariyle kötü olduğu inancına dayalıdır. Varoluşunu gerçekleştirirken şiddet, baskı, işkence, katliam gibi yolları kullanan otoriteler meşru olmadıklarından soyut varsayımlara, yalanlara başvurmaktadır.

İnsanların otorite olmadan birbirlerine saldıracakları, şiddetin artacağı ancak bir yalandan ibarettir. Zira insanlık tarihi bize her zaman tiranların, zorbaların ortadan kalktığı zamanlarda paylaşma ve dayanışmanın yükseldiğini göstermiştir. Otoritenin savunucuları, her zaman otoriteyi insanlığın ahlakı, etiği olarak kavratmaya çalışmıştır. Ancak otorite; sosyalistinden liberaline, demokratından faşistine bütün iktidarların mutlaklaşmasından başka bir işe yaramamıştır.

*** Bütün Otoritelere Karşı Özgürlük için Anarşizm


<quote>
“Giyotin sehpalarını yakalım, hapishaneleri yıkalım, yargıçları, polisleri ve muhbirleri kovalım, öfkeyle hemcinsine kötülük yapmaya itilmiş olanlara kardeşçe davranalım… Toplumumuzda bundan böyle lafı edilebilecek çok az suç söreceğimizden emin olabiliriz. Suçu ayakta tutan yasa ve otoritedir.”

Pyotr Kropotkin

</quote>

Tarih boyunca bütün otoriteler, bireylerin kendini gerçekleştirmesinin, özgürlüğünün tam karşısında olmuştur. Bizler biliyoruz ki özgürlük ancak otoritenin yıkılmasıyla gerçekleşebilir. Biz anarşistler, devrimi ertelemeden, şimdi şu anda otoriter tüm mekanizmalara karşı anti-otoriter örgütlenmeler yaratarak özgürleşiyoruz.

Yaşadığımız topraklarda otorite, bir konu üzerinde tartışarak ortak bir karara varamayacağımızı bize dikte etmeye çalışmaktadır. Biz, ancak kararlaşma süreçlerini işlettikçe kendimizi gerçekleştirebiliriz. Anarşist ilişki biçimleriyle örgütlendiği sürece toplumda adaletsizliği ve itaati kurumsallaştırabilecek herhangi bir zemin oluşamaz. Çünkü, otorite ve otoriter mekanizmaların kendini meşrulaştırabileceği araçlar yok edilmiştir.

*** Özgürlük İçin Anarşizmde Örgütlenmeye!

Anarşizm düşüncesi, toplum baskısıyla bireyin iradesini ezmez aksine özgürlük anlayışın birey ve toplum olarak birlikte ele alır. Peki böylesi bir toplumda hangi ilkeler toplumsal yaşama sirayet edecektir? Toplum İtaat yerine, karşılıklı uyum ve kararlaşmayla örgütlenir. Bu toplumda kimsenin kimseyi baskı altında tutmadığı, otoritenin olmadığı özgür bir ilişki biçimi oluşmaktadır. Sonuç olarak bu toplumda bireyin iradesinin iktidarlar tarafından şekillendirdiği bir ilişki biçimi olmayacaktır. Anarşist bir toplumda iradelerini özgürce gerçekleştirebilecek bireyler vardır.

Başkaları üzerinde hak iddia edenler, kazançlarını her zaman onların itaati üzerinden şekillendirdiler ve şekillendirmeye devam etmek istiyorlar. Özgürlük için bütün otoriteleri yıkmaya, anarşizmde örgütlenmeye!
