#title Erkeklik Öldürür Kadın Yaşatır!
#author Meydan Gazetesi
#SORTtopics kadın
#date 01.03.2018
#lang tr
#pubdate 2019-12-13T21:23:00


Kadın olmak kolay olmadı hiçbir zaman. Nerede olursak olalım… Köylü, şehirli, doğulu, batılı, hayatlarımız hep zordu… Genç olalım, yaşlı olalım; sorumluluklar çoğu kez zorunluluk oldu bize… Evli olalım bekar olalım, dul olalım; yaşadıklarımız hep benzedi birbirine… Biz, ne kadar farklı olsak da, farklı dilleri konuşsak da, birbirimize hiç benzemesek bile, hiç birimizde farklılaşmayan bir parçamız vardı.

Hep aynı şeylerdi bize öğretilen. Narin, kırılgan olduğumuz söylendi bize. Ağaca çıkmak öğretilmedi örneğin, düşeriz diye. Yüzmek öğretilmedi, ayıp diye… Beceriksiz olduğumuz, akılsız olduğumuz öğretildi… Dizimizi kırıp evimizde oturmamız, çocuklarımıza annelik, kocalarımıza karılık yapmamız, elimizin hamuruyla başka işlere karışmamamız…

“Hayır” demek öğretilmedi örneğin. Söz dinlemek öğretildi; uslu kız olmamız, babamız, kocamız, abilerimiz ne derse onu yapmamız… Kavga etmek öğretilmedi; kafa tutmak, diretmek öğretilmedi. Töre ne derse boyun eğmemiz, birilerinin namusu olmamız, kaderimiz neyse razı gelmemiz, başa geleni çekmemiz…

Onların öğrettiği gibi davranmadığımızda şeytan olduğumuzu, fettan olduğumuzu, cadı olduğumuzu söylediler. Kafamıza kaktıkları ahlak öğretilerine uymadığımızda oynak, kaltak, sürtük olduğumuzu söylediler.

“Kadın”lığımızın ayıp, utanılacak bir şey olduğunu öğrettiler, bayan dediler bize. “Karı gibi” iş yaptığımızda aşağıladılar, “erkek gibi kadın” olduğumuzda takdir ettiler. Bizi erkekleştirmeye çalıştılar…

“Dişileştirmeye” çalıştıklarında ise bize bedenlerimizle ne yapacağımızı, nasıl en güzel görüneceğimizi, kaç beden olacağımızı, saçımızı yüzümüzü ne şekle sokacağımızı, kendimizi nasıl pazarlayacağımızı söylediler.

Erkekliğin öğretileri birbiri ardına sıralanırken hayatlarımız gittikçe zorlaşıyordu.

Annelerimiz, kardeşlerimiz, komşularımız, arkadaşlarımız, çocuklarımız vardı. Hepsi bize, hepsi birbirine benzeyen.

Her biri her gün aşağılanan, sıkıştırılan, kapatılan, korkutulan, sindirilen, itilip kakılan, pazarlanan, satılan, ezilen, tartaklanan, dayak yiyen, taciz edilen, tecavüze uğrayan, katledilen, bedeni parça parça edilen… Sonra yine aşağılanan, “hak etti” denilen, neden kısa etek giydiği, neden o saatte sokakta olduğu sorgulanan… Yaşadıklarında “rızası”nın olmadığını ispatlamak zorunda bırakılan… Niye o yemeği tuzsuz yaptığının, niye o adama “öyle” baktığının hesabı sorulan…

Biz onları çok iyi tanıyorduk, onlar bizdik. Her gün aynaya baktığımızda gördüğümüz bakıştan tanıyorduk onları, kafamızın içinde duyduğumuz seslerden tanıyorduk, boğazımızda düğümlenen yumrudan tanıyorduk.

Ve her geçen gün kendimize benzeyen başka kadınlar tanıdık. Tanıdıkça birbirimizi, birbirimizden öğrendik vazgeçmemeyi, yılmadan yeniden yeniden denemeyi.

Her attığımız adımda daha da dik durarak, başımızı öne eğmemeyi öğrendik. “Kirpiğiniz yere düşmesin” diyen kadınlar tanıdık. Kaderimize razı gelmemeyi, kafa tutmayı öğrendik.

“Hayır” demeyi öğrendik birbirimizden, “yetti be” deyip elimizi belimize koymayı öğrendik.

Kavga etmeyi öğrendik, sineye çekmemeyi… Bize öğretilenlerle kavgaya tutuştuk önce; bize yakıştırılan, üstümüze yapıştırılan ne varsa. Ve bize bunu dayatan erkeklikle, bize bunun başka türlü olamayacağına inandırmaya çalışanlarla; kendimiz için, kardeşlerimiz için tutuştuk kavgaya.

Birbirimizi tanıyorduk biz. Her birimizin yaşadıkları, herhangi birimizin yaşadıklarıydı aslında. Bu yüzden aynı dili konuşmasak da dinledik birbirimizi ve anladık. “Erkeklikten” kaçmak zorunda kaldığımızda, birimiz bir diğerine “sığınak” olduk.

Hikayelerimiz unutulsun istediler, ama biz unutmadık, dilden dile çoğalttık, hiç birimizin hikayesi yarım kalmasın diye tamamladık birbirimizle…

Birbirimize göz, kulak olmayı, birbirimizin sessizliğine ses olmayı öğrendik.

Birimiz diğerinin gözündeki bakışı gördü, birimiz diğerimizin bağırışını duydu, birimiz diğerimizin atamadığı çığlığı oldu.

Empatiyi öğrendik. Karşılık beklemeksizin birbirimizin sıkıntısına çözüm bulmak refleksimiz oldu. Kendini kurtarmanın ancak yanımızdakilerin elini tutarak mümkün olduğunu öğrendik. Birimizin hepimiz, hepimizin birimiz için olduğunu. Benlerden biz olmayı, bizi özgürleştirenin “biz olmak” olduğunu…

Biz birbirimizden, kadın olmanın ne demek olduğunu -ne daha eksik ne de daha fazla- sadece “kadın” olduğumuzu öğrendik. Kadın olmanın bizi biz yaptığını öğrendik. Biz kadındık, biz birbirimize yaşamdık.

Her gün yeniden yeniden üretilen “erkeklik”, bir başka kardeşimizi katlederken kadın anlar kadını, kadın bulur kadının derdinin çaresini, kadın kadına empati yaşatır kadını!

Erkeklik Öldürür Kadın Yaşatır!



