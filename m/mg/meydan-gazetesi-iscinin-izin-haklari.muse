#title İşçinin İzin Hakları
#author Meydan Gazetesi
#SORTtopics işçi, iş kanunu
#date 20.11.2012
#source 03.05.2020 tarihinde şuradan alındı: [[https://meydan.org/2012/11/20/kullan-at-kilavuz-iscinin-izin-haklari/][meydan.org]]
#lang tr
#pubdate 2022-04-16T13:25:25



<em>Kapitalist işleyiş içerisinde zaman zaman kullanılabilecek ama paylaşma ve dayanışmayla örülü özgür dünyada hiçbir şeye yaramayacak bilgiler…</em>

*** Yıllık İzin:

İşçi-işveren ilişkisinde en sık yaşanan sorunlardan birisi de yıllık izin kullanımında yapılan haksızlıklardır. Çoğunlukla güvencesiz çalışma koşulları ve taşeronlaşmanın yoğun yaşandığı sektörlerde görülen bu ihlal, şikayet edilmesi halinde ceza yaptırımıyla karşı karşıya kalır.

**** Yıllık izni kimler kullanabilir?

İşyerinde işe başladığı günden itibaren, deneme süresi de içinde olmak üzere, en az bir yıl çalışmış olan işçilere yıllık ücretli izin verilir. Bir yıldan az süren kampyanya işlerinde çalışan ya da mevsimlik işçilerde ücretli izinlere ilişkin hüküm uygulanmaz.

Yıllık ücretli izine hak kazanmak için gerekli olan bir yıllık çalışma süresinin hesabında işçilerin, aynı işverenin bir veya çeşitli işyerlerinde çalıştıkları süreler birleştirilerek göz önüne alınır.

**** Yıllık İzin Süreleri Ne Kadardır?

Bir yıldan beş yıla kadar çalışanlar on dört günden; beş yıldan fazla, on beş yıldan az olanlara yirmi günden; on beş yıl (dahil) ve daha fazla olanlara ise yirmi altı günden az olmamak kaydıyla, ücretli izin kullanma hakkı vardır. Bununla birlikte 18 yaşından küçükler ve 50 yaşından büyükler, 20 günden az yıllık izin süresine sahip olamaz.

**** İşçinin yıllık ücretli izni, patron tarafından üçer beşer bölünebilir mi?

Yıllık ücretli izin süresi patron tarafından bölünemez. İşçinin kıdemine göre hak etmiş olduğu izin sürelerinin sürekli bir biçimde verilmesi zorunludur. Ancak hak edilen izin süreleri tarafların anlaşması suretiyle, bir bölümü on günden aşağı olmamak üzere en çok üçe bölünebilir.

**** İzin esnasında raporlu hastalık, izin içerisine mi dahil edilir?

İş Kanunu’nda yıllık ücretli iznini kullanan işçinin hastalanması ve istirahat alması durumunda geçen sürenin yıllık izinden sayılıp sayılmayacağı konusunda bir düzenleme yok. İşçinin yıllık izin kullanmasındaki asıl amacın dinlenmesi olduğunu kabul ettiğimize göre, izinde hastalanan işçinin istirahatli olduğu sürenin yıllık izin süresinden sayılmaz. Daha sonra, uygun bir zamanda izinde raporlu olarak geçirdiği süre kadar izin kullanma talebinde bulunabilir. Dinlenme ve hastalık izinleri yıllık izine mahsup edilemez. İzinde çalışma yasağı ve izinin bölünemezliği kuralları bunu destekler. Yani yıllık izinden amaç anayasada da belirtildiği üzere, işçinin tam olarak dinlenmesinin sağlanmasıdır.

**** İzin ücretimi ne zaman alabilirim?

İşveren, yıllık ücretli iznini kullanan her işçiye, yıllık izin dönemine ilişkin ücretini, ilgili işçinin izne başlamasından önce peşin olarak ödemek veya avans olarak vermek durumundadır. Yıllık ücretli izin hakkını kazanan işçinin iznini kullanmadan iş akdinin sona ermesi durumunda, hak edip de kullanmadığı izin ücreti işçiye ödenir. Hizmet akdinin işçi veya işveren tarafından feshedilmesinin önemi yoktur. Hatta sözleşmenin işveren tarafından feshedilmesinin haklı bir nedene dayanmasının da izin ücretinin ödenmesine etkisi yoktur. Eğer sözleşme sona ermeden önce işçi yıllık ücretli izin hakkını kazandı ve kullanmadıysa, ücretini alacaktır.

**** İşçi haklarından doğan alacakların zaman aşımı süresi nedir?

Kıdem tazminatı dışındaki işçilik hakları, 5 yıllık zaman aşımına, kıdem tazminatı ise iş sözleşmesinin feshinden itibaren on yıllık zaman aşımına tabidir. Bu dava için İş Mahkemesi’ne başvurmak gerekir. Fazla çalışma, genel tatil, bayram ve yıllık ücretli izin paraları beş yıllık zamanaşımına bağlı işçilik haklarındandır. Bununla birlikte dava aşamasında zorluk çıkarmaması açısından işten ayrılıken ‘ibraname’ adı altında işçinin tek tek hak ve alacakların sayıldığı ibra sözleşmesini, hiçbir şekilde imzalamamak gerekmektedir.
