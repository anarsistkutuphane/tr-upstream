#title Suyu Sel, Ateşi Yangın, Barışı Bomba, Savaşı İşgal. İşte Devlet
#author Meydan Gazetesi
#SORTtopics bomba, devlet, isyan, katliam, savaş, yangın
#date 03.09.2015
#lang tr
#pubdate 2020-02-14T23:00:00



Su temizler, ateş ısıtır. Ama bu su devletin seliyse, bu ateş de devletin yangınıysa, getirdikleri şey yalnızca ve yalnızca ölüm olur. Aksini iddia etse de, adına barış dediği zaman da katleder devlet, hem de bombalarıyla. Savaş zamanı da katleder elbet, hem de işgal ettiği tüm coğrafyalarda.


*** Devletin Suyu Seldir!

Bu sel öyle bir seldir ki, karşısına çıkan ne varsa önüne katıp ölüme taşır. Çocuklarımız, kardeşlerimiz, annelerimiz ve babalarımız gözlerimizin önünde yitip gider. Ağaçlar sökülür, ormanlar gözden kaybolur. Evlerimizden, pencerelerimizden girer sel, dükkanlarımızdan, kapılarımızdan çıkar; bizimle beraber olan ne varsa alır götürür …

Utanmazdır, sahtedir devletin seli. Dünyanın her dilinde “katliam” denilen şeylere “felaket” der, “doğal afet” der. HES’lerle, barajlarla, madenlerle talan eder köyleri, ormanları, çayırları; duble yollarla, sözde “yeşil yol”larla katleder ormanları. Derelere hükmetmek ister; yataklarını daraltır, yolunu değiştirir. Suyu elinde tutmak, kendine saklamak için, kapatır onu. Dolup dolup akamayan ırmaklar, taşmak ister ve taşar da sonunda. Artvin’de 8 kişi yaşamını yitirdiğinde, bu devlet kayıtlarına felaket diye geçerken, bizlerin yüreklerine yeni bir öfke tohumu düşüren bir katliam olur artık.


*** Devletin Ateşi Yangındır!

Alevle ağacın en çirkin buluşmasıdır devletin yangını. Çünkü bir şeyi yok etmenin en iyi yolu, yakıp yıkmaktan geçer. “Terörist”leri saklandığı yerden çekip çıkarmak için yakılır ormanlar; orada yaşayanlar artık yaşayamasın diye yakılır çayırlar ve köyler; bir dil artık unutulsun, devletin düzenine uymayan, bir türlü baş eğmeyen bir halk, artık susturulsun diye ateşe verilir bütün bir coğrafya. Devlet her şeyi görebilsin diye, her şeye kanlı elini daldırabilsin diye vardır devletin yangını.

Fakat unutulmamalıdır ki, çıkan her yangından sonra yeniden doğar ormanlar. Gerçekten böyledir bu; en baştan filizlenir, köklerini en derine salar ağaçlar. Kimi çam ağaçlarının kozalakları yangın esnasında patlayarak, ileriye doğru fırlatır kendini; içindeki tohumlar saçılır, yeni topraklarda, yeni umutlarla…



*** Devletin Barışı Bombadır!

Devlet kendisine barış diye uzatılan her eli kırar! Devletle barış olmaz; yapılan onca müzakerenin, görüşmenin, huzur söylemlerinin arkasında ağır ağır yürütür “savaş planları”nı. Devletin barışı bombadır çünkü, onun için her barış anlaşması, yeni bir savaş başlatmak için bir fırsattır. Öyle ki, yüreklerinde taşıdıkları umutları, oyuncaklara yükleyerek sınırın öte yakasına taşımak isteyen devrimcilere, çantasında bomba taşıyan bir alçakla karşılık verir.

Barış ile devlet ancak böyle alçakça bir oyunla bir araya gelebilir. Bu yüzden, devlet ne zaman barışı ağzına alırsa, emin olun bir yerlerde bombalar patlar, “teröristler” imha edilir. Ve bu topraklarda yaşayanların en azından bir kısmını terörist olmaktan kurtaran şey, belki de yanı başlarında henüz bir bomba patlamamış olmasıdır!


*** Devletin Savaşı İşgaldir!

Kavga ya da mücadele demez hiçbir zaman, dese bile yakışmaz ağzına; eğreti durur. Çünkü, devletin savaşı işgaldir! O kontrol etmek ister, ele geçirmek ister, kendine saklamak, tuttuğu her şeyi kendine dönüştürmek ister.

Komşuluk nedir bilmez devlet, onun için dünyada iki tane toprak parçası vardır: Kendisin olanlar ve kendisinin olmayanlar. Hele bahsi geçen komşu Kürt ise, bu komşuluktan onların payına top, tüfek mayın ve bomba düşer; işgal düşer! Topyekün bir coğrafyanın işgalinin tetiklediği halk isyanları düşer.



