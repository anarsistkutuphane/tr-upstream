#title Dünya Anarşistlerinden Kobanê Dayanışması
#author Meydan Gazetesi
#SORTtopics kobanê, rojava, enternasyonal, anarşist hareket, kürt özgürlük hareketi, kürtler
#date 23.10.2014
#lang tr
#pubdate 2020-03-24T16:17:16


<quote>
Rojava Devrimi biz devrimci anarşistler için her geçen gün büyükmete olan yeni bir dünyanın habercisiydi. Yaşamın yeniden yapılandırıldığı bir deneyim oldu Rojava. Kobenê’yi savunmak da bizim için devrimi yani Rojava’yı savunmaktır.

Kobanê Direnişi başladığından bu yana, uluslararası dayanışmanın önemi bir kez daha sergilendi. Özellikle sınır köylerindeki DAF’lı yoldaşlarımızın bilgilendirmeleri sayesinde, aldığımız bilgileri dünyanın farklı yerlerinden yoldaşlarımızla paylaştık. Paylaştık, çünkü Kobanê’deki direnişin büyümesi ve dünyanın sessizliğini kırmak adına bu sorumluluğun bir parçası da bizlerdik. Bulgaristan Anarşist Federasyonu’undan yoldaşlarımızın söylediği gibi devrimci dayanışma en büyük silahımızdı.

Bu bilgilendirmeler doğrultusunda, İrlanda’dan Katalonya’ya birçok dayanışma eylemi gerçekleştirdi. Ummanita Nova gibi anarşist gazeteler Kobanê Direnişi’ne ön sayfalarında yer verdi. Yunanistan’da yoldaşlarımız Atina ve Selanik sokaklarını hazırladıkları Kobanê afişleriyle donattılar. Farklı örgütlerden yoldaşlarımız, sınır köylerinde bulunan yoldaşlarımızın yanında giderek, Kobanê dayanışmasına bizzat dahil oldular.

Meydan Gazetesi olarak farklı coğrafyalardan ve örgütlerden anarşist yoldaşlarımızdan istediğimiz dayanışma metinlerini siz okuyucularımızla paylaşıyoruz.
</quote>

<br>


*** Kobanê Direnişi’nin 24. gününde Devrimci Anarşistler tarafından yazılan mesaj;

Kobanê’de IŞİD’in saldırıları 24. gününe vardı. Saldırılara karşı tüm sınır köylerinde halk savunma birlikleri Kobane için canlı kalkan nöbetinde ve Kobanê’nin düşmemesi için yaşadığımız coğrafyanın her yerinde, herkes ayakta.

Biz yaklaşık üç haftadır Kobanê’nin batısında bulunan sınır köyü Boydê’de canlı kalkan nöbetindeyiz. Son iki gündür ise Kobanê’de, şehrin dış mahallelerinde ve şehir merkezinde patlamalar ve çatışma sesleri yoğunlaştı. Çatışmaların bu kadar yoğunlaştığı bir süreçte jandarma da sınır köylerindeki canlı kalkan nöbetlerine saldırılarını yoğunlaştırdı. Sınırın iki yanından sınıra yaklaşanlara gaz bombalarıyla saldıran TC askerleri Salı günü de bulunduğumuz köye gaz bombalarıyla saldırdı. TC askeri saldırılarında zaman zaman da silah kullandı ve çeşitli şekillerde yaralanmalar oldu.

Özellikle bu saldırıların sınır köylerine yapılmasının anlamı sınırda IŞİD geçişi olduğu anlamına geliyor. TC’nin IŞİD’e olan desteği oradan gözüktüğü gibi burada da net bir şekilde gözüküyor. Tabi sadece gözükenler bunlar değil. IŞİD’in, Kobanê’deki saldırılarını yöneten liderlerinden birinin YPJ/YPG güçleri tarafından öldürüldüğünü öğrendik. Diğer taraftan tüm gün süren çatışmalar bugün de aynı yoğunluğunda sürüyor. Sabah saatlerinden bu yana patlama sesleri neredeyse hiç dinmedi. Ancak artık bu patlamaların YPJ/YPG güçleri tarafından gerçekleştiği biliniyor. YPJ/YPG’nin taktiksel olarak Kobanê merkezindeki sokakları boşaltarak buraya gelen IŞİD’cilerin abluka altına alınarak etkisizleştirildiği bilgisi de taktiksel bir başarı olarak aktarılanlar arasında.

Dilden dile köy meclislerinde anlatılanlar herkesi heyecanlandırıyor. Bunlardan biri de IŞİD’cilerin kadın gerilla korkusu. Devletin, terörün, katliamın temsilcisi IŞİD’ciler tabii ki erkek egemenliğinin de temsilcisi. Bir kadın gerilla yani bir YPJ’li tarafından öldürüldüklerinde sözde “şehit” olamayacaklarına inandıklarından YPJ ile karşılaşmaktan korkuyorlar. Çünkü her karşılaştıklarında kendileriyle “savaşan” kadınlar IŞİD’cileri affetmiyor. İşte bu YPJ’nin erkek egemenliğine karşı savaşarak yarattığı özgürlüktür.

Son iki gündür tüm Kürdistan’da ve Anadolu’nun tüm şehirlerinde yükselen isyan bizlere yeniden örgütlü halkların yenilmezliğini yaşatıyor. Kobanê ve Kobanê sınırındaki köylülerinin yanı sıra tüm Rojava’da bu isyanlar herkesin devrime olan inancını arttırıyor. Artan inancımız düşen her kardeşimizde bir burukluk yaşatsa da buradaki herkesin öfkesine öfke, gücüne güç katıyor. Dizlere vurulan yumrukla başlayan ağıtlar yeryüzünü çatlatacak kadar güçlü ve hızlı vurulan adımların halayına dönüşüveriyor. Böylece üzüntümüz güçle ve hızla öfkeleşiyor.

Buralarda herkesin tam da ihtiyacı olan bu. Her şeye rağmen istenen özgürlük ve devrim için.

Yaşasın Halkların Kobanê Direnişi!

Yaşasın Rojava Devrimi!


*** Devrimci Dayanışma Silahımızdır – FAB Bulgaristan Anarşist Federasyonu

Kobanê halkının IŞİD’e karşı direnişini ve Rojava Devrimi’ni -DAF’tan öğrendiğimiz andan beri- takip ediyoruz.

Rojava’da kuşkusuz, halkın söz sahibi olduğu ve polis ve ordu gibi ezici yapıları yok eden devletsiz ve devrimci bir süreç başlatılmıştır. Halkın kendi yaşamını, ezici kurumların zorlamasıyla değil dayanışma ilişkileri üzerinden örgütleyebildiği görülüyor. Rojava’da yaşananlar, insanları zorla yöneten bütün çeteleri rahatsız ediyor. Bu çete ister kendinden menkul İslam devleti olsun, ister Türkiye, Bulgaristan ya ABD gibi “tanınan” bir devlet.

Bölgedeki halklar en güçlü çete tarafından tekrar köleleştirilene kadar bu çeteler rahat durmayacaklar. Riyakarlık ve zulümden tanklar ve hava saldırılarına kadar her silahı kullanacaklar. Rojava’nın özgür halkı için tek umut, kendi örgütlü iradeleri ve tüm dünyadaki sıradan insanların dayanışmasıdır.

Korkarız dünya çapındaki sıradan insanlar böyle bir meydan okumaya hazır değiller çünkü kendi devletlerine karşı anlayışları ve örgütlülükleri eksik. Avrupa sokaklarındaki eylemler dayanışma açısından iyi olsa da, Rojava halkını tüm dünyanın askeri gücüne karşı korumak için bundan çok daha fazlasına ihtiyaç var. Orduları ve devletleri durdurabilecek dünya çapında devrimci örgütlülüğe ihtiyacımız var.

Devrimci dayanışma örgütlenirse şimdi, şu anda, dünyanın herhangi bir yerinde insanların ayaklanması, bu çetelerin hepsini birden ortadan kaldırmaya yeter.


*** Yüreğimizde Yeni Bir Dünya Büyümekte – El Comitè de Solidaritat amb el Kurdistan de Granollers – Alvaro İnigo – Katalonya

Bugün Kobanê’dekiler vahşilik karşısında özgürlük için savaşıyorlar. Bir Katalonya şehri olan Granollers’da biz anarşistler, insanlara Kobanê’de olanları anlatmaya çalışıyoruz.

Ana akım medya bizlere ABD ve diğer ülkelerin, Suriye ve Irak’da IŞİD’e saldırdığını söylüyor. Biz ise insanlara bunun doğru olmadığını, gerçekte tüm dünya sessizce izlerken IŞİD’in Kobanê’ye saldırdığını anlatıyoruz.

Suudi Arabistan, İsrail, Katar, Türkiye ve diğer devletler Rojavalıların özgür olmalarını istemiyor. Azınlıkların ve kadınların haklarına saygı duyulan, bu demokratik topluma benzer toplumlar istemiyorlar. Özgürlüğün, sosyal adaletin ve kadın haklarının düşmanları olarak dini yobazlığa ve sosyal adaletsizliğe dayanan toplumlar istiyorlar.

Granollers’da bir grup anarşist olarak Kürdistanla dayanışma adına insanlara Rojava’daki durumu açıklamak istediğimiz bir komite kurduk. Haftalık olarak tekrarlamak istediğimiz bir buluşma gerçekleştirdik. Kürt mücadelesinin gerçek demokrasi, kooperatif ekonomi, kadın hakları ve bürokratik ulus-devletlerin nihai çözülüşü adına dünya çapında bir hareket için örnek olabileceğini anlatmak istiyoruz.

Bu komite sayesinde Kobanê’deki insanların çektiği acıları ve direnişleri yerel basında yer buluyor. Mücadelede Rojavalılarla biriz. Ne gerekiyorsa yapmak için buradayız.

Kobanêliler! Sizinleyiz, yüreğimiz sizinle, ve yüreğimizde yeni bir dünya büyümekte.


*** Dini ve Askeri Diktatörlüklere Karşı Rojava Umuttur – IFA Uluslararası Anarşist Federasyon

Kürt halkları cihatçı IŞİD’in saldırılarına direniyor. Az miktarda ve hafif silahla korudukları Kobanê şehrinde cesaret ve kararlılıkla sürdürdükleri direnişe saygı duymak gerekir. Maalesef, takviye ve destek hiç yeterli değil. Türk devletinin silah ve yardım geçişini engellemek için sınırı kapatarak polis ve asker yığması, Kobanê’ ye yapılan cihatçı saldırıyı destekledi. Bugün, cihatçılar şehrin içinde.

Türk devleti, halka yardım etmek şöyle dursun, Kürt direnişini zayıflatıp yok etmek için elinden geleni yapıyor. Çünkü bu direnişin, bağımsızlığını reddettiği Kürt halkının bölgedeki gücünü artırdığını biliyor. Türk devleti sınırın Suriye tarafında, kendi kontrolünde bir tampon bölge yaratmak istemesi, Rojava’yı oluşturan üç kantonu işgal etmek için bir bahane. Fransa devleti, Kürtlerin reddettiği bu öneriyi, Kürt direnişi için en kötü çözümü destekleyerek, tümüyle jeopolitik çıkarcılığı seçmiştir.

Yerleşik güç ilişkilerinin (kayırmacılık, çürüme, erkek-egemenliği, itaat ve dayatılan inanç sistemleri) çöktüğü bir ortamda Kürt savaşçıların öz-örgütlülüğü kararlılıkla savunmasında, hayranlık uyandırıcı bir özgürlük tutkusu, tehdit edici bir bağımsızlık ve itaatsizlik var. Bu, Arap-Batı koalisyonunun destekleyeceği bir şey değil.

Fakat bizim için, umut ettiğimiz şey bu. Bu umut, Ortadoğu’nun keşmekeşinden, hem dinci gericiliğe, hem de bölgede on yıllardır süren ordu-devletlere karşı savaşabilecek güçtedir. Bu umut, özgürlüğü engelleyen güçlere karşı direniş hareketlerinin yeniden doğduklarını görecektir. Kobanê’ye yapılan saldırılar karşısında, Türkiye’de yaşayan Kürtler şimdiden inanılmaz bir dayanışmayı harekete geçirdiler (ve şiddetli baskılarla karşılaşıyorlar). Diasporada Avrupa çapında birçok şehirdeki protestoları, sınırda binlerce insanın toplanmasını ve şehri korumak için Kobanê’ye gelen yüzlerce gönüllüyü gördük. Bize düşen, direniş hareketlerine açıkça destek vererek harekete geçmek ve özgürlük güçlerinin çağrılarını yükseltmektir.

Beraber, devletsiz ve din baskısı olmayan yeni bir toplumu inşa edene dek desteğimize güvenebilirsiniz.


*** Kobanê’deki Kavga Kavgamızdır- Anti Otoriter Hareket – Atina

Suriye – Kobanê’deki kavga hepimizi ilgilendiriyor; Kobanê’de süren çarpışmalar, Kürt milislerin IŞİD denen üretilmiş teröre karşı direnişinin ötesinde bütünüyle iki dünyanın çarpışmasını içeriyor: Bizim kendi dünyamız; özgürlük, toplumsal adalet ve doğrudan demokrasi için mücadele eden sıradan insanların dünyası ve sadece kar ve egemenlik arayışının etrafında dönen, devletlerin ve kapitalizmin dünyası.

Bir yanda IŞİD var ki geçmişten gelen ve Batı kültürüyle çelişen bir çeşit toplumsal güç değil bu; Batı’nın on yıllık terörünün ve Ortadoğu halklarına dayattığı yerel takipçilerinin ayna görüntüsü; Batılı aktörlerin bölgede yerleşik tüm güçleri bertaraf etmesinin ürünü; Irak’taki işgal güçlerinin kurduğu şirket devleti dağıldıktan sonra kolayca elde edilebilen, son-teknoloji-ürünü silahlarla donanmış bir grup. İmparatorluğun Irak’ta ve dünyanın başka yerlerinde kullandığı, kendi özel ordularına benzeyen, paralı askerlerden oluşan bir ordu. Halkların üzerinde totaliter egemenlik dışında bir pusulası olmayan IŞİD, bölgede ve yavaş yavaş imparatorluğun kalbinde, aynı neoliberal batı devleti-kapital kompleksinin yöntemlerini kullanıyor. Gazze’de ve Bağdat’ta kullanılanlar gibi, katledilen yaşamlardan geriye sadece yıkılmış duvarlarda asılı izler bırakan kitle imha silahlarını kullanmak yerine, terör kültüründen tümüyle istifade eden, gösterişli ve dehşet verici kelle uçurma görüntülerini kullanıyor.

Diğer yanda biz varız; hayatlarımız pahasına da olsa özgürlüğümüzü geri kazanmak için ayaklanan tüm bu insanlar; Kobanê’de kahramanca çatışan Kürt militanlarında vücut bulan ideal budur; demokratik federalizm idealinden ilham alarak kendi toplumlarını devletlerin / kapitalizmin mutlak idaresine karşı korurken, halkın ilk söze sahip olduğu devletsiz bir demokrasinin kavgasını veriyorlar. Kobanê’deki kavga, bir mücadelenin sonunu değil, Kürt halkının özgürlük mücadelesinin genişleyeceği, devletlerden ve kapitalizmden uzak yeni bir dünya için savaşan tüm bu insanlara ilham vereceği ve referans olacağı bir mücadelenin başlangıcıdır.


*** Rojava Toplumsal Devrimini Dayanışmayla Selamlıyoruz – IFA – İtalya Anarşist Federasyonu

“Rojava’da Toplumsal Devrimi başlatabilecek, geniş çapta bölgesel özyönetim deneyimlerine ve cinsiyet ayrımcılığının üstesinden gelinmesine hayat veren – ve bu yüzden hem IŞİD, hem Türk ordusu tarafından saldırı altında olan- halkların direnişi önünde İtalya Anarşist Federasyonu meclisi, dayanışmasını ve desteğini beyan eder.

Bu dayanışma ve bu destek öncelikle bu mücadelenin özgürlükçü parçalarınadır ve bölge halklarına ve onların özgürlüğüne saldıran küresel ve bölgesel güçlerin müdahalesi ile yapılamaz. FAI meclisi bu aşamada, çoğu zaman ana akım medya tarafından saklanan ya da çarpıtılan sürecin dinamikleri hakkında, bilgi toplamak üzere harekete geçmeyi gerekli görmektedir.

FAI meclisi, federasyonun bireylerini ve gruplarını ve tüm toplumsal muhalefet hareketini, dayanışmayla, Rojava kantonlarındaki halkların deneyimlerine doğru harekete geçmeye davet eder. Bu hareketin bir hedefi Rojava etrafındaki sınırların açılması olmalıdır.”


*** Anarşist Komünistlerden Kobanê İçin Açıklama

Rojava’daki yoldaşlarımız tarafından başlatılan devrim, Kürt halkının ve onlar yoluyla tüm Ortadoğu halklarının özgürlüğü için çok daha büyük bir hareketin bir parçası; özgür, eşitlikçi, ezen ve ezilenin olmadığı bir dünyanın özlemini çekenler için bir ilham kaynağıdır. Özgür, öz-yönetimle işleyen, doğaya ve halkların özerkliğine saygı duyan bir anarşizmin altyapısını barındıran bir doğrudan demokrasi deneyimidir. Tüm dünyadaki anarşist komünistler, bu devrimi kendi devrimleri olarak hissetmektedir.

Bugün bu devrim, üç yönlü bir tehditle karşı karşıyadır; emperyalizm, devlet otoriteciliği ve Ortadoğu halklarının işgal edilmesiyle ortaya çıkmış çürümüşlükten doğan bağnaz, fanatik ve aşırı muhafazakar bir tepki. Bu bağnaz tepki emperyalizm sayesinde elde edilen silahlarla saldırırken, Türk silahlı kuvvetleri ise Kürt halkını adeta bir örs ve çekiç gibi çevrelemek için tankları ve ağır silahlarıyla bir zırhlı duvar örmektedir. Rojava’daki yoldaşlarımız, bu üçlü tehdite ve bu çarpık işgale karşı harekete geçen halklar olarak, elde silah savaşmakta ve mücadeleyle elde ettikleri özerkliklerini ve özgürlüklerini savunmaktadırlar. Ancak bu savaşta yalnız değiller. Arkalarında milyonlarca özgür ruhlu insan olarak toplanmakta, onları desteklemekte ve yüreğimizde büyümekte olan yeni bir dünya için verdikleri savaşta ellerimizi ellerine uzatmaktayız. Kobanê şehrinde bugün verilen bu kahramanca savaş, “¡No Pasarán!” (Geçit Yok!) şiarıyla mücadele etmiş insanların uzun tarihinden bir mirastır.

Dünyanın pek çok farklı yerinden açıklamak istiyoruz; mücadeleniz mücadelemizdir. Sizlerleyiz, toplumsal devrim için verilen bu savaşta üzerimize düşeni yapmamız her zamankinden daha fazla gerekli ve bunu yapmaya hazırız. Baskılara son verilmesini, Kürt devrimcileri suçlu göstermeye yönelik girişimlerden uluslarası boyutta vazgeçilmesini, kaderinin dizginlerini ellerine alan toplulukların özerkliğine saygı gösterilmesini talep ediyoruz. Baskının ve sömürünün her türlüsünden kurtuluş için, özgür ve eşit yeni bir toplumun inşası için verilen mücadelede hepimiz biriz. Devletin, kapitalizmin, emperyalizmin ve patriyarkinin sistematik şiddetinden kurtuluş adına, devrimci toplum mücadelenizi gereken her şekilde destekliyoruz.

Devrimci ve özgürlükçü dayanışma ile,

Periódico Solidaridad (Şili), <strong>Frente de Trabajadores Ernesto Miranda –FTEM (Şili)</strong>, Workers Solidarity Movement / İşçi Dayanışma Hareketi (İrlanda), F<strong>ederazione dei Comunisti Anarchici / Anarşist Komünist Federasyon –FdCA (İtalya)</strong>, Organisation Socialiste Libertaire –OSL (İsviçre), <strong>Collectif Communiste Libertaire Bienne –CCLBienne (İsviçre)</strong>, Alternative Libertaire (Fransa), <strong>Melbourne Anarchist Communist Group / Melbourne Anarşist Komünist Grup (Avusturalya)</strong>, Melissa Sepúlveda, Federation of Students of University of Chile –Fech / Şili Üniversite Öğrencileri Federasyonu Başkanı (Şili)


*** Kazanacağız

Kobanê’deki direniş ve Rojava Devrimi, bugün özgürlük ve eşit bir dünyaya inananlar için güçlü bir mesajdır. Muktedirlerin başka bir alternatif olmadığını ilan ettiği ve ezilenlere ellerindekilerle yetinmek zorunda olduklarını söyledikleri bir zamanda, bu kitle hareketi geliyor ve dünyaya direnişin bereketli olduğunu gösteriyor. Bu hareket, Chiapas’ta, içinde birçok dünyaya yer olan bir dünya yaratan yerli kardeşlerimizdir. Tüm dünyada sokağa dökülerek gücü doğrudan ellerinde isteyen kardeşlerimizdir. Siyonist işgale karşı taşlarla ve mermilerle savaşan kardeşlerimizdir. Kolombiya’da toprak ve özerklik isteyen devrimci köylü kardeşlerimizdir.

Kobanê halkı direnişinde yalnız değildir. Yaşama hakkımızı, yeryüzünde olma hakkımızı isteyen geniş hareketin parçasıdır. Devletler dilimizi yasaklayabilir, örgütlerimize eziyet edebilir, gücümüzü elimizden alabilirler. Ama hiçbir zaman direniş ruhumuzu ezemezler. Bugün, Kürtlerin emperyalizme, devlete ve kapitalizme, erkek egemenliğine ve dini muhafazakarlığın cani fanatiklerine karşı örnek alınacak, kahramanca ve zorlu bir mücadele yürütürken dünyaya direnişin değerini gösteriyorlar. Direniş bereketlidir ve dünya çapında bir, iki, yüz Kobanê yaratmamız gerekir.

Kazanacağız!


*** Halkların Kaçınılmaz Zaferine ve Kardeşliğine İNANIYORUZ – Veselin Nikolov – ARSINDIKAT – Bulgaristan


<quote>

“…Şimdi kaderim sensin,

Şimdi senin kaderini yaşıyorum ve paylaşıyorum.

Özgürlük mücadelene

Tüm varlığımla katılıyorum.

Şimdi coşkuluyum, şimdi kutluyorum

Kavgada her zaferini.

Güveniyorum gençliğine ve gücüne

Ve kendi gücüme seninkiyle birleşen…”
</quote>




1936-1939 İspanya Devrimi’ne adanmış devrimci şair Nikola Vaptsarov’un şiirinden

Yukarıdaki her satır, her kelime, nokta ve virgül, benim ve diğer anarşist arkadaşlarımın Kobanê’de devam eden destansı mücadele için hissettiklerimizi içeriyor.

Kobanê; dünden önce mesafe, dil engeli ve en çok da bilgi eksikliğimiz, sizi uzakta ve muğlak tutuyordu ve sadece bize değil.

Bugün sen, Kobanê, bir davadan ötesin. Sen binlerce yüreğin aynı ritimde atması için devrimci nabızsın…

Biz, sebepli ya da sebepsiz; ama genelde içi boş sözün iddialı söylendiği bir dünyada yaşıyoruz. “Kobanê’deki mücadele ile dayanışma” dediğimiz de böyle değil, sahici. Çünkü bunu hissediyoruz ve çünkü –etrafı önde cani IŞİD sürüsü, arkasında “uluslararası camianın” inşa ettiği riyakarlık duvarıyla çevrilmiş- ön saflardaki milislerin sahip olduğu tek şey dayanışma.

Birkaç hafta önce NATO devletleri hava saldırısı başlattı, pratikte ne oldu? Cihatçılar kuzeyde Kobanê’yi sıkıştırıp Bağdat’a doğru ilerlediler.

Garip şey, belli ki ya Batı devletlerinin ultramodern silahları bozuk, ya da Bulgaristan’da dediğimiz gibi, “ninenin uçurtmasını” bombalıyorlar…

Bilgisayarların başına oturmuş bilgi arıyoruz. Türkiye’deki yoldaşlarımız sınır civarına, yanınıza gelmişler, yazıyorlar. Sosyal ağlarımız var; haber, fotoğraf ve videolar ulaşıyor. Kobanê haritasında bakıp değişik kaynaklardan son çatışmaların bilgilerini alıyoruz, nasıl savunduğunuzu ve geri püskürttüğünüzü dinliyoruz, hiç uyumadığımız geceler oldu.

Dünya medyası en az on kere “Kobanê düştü!”, dedi. İnanmadık ve haklıydık.

Çünkü Kobanê hiçbir zaman düşmeyecek!

Sizinleyiz!

Halkların kaçınılmaz zaferi ve kardeşliğine inancımızla, Bulgaristan’dan devrimci selamlar!

Yaşasın Kürt Halkı’nın Toplumsal Devrimi!

Yaşasın Toplumsal Devrim!



*** Rojava Devrimi Kapitalizme Darbedir – İlan Shalif – AHDUT- İsrail

Kobanê’deki mücadele, devlet henüz tümüyle yenilmeden – uygun koşullar altında kitlelerin otoriter olmayan bir yaşam biçimine başlamaya hazır olduğunu göstermesi açısından- ancak Chiapas’la karşılaştırılabilir. Fakat Suriye Kürdistanı’ndaki yeni toplumsal düzen bölgedeki elitlere karşı çok ciddi bir tehdit oluşturmaktadır.

Rojava Devrimi, devletsiz bir deneyim olarak, kapitalist toplumsal düzen ya da onun ürettiği Marksist otoriter devletlerin alternatifi olmadığı iddiasını boşa çıkararak katkı sundu.

Tarih boyunca bölgelerdeki gerici rejimlere karşı kitlesel devrimler olmuştur. Uzun vadede, mücadele yenilse de iyi yönde tarihsel değişimleri hızlandırmışlardır.

Türk devleti köktencilerin Rojava Devrimi’ni yok etmelerine yardımcı olmak için kendi payına düşeni yapacaktır. ABD ve diğer emperyal güçler, köktencilere karşıymış gibi davranıp Kürtlerin, Irak benzeri gerici bir özerklik bulmasına yardım edebilir.

Rojava Devrimi kapitalist toplumsal düzene fazla büyük bir tehdittir – sadece Türkye değil bölgedeki tüm elitlere. Bu yüzden Kobanê’yle dayanışma, bu kapitalist toplumsal düzene sözü olan herkes için zorunluluktur.


*** Öz-yönetim ve Özgürlük Mücadelesini Selamlıyoruz- Paul Bowman – WSM – İşçi Dayanışma Hareketi – İrlanda

WSM (İşçi Dayanışma Hareketi),’ne göre, Kobanê ve özerk Rojava, bölgede siyasi bir alternatifin gelişmesi adına hayati önem taşımaktadır. IŞİD’i, Suriye ve Irak üzerinde küresel ve bölgesel emperyalizm müdahalesinin zehirli atıkları olarak görmekteyiz.

Diğer yandan bu durum 2003’te ABD’nin Irak’ı işgali ve giderek daha fazla iflas eden, kiralık korumaları yani ünlü “ılımlı milisleri” finanse etme politikası haline dönüşmüştür. Bu gibi “ideolojik olmayan”, Amerikan bakış açısıyla uyumları ve ABD emperyalizminin bölgedeki rakiplerine karşı ideolojik düşmanlıkları yüzünden kiralanan güçlerin sorunu göründükleri gibi olmamalarıdır. Suikastinden kısa süre önce Ahrar el Şam lideri Hassan Aboud, Irak ordusunun IŞİD karşısında hezimetinin sebebinin “ordunun askeri ideolojiye sahip olmaması” olduğunu söylemişti.

ABD ve Batı’nın “ılımlı milisler” için körce arayışının karşısında, AKP yönetimindeki Türkiye devleti, Katar, Suudi Arabistan ve Birleşik Arap Emirlikleri, mezhepçi nefretin negatif ideolojisiyle işleyen güçleri mutlulukla finanse ettiler. Her biri diğerleriyle rekabeti için kendi favori çetesine sahip olsa da, Kardeşlik ya da Selefiler olsun, mezhepçi cihadçılara para ve silah kapılarını açmaları IŞİD’in marjinalleşmiş Iraklı gerillalardan Mezopotamya ve Doğu Akdeniz Bölgesi’ni tehdit eden uluslararası bir kiralık tehdit haline gelmesi için uygun ortamı sağlamıştır.

Bugün Kobanê’de iki güçlü ideoloji karşı karşıyadır. Bir tarafta emperyalizmin köleliği ve mezhepçi totaliterliği kucaklayan gayri meşru çocuğu, diğer taraftaysa öz-yönetim ve özgürlük fikirleri için savaşanlar var. Bu karşılaşmada Türkiye ve Batı’daki müttefiklerin bu iki ideoloji arasında özgürlüğü daha büyük tehdit olarak görmeleri şaşırtıcı değildir. İşçi Dayanışma Hareketi olarak Kobanê’yi abluka altına alırken IŞİD’e destek ve silah geçişine izin veren AKP rejiminin vahşi hareketlerini kınıyoruz.

YPG/YPJ gönüllülerinin Kobanê ve Rojava’da mücadelesini ve Kürtlerin Türkiye devletinin baskıları karşısındaki mücadelesini destekliyoruz.


*** Dünyanın Bütün Devletleri İliklerine Kadar Kobanê’den Korkuyorlar – İskender Doğu – ROAR Dergisi editörü

Türk sınırında, Kobanê’yi gören bir tepenin üzerinde dururken, gözlerimizin önünde küresel politikanın oyununu izlediğimizi fark etmek garip. Koalisyonun bombaları şehre düşerken Türk ordusu izliyor, YPG/YPJ’nin kahraman Kürt direnişçileri ABD’nin Ortadoğu’daki emperyalist emelleri ile yükselişine yardım ettiği IŞİD’le savaşıyor. Dünya liderlerinin çıkarları Kürt direnişine gelen lojistik ve maddi desteği engellemediği durumda bu dehşetin günler hatta saatler içinde sona erebileceğini bilmek, önümüzdeki gösteriyi katlanılmaz kılıyor.

İronik olarak, 19 Haziran 2012’de kurulan radikal demokratik kanton sistemi ile Kobanê halkları, tam da bu tip politik oyunlara karşı örgütlenmişti. Bozan Karayılan “Yaşamlarımız için aldığımız kararlarda profesyonel politikacılara ihtiyacımız yok” demişti. “Gücün halkta olduğu ve kararların demokratik konsensusla alındığı halk meclislerine ihtiyacımız var.”

Kürtlerin Türkiye sınırları içindeki mücadelesi, Suriye’deki kardeşlerininki ile yakından ilişkili. Suriye’deki iç savaş nedeniyle güçlü bir devletin yokluğu, Kuzey Suriye’deki Kürtlerin, Türkiye’deki çoğu yoldaşlarının yapmaya çalıştığı şeyi, demokratik özerklik ve halk meclislerinin kararlarıyla yönlendirilen yerel örgütleri kurmayı başarmış.

Kobanê halklarının yalnız bırakılmasının nedeni; dünyanın tüm devletlerinin, Türkiye’den Amerika’ya kadar, Kobanê ve Rojava’nın diğer kantonları Afrîn ve Dêrik’te kazanılanlardan iliklerine kadar korkmalarıdır.

Kobanê’deki durum günlük yaşamlarını yürütmek için kariyer politikacılarına ve hiyerarşik kurumlara ihtiyaç duymayan halkların gücü. Şimdi, Kobanê’nin her zamankinden daha fazla emperyalizme, kolonyalizme ve ezenlere karşı gelenlerin desteğine ihtiyacı var. Birleşin ve ayaklanarak bize yol gösterenlere saygınızı ve dayanışmanızı gösterin.

Bıjî Berxwedana Kobanê!


*** Kobanê’deki Zafer Şimdi Her Zamankinden Daha Mümkün Görünüyor – David Graeber – Londra LSE’de Antropoloji Profesörü, Yazar, Anarşist

Belli ki bölgedeki devletler – Türkiye, ABD ve Körfez devletleri – Kobanê’nin mezarını kazmaya başlamışlardı. Erdoğan’ın İslamcı hükümeti, evindeki ayaklanmalara bağışıklık geliştirdiğini düşünerek, örtük destek verip ve teşvik ettiği IŞİD, Rojava devrimci demokrasi deneyimini yok ettikten sonra, NATO kisvesi altında devreye girip Suriye içinde bir Türk güvenlik bölgesi oluşturabileceğini tasarladı.

ABD’nin örtük desteği ise, IŞİD’in Kobanê önleri hariç her yerdeki ağır silahlarını bombalayıp, Kobanê’yi IŞİD’in tanklarını ve ağır silahlarını güvenle kullanabileceği tek yer haline getirmesi oldu. Bütün devletler Kobanê’nin düşeceğine kesin gözüyle bakıyordu. Kimsenin beklemediği şey, YPG/YPJ direnişinin korkusuzluğu ve kararlılığıydı. Zamanında İspanya Uluslararası Tugaylar’ında savaşan babamın deyimiyle: İşte devrimin farkı budur. Musul’da ve Irak’ın başka şehirlerinde 800 IŞİD’ci, Irak ordusunun gelişmiş silahlara sahip eksiksiz tümenlerini kolayca dağıtırken; bu sayının 20 katıyla, ağır silahları olmayan, AK47 lerle silahlanmış birkaç bin devrimciyi yerinden oynatamadı. Şüphesiz ki Rojava, bugün dünyada eşi olmayan türde, bütünsel bir toplumsal hareket yaratmayı başarmıştır. Ve bu hareket, nüfusun bütün aklını ve kabiliyetini kullanıyor. Kobanê Direnişi, kahramanlığı ile ilham verici ama asıl ilhamını, bize gerçekten özgür bir toplumda açığa çıkan katıksız insani parıltıyı göstererek veriyor.

Kobanê’de bir zafer – ki şimdi her zamankinden daha mümkün gözüküyor – bölge halklarının yapabilecekleri konusundaki algılarını tümüyle dönüştürebilir. Emperyal güçlerin, şehrin savunmasını isteksizce destekliyor görünseler de, bunu engellemek için hemen her şeyi yapacaklarına şüphe yok: Üye olarak almaktan, açıkça baskıya kadar.


*** Kobanê’deki Direniş İlham Veriyor – Max Haiven – Kanada

Kobanê’yi, kuşağımızın Guernica’sı olacağı endişesiyle, günlerdir dakika başı sosyal medya ve haber sitelerinden takip ediyoruz. Picasso dehşet verici başyapıtı ‘Guernica’yı, 1937’de İspanyol faşist güçlerin isteği üzerine Alman ve İtalyan güçlerince bombalanan, aynı adlı kasabanın anısına yapmıştır. Pazarın kurulduğu gündü: Binlerce Bask’lı sivil ve direnişçi katledildi ya da sakat bırakıldı. Tüm bunlar olurken müttefik güçler, faşistlerin, komünistlerin ve anarşistlerin birbirlerini yok etmelerini görmek arzusuyla uzakta durdular (gerçi sessizce faşistlerin tarafını tutuyorlardı). Resimdeki işkence gören yüzler ve bükülmüş bedenler, faşizmin ve suç ortaklığının korkunç sonuçlarını hatırlatırlar.

Biliyoruz ki NATO emperyalist güçleri, kopardıkları tüm yaygara bir yana, AKP ile birlikte oynadıkları oyundan memnunlar ve IŞİD’in isyankâr Kürtleri dehşet verici biçimde yok etmesini ümit ediyorlar. Ve yine biliyoruz ki, son haftalarda bölgenin her yerinden ve dünyadan gönüllüler, küçük silahlarla IŞİD’i püskürtmek için verilen kahramanca kavgaya katılmak için Kobanê’ye akın etti. Rojava’da Kürt demokratik özerkliğini savunmak ve sürdürmek için direniyorlar. Tıpkı CNT-FAI ve Uluslararası Tugaylar’ın yüzyıl önce devrimi savunmak için İspanya’da yaptığı gibi bütün dünyaya ilham veriyorlar.

Kobanê’yi savunanların resmi yapılacağı gün geldiğinde, bu resim yurtlarını ve toplumlarını ümitsizce koruyan insanların değil, bütün bir imparatorluğa karşı duran insanların olacak. Karşılarındaki faşist ve küresel ittifak (Washington’dan Ankara’ya, Pekin’e ve Moskova’ya kadar), çıkarları için IŞİD’in düşmanı gibi davransa da, aynı temel nefreti paylaşırlar: Özgürlük, eşitlik, dayanışma, özerklik ve barış. Bu anlamda Kobanê, hepimizin kavgasını verdiği, küresel bir mücadelenin ön saflarıdır.


*** Ortadoğu’nun Umudu Rojava – Anarşist Gazete Alternative Libertarie – Guillaume Davranche – Fransa

Bu yazdan beri Özgürlükçü Alternatif (Alternative libertaire, AL) dahil Fransa Devrimci sol içinde Kürt özgürlük mücadelesi çok önemli bir tartışma konusu haline geldi. YPG-YPJ’nin IŞİD karşısında kahramanca mücadelesi, Şengal’daki Ezidilerin kurtarılması, Rojava’daki “demokratik özerklik”, Kobanê’deki trajik çatışmalar, Kobanê’yi savunmak için yükselen toplumsal hareketlilik… Tüm bu olaylar sonucunda anarşistler ve diğer devrimciler Fransa Kürt Birlikleri Federasyonu (Feyka)’nın Paris, Strazburg ve Marsilya’da örgütlediği Rojava ve Kobanê dayanışma eylemlerine katıldılar.

27 Eylül’de AL, Feyka’nın Paris’teki eylemine ve devrimci dayanışmaya katıldı.

AL, Fransa Kürt diasporasındaki eylemciler ve sürgündeki ya da Türkiye’deki Kürt anarşistlerle ilişkiler geliştirdi. DAF’ın Suriye-Türkiye sınırından gönderdiği fotoğrafları sürekli paylaştık, mesajları tercüme ettik. “Alternative Libertaire” Kasım sayısı Kürdistan’a ayrıldı ve DAF’la röportaj yapıldı.

Rojava’daki siyasi deneyim bizi gerçekten ilgilendiriyor. Kürt solunun öz-yönetim düşüncesinde aldığı yolu ve bu konudaki muğlaklığı engellemek için Mezopotamyalı anarşistlerin bakış açısına ihtiyacımız var.

Artık tümüyle kesin olan bir şey var: Suriye ve Irak’ı yok eden korkunç savaşta sadece gerici güçler (Esad, Erdoğan, IŞİD, Washington, Katar, Suudi Arabistan, Londra, Paris, Bağdat, Tahran…) yok. Ortadoğu’da umudu temsil eden en azından bir tane önemli aktör var: Kürt halkının “demokratik konfederalizmi”.



