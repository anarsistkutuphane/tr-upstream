#title Bilginin Korsanları
#author Meydan Gazetesi, Korsan Parti
#SORTauthors Meydan Gazetesi, Korsan Parti
#SORTtopics korsan parti, internet, bilişim, telif hakkı, mülkiyet
#date 27.08.2013
#lang tr
#pubdate 2020-04-21T23:12:00



<quote>

İktidarlar kontrol, korsanlar hayal ederek; iktidarlar sahiplenerek, korsanlar paylaşarak; iktidarlar bilgiyi satarak, korsanlar bilgiyi çalarak özgürleşiyorlar. Günümüzde medyadan yaşamlarımıza akan özgürleştirici bilginin korsanları iktidarların da kabusu haline geldi. Meydan Gazetesi olarak, küresel medya direnişinin Türkiye ayaklarından biri olan Korsan Parti Hareketi’yle yaptığımız röportajı sizlerle paylaşıyoruz.
</quote>

<strong>Meydan: İletişim teknolojilerinin bu kadar gelişkin ve ulaşılabilir olmasına rağmen, bilgi yine de iktidarların kontrolünde. Sizler, bilginin şeffaflığı ve paylaşımı konusunda ne düşünüyorsunuz?</strong>

<strong>Şevket:</strong> İktidarlar uzun yıllardır, her koşulda bilgiyi tekeline almaya çalışmaktadır. Ancak bunda hiçbir zaman başarılı olamazlar. En son Wikileaks örneğinde olduğu gibi, sızıntılar örneğinde olduğu gibi. Hiçbir zaman bilgi denetim altına alınamaz, çünkü bilginin yapısı buna izin vermez. Her zaman ulaşmayı ister, özgür olmayı ister. Buna rağmen özellikle son on yıldır, bilginin önemli bir hammadde haline gelişi, iktidarları bilgiyi denetlemeye itiyor.

<strong>Harun</strong>: İletişim ağlarını kuran teknik adamlardan biri olarak şunu söyleyebilirim; internetin kurulmasının temel sebebi bilgi özgürlüğüydü. Ama bugün internet, kurucularının amaçladığı pozisyonda değil. Artık insanlar, internette o kadar da özgür değil. Bunun en temel nedeni, iletişim ağlarının teknik altyapısına sahip olanlar iktidarlar ve iktidara yakın olan kesimler. İletişim ağlarının altyapısına müdahale edebilme şansları, onların söz sahibi olmalarında temel neden. Bizim buna hayır diyebilmemiz gerekiyor. Ya da internet gibi alternatif iletişim yollarını kendimiz geliştirmemiz gerekiyor. Çünkü bilgi iletişim teknolojilerinin ulaştığı boyut, hayallerimizle sınırlı. Korsan Parti Hareketi olarak biz bu meseleye ilişkin de çok düşünüp tartışıyoruz. Bu iletişim yollarının tek merkezden idare edilmesine karşı bir başkaldırı gerektiği kanaatindeyim. İnternetin şu an geldiği konuma evrilmesinde pay sahibi olan insanların bakış açılarından bir uzaklaşma söz konusu. Buna ilişkin çalışmalar var, yani alternatif internete ilişkin. Ama Türkiye’de buna ilişkin bir örgütlenme henüz yok.

<strong>Selin:</strong> Hukuki açıdan, iktidarların en sevdiği şey kısıtlamaktır. Buna karşı girişimler olmalıdır. Ancak Türkiye üzerinden bahsetmek gerekirse, internetle ilgili sınırlamalar ve buna karşı girişimler düşünüldüğünde kimse neden bahsedildiğini bilmiyor. Bağlantı kesmek, site kapatmak bilginin şeffaflığını engelleyen durumlar. İktidar yapıları da bilgi teknolojileriyle ne yapacağını bilmiyor. Bu tarz bir bilgiyle ne yapacağını bilmeyen iktidar kurumlarının var olması çok tehlikeli. İnternet tehlikelidir, bilgi tehlikelidir düşüncesinin kendisi tehlikeli. Bu düşünceyle ilgili olumsuz bir durum. Herhangi bir konuda fikir sahibi olmak isteyen insanların, bilgiden mahrum bırakılmasıdır bu durum. Bireyin düşüncesine ve dolayısıyla hayatına bir müdahaledir. Bu konuya ilişkin hukuki açıdan çok boşluk var. Bu boşluklar, bu kısıtlamalara karşı kullanılabilir. Ancak bilgisizlikten kaynaklı, bu boşluklar örtbas edilmeye çalışılıyor. Korsan Parti Hareketi’nin yapmak istediği şey bu boşlukları göstererek bir bilinçlendirme yaratmaya çalışmaktır.

<strong>Meydan: Kişinin yarattığı herhangi bir ürün, diğer kişinin kullanımına yönelik olduğunda, telif ve patent yasalarıyla birlikte bu kullanım ücretlendirilerek engelleniyor. Bu mülkiyet yasalarıyla ilgili ne düşünüyorsunuz, bu konuda çalışmalarınız var mı?</strong>

<strong>Selin:</strong> Patent sizin herhangi bir icat sonucu edindiğiniz haktır. Bunun için başvurmanız gereklidir. Telif ise eser sahibinin yaratımından itibaren otomatik kazanılan bir haktir. İçeriğinde dağıtma, kopyalama, performans, işleme gibi hakları barındırır.

Avrupa’da telif hakkının korunması, eser sahibinin ölümünden 40-50 yıl kadar sürüyor. Bu süre sonrasında eserin kullanım hakkına herkes erişebilir. Türkiye’de ise bu süre 70 yıl. Korsan Parti Hareketi, bu sürenin çok uzun olduğunu ve bunun daha minimum bir süreye indirilmesini savunur. Çünkü ortaya çıkan eser, aslında tüm toplumun kendi mirasıdır. Telifin de çok yüksek ücretlerinin olmasından dolayı, daha uygun bir seviyeye çekilmesi için çalışmalarımız var. Telifin eser sahibinin yaratıcılığını desteklemek için olduğunu düşünürsek, telif hakkının sıfırlanmasından çok, doğrudan eser sahibine bu telif ücretinin gitmesi gerektiğini düşünüyoruz. Kitabevi ve prodüktör gibi aracılara giden parayı engellemek için bunun gerekli olduğu kanaatindeyiz. Ya da eser sahibinin ortaya çıkardığı ürünü “open source” veya daha farklı iş modelleriyle herkesin kullanım ve erişimine açmasını savunuyoruz.

<strong>Şevket:</strong> İnsanlara gidip bu meseleleri anlattığımızda, farklı ekonomik modellerin de olduğunu anlatıyoruz. Ben bu tarz tartışmalara genelde şöyle başlıyorum; her şey bir tarlanın etrafına çit çekmekle başladı. O çit çekilmeseydi, belki tüm bu sorunlar olmayacaktı. Mülkiyet meselesi her şeyi etkileyen ancak çok da göz ardı edilen bir durumdur. Proudhon’un söylediği gibi “mülkiyet despotluğun anasıdır”. Bunu günümüze çektiğimizde, benzer tartışmaları yaşadığımızı düşünüyorum. MÜYAP gibi kurumlar bu tartışmada aynı tarz mülkiyet savunusu yapar bir pozisyona düşerken, geride kalan bizler bunun tamamen karşısında bir tavır alıyoruz.

<strong>Harun:</strong> Kişisel görüşüm, her şeyin ücretsiz olması gerektiği. İstediğim kitabı, istediğim müziği, istediğim filmi para vermeden öncelikli olarak izleyebilmeliyim, okuyabilmeliyim. Bu biraz insanların kendileriyle ilgili. Bunu para için mi yapıyorlar, yoksa kendilerini ifade etmek için mi? Mevcut sistemde sadece para için yapılmış o kadar fazla şey var ki. İyi iş yapanların öncelikli hedeflerinin para olmadığı görüşündeyim. Örneğin açık kaynak kodlarını herkesin kullanımı için dağıtırsın. Ortaya çıkan ürünleri, sermaye ile bu kadar ilişkilendirmek, o ürünlerin anlamını kaybetmesine neden oluyor. Bu ürünlere karşı saygının yitmesine neden oluyor. Korsan Parti Hareketi bu meseleye ilişkin durumları daha fazla netleştirmek için yola çıkıyor.

<strong>Meydan: Korsan Parti Hareketi’nin tüm bu durumlara ilişkin nasıl bir yapılanması var? Parti, seçimler, oluşumun içindekiler…</strong>

<strong>Şevket:</strong> Manifestoda yazdığı gibi, manifestodakileri kabul eden herkes Korsan olabilir. Yataybir hiyerarşi var. Kimse kimsenin üstünde değil. Dün aramıza katılan biri, bugün partiyle ilgili herhangi bir sorumluluğu alabilir. Partileşme süreci ise biraz tartışmalı.

<strong>Harun:</strong> Yapılanmamızı nereye götüreceğimiz noktasında beyin fırtınası yapıyoruz. Önce bir STK ya da dernek olarak yola çıkalım sonra partileşiriz dedik. Bu durumun artısını eksisini düşünmek zorundayız. Kimi arkadaşımız partileşelim de ondan sonrasını düşünürüz diyor. Şahsen korsan denilen bir şeyin parti çatısı altına toplanamayacağını düşünüyorum. Kendimizi devlet tarafından tescillemeye gerek yok. Ama bunu tartışmamız gerektiği kanaatindeyim. Şu anda bunun aciliyetli çözümünü gerektiren bir süreç yok. Uzun vadede partileşme kaçınılmaz gibi gözüküyor. Korsan Parti şimdilik bir hareket.

<strong>Meydan: Assange, Anonymous, Red Hack, Snowden gibi kişi ve oluşumlar özellikle son dönemde, iktidarların bilgi teknolojilerindeki hegemonyasında çatlaklar oluşturarak toplumsal muhalefetin bu alandaki gücü haline geldi. Bu muhalefete nasıl bakıyorsunuz?</strong>

<strong>Harun:</strong> Son derece önemli ve meclisteki muhalefetten bile daha meşru bu durumu görmemenin, olayları değerlendirirken tahlil yapma yetersizliği olduğu düşüncesindeyim. Bunun en büyük avantajlarından biri, merkezi olmayan dağınık haldeki medyaydı. Alternatif sosyal paylaşım siteleri, meclisteki muhalefetin ne kadar gerekli olup olmadığını sorgulamamıza yol açtı. Ve burada muhalefet sadece mevcut hükümete karşı yapılmadı. Kendine muhalefet diyen siyasi mekanizmaların da kendisini sorgulamasına yol açtı. Demokrasinin vazgeçilmez unsurlarından biri olarak Türkiye’de de yerini aldığı düşüncesindeyim.

<strong>Selin:</strong> Ana akım medyanın süreç boyunca tutumundan dolayı, insanların alternatif medyayı, interneti kullanmayı öğrendiğini düşünüyorum. Farklı haber alma kaynaklarını keşfetme fırsatları oldu insanların. Birçok kişinin bu alternatif medya kaynakları üzerinden birbiriyle tanıştığını gördüm. Ana akım medyada çalışan insanlar, yoğunluklu baskı yüzünden gerçekleri yansıtmaları engellendi. Gerçekleri yansıtmakta ısrar edenler, zaten işlerinden kovuldu. Duruma vicdan yapan birçok insan da, katlanamadıklarından işlerini bıraktı.

<strong>Şevket:</strong> Geleneksel ya da ana akım medya zaten yapısı gereği çıkar gruplarıyla, yani şirketlerle son derece içli dışlı. Bunun dışında bir şey beklemek zaten olanaksızdı. Beni en çok mutlu eden, eylemden eve döndüğümüzde neler olduğunu takip edeceğimiz alternatif medya alanlarıydı. Cep telefonu olan herkes artık haberci oldu. Bilgi aktarımı inanılmaz derece merkezsiz bir ağ üzerinden döndü. Son söyleyeceğim şey, aslında bu sansürün yeni bir şey olmadığıydı. Roboski’den beri devam eden bir süreç bu. Roboski’yi de göstermediler. Biz neler olduğunu yabancı kanallardan öğrendik.

<strong>Harun:</strong> Alternatif medya, Taksim Gezi Direnişi’nde öyle bir rol oynadı ki, mevcut iktidar bu duruma muhalefet etmek zorunda kaldı. Mevcut iktidarı muhalefet yaptırabilecek kadar güçlü bir akım oluştu. Hatta yakın bir zamanda, bu tarz bir bilgi akışının mevcut iktidarları yıkacağına tanıklık edeceğiz. Doğrudan demokrasi için vazgeçilmez olan şeyleri alternatif medya bize sundu.

<strong>Meydan: Teşekkür ediyoruz, çalışmalarınızda başarılar.</strong>



