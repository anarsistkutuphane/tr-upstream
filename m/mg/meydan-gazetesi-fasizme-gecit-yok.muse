#title Faşizme Geçit Yok
#author Meydan Gazetesi
#SORTtopics anti faşist, kürdistan, direniş, kürt özgürlük hareketi
#date 04.02.2015
#lang tr
#pubdate 2020-03-09T18:09:30


<quote>
<em>Faşist iktidarlar bizim bulunduğumuz yerden asla geçemeyeceklerdir. Bu, ezilenlerin parolasıdır. Biz onlara şöyle sesleniyoruz: "Geçemeyeceksiniz!"</em>

5 Kasım 1936, Solidaridad Obrera’daki Durruti Konuşmasından bir bölüm
</quote>

4 Kasım 1936’da, faşistler Madrid girişinde bulunan Leganes, Alcorcon ve Getafe’yi almışlardı. Buraya kadar antifaşist milisler geri çekilmişti. Almanya ve İtalya destekli Franco’nun faşist orduları, ilerleyebildiklerini görünce Madrid’i kolayca alabileceklerini düşünmüşlerdi. Faşistler Madrid’i ele geçirmeden, kimleri kurşuna dizeceklerinin listesini bile çıkarmışlardı. Madrid’e kadar çekilen antifaşist milisler durdu. Madrid’de çocuk yaşlı demeden herkes, faşistlerin ilerlemesini durdurmak, Madrid’i savunmak için barikatlar kurmaya başlamışlardı. Direnmek, tek çözüm buydu.

Madrid hükümeti’nin başındaki Caballero, Valencia’ya çekilmeyi tavsiye ettiğinde, CNT’liler yanıt verdi; herkes Madrid’de kalmalı, barikatlarda savaşmalıdır. Hükümet üyelerinin bir kısmı Valencia’ya “özel görev” nedeniyle kaçıyorlardı. Ortada görev falan yoktu aslında.

Her geçen gün baskısını arttıran faşistler, 7’si gecesinde Madrid halkının görkemli direnişiyle karşılaştı. Hoparlörlerden CNT’nin sesi yankılanıyordu; Madrid faşizme mezar olacak. Madrid, korkak bakanlardan ve hükümet yetkilerinden temizlenmiştir. Madrid’i halk savunacaktır. Madrid faşizme mezar olacak. Yaşasın hükümetsiz Madrid, yaşasın toplumsal devrim!

Halk, Madrid’i savunuyordu. Gittikçe yoğunlaşan bombardımana ve silahların sınırlılığna rağmen… Sınırsız olan halkın cesaretiydi. Faşistler şehri yoketmek için uğraşıyordu. Antifaşist milislerin yoğunluktan bitap düştüğü 14 Kasım’da, Vallecas’ta bulunan ilk barikattan bir haber geldi, Durruti Birliği Madrid’deydi. İberya’daki devrim sürecinin bu efsane ismi, cesarete cesaret, güce güç katıyordu. 19 Kasım’da faşistler tarafından katledilene kadar, Madrid’de direnenlerle beraber, Madrid Direnişi’ni faşistlerin aklına mıh gibi kazıdı.

İktidarların kendisini güçlü ve daimi bir şekilde beslediği faşizm, hüküm sürdüğü tüm topraklarda ölümün, katliamın, soykırımın ve tüm bunların karşısında öfkenin adı oldu yıllar boyu. Var olduğu her coğrafyada, milliyetçilikten, etnik-dini-kültürel birlikten yararlanan iktidarların, kendinden olmayan her şeyi-herkesi ötelediği, yok saydığı, kimi zaman katlettiği bir düzenin adı oldu. İnandığından vazgeçmeyen, kimliğini savunup var olma mücadelesi veren, iktidarların karşısında pes etmeyip, söylediğinden vazgeçmeyen herkese bir tehdit oldu faşizm yıllar boyu. Tehdide boyun eğmeyenlerinse dillerindeki tek slogan: “Faşizme geçit yok!”

Henüz 12’sinde bir polisin mermisiyle, 16’sında askerin namlusunun ucunda katledilmek oldu faşizm. Sokakta, “nereden geldiği tespit edilemeyen bir merminin” kafatasına saplanması oldu, gizlenen otopsi raporları, aklanan katiller oldu dikildi karşımıza faşizm. 14 yaşındayken Kürdistan’da sokak ortasında, 8 yaşındayken Qamişlo sınırında savaştan kaçarken dikildi faşizm karşımıza, ölüm oldu.

Patronun daha fazla kar hırsı uğruna ölüme sürüklediği her bir işçi için, ölümün adı oldu faşizm. Yerin metrelerce üstünde bir gökdelenin çatısında, yerin metrelerce altında bir madenin karanlığında, bir filikanın ağırlığında, bir atölyede, fabrikada, bitmek bilmeyen bir sömürü oldu. Güvencesizlik, taşeron, “kaza” adı altında işlenen her bir cinayet giderek normalleştirilirken; faşizm, bu “normalleşme”nin kendisi oldu.

Yalnızca ten rengi farklı olduğu için, bir polis tarafından, sokak ortasında kimi zaman kurşunlanarak, kimi zaman boğazı sıkılarak katledilmek oldu faşizm. Yıllar boyu köleleştirilmenin, işkence görmenin, katledilmenin sesi bugünlere taşındı; faşizm 19. yüzyıldan bugüne soykırımın kendisi oldu. Daha iyi bir yaşam hayaliyle terk edilen topraklardan göç edilen her yeni coğrafyada kölelik, sömürü, zulüm oldu. “Avrupalılaşmaya” çalışan her bir göçmen için, inkar, tehdit, sınır dışı edilmek oldu.

Devletlerin yağdırdığı her bir bombayla, IŞİD gibi her bir çete ile gerçekleştirdiği katliamlarının adı oldu. Ortadoğu coğrafyasında yıllardır bitmeyen zulmün sürdürücüsü özgürlükleri için direnenlere durmaksızın saldırsa da, “faşizme geçit yok” diyenlerin çığlıkları bertaraf etti her bir saldırıyı, soykırımı, katliamı. Bu kez yeni bir yaşam dirildi faşizmin karşısında, umut oldu coğrafyanın dört bir yanına.

Erkek egemenliğinin hüküm sürdüğü her coğrafyada, kadın için ölümün adı oldu faşizm. Kimi zaman maruz kaldığı tacizin, yalnızca kadın olduğu için karşı karşıya bırakıldığı yok sayılmanın, kimliksizleştirmenin, kişiliksizleştirmenin adı oldu. “Erk”eğin iktidarına karşı pes etmeyen her bir kadına yönelik bir tehditte; baskıyla, şiddetle, işkenceyle çıktı kadının karşısına. “Ahlak” tanımına uymayanı, hissettiği gibi var olan herkesi yok saydı, “hasta/çürük” ilan etti. Yaşam alanlarını gasp ettiği her bir eşcinsel için, trans için, “intihar” denilen toplumsal soykırımların, nefret cinayetlerinin, ölümlerin adı oldu. Ezilenin ezilene iktidarı oldu çıktı karşımıza faşizm; şehir ortasında taşlanan-avlanan domuzun “kader”i oldu.

Faşizm, saldırsa da yaşamın dirildiği her yerde, 1936’dan bu yana durmuyor “Geçit Yok” diyenler. Sokaklara çıkıp hesap sorarak, “buradayız” diyerek, direnmeye devam ediyorlar faşizmin karşısında. Yasaklanan her greve inat patronlardan hesap sormayı sürdürüyor, katledilen her bir kardeşinin hesabını sordukça özgürleşiyor…

1936’nın İberyası’dan (İspanya-Portekiz-Fransa’nın güneyi) Kobane’nin özgürleşen topraklarına, İstanbul’un meydanlarına yankılanıyor bugün aynı slogan: “Faşizme Geçit Yok!” Yaşamları çalanlara, bizleri köleleştirenlere, yok sayanlara, katledenlere karşı öfkemiz büyüyor ve yaşam buluyor anarşizme olan inançla çarpan yüreklerimizde.



