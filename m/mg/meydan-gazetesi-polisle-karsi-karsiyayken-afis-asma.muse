#title Polisle karşı karşıyayken: Afiş Asma
#author Meydan Gazetesi
#SORTtopics afiş, polis
#date 06.09.2012
#lang tr
#pubdate 2020-05-04T18:51:17


<em>Kapitalist işleyiş içerisinde zaman zaman kullanılabilecek ama paylaşma ve dayanışmayla örülü özgür dünyada hiçbir şeye yaramayacak bilgiler…</em>


Güncel hayatta karşılaştığımız bir başka hukuksuzluk, hak ihlali biçimi de 5326 sayılı Kabahatler Kanunu’nun polis tarafından kasıtlı bir şekilde keyfi yorumlanması ve uygulanmasıdır. Devletin polisi; devletin, vatandaşının rızasını alabilmek için oluşturduğu sözde suç ve ceza olaylarını çözebilmek için uydurduğu yasaları dahi uygulayamamaktadır.

Belirtmeliyim ki, öğrencilik ve avukatlık yaptığım yıllar süresince suç, ceza ve benzeri olayların gerçekliği olmayan, zorlama yasalarca çözülmeye çalışılmasında hukuk sisteminin saçmalığını deneyimledim. Bu deneyimden çıkarımlarım sonucu hiçbir şekilde ne devletin hukukundan ne de devletin polisinden vb. herhangi bir beklenti içerisinde değilim. Ve herhangi bir beklenti içinde olunmaması gerektiğini düşünüyorum.

Bahsettiklerimizin en yaygın örneklerinden biri, afiş asma eylemi esnasında polis uygulamalarıdır. Afiş asma eylemi gerçekleştirildiği takdirde kanundan da anlaşıldığı gibi, bu eylem karşılığındaki yaptırım idari para cezasıdır. Ancak polisin afiş asan kişilere rastladığı takdirde ilk yaptığı hukuksuzluk, kişiyi karakola götürmektir. Polis burada bunun doğal olduğunu, bu yetkiyi kanundan aldıklarını, hukukun bunu gerektirdiği yönünde bahaneler öne sürmektedir. Ancak hukuken polisin Kabahatler Kanunu’na dayanan bir eylemden dolayı vatandaşı gözaltına alma, “karakolda misafir etme” gibi bir hakkı yoktur. Kişinin idari para cezasını, kanunu ihlal eden fiilin işlendiği yerde, hemen o an kesilmesini istemesi gibi bir hakkı vardır. Kişi karakola gitmek zorunda değildir.

Polislerin karakola gidilmesi yönünde ısrar etmesi halinde, polislere sorulması gereken bunun gözaltı işlemi olup olmadığıdır. Eğer polis gözaltı işlemi yaptığını ileri sürüyor ise, polisten Türk Ceza Kanunu ve Ceza Usul Hukuku’nda belirtilen prosedürü izlemesi, gözaltına almayı Türk Ceza Kanunu’nda belirtilen suçlardan birine göre yapması gerektiği söylenmelidir. Dolayısıyla polisin gözaltına alma işlemini yaptığını söylediği andan itibaren, kişinin avukat isteme hakkı mevcuttur, yani polisin Ceza Muhakemesi Kanunu uyarınca kişiye avukat isteme hakkını sorması, hatırlatması mecburidir. -Ki bunların olmasını beklemenin bana komik geliği gibi size de komik geleceğinin farkındayım, ama yine de bunları bilmek önemlidir.-

Ancak polisin gerçekleştirdiği hukuksuzluk sadece fiilen gözaltına alma, saatlerce karakolda bekletmekten ibaret değildir. İdari para cezasının kesilebilmesi için gerekli olan tutanak da polis tarafından genellikle yanlış bilgilerle doldurulmaktadır. Böyle bir durumda yapmamız gereken, tutanağı dikkatlice okumak ve imzadan imtina ederek tutanağı imzalamamaktır. Afişi asan kişinin tutanağı imzalaması zorunlu değildir, aynı şekilde idari para cezasının kesildiğine dair tebliği de kişinin imzalamama hakkı vardır. Tebliği imzalamamak, ceza kesilen kişiye fazladan zaman kazandırmakta ayrıca her idari para cezasına karşı açmaya hakkımız olan itiraz davaları için de şansımızı arttırmaktadır.

Polisin keyfi işlemlerine karşı ürettiği bahanelerden bir tanesi de afişlerin içerik incelemesinin karakolda yapılacağının söylenmesidir. Bu durum da polis tarafından uydurulmuş yöntemlerden biridir. İçerik incelemesi diye bahsedilenin afişin herhangi bir yasadışı unsur taşıyıp taşımadığının belirlenmesidir. Böyle bir durumda afişin içeriğinin ne olduğu sorusundan kaynaklı sorumluluk polis güçlerindedir. Kişinin bunu kanıtlamak gibi bir sorumluluğu yoktur. Polis afişin yasadışı içeriğe sahip olduğunu düşünüyor ise hemen orada gözaltı işlemine başlamalıdır. Polis böyle durumlarda kağıt üstünde gözaltı yaparak sorumluluk almak istemediğinden sanki afişin içeriğinin ne olduğunun kanıtlanması kişiye aitmiş gibi afiş asan kişiyi fiilen saatlerce gözaltında tutmakta, saatler sonunda içerikte sorun yok diyerek kişiyi serbest bırakmaktadır. Ve karakolda geçirilen bu süre resmen sanki hiç gerçekleşmemiş gibi gözükmektedir.

Polislerin afiş asanlara karşı bu keyfi uygulamaları polislerin bilgisizliğinden veya o an orada bulunan polislerin karakterleriyle alakalı olarak meydana gelmemektedir. Kendisinin bir hukuk devleti olduğunu iddia eden her devlet gibi yaşadığımız topraklara egemen olan devlet de, en demokratik hak olarak beyan ettiği fikir ve düşünce özgürlüğüne kağıt üstünde saldırmaktan zaman zaman çekindiği için, uygulamada bahsettiğimiz hukuksuzluklara başvurmaktadır. Yani afiş asarak gerçekleştirdiğimiz eylemin kağıt üstündeki sonucu idari para cezası olmakta, ancak fiili olarak maruz kaldığımız durum resmi olmayan bir gözaltı süreci, yıpratma ve yıldırma olmaktadır.



