#title Şaşı Bak Şaşır
#author Meydan Gazetesi
#SORTtopics oy, seçim
#date 29.05.2015
#lang tr
#pubdate 2020-02-16T18:13:57



Gözümüzün görme kusurları esas alınarak ustalıkla hazırlanmış öyle görüntüler resmedilir ki, içinde başka başka görüntüler vardır. İlk bakışta içine gizlenmiş görüntüyü görmemiz neredeyse imkansızdır. Resmin içine gizlenmiş ikinci resmi, ancak resmin bütününe odaklanıp bir süre gözümüzü ayırmaksızın baktığımızda görebiliriz. Bazen görülmesi daha da zorlaştırılmış olduğundan, herkes baksa da, pek azımız görebiliriz resmin içindeki ikinci resmi.

Adayları, aday listeleri, vaatler, projeler, seçim kampanyaları, mitingler, pusulalar sandıklar… Bir süredir gündemimizi işgal eden, hemen herkesin konuştuğu resim yalnızca bunlardan ibaret. Herkes kendi durumuna, konumuna göre bu büyük resimdeki çizgisini bulma gayretinde. Resim her geçen gün daha da genişliyor, kalabalıklaşıyor.

Ama tüm bu şaşalı resminden biraz uzaklaşıp, resme daha dikkatlice baktığımızda, gördüklerimiz yavaş yavaş önce bulanıklaşmaya başlayacak. Sonrasında az önce yukarıda bahsettiğimiz gibi, resim içine gizlenmiş ikinci bir resim yavaşça belirmeye başlayacak ve eğer biz resmi görmek istiyorsak biraz yoğunlaşacağız.

5 yılda bir önümüze konulan seçim sandıkları gündemimizi işgal etmeye başladığından itibaren politikacıların yalanları ardı ardına sıralanır. Askeri ücret şu olacak bu olacak yalanları, seçim sonrasında “Bizden önceki hükümet kasayı bom boş bırakmış bu şartlarda askeri ücretlere zam yapamayacağız” sözleriyle değişir. Parti başkanlarının vaatleri, projeleri sınırsızdır. Bu sınırsız projelere inanmamız; şovunda şapkasından tavşan hatta fil çıkaran bir sihirbazın sihrine inanmamızdan daha saçmadır. Şov sürdükçe, sihirbazı izleyen bizler sihirbaza inanırız. Ama şov bitince hepimiz bunların bir sihirbazlık olduğunu da biliriz. Seçimler süresince evde, mahallede, okulda, fabrikada tüm sohbetimizin siyasi partiler ve politikacılarla alakalı olmasını engelleyemeyiz. Karizmatik fotoğraflar, karizmatik reklam filmleriyle gazetelerin renkli kapaklarından, televizyonların bol bol ışıklandırılmış programlarında beden dilinden konuşma diline, tamamıyla yapmacık olan bir ultra retorik ile hipnoz ediliriz. Sohbetlerimize bile sızan “öyle değil mi?” ile biten anket sorularının sürekli değişen verileriyle manipüle oluruz. Manipüle olmakla kalmaz, bir başkamızı da manipüle ederek bir sürüye dönüşür, bu büyük manipülasyonun parçası oluruz. Tüm seçim süreci angaryasında, sürecin en önemli gününe, yani seçim gününe yoğunlaşırız. Atacağımız bir oyla kendimizi bu şaşalı sürecin en önemli karakteri olduk sanarken, attıracağımız bir oyla ise kendimizi adeta partinin başkanı gibi hissederiz. Atılan oylara sahip çıkarak sayarız. Sandıkların özel güvenlik görevlilerine dönüşürüz. Kazanırsak “hile yapmak istediler ama yaptırmadık” deriz. Kaybedersek ise “kesinlikle hile yaptılar” deriz.

İkinci resimde sihirbazların sihirlediği seçim kampanyalarıyla, kampanya medyalarıyla, ultra retorikleriyle etkilediği manipülatif verilerin bilgiye dönüştüğü, kesinlikle hileye dayalı olan bir seçim resmi var. Bu resim birkaç gün sonra gireceğimiz seçimlerin değil, bundan önce ve bundan sonra olacak tüm temsili demokrasi seçimlerinin resmidir.

Şaşı bak şaşır eğlenceli bir duyu oyunudur. Duyu organlarımızın geçici olarak yanılması üzerine kuruludur. Ama kendi yaşamlarımızın iradesini, bizleri sürekli sömüren bir zümreye vermemiz, geçici değil sürekli bir yanılsamadır ve eğlenceli de değildir. Çünkü biz yoksunluk ve yoksulluk içinde yaşarken, seçimler yanılmasını yaratan zümre ise bolluk içinde yaşamaktadır. Bu adaletsiz sistemin bir oyla da olsa parçası olmamalıyız. Yaşamlarımızın bir başkasının iradesine ait olmasına tahamül edemeyiz ve etmeyeceğiz.



