#title 1 Mayıs: İşçilerin Hayatları ve Mücadelelerinde Yeni Bir Çağın Sembolü
#author Nestor Makhno
#SORTtopics 1 mayıs, tarih, işçi mücadelesi, işçi
#date 01.05.1928
#source 16.08.2022 tarihinde şuradan alındı: [[https://www.marxists.org/turkce/mahno/1928/1928b.htm][marxists.org/turkce]]
#lang tr
#pubdate 2022-08-16T16:44:24
#notes Çeviri: Deniz Muratli <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/nestor-makhno-the-struggle-against-the-state-and-other-essays#toc12][The Struggle Against the State and Other Essays: Chapter 12. The First of May: Symbol of a New Era in the Life and Struggle of the Toilers]], <em>Dyelo Truda</em>, No. 36 (1 Mayıs 1928), s. 2-3.

Sosyalist dünyada 1 Mayıs İşçi Bayramı olarak kabul görüyor. Bu, birçok ülkedeki işçilerin yaşamlarına, bu günü bayram gibi kutlamalarına sebep olacak kadar nüfuz etmiş yanlış bir tanım. Aslına bakılırsa 1 Mayıs işçiler için kesinlikle bir bayram değildir. Hayır, işçiler bu tarihte atölyelerinde veya tarlalarında durmamalıdır. Bu tarihte, dünyanın dört bir yanından işçiler, günü devletçi sosyalistlerin ve özellikle de Bolşeviklerin anladığı şekilde damgalamak yerine güçlerini tartabilmek ve yalan ile şiddete dayalı çürümüş, ödlek, köleci düzene karşı doğrudan silahlı mücadele imkânlarını değerlendirebilmek için her köyde, her şehirde bir araya gelip kitlesel toplantılar düzenlemek için kullanmalıdır. İşçilerin, zaten takvimin bir parçası olan bu tarihî günde bir araya gelmesi ve toplu iradelerini ifade etmenin yanında hem bugüne hem de geleceğe ilişkin tüm temel meseleleri tartışmaları en mantıklısı olacaktır.

Kırk yıldan fazla bir süre önce, Chicago ve çevresindeki Amerikalı işçiler, 1 Mayıs’ta toplandı. Orada birçok sosyalist konuşmacıyı; hatta üstünkörü edindikleri özgürlükçü fikirlere ve açıkça anarşistlerle taraf olmalarına bakarsak özellikle de anarşist konuşmacıları dinlediler.

O gün Amerikalı işçiler mülk sahiplerinin devlet ve sermayesinin adaletsiz düzenini protesto etmek için örgütlendi. Amerikalı özgürlükçüler olan Spies, Parsons ve diğerlerinin bahsettiği şey buydu. İşte tam bu noktada protesto mitingi sermayenin paralı askerleri tarafından sekteye uğratıldı ve silahsız işçilerin katliamı ve devamında Spies, Parsons ve diğer yoldaşların önce tutuklanması sonra da cinayetiyle sonuçlandı.

Chicago ve çevresinin işçileri 1 Mayıs Bayramı’nı kutlamak için toplanmamışlardı. Yaşam ve mücadele sorunlarını ortaklaşa çözebilmek için toplanmışlardı.

İşçilerin kendilerini burjuvazinin ve sosyal demokrasinin himayesinden azat ettikleri, hatta azat etmeye çabaladıkları her yerde (Menşevik ya da Bolşevik, fark etmez) 1 Mayıs hâlen kurtuluş mücadelesiyle ilgilenmek için bir fırsat olarak görülüyor. Chicago şehitleriyle dayanışma ve onları anma etkinlikleri bu amaçla gerçekleşiyor. İşçiler, 1 Mayıs’ın onlar için bir bayram olmadığının farkındalar. Yani, “profesyonel sosyalistlerin” iddialarının aksine, bu günün İşçi Bayramı olması bilinçli işçiler için söz konusu bile değildir.

Mayısın ilk günü, işçilerin yaşamları ve mücadeleleri için yeni bir çağın sembolü—ellerinden alınmış olan özgürlük ve bağımsızlığı kazanmak, toplumsal hayallerine ulaşmak isteyen işçilerin burjuvaziye karşı yeni, gittikçe daha zorlu ve nihai savaşlar verdiği bir çağ.
