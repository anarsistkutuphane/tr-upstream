#title 6 Şubat 2023 Depremleri Ön İnceleme Raporu
#author TMMOB Maden Mühendisleri Odası
#date 11.02.2023
#source 12.02.2023 tarihinde şuradan alındı: [[https://www.maden.org.tr/genel/bizden_detay.php?kod=13675&tipi=&sube=0][maden.org.tr]]
#lang tr
#pubdate 2023-02-12T19:31:11
#topics afet



TMMOB Maden Mühendisleri Odası olarak 9 Şubat 2023 Perşembe günü Oda Yönetim Kurulu’ndan oluşan beş kişilik bir heyetle madencilik kurum ve kuruluşları tarafından oluşturulan ve maden mühendisleri ile maden işçilerinden oluşan kurtarma ekiplerinin ihtiyaçlarını karşılamak, onlara moral destek sağlamak ve kurtarma faaliyetlerin yerinde incelmek amacıyla deprem bölgesine gidilerek çalışmalar yürütülmüştür.

Adana, İskenderun, Antakya, Kırıkhan, Nurdağı ve Gaziantep il ve ilçelerinde yapılan inceleme, gözlem ve görüşmelerde edinilen ilk bulgular şöyledir:

 1. En temel sorun depremin büyüklüğüne karşı Hükümetin dolayısıyla AFAD’ın geç harekete geçmesi olmuştur. Bu nedenle arama kurtarma çalışmalarında son derece önemli olan ilk 1,5 gün kaybedilmiş, deprem bölgelerine, özellikle ilçelerde ikinci gün öğleden itibaren kurtarma çalışmaları başlatılmıştır.

 1. Her türlü afet çalışmasında tek kamusal yetkili olan AFAD bir yandan gerek kapasite gerek zamanında ve etkin kararlar alma konusunda son derece yetersiz kalmış, diğer yandan sivil toplumun çalışmalara katılımını engelleyerek vahametin artmasına yol açmıştır. AFAD’ın bu başarısızlığına rağmen çeşitli organizasyonlar ve sivil toplum hem arama kurtarma çalışmalarında hem de gerekli yardımları temin etme konusunda önemli katkıları sağlamıştır.

 1. Kurtarma çalışmalarında kullanılacak makine ve ekipman eksikliği, kurtarma çalışmalarının son derece yavaş yürütülmesine neden olmuştur. Depremin ilk dört gününde koordinasyon sorunu hala çözülememiş olduğu gibi barınma ve iaşe sorunları da büyük ölçüde devam ettiği görülmüştür.

 1. İaşe sorunlarının 3. Günden itibaren azalmaya başladığı, ilk 4 gün çadır sorunun çözülememiş olduğu, özellikle yıkımın büyük olduğu Antakya’da çadır ve tuvalet sorunlarının oldukça acil ve öncelikli sorun olduğu, bu durumun salgın hastalık riski taşıdığı,

 1. Arama kurtarma çalışmalarının 5. Günden itibaren, canlı bulgusu olan yerler dışında faaliyetin enkaz kaldırma faaliyetine dönüştüğü görülmüştür.

 1. <strong><em>Arama kurtarma faaliyetleri yürüten madencilerin günde 20 saate varan, zaman zaman 50 saat aralıksız çalışarak büyük bir özveri gösterdikleri, ancak barınma ve ulaşım sorunları yaşadığı, kendi araçları bulunmayan ekiplerin ne yazık ki otostop yapmak zorunda oldukları, bu sorunların arama kurtarma çalışmalarına oldukça olumsuz yansıdığı,</em></strong>

 1. Tüm özverili çalışmaya rağmen makine ve ekipman yetersizliği nedeniyle çalışmalarda istenen sonucun elde edilemediğini,

 1. Özellikle Antakya ve Kırıkhan’da mobil iletişimin sorunun 5. Günde giderilememiş olmasının kurtarma ve yardım çalışmalarını ciddi olarak etkilediği, geçen bunca zamana karşın seyyar mobil yapı güçlendirilememiş, iletişim altyapısı geliştirilememiş olmasını sorgulanması gerektiği,

 1. AFAD ve halk nezdinde madencilerin çok başarılı kurtarma çalışmalar yürüttükleri, bulundukları yerlerde diğer arama-kurtarma ekiplerinin geri plana çektirilerek kurtarma faaliyetlerinin madenciler devredildiği, ancak kurtarma çalışmasında canlı çıkartıldığı sırada AFAD ekiplerinin devreye girerek görüntü alıp sosyal medyada paylaştıkları

 1. <strong><em>Kurtarma çalışmaları sırasında AFAD yetkililerinin yanlış tutum ve davranışları sonuç almada eksiklikler yaşanmasına neden olmuştur. AFAD’ın kontrolsüz kayıtsız faaliyetleri bazı yerlerde aynı enkazlarda çifte çalışmaya neden olmuştur.</em></strong>

 1. Maden kurtarma ekipleri çalışmalarını tamamladıklarında ihtiyaç olan başka il ve ilçelere gitmek istediklerinde “talimat gelmeden olmaz” gerekçesiyle bekletilmekte olup bu durum kurtarma çalışmalarını geciktirmekte ve engellemektedir.

 1. <strong><em>Yine AFAD, enkazın en üst tarafından delik delerek kişiye ulaşma yöntemi hem daha zor gerçekleşmekte hem de can kaybına neden olmaktadır. Oysa madenciler enkaz içerisinde madencilik yöntemleriyle tahkimatlarla alt katlardan galeri açarak ilerlemekte, hedefe daha hızlı ve zarar vermeden ulaşabilmektedirler. Ancak bu farklı müdahale tarzı nedeniyle AFAD, bazı yerlerde madencilerin arama kurtarma yöntemlerine müdahale etmiş, sonuçta daha fazla yurttaşın kurtarılması mümkün olmamıştır.</em></strong>

İncelemelerimiz Kahramanmaraş, Elbistan, Adıyaman, Doğanşehir ve Malatya ile devam edecektir. <strong><em>Arama-kurtarma çalışmalarında katılan madencilik kurum ve kuruluşlarına, bu çalışmalarda görev alan ve büyük bir özveriyle çalışan maden mühendisleri ve maden işçilerinden oluşan tahlisiye ekiplerine Odamız ve halkımız adına sonsuz teşekkürlerimizi sunuyoruz.</em></strong>

Üyelerimizin ve kamuoyunun bilgisine sunarız.

<right>
TMMOB Maden Mühendisleri Odası
<br>
Yönetim Kurulu
<br>
11 Şubat 2023, Hatay
</right>

