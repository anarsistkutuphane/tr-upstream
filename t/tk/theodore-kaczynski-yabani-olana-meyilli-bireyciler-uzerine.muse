#title Yabani Olana Meyilli Bireyciler Üzerine
#author Theodore Kaczynski
#SORTtopics vahşi doğaya yönelen bireyciler, tekno-endüstriyel karşıtı, eleştiri, ilkelcilik, bireycilik, eko-ekstremizm
#date 2017
#source 10.06.2022 tarihinde şuradan alındı: [[https://gayrnesriyat.substack.com/p/yabani-olana-meyilli-bireyciler-uzerine][gayrnesriyat.substack.com]]
#lang tr
#pubdate 2022-06-10T17:06:15
#notes Çeviri: Gayr Neşriyatı <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/ted-kaczynski-ted-kaczynski-on-individualists-tending-toward][Ted Kaczynski on Individualists Tending Toward Savagery (ITS)]]



[[https://en.wikipedia.org/wiki/Individualists_Tending_to_the_Wild][Yabani Olana Meyilli Bireyciler (ITS)]][1] konusunu ele alırken, tekno-endüstriyel sistemin bertaraf edilmesine adanacak bir hareket yaratılması gerektiğine inanmadıklarını ve niyetlerinin teknolojik ilerlemeyi durdurmak olsa da sistemin elimine edilmesine yönelik herhangi bir niyet ya da umut taşımadıklarını belirttiniz.

Gerek bundan, gerekse [[https://web.archive.org/web/20190406185918/https://ultimoreductosalvaje.blogspot.com/][UR'nin]] bana gönderdiği tebliğlerin kimi bölümlerinden, ITS’nin konu siyaset olunca ne kadar cahil olduğu anlaşılıyor. Tüm teknolojik sistemi elimine etmeden teknolojik ilerlemeyi durdurmak, hatta yavaşlatmak bile kesinlikle mümkün değildir. Ayrıca tebliğlerin bahsettiğim kısımları ITS’nin devrim anlayışının anaokulu seviyesinde olduğunu gösteriyor. Devrimin bir halk ayaklanmasından ("halk ayaklanması", "bir insan seli... şiddet içeren bir şekilde [hareket etmesi]") ibaret olduğuna inanıyorlar. Devrimler kimi zaman bu şekilde gerçekleşse de çoğu durumda bir elin parmaklarını geçmeyecek sayıda siyasi lider tarafından tepeden kumanda edilen politik süreçlerdir. Alışılageldiği gibi, halk ayaklanmaları politik süreçte yalnızca birer vuku yahut epizottan ibarettir ve bu ayaklanmalara nüfusun yalnızca küçük bir bölümü katılım gösterir. Örneğin, Rusya'daki Şubat "Devrimi" ( aslında bir devrim değil, sadece bir ayaklanmaydı) Rus nüfusunun çok küçük bir yüzdesine tekabül eden Petersburg'daki sanayi işçileri tarafından yürütülmüştü. İşçilerin eylemi sayesinde Bolşevikler birkaç ay sonra tüm ülkenin kontrolünü ele geçirme konusunda istifade edebilecekleri bir çıkış noktası bulmuş oldular - esasen siyasi maharetleriyle.

Herhangi bir halk ayaklanması olmadan da devrimler gerçekleşebilir. Örneğin Naziler Almanya'da iktidarı tamamen politik yöntemlerle devralmıştır. On yıl önceki Münih Birahane darbesi dışında (ki bu feci bir başarısızlık olup Hitler'in daha sonra gücü salt legal yollarla sürdürmesine vesile olmuştur) Naziler hiçbir zaman bir ayaklanma girişiminde bulunmamışlardı. İktidarı ele geçirmeden önce Naziler bazı şiddet eylemlerine - sözgelimi komünistlerle sokak kavgalarına - karışmışlardı ve iktidarı ele geçirdikten sonra Hitler'in Nazi partisi içindeki muhaliflerini fiziki olarak ortadan kaldırdığı "uzun bıçaklar gecesi" yaşandı. Ancak Nazilerin yukarıda bahsettiğimiz başarısız darbe girişiminden sonra Naziler bir daha asla yerleşik otoritelere karşı şiddet kullanmadı.

Nazi devriminin bir ölçüde uygarlığa karşı bir devrim olduğu da akıldan çıkarılmamalıdır. Fakat Hitler yalnızca kişisel güç ve kendini yüceltmekle ilgilendiğinden medeniyet karşısında hiçbir sonuç elde edememiştir. O ve yandaşları, Alman toplumunda var olan potansiyel devrimci güçleri (diğerlerinin yanı sıra anti-medeniyet akımını da içeren) kendilerine mâledip güç kazanmak için kullandılar.

ITS, naif bir devrim kavramı sergilemenin yanı sıra, siyasi cehaletini başka şekillerde de göstermektedir. Eğer bu insanlar tarih hakkında herhangi bir şey okumuşlarsa bile bunu anlamamışlardır. Sonuç olarak, ister yasal ister yasadışı olsun, gerçekleştirecekleri her türlü eylemin ters etki yaratması muhtemeldir. Devrimci eylemler, yasal olsun ya da olmasın, siyasi hedeflere hizmet edecek şekilde akıllıca çizilmeli ve bunlara eşlik eden bildiriler de siyasi açıdan akıllıca yazılmalıdır. Bu da toplumların gelişim ve değişim yolları hakkında mümkün olduğunca çok bilgi edinme zahmetine katlanmış insanların liderliğini gerektirir.

ITS’nin düştüğü en kritik hata, teknolojik sistemin elimine edilebileceğine umutsuz bir bakış açısı geliştirmeleri ve dolayısıyla bu umutsuzluğu da teşvik etmeleridir. Birçok insan tarafından çılgın, aptal ya da "romantik" olarak görülen ufak ve görünüşte etkisiz grupların her şeye rağmen başarıya ulaşmış devrimler gerçekleştirdikleri tarihsel örnekleri anlatacak vaktim yok. Fakat böyle bir girişimin zaferi için olmazsa olmaz bir unsur, başarının mümkün olduğuna duyulan inançtır. ITS, konu teknolojik sistemle mücadele olduğunda başarı ihtimaline duyulan güveni baltalamaya çalıştığından, bu insanları dışlamalı ve onları siyasi hasımlarımızın bulunduğu listeye eklemeliyiz.

UR'nin bana gönderdiği tebliğlerin bazı bölümlerinde, ITS’nin dayandığı verilerden bazılarının hatalı olduğunu ve ITS’nin bana hiçbir zaman ifade etmediğim beyanlar ve savunmadığım görüşler atfettiğini eklemekle kalacağım.

[1] (Anarşist Kütüphane) ITS'nin bildirilerine ulaşmak için: [[https://anarcho-copy.org/libre/vahsi-dogaya-yonelen-bireyciler-bildiriler-its.pdf]]





