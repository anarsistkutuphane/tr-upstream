#title Yaralanacağı Yerden Vur
#author Theodore Kaczynski
#SORTtopics ekoloji, teknoloji, tekno-endüstriyel karşıtı
#date 2002
#source İngilizce Aslı: [[https://theanarchistlibrary.org/library/ted-kaczynski-hit-where-it-hurts][Hit where it hurts]],  *Green Anarchy*  #8, Spring 2002
#notes Çevirenin Notu: Kaczynski’nin bu makalesi Green Anarchy dergisinin Bahar 2002 tarihli 8. sayısında yayınlanmıştır. Özgür Hayat’ın 15 Eylül 2002 tarihli 10. sayısında yayımlanmıştır.
#lang tr
#pubdate 2020-06-25T18:18:18


*** 1. Bu Makalenin Amacı

Bu makalenin amacı, insan çatışmasının çok basit bir ilkesine, tekno-endüstriyel sistemin düşmanlarının dikkate almaz göründükleri bir ilkeye dikkat çekmektir. Söz konusu ilke, herhangi bir çatışma biçiminde, eğer kazanmak istiyorsanız, düşmanınızın yaralanacağı yerine vurmanız gerektiğidir.

“Yaralanacağı yerden vurmak”tan bahsettiğimde illaki fiziksel darbelere ya da fiziksel şiddetin başka herhangi bir biçimine gönderme yapıyor olmadığımı açıklamak zorundayım. Örneğin sözlü tartışmada “yaralanacağı yerden vurmak”, iddialarınızı rakibinizin pozisyonunun en zayıf olduğu noktaya yöneltmeniz anlamına gelir. Başkanlık seçiminde, “yaralanacağı yerden vurmak”, seçimle ilgili hayati olayları barındıran durumları rakibinizden kazanmanız anlamına gelir. Ben yine de, bu tartışmayı yürütürken fiziksel çarpışmayla benzerlikler kuracağım, çünkü bu daha etkili ve açık bir yol.

Birisi size yumruk attığında, kendiniz onun yumruğuna vurarak savunamazsınız, çünkü onu bu yolla yaralayamazsınız. Kavgayı kazanmak için yaralanacağı yerden vurmanız gerekir. Bu da demektir ki, yumruğun ardına geçmeli ve o kişinin bedeninin duyarlı ve zayıf yerlerine vurmalısınız.

Bir kereste şirketine ait bir buldozerin evinizin yakınındaki ormanı yıktığını ve sizin de onu durdurmak istediğinizi farz edin. Toprağı yaran ve ağaçları alaşağı eden, o buldozerin kepçesidir ama kepçeye bir balyoz indirmek zaman kaybı olacaktır. Eğer balyozla kepçe üzerinde uzun ve zorlu bir çalışma yürütürseniz, ona kullanılmaz hale gelecek zararı vermeyi başarabilirsiniz [1]. Fakat buldozerin geri kalanıyla kıyaslandığında, kepçe görece ucuz ve kolay yenilenebilirdir. Kepçe, yalnızca, buldozerin toprağa vurmak için kullandığı “yumruk”tur. Makineyi yenmek için “yumruğun” ardına geçmeniz ve buldozerin hayati parçalarına saldırmanız gerekir. Örneğin motor, radikallerin iyi bildiği araçlarla, az bir zaman ve çaba harcayarak tahrip edilebilir.

Bu noktada, kimseye bir buldozeri tahrip etmesini tavsiye etmediğimi vurgulamalıyım (kendi malı olmadığı sürece). Ya da bu makaledeki herhangi bir nokta, herhangi türden illegal bir faaliyeti tavsiye ediyor gibi de yorumlanmamalıdır. Ben bir mahkumum ve eğer illegal faaliyeti özendirecek olsaydım, bu makalenin hapishanenin dışına çıkmasına izin bile verilmezdi. Buldozer benzeşimini kullanıyorum, çünkü bu açık ve etkili; ve aynı zamanda radikaller tarafından takdir edilecektir.

*** 2. Teknoloji Hedeftir

“Çağdaş tarihsel süreci belirleyen temel değişken(in) teknolojik gelişme tarafından sağlandı”ğı (Celso Furtado) genel olarak kabul edilir. Teknoloji, dünyanın mevcut durumundan – geri kalan her şeyden daha fazla – sorumludur ve dünyanın gelecekteki gelişimini kontrol edecektir. Bu yüzden ortadan kaldırmamız gereken “buldozer”, modern teknolojinin ta kendisidir. Çoğu radikal bunun farkındadır ve bu nedenle görevin tekno-endüstriyel sistemin tümünü yok etmek olduğunu anlamaktadır. Ama malesef, sistemi yaralandığı yerden vurma gerekliliğine çok az dikkat edilmiştir.

McDonald’s ya da Sturbuck’s ı tarumar etmek anlamsızdır. McDonald’s ya da Sturbuck’s i iplediğim için söylemiyorum. Birisinin bunları dağıtıp dağıtmaması umurumda değil. Fakat bu, devrimci faaliyet değildir. Dünyadaki tüm fast-food zinciri yok edilse bile tekno-endüstriyel sistem sonuçta asgari düzeyde bir zarara uğrayacaktır, çünkü sistem, fast-food zinciri olmadan da rahatlıkla yaşamaya devam edebilir. McDonald’s ya da Sturbuck’s a saldırdığınızda, yaralanacağı yerden vuruyor olmuyorsunuz.

Bundan birkaç ay önce Danimarkalı genç bir adamdan mektup aldım. Genç adam tekno-endüstriyel sistemin yok edilmesi gerektiğine inanıyordu, çünkü – söylediği gibi – “Böyle devam edersek ne olacak?”. Bununla birlikte, göründüğü kadarıyla onun “devrimci” faaliyeti kürk çiftliklerine baskın yapmaktı. Bu faaliyet, tekno-endüstriyel sistemi zayıflatma aracı olarak, tamamen kullanışsızdır. Hayvan özgürlükçüleri kürk endüstrisini tamamen yok etmekte başarılı olsalar bile, sisteme hiç zarar vermiş olmayacaklar çünkü sistem kürkler olmadan da mükemmel bir şekilde işleyebilir.

Vahşi hayvanları kafeslere tıkmanın dayanılmaz olduğuna ve bu uygulamaya son vermenin soylu bir amaç olduğuna katılıyorum. Fakat başka soylu amaçlar da vardır: trafik kazalarının önüne geçmek, evsizler için barınak sağlamak ya da yaşlıların yolda karşıdan karşıya geçmesini sağlamak gibi. Yine de yeterince aptal olmayan kimse bunu devrimci faaliyetle karıştırmaz ya da bunların sistemi zayıf düşürecek şeyler olduğunu sanmaz.

*** 3. Kereste Endüstrisi İkincil Bir Sorundur.

Başka bir örneği ele alırsak, aklı başında olan kimse, gerçek vahşi hayat gibi bir şeyin tekno-endüstriyel sistem var olmaya devam ettiği sürece hayatta kalabileceğine inanmaz. Çoğu çevreci radikal, bunun sistemin çöküşü için neden ve umut olduğu konusunda hem fikir. Fakat pratikte, tüm yaptıkları kereste endüstrisine saldırmak.

Kereste endüstrisine saldırmalarına kesinlikle hiçbir itirazım yok. Aslında bu, kalben yakın hissettiğim bir sorundur ve radikallerin kereste endüstrisine karşı kazandıkları başarılardan keyif alıyorum. Buna ek olarak, burada açıklama ihtiyacı hissettiğim nedenlerle, kereste endüstrisine karşı çıkmanın sistemi yıkma çabalarının bir öğesi olması gerektiğini düşünüyorum.

Fakat kereste endüstrisine saldırmak sisteme karşı çalışmak için kendi başına etkili bir yol değildir; hatta pek mümkün olmasa da, radikaller dünyanın her tarafındaki keresteciliği durdurmayı başarsalar dahi, bu, sistemi alaşağı etmeyecektir. Ve vahşi hayatı kalıcı bir şekilde kurtaramayacaktır. Politik iklim er ya da geç değişecek ve kerestecilik yeniden başlayacaktır. Kerestecilik dirilmese bile, vahşi hayatın yok edileceği ya da yok edilmese de uysallaştırılacağı ve evcilleştirileceği başka olaylar olacaktır. Madencilik ve maden araştırmaları, asit yağmuru, iklim değişiklikleri ve türlerin nesillerinin tükenişi vahşi hayatı tahrip ediyor; vahşi hayat – diğer şeylerin yanı sıra, hayvanların elektronik takibi, nehirlerin planlı üretilen balıklarla doldurulması ve genetik müdaheleye uğramış ağaçların dikilmesini kapsayan yollarla – yeniden yaratım, bilimsel çalışma ve kaynak yönetimi aracılığıyla uysallaştırılıyor ve evcilleştiriliyor.

Vahşi hayat yalnızca tekno-endüstriyel sistemin yok edilmesiyle kalıcı bir şekilde korulanabilir ve sistemi kereste endüstrisine saldırarak yok edemezsiniz. Sistem kereste endüstrisi ölse de kolaylıkla ayakta kalabilir, çünkü odun ürünleri sistem için faydalı olsa da, gerekirse başka malzemelerle değiştirilebilir.

Sonuç olarak, kereste endüstrisine saldırdığınızda sisteme yaralanacağı yerden vuruyor olmuyorsunuz. Kereste endüstrisi yalnızca, sistemin vahşi hayatı tahrip ettiği “yumruk”tur (ya da yumruklardan biridir) ve aynı yumruk yumruğa kavgada olduğu gibi, yumruğa vurarak kazanmazsınız. Yumruğun ardına geçmeli ve sistemin en duyarlı ve hayati organına vurmalısınız. Tabii, barışçıl protestolar gibi yasal araçlarla.

*** 4. Sistemin Dayanıklı Olmasının Nedeni

Tekno-endüstriyel sistem, sözde “demokratik” yapısı ve sonuçtaki esnekliğine bağlı olarak, olağanüstü derecede dayanıklıdır. Diktatoryal sistemler katı olmaya eğilimlidirler; sisteme isabetli bir şekilde zarar verecek onu zayıflatacak toplumsal gerilimler ve direniş inşa edilebilir ve bunlar devrime yol açabilir. Fakat “demokratik” sistemde, toplumsal gerilim ve direniş tehlikeli bir şekilde inşa edildiğinde, sistem bu gerilimleri güvenli bir seviyeye çekmek için yeterince esner ve yeterince uzlaşır.

1960’larda insanlar, daha çok büyük şehirlerimizin havasındaki görünür ve koklanabilir pislik kendilerini fiziksel olarak rahatsız etmeye başladığı için, çevre kirliliğinin ciddi bir sorun olduğunu fark ettiler. Çevre Koruma Ajansı’nın kurulmasına ve sorunun azaltılması için önlemler alınmasına yetecek kadar protsto baş gösterdi. Tabii ki, kirlilik sorunlarımızın çözülmekten çok çok uzak olduğunu hepimiz biliyoruz. Ancak halkın şikayetlerinin durulmasını ve sistem üstündeki baskının birkaç yıllığına azalmasını sağlayacak müdahaleler yapıldı.

Bu nedenle, sisteme saldırmak bir lastik parçasına vurmaya benziyor. Çekiçle bir darbe bir döküm demiri tuzla buz edilebilir çünkü döküm demir katı ve kırılgandır. Fakat lastik parçasını ona hiç zarar vermeden yumruklayabilirsiniz çünkü esnektir: Protestonun önünde size yol verir, protestonun gücünü ve önemini yitirmesine yetecek kadar… Sonra sistem geri seker.

Bu yüzden, sisteme yaralanacağı yerden vurmak için, sistemin geri tepmeyeceği, sonuna kadar savaşacağı konular seçmek zorundasınız. Çünkü ihtiyacınız olan, sistemle uzlaşmak değil, ölüm kalım mücadelesidir.

*** 5. Sisteme Kendi Değerlerine Göre Saldırmak Yararsızdır

Sisteme onun teknolojik-temlli değerlerine göre değil, sistemin değerleriyle uyuşmaz olan değerlere göre saldırmak kesinlikle esastır. Sisteme onun değerlerine göre saldırdığınız sürece, sistemi yaralanacağı yerden vurmuş olmazsınız, sistemin yol vererek ve geri teperek protestonun altını boşaltmasına izin vermiş olursunuz.

Örneğin, eğer kereste endüstrisine öncelikle ormanlara su kaynaklarını ve yeniden yaratım fırsatlarını korumak için gerek duyulduğu temelinde saldırırsanız, sistem kendi değerlerinden taviz vermeden protestonun içini boşaltmak için zemin sağlar: Su kaynakları ve yeniden yaratım sistemin değerleriyle tamamen uyumludur ve eğer sistem esnerse, su kaynakları ve yeniden yaratım adına keresteciliği kısıtlarsa, o halde yalnızca kendi değerler kodu için taktiksel olarak geri çekilmiş olur, yoksa stratejik bir bozguna uğramış olmaz.

Mağdurlaştırma konularını (ırkçılık, cinsiyetçilik, homofobi ya da yoksulluk gibi) öne sürerseniz, sistemin değerleriyle çatışıyor ve hatta sistemi esnemesi ve uzlaşması için bile zorluyor olmazsınız. Doğrudan sisteme yardım ediyor olursunuz. Sistemin en bilgeli destekleyicileri, ırkçılık, cinsiyetçilik, homofobi ve yoksulluğun sisteme zararlı olduğunun farkındadır ve bu nedenle sistemin kendisi mağdurlaştırmanın bu ve benzeri biçimleriyle savaşmaya uğraşır.

“Sweatshop”lar [2], düşük ücret ve berbat çalışma koşullarıyla bazı şirketlere kar getiriyor olabilir, ama sistemin bilge destekleyicileri işçilere daha uygun davranıldığında bir bütün olarak sistemin daha iyi işlediğini çok iyi biliyorlar. Sweatshop’ları konu edindiğinizde sistemi zayıflatmıyor, onu güçlendiriyorsunuz.

Çoğu radikal, ırkçılık, cinsiyetçilik ve sweatshop’lar gibi gereksiz konular üzerine odaklanmanın cazibesine kapılıyorlar, çünkü bu daha kolay. Sistemin taviz verebileceği ve Ralph Nader, Winona La Duke, işçi sendikaları ve diğer tüm pembe reformculardan destek görebilecekleri bir konu seçiyorlar. Belki sistem baskı altında biraz geri adım atacak, eylemciler çabalarının belli bazı sonuçlarını görecekler ve bir şeyler başardıkları gibi tatminkar bir illüzyon elde edecekler. Fakat gerçekte, tekno-endüstriyel sistemi yok etmek yolunda hiç ama hiçbir şey başarmış olmayacaklar.

Küreselleşme konusu teknoloji sorunuyla tamamen ilgisiz değildir. “Küreselleşme” adı verilen ekonomik ve politik tedbirler paketi, ekonomik büyümeyi ve sonuçta teknolojik ilerlemeyi teşvik ediyor. Yine de, küreselleşme önemi az olan bir konudur ve devrimciler için iyi seçilmiş bir hedef değildir. Sistem küreselleşme konusunda zarar görmeden zemin sağlayabilir. Küreselleşmeyi aslında sona erdirmeden, protestoların içini boşaltmak amacıyla küreselleşmenin olumsuz çevresel ve ekonomik sonuçlarını hafifletebilir. Hatta sıkıştığında, küreselleşmeye toptan son vermek için uğraşabilir bile. Büyüme ve ilerleme yine de sürecektir, sadece biraz daha düşük bir düzeyde. Ve küreselleşmeyle savaştığınızda sistemin asıl değerlerin saldırmış olmuyorsunuz. Küreselleşme karşıtlığı, işçiler için uygun ücretleri muhafaza etmek ve çevreyi korumaktan hareket ediyor ve bunların her ikisi de sistemin değerleriyle tam anlamıyla uyumludur. (Sistem, kendi çıkarı için, çevresel bozulmanın çok ileri gitmesine izin veremez.) sonuçta, küreselleşmeyle savaştığınızda sistemi gerçekten yaralandığı yerden vurmuş olmuyorsunuz. Çabalarınız bir reformu teşvik edebilir, fakat tekno-endüstriyel sistemin alaşağı edilmesi amacı için faydasızdır.

*** 6. Radikaller Sisteme Kesin Sonuç Getiren Noktalardan Saldırmalıdır.

Tekno-endüstriyel sistemin imhası doğrultusunda etkili çalışmak için devrimciler, sisteme kendisinin zemin sağlamaktan çekineceği noktalarda saldırmalıdır. Sistemin hayati organlarına saldırmalılar. Tabii, “saldırı” sözcüğünü kullandığımda fiziksel saldırıya gönderme yapmıyorum, yalnızca yasal protesto ve direniş biçimlerinden söz ediyorum.

Sistemin hayati organlarına örnekler şunlardır:

A. Elektrik gücü endüstrisi. Sistem tamamen elektrik gücü şebekesine bağımlıdır.

B. İletişim endüstrisi. Telefon, radyo, televizyon, e-posta ve benzerleri aracılığıyla hızlı iletişim olmadan sistem hayatta kalamaz.

C. Bilgisayar endüstrisi. Hepimiz biliyoruz ki, bilgisayarlar olmadan sistem çabukça çöker.

D. Propaganda endüstrisi. Propaganda endüstrisi, eğlence endüstrisini, eğitim sistemini, gazeteciliği, reklamcılığı, halkla ilişkileri ve aşağı yukarı tüm politika ve akıl sağlığı endüstrisini kapsar. Sistem, insanlar yeterince yumuşak başlı ve uyumlu olmadıkları ve sistemin onlarda ihtiyaç duyduğu alışkanlıklara sahip olmadıkları sürece işleyemez. İnsanlara bu tür düşünme ve davranışları öğretmek propaganda endüstrisinin işlevidir.

E. Biyoteknoloji endüstrisi. Sistem (bildiği kadarıyla) ileri biyoteknolojiye henüz fiziksel olarak bağımlı değil. Bununla birlikte, kendisi için kritik bir öneme sahip olan Biyoteknoloji konusu üzerinden karşıtlığa yol vermeye katlanamaz. Bunu birazdan tartışacağım.

Tekrar: Sistemin bu hayati organlarına saldırdığınızda, bunlara sistemin değerlerine göre değil, sistemin değerleriyle uyuşmaz olan değerlere göre saldırmanız zorunludur. Örneğin, elektrik gücü endüstrisine çevreyi kirlettiğinden hareketle saldırırsanız, sistem elektrik üretmenin daha temiz yöntemlerini geliştirerek protestonun içini boşaltabilir.

Hatta daha da beteri, sistem tamamen rüzgar ve güneş enerjisine geçebilir. Bu durum çevresel tahribatı azaltmak için çok işe yarayabilir ama tekno-endüstriyel sisteme son vermez. Ya da sistemin esas değerleri için bir bozgunu temsil etmez. Sisteme karşı herhangi bir şey başarmak için, bir ilke olarak, elektriğe bağımlılığın insanları sisteme bağımlı kılması zemininde, elektrik gücü üretiminin tümüme saldırmalısınız. Bu, sistemin değerleriyle bağdaşmaz bir zemindir.

*** 7. Biyoteknoloji Politik Saldırı İçin En İyi Hedef Olabilir.

Politik saldırı için bekli de en umut verici hedef Biyoteknoloji endüstrisidir. Her ne kadar devrimciler azınlıklar tarafından tatbik edilse de, genel nüfustan bir derecede destek, sempati ya da en azından rıza gelmesi yararlı olur. Bu tür bir destek ya da rıza kazanmak politik eylemin hedeflerinden biridir. Politik saldırınızı, örneğin, elektrik gücü endüstrisine yoğunlaştıracak olursanız, radikal bir azınlık dışındakilerden destek almak aşırı derecede zor olacaktır, çünkü çoğu insan yaşama tarzlarındaki değişikliklere direnir, özellikle de onları rahatsız eden değişikliklere. Bu nedenle, çok azı elektrik kullanımına son vermeyi isteyecektir.

Fakat insanlar henüz kendilerini ileri biyoteknolojiye elektriğe olduğu kadar bağımlı hissetmiyorlar. Biyoteknolojiyi yok etmek hayatlarını radikal bir şekilde değiştirmeyecek. Aksine, biyoteknolojinin süre giden gelişmesinin hayat tarzlarını dönüştüreceği ve uzun zamandır var olan insan değerlerini sileceği insanlara gösterebilir. Böylece radikaller, biyoteknolojiye meydan okurken, insanın değişime yönelik doğal direnişini kendi lehlerine seferber etmeyi başarabilirler.

Ve Biyoteknoloji sistemin kaybetmeyi göze alamayacağı bir konudur. Sistemin sonuna kadar savaşmak zorunda kalacağı bir konudur, ki ihtiyacımız da tam olarak bu. Fakat –bir kez daha yineleyecek olursam- biyoteknolojiye sistemin kendi değerlerine göre değil, sisteminkilerle bağdaşmaz olan değerlere göre saldırmak zorunludur. Örneğin biyoteknolojiye öncelikle onun çevreye zarar verebileceği veya genetik müdahaleye uğramış yiyeceklerin sağlığa zararlı olabileceği üzerinden saldırırsanız, sistem zemin sağlayarak veya uzlaşarak –örneğin, genetik araştırmalarda yüksek gözetim ve genetik müdahaleye uğramış mısırlarda özenli tahlil ve denetimi ortaya koyarak- saldırınızı hafifletecektir. İnsanların endişeleri böylece azalacak ve protesto sindirilecektir.

*** 8. İlke Olarak Tüm Biyoteknolojiye Saldırılmalıdır.

Böylece, biyoteknolojinin şu yada bu olumsuz sonucunu protesto etmek yerine, (a) biyoteknolojinin canlıların tümüne hakaret olduğu; (b) sistemin eline çok fazla güç verdiği; (c) binlerce yıldır var olan temel insan değerlerini radikal bir şekilde dönüştüreceği; ve sistemin değerleriyle bağdaşmaz olan benzer gerekçeler dayanarak, ilke olarak modern biyoteknolojinin tümüne saldırmalısınız.

Bu tür saldırıya karşılık, sistem karşılamak ve savaşmak zorunda kalacaktır. Aşırı düzeyde esneyerek saldırıyı hafifletmeye kalkışamaz, çünkü biyoteknoloji tüm teknolojik ilerleme girişimi için çok merkezidir ve sistem, esnediğinde taktiksel bir geri çekilme yapıyor olmayacak, değerler kodunda büyük bir stratejik bozguna uğruyor olacaktır. Sistemin bu değerleri yavaş yavaş yıpratılacak ve kapı, sistemin temellerini yaracak sonraki politik saldırılar için açılacaktır.

Doğru, ABD Temsilciler Meclisi yakın zamanda insan kopyalamanın yasaklanması yönünde oy kullandı ve en azından bazı kongre üyeleri bunun için doğru gerekçeler gösterdiler. Okuduğum gerekçeler dinsel terimlerle çerçevelenmişti, fakat söz konusu dinsel terimler hakkında ne düşünürseniz düşünün, bu gerekçeler teknolojik olarak kabul edilemez gerekçelerdi. Ve önemli olan da bu.

Bu nedenle, kongre üyelerinin insan kopyalama üzerine yaptıkları oylama, sistem için hakiki bir bozgundur. Fakat bu, çok ama çok küçük bir bozgundu, çünkü yasaklanmanın kapsamı çok dar –biyoteknolojinin ufacık bir bölümü etkilendi- ve zaten yakın gelecekte insan kopyalama sistem için az bir pratik faydaya sahip olacak. Fakat Temsilciler Meclisi’nin hareketi, bunun sistemin zayıf bir noktası olduğunu ve biyoteknolojinin tümüne yönelik daha geniş bir saldırının sisteme ve onun değerlerin büyük bir zarar verebileceğini akla getiriyor.

*** 9. Radikaller Biyoteknolojiye Henüz Etkili Bir Şekilde Saldıramıyor.

Bazı radikaller biyoteknolojiye politik veya fiziksel olarak saldırıyorlar, ama bildiğim kadarıyla biyoteknolojiye karşıtlıklarını sistemin kendi değerlerine göre açıklıyorlar. Yani temel şikayetleri, çevresel yıkım ve sağlığa zarar riskleri.

Ve biyoteknoloji endüstrisine yaralanacağı yerden vurmuyorlar. Yeniden fiziksel çarpışmayla benzerlik kurarsam, farz edin ki kendinizi dev bir ahtopota karşı savunmak zorundasınız. Ahtapotun dokunaçlarının ucuna vurarak etkili bir şekilde savaşamazsınız. Kafasına saldırmak zorundasınız. Faaliyetlerini okuduğum kadarıyla, biyoteknolojiye karşı çalışan radikaller hala ahtapotun dokunaçlarının ucuna vurmaktan daha fazla bir şey yapmıyorlar. Sıradan çiftçileri genetik müdahaleye uğramış tohumları ekmemeleri konusunda ayrı ayrı ikna etmeye çalışıyorlar. Fakat Amerika’da binlerce çiftlik var, bu yüzden çiftçileri ayrı ayrı ikna etmek, genetik mühendisliğiyle savaşmak için aşırı derecede verimsiz bir yol. Biyoteknolojik işle uğraşan araştırmacı bilim adamlarını ya da Monsanto gibi şirketlerin yöneticilerini biyoteknoloji endüstrisini terk etmeleri konusunda ikna etmek çok daha etkili olacaktır. İyi araştırmacı bilim adamları özel yetenekleri ve kapsamlı eğitimleri olan insanlardır, bu nedenle yenilerini bulmak zor olur. Aynısı, şirketlerin yüksek yöneticileri için de geçerli. Bunların birkaçını biyoteknolojiden uzaklaşmaları yönünde ikna etmek, biyoteknolojiye, bin çiftçiyi genetik müdahaleye uğramış tohumları ekmemesi konusunda ikna etmekten çok daha fazla zarar verecektir.

*** 10. Yaralanacağı Yerden Vur.

Sisteme politik saldırı için biyoteknolojinin en iyi konu olduğunu düşünmekte haklı olup olmadığım tartışmaya açıktır. Fakat bugün radikallerin, enerjilerinin çoğunu teknolojik sistemin ayakta kalmasıyla hemen hemen hiç ilgisi olmayan konulara harcadıkları tartışmasız. Hatta doğru konuyu işaret ettiklerinde bile, yaralanacağı yerden vurmuyorlar. Bu yüzden radikallerin küreselleşme üzerinde öfkeli tepinmeler yapmak için sonraki dünya ticaret zirvesine koşturmak yerine, üzerine düşünmek için zaman ayırmaları gereken şey, sisteme gerçekten yaralanacağı yerden nasıl vuracakları olmalıdır. Yasal yollarla tabi…

[1] Vurguların tümü Kaczynski’ye aittir.
[2] Sweatshop: İşçilerin düşük ücret karşılığında, kötü koşullarda uzun mesaiyle çalıştığı iş yeri.



