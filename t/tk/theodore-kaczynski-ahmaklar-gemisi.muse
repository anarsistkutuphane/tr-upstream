#title Ahmaklar Gemisi
#author Theodore Kaczynski
#SORTtopics eleştiri, kimlik, şiddet, tekno-endüstriyel karşıtı, giriş
#date 1999
#source 15.11.2019 tarihinde şuradan alındı: [[https://dunyalilar.org/ahmaklar-gemisi-ted-kaczynski.html/][dunyalilar.org]]
#notes Çeviri: Serhat Elfun Demirkol <br> İngilizce Aslı: [[https://theanarchistlibrary.org/library/ted-kaczynski-ship-of-fools][Ship of Fools]]
#lang tr
#pubdate 2019-11-15T20:11:08




Bir zamanlar, bir geminin kaptan ve zabitleri kendi denizciliklerini çok beğenir ve kendilerine çılgınca hayran olurlardı. Gemiyi kuzeye çevirdiler ve tehlikeli buzullarla karşılaşıncaya kadar yol aldılar. Kendilerine yalnızca denizcilikteki ebedi başarılarını gösterme fırsatı vermek için kuzeye doğru çok daha tehlikeli sularda yol almaya devam ettiler.

Gemi daha yüksek enlemlere ulaştıkça, yolcular ve mürettebat giderek rahatsız oldu ve aralarında tartışmaya, içerisindeki bulundukları koşullar hakkında şikayet etmeye başladılar.

<em>“Titriyorum”</em> dedi usta gemici, “<em>Bu kadar kötü bir yolculukta daha önce hiç bulunmamıştım. Güverte buzla kaplı; gözetleme yerindeyken rüzgar ceketimi bıçak gibi kesiyor; ön yelkene camadana vururken neredeyse parmaklarım donuyor; ve tüm bunlar için ayda 5 şilin alıyorum.”</em>

“<em>Bunun kötü olduğunu mu düşünüyorsun!</em>” dedi kadın yolcu. “<em>Soğuktan geceleri uyuyamıyorum. Bu gemideki kadınlar erkekler kadar battaniye alamıyor. Bu adil değil!</em>”

Meksikalı denizci sözü kesip konuşmaya katıldı: “<em>Chingado! Ben, İngiliz gemicinin aldığı maaşın sadece yarısını alıyorum. Bu iklimde kendimizi sıcak tutmak için bol yiyeceğe ihtiyacımız var; İngiliz daha çok alıyor. Ve en kötüsü, ikinci kaptanlar sürekli emirlerini İspanyolcanın yerine İngilizce olarak veriyorlar.</em>”

“<em>Herkesten daha çok şikayet edecek nedenim var</em>” dedi Amerikan Yerlisi gemici. “<em>Eğer soluk benizliler atalarımın topraklarını yağmalamasaydı, bu gemide, buzdağlarının ve kutup rüzgârlarının arasında olmayacaktım. Hoş, sakin bir gölde kanoyla gezinecektim. Tazminatı hak ediyorum. En azından, kaptan bana barbut oynatmam için izin vermeli ki biraz para kazanabileyim.”</em>

Lostromo söz aldı: “<em>Dün, birinci zabit bana ‘ibne’ dedi. İsimler takılmadan eşcinsel ilişkiye girme hakkım var.</em>”

“<em>Bu gemide kötü davranılan sadece siz insanlar değilsiniz.</em>” diyerek yolcuların arasındaki hayvansever araya girdi. Sesi öfkeyle titriyordu. “<em>Geçen hafta ikinci zabiti geminin köpeğini iki kere tekmelerken gördüm!</em>”

Yolculardan biri üniversite profesörüydü. Ellerini ovuşturarak hiddetle söylendi, “<em>Bunların hepsi korkunç! Ahlaksız! Irkçılık, seksizm, türcülük, homofobi, işçi sınıfının sömürülmesi! Ayrımcılık! Toplumsal adalete sahip olmalıyız: Meksikalı gemici için eşit maaş, bütün gemiciler için yüksek maaş, Amerikan Yerlisi için tazminat, kadınlar için eşit battaniye, eşcinsel ilişki hakkı ve köpeği daha fazla tekmelemek yok!”</em>

Yolcular “<em>Evet, evet!</em>” diye bağırdı. Mürettebat “<em>Hay hay!</em>” diye bağırdı. “<em>Ayrımcılık! Haklarımızı talep etmeliyiz!</em>”

Kamarot boğazını temizledi.

“<em>Hepinizin şikayet etmek için iyi nedenleri var. Fakat bana göre gerçekten yapmamız gereken şey gemiyi döndürmemiz ve güneye doğru gitmemiz, çünkü eğer kuzeye gitmeye devam edersek er geç batacağız. Sonra maaşlarınızın, battaniyelerinizin, eşcinsel ilişki haklarınızın size yararı olmayacak, çünkü hepimiz boğulacağız.</em>”

Fakat kimse onu dinlemedi, çünkü o sadece bir kamarottu.

Kaptan ve zabitler, kıç güvertedeki makamlarından tartışmayı izliyor ve dinliyordu. Birbirlerine gülümsediler ve göz kırptılar. Kaptanın el hareketiyle üçüncü zabit kıç güverteden indi. Yolcular ve mürettebatın toplandığı yere ağır adımlarla yürüdü ve onların arasında durdu. Çok ciddi bir ifade takınarak konuştu:

“<em>Biz kaptanlar kabul etmeliyiz ki bu gemide mazur görülemez şeyler olmakta. Şikayetlerinizi duyana kadar bu kadar kötü bir durum olduğunu anlayamadık. Bizler iyi niyetli insanlarız ve sizler için en iyisini yapmak istiyoruz. Ancak kaptan oldukça eski kafalı ve kendi bildiği yolda ilerler. Somut değişiklikler yapmadan önce biraz kışkırtılması gerekebilir. Benim şahsi fikrim, eğer gayretle protesto ederseniz – fakat her zaman barışçıl ve geminin kurallarını ihlâl etmeden – kaptanın ataletini sarsar ve gayet haklı olarak şikayet ettiğiniz problemlere çözüm getirmeye zorlarsınız.”</em>

Bunu söyledikten sonra üçüncü zabit kıç güverteye doğru yol aldı. Gider gitmez yolcular ve mürettebat arkasından, <em>“Orta yolcu! Reformcu! Liberal! Kaptanın yardakçısı!”</em> diye bağırdı. Fakat yine de söylediği gibi yaptılar. Kıç güvertenin önünde buluştular. Kaptanlara hakaretler savurdular ve haklarını talep ettiler: Usta gemici “<em>Daha yüksek maaş ve daha iyi çalışma koşulları istiyorum”</em> diye haykırdı. Kadın yolcu <em>“Kadınlar için eşit battaniye”</em> diye haykırdı. Meksikalı gemici <em>“Emirleri İspanyolca olarak almak istiyorum”</em> diye haykırdı. Amerikan Yerlisi gemici <em>“Barbut oynatma hakkı istiyorum”</em> diye haykırdı. Lostromo “<em>İbne olarak adlandırılmak istemiyorum”</em> diye haykırdı. Hayvansever <em>“Köpeğin daha fazla tekmelenmesine hayır”</em> diye haykırdı. Profesör <em>“Devrim, hemen şimdi”</em> diye haykırdı.

Kaptan ve zabitler aceleyle bir araya toplandı ve birkaç dakika görüştü. Bütün bu süre boyunca birbirlerine göz kırptılar, gülümsediler ve birbirlerini doğrularcasına kafalarını öne eğdiler. Daha sonra kaptan kıç güvertenin önünde durdu ve büyük bir cömertlik göstererek, usta gemicinin maaşının ayda 6 şiline yükseltileceğini; Meksikalı gemicinin maaşının İngiliz gemicinin üçte ikisi kadar olacağını, ve ön yelkene camadana vurma emrinin İspanyolca verileceğini; kadın yolcuların bir battaniye daha alacağını; Amerikan Yerlisi gemicinin Cumartesi akşamları barbut oynatabileceğini; lostromonun gizlice eşcinsel ilişkiye girdiği sürece ibne olarak anılmayacağını ve mutfaktan yemek çalmak gibi gerçekten ahlaksız şeyler yapmadığı sürece köpeğin tekmelenmeyeceğini duyurdu.

Yolcular ve mürettebat bu imtiyazları büyük bir zafer olarak kutladı. Fakat ertesi sabah, tekrardan memnuniyetsizlik hissettiler.

Usta gemici <em>“Ayda altı şilin çok düşük bir ücret ve hâlâ ön yelkene camadana vururken parmaklarım donuyor”</em> diyerek homurdandı. Meksikalı gemici <em>“Hâlâ İngilizlerle aynı maaşı veya bu iklim için yeterli yiyeceği alamıyorum”</em> dedi. Kadın yolcu <em>“Biz kadınlar hâlâ kendimizi sıcak tutacak kadar battaniyeye sahip değiliz”</em> dedi. Diğer yolcular ve mürettebat da benzer şikayetlerde bulundu. Profesör onları kışkırttı.

Konuşmalarını bitirdiklerinde, kamarot, bu sefer diğerlerinin duymamazlıktan gelemeyeceği kadar yüksek bir sesle konuştu:

<em>“Köpeğin mutfaktan bir parça ekmek çaldığı için tekmelenmesi, kadınların eşit battaniyeye sahip olmaması, usta gemicinin parmaklarının donması gerçekten korkunç; ve Lostromo’nun istediği halde neden erkeklerle ilişkiye giremediğini anlamıyorum. Fakat buzulların şu an nasıl kalın olduklarına ve rüzgârın nasıl daha fazla sert estiğine bakın! Bu gemiyi geriye, güneye doğru çevirmemiz gerekiyor. Eğer kuzeye gitmeye devam edersek, buzullara çarpacak ve batacağız.”</em>

<em>“Ah, evet”</em> dedi Lostromo, <em>“Kuzeye doğru gitmeye devam etmemiz gerçekten korkunç bir şey. Fakat neden tuvalette sevişmek zorundayım? Neden ibne olarak anılmam gerekiyor? Diğer herkes gibi iyi biri değil miyim?”</em>

<em>“Kuzeye doğru ilerlemek korkunç”</em> dedi kadın yolcu. <em>“Fakat görmüyor musun? Tam da bu nedenle kadınların kendilerini sıcak tutmak için daha çok battaniyeye ihtiyacı var. Hemen şimdi kadınlar için eşit battaniye talep ediyorum!”</em>

<em>“Tamamen doğru”</em> dedi profesör, <em>“Kuzeye doğru yol almak hepimiz için büyük sıkıntılar yaratıyor. Fakat yönümüzü güneye doğru çevirmek gerçekçi olmaz. Zamanı geri çeviremezsin. Durumumuzun üstesinden gelmek için iyi hazırlanmış bir yol bulmalıyız.”</em>

<em>“Bak”</em> dedi kamarot, <em>“Kıç güvertedeki bu dört kaçık adamın yollarına devam etmesine izin verirsek, hepimiz batacağız. Eğer gemiyi tehlikeden uzaklaştırırsak, daha sonra çalışma koşulları, kadınlar için battaniye ve eşcinsel ilişki hakkı için endişelenebiliriz. Ama önce bu gemiyi çevirmemiz gerekiyor. Eğer bir kısmımız birlik olur, bir plan yapar ve biraz cesaret gösterirsek, kendimizi kurtarabiliriz. Çok fazla insana gerek yok – yedi veya sekizimiz yeterli. Kıç güverteye saldırabilir, bu delileri gemiden atabilir ve gemiyi güneye çevirebiliriz.”</em>

Profesör sesini yükseltti ve sert bir şekilde “<em>Şiddete inanmıyorum. Ahlaksızca”</em> dedi.

Lostromo <em>“Şiddet kullanmak etik değil”</em> dedi.

Kadın yolcu <em>“Şiddetten çok korkuyorum”</em> dedi.

Kaptan ve zabitler herşeyi izliyor ve dinliyordu. Kaptanın bir işaretiyle üçüncü zabit ana güverteye indi. Yolcuların ve mürettebatın arasına kadar geldi ve gemide hâlâ bir takım sıkıntılar olduğunu söyledi.

<em>“Epey ilerleme kaydettik”</em> dedi. <em>“Fakat daha fazlası gerçekleşmeyi bekliyor. Usta gemicinin çalışma koşulları hâlâ sert, Meksikalı hâlâ İngiliz ile aynı maaşı alamıyor, kadınların hâlâ erkekler kadar battaniyesi yok, Amerikan Yerlisi’nin Cumartesi geceleri oynattığı barbut ellerinden alınan toprakları için değersiz bir karşılık, Lostromo’nun eşcinsel ilişkiye tuvalette girmesi adil değil ve köpek hâlâ kimi zaman tekmeleniyor.”</em>

<em>“Bence kaptanın yeniden harekete geçirilmeye ihtiyacı var. Eğer hep birlikte başka bir protesto gerçekleştirirseniz işe yarayacaktır – şiddetsiz olduğu sürece.”</em>

Üçüncü zabit geminin kıç tarafına doğru ilerlerken, yolcular ve mürettebat arkasından hakaretler yağdırdı. Ama yine de ne dediyse yaptılar ve başka bir protesto için geminin kıç güvertesi önünde toplandılar. Çılgınca bağırıp çağırdılar, yumruklarını savurdular ve hâttâ kaptana çürük yumurta attılar (ustalıkla yana çekildi).

Kaptan ve zabitler şikayetleri dinledikten sonra aceleyle bir araya toplandı. Konuşmaları süresince birbirlerine göz kırptılar ve sırıttılar. Daha sonra kaptan kıç güvertenin önüne geldi ve usta gemiciye parmaklarını sıcak tutsun diye bir eldiven verileceğini, Meksikalı gemicinin İngiliz gemicinin dörtte üç maaşı kadar maaş alacağını, kadınlara bir battaniye daha verileceğini, Amerikan yerlisi gemicinin Cumartesi ve Pazar geceleri barbut oynatabileceğini, Lostromo’nun karanlıktan sonra alenen eşcinsel ilişkiye girebileceğini ve kimsenin kaptanın özel izni olmadan köpeği tekmeleyemeyeceğini söyledi.

Yolcular ve mürettebat bu büyük devrimci zafer karşısında çok mutluydu. Fakat ertesi günle birlikte tekrardan memnuniyetsizlik hissettiler ve aynı eski sıkıntılar hakkında söylenmeye başladılar.

Kamarot bu sefer sinirleniyordu.

<em>“Sizi ahmaklar!”</em> diye bağırdı. <em>“Kaptanın ve zabitlerin neler yaptıklarını görmüyor musunuz? Bu gemiyle ilgili yanlışın ne olduğunu düşünemeyesiniz diye battaniyeler, maaşlar ve köpeğin tekmelenmesi hakkındaki saçma şikayetlerinizle sizleri meşgul etmeyi sürdürüyorlar – gemi kuzeye doğru daha da ilerliyor ve hepimiz boğulmuş olacağız. Eğer sadece bir kaçınız aklını başına toplar, bir araya gelir ve kıç güverteyi basarsak, bu gemiyi çevirebilir ve kendimizi kurtarabiliriz. Fakat tüm yaptığınız, çalışma koşulları, barbut oynatma ve eşcinsel ilişki hakkı gibi önemsiz küçük konular hakkında ağlaşmak.”</em>

Yolcular ve mürettebat öfkelendi.

<em>“Önemsiz!!”</em> diye ağladı Meksikalı, <em>“İngiliz gemicinin sadece dörtte üçü kadar maaş almam sence adil mi? Önemsiz mi?”</em>

<em>“Benim sıkıntıma nasıl saçma diyebiliyorsun?”</em> diye bağırdı Lostromo. <em>“İbne olarak anılmanın küçük düşürücü olduğunu bilmiyor musun?”</em>

<em>“Köpeği tekmelemek ‘önemsiz küçük bir konu’ değil!”</em> diye haykırdı hayvansever. <em>“Zalimce, insafsızca, vahşice!”</em>

Kamarot, <em>“Pekâlâ”</em> dedi. <em>“Bu konular önemsiz ve saçma değil. Köpeği tekmelemek insafsız ve vahşice. İbne olarak anılmak küçük düşürücü. Fakat gerçek sorunumuzla karşılaştırıldığında – geminin hâlâ kuzeye gidiyor olduğu gerçeğiyle karşılaştırıldığında – sizin şikayetleriniz önemsiz ve saçma, çünkü eğer bu gemiyi derhal çeviremezsek hepimiz boğulacağız.”</em>

<em>“Faşist!”</em> diye haykırdı profesör.

<em>“Karşı devrimci!”</em> dedi kadın yolcu. Tüm yolcular ve mürettebat birbirlerinin ardından konuşmaya katıldı. Kamarotu faşist ve karşı devrimci olarak suçladılar. Onu bir kenara itip maaşlar, kadınlar için battaniye, eşcinsel hakları ve köpeğe nasıl davranılması gerektiği hakkında söylenmeye devam ettiler. Gemi kuzeye doğru yol almaya devam etti. Bir süre sonra iki buzdağı arasında parçalandı ve herkes boğuldu.
