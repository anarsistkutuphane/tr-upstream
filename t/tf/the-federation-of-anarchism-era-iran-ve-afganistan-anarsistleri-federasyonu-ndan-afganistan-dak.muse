#cover t-f-the-federation-of-anarchism-era-iran-ve-afgani-1.png
#title İran ve Afganistan Anarşistleri Federasyonu'ndan Afganistan'daki Siyasi Krize İlişkin Uluslararası Çağrı
#author The Federation of Anarchism Era
#SORTtopics afganistan, iran, göçmen
#source 30.07.2021 tarihinde şuradan alındı: [[https://www.yeryuzupostasi.org/2021/07/18/iran-ve-afganistan-anarsistleri-federasyonundan-afganistandaki-siyasi-krize-iliskin-uluslararasi-cagri/][www.yeryuzupostasi.org]]
#lang tr
#pubdate 2021-07-30T15:55:23
#notes <br><strong>Çeviri:</strong> Yeryüzü Postası <br><strong>İngilizce Aslı:</strong> [[https://asranarshism.com/1400/04/20/international-call-on-the-political-crisis-in-afghanistan][asranarshism.com]]



Bu, Afganistan’ın mevcut iç siyasi krizi hakkında uluslararası bir çağrıdır.

Bildiğimiz gibi, ABD, Rusya, İran ve Afganistan’ın merkezi hükümeti de dahil olmak üzere hükümetler arasında Taliban İslamcı grubunun temsilcileriyle yapılan biz dizi toplantı ve müzakerelerden sonra, bu cihatçı grup şimdi Afganistan’ın çeşitli yerlerinin kontrolünü hızla ele geçiriyor. Nihayetinde IŞİD gibi bir İslami yönetim kurmayı amaçlıyor. Dinsel gerici fikirleri savunan ve Afganistan’da kanlı bir geçmişe sahip olan bir grubun geri dönüşü, sadece Afganistan halkının özgürlük mücadelesine karşı değildir. Bu aynı zamanda bölge halkına başka bir teokratik yönetimin dayatılması anlamına gelmektedir. Bu, ancak dünyaya egemen olan, sisteminin emperyalist politikaları, bunların bölgedeki ve dünyanın genelindeki kendine özgü oyunları ile mümkün olabilir. ABD, hiçbir zaman Taliban’la gerçekten yüzleşmek için Afganistan’a girmemişti.

Son zamanlarda, Taliban’a siyasi zemin sağlamanın, bu İslamcı gruba nasıl daha fazla güç verdiğini açıkça gördük. ABD ile müzakereler sırasında on binlerce Taliban cihatçısının Afganistan hapishanelerinden eş zamanlı olarak salıverilmesi, dünyadaki egemen kapitalist sisteminin emperyalist çıkarlarının, çoğunlukla aynı sistemin hükümetleri tarafından oluşturulan radikal İslamcı gruplara imkan tanımakla çelişmediğinin açık bir kanıtıdır. ABD’nin Afganistan’daki yirmi yıllık askeri varlığının ve Afganistan’da merkezi bir hükümetin kurulmasından sonra, terörist grup Taliban’ın, bugün bölgedeki rakip hükümetlerden onay almasının ardından birçok eyaletin kontrolünü ele geçirebilecek kadar güç kazanmasına şaşmamalı.

İran ve Afganistan Anarşistleri Federasyonu (Anarşizm çağı Federasyonu), Afganistan’daki geçmiş ve şimdiki durum hakkında net bir tutum ortaya koymaktadır. Gözlemlerimiz, bugün Afganistan halkının çoğunun küresel kontrol sisteminin yıkıcı politikalarının her zamankinden daha fazla farkında olduğunu ve Taliban teröristleriyle başa çıkmak için kendi örgütlerini kurmak ve aynı zamanda ülkeyi, hükümetsiz ve hiyerarşik olmayan bir şekilde yönetmek için irade ve güçlerini yeniden kazanmak istediklerini gösteriyor. İran ve Afganistan Anarşist Federasyonu, Afganistan’ın Taliban’a karşı halk direniş hareketini desteklemek için dünya çapındaki yoldaşlarımızla, Afganistan’ın sorunlarına pratik ve özgürlükçü bir çözüm bulmak için (Farsça ve İngilizce) açık bir uluslararası çağrıda bulunuyor.

Devrimci ideallerimiz, kapitalist sistemin egemen olduğu geniş coğrafyalarda, toplumun alt kesimlerinin gelecekteki zaferini vadediyor.

Bu uluslararası foruma katılmak için bize bir e-posta gönderin: info {et} asranarshism{nokta}com

Anarşist Federasyon (Eski Afganistan ve İran Anarşist Birliği)



