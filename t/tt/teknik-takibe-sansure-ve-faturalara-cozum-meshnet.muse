#title Teknik Takibe, Sansüre ve Faturalara Çözüm: Meshnet
#author Meydan Gazetesi, Korsan Parti
#SORTauthors Meydan Gazetesi, Korsan Parti
#SORTtopics hacker, internet, meshnet, sansür, korsan parti
#date 05.12.2013
#lang tr
#pubdate 2019-11-15T23:00:00




[[t-t-teknik-takibe-sansure-ve-faturalara-cozum-mesh-1.jpg 50 l]]



<quote>
Gün içinde saatlerimizi geçirdiğimiz internet için milyonlarca lira para ödüyor, kota sınırını aşmamak için uğraşıyoruz. Devletler ve şirketlerse tüm maillerimizi, Facebook ve Twitter üzerinden yaptığımız tüm paylaşımları ve girdiğimiz tüm internet sitelerini takip ediyor. Birçok internet sitesi çeşitli sebeplerle sansürlenip yasaklanırken, bu sitelere girme yolları, yapılan paylaşımların takip edilmemesi için kullanılacak yöntemler ve “takip edilemeyen internet kullanımı” konusunda da tartışmalar sürüyor. Son zamanlarda adını duymaya başladığımız “Meshnet” uygulaması da işte bu noktada devreye giriyor.

Bilgisayar adresinizi gizleyerek takip edilme riskinizi ortadan kaldıran, maillerinizin üçüncü kişiler tarafından okunmasını engelleyen, herkesin ücretsiz bir şekilde kullanabileceği bir uygulama olan Meshnet’i, Bilişim Teknolojileri Uzmanı ve Korsan Parti üyesi Kozan Demircan ile konuştuk. Meshnet nedir, nasıl işler ve neden kullanılmalıdır?
</quote>


<strong>Meydan: Merhaba. Özellikle internet üzerinden takip edildiğimiz, çok da gizli olmayan bir bilgi. Ulusal Güvenlik Dairesi (NSA) gibi istihbarat birimlerinin ve bu amaçla kurulmuş özel şirketlerin, kişisel bilgilere ulaştığı örneklerini son dönemde her yerde görüyoruz. Bunun bir nedeni de internet hizmetini sağlayanların, bu kurumlar ve şirketlerle ilişkili mekanizmalar olmasından kaynaklandığını söylemek mümkün mü?</strong>

Kozan Demircan: Tabi ki, zaten Meshnet’in kurulma nedenlerinden biri bu. Devletler telekom sektörünü tekeline aldığı zaman, baskı yaparak onların sunucularını takip etmeye gerek duymaz. Tüm yollar Roma’ya çıkıyorsa, devletin burada yaptığı, Roma’yı takip etmek. Dolayısıyla evet, dediğiniz doğru.

<strong>Kuruluş itibarıyla düşünüldüğünde, bu internetin mantığına uymayan bir şey değil mi?</strong>

Çıkış itibarıyla düşünüldüğünde, aslında internetin mantığı bu. İnternet, Soğuk Savaş yıllarında, nükleer silahlanma sürecinde planlandı, silah laboratuvarlarını ve karargahları birbirine bağlamak için kullanıldı. Fakat bu hat merkeziydi. Şimdilerde konuştuğumuz Meshnet ise bunun alternatifi; yani gayrı merkezi; dağınık değil dağıtık bir yapı.

Meshnet’in Türkçesi “örme ağ” demek. Bunun farkı nedir? İnternet merkezidir. Ancak Meshnet ağının unsurları kurumsal veri merkezleri değil, insanlardır. Meshnet’e kişilerarası internet diyebiliriz, kişiler modem ağıyla birbirlerine bağlanırlar. Bir “Peer to Peer network” yani eşler arası ağ mantığıyla çalışır. Bu yüzden takip edilme problemi de yoktur.

<strong>Peki, Meshnet nasıl işler, nasıl kullanılır?</strong>

Bir kişi uydu internet şirketine para ödeyerek uydudan internete çıkar ve daha sonra geniş bant internet erişimini mahalleye ücretsiz dağıtır. Siz de karşı komşunuzun modemi ile internete girmiş olursunuz. Bunun için başkaca internet şirketlerine mecbur kalmazsınız.

Evinizdeki modemden şirket hattına değil, kablosuz olarak komşunuzun internetine bağlanırsınız. İnterneti dağıtan kişiye modeminizle doğrudan bağlanmazsınız. Bunun yerine bu kişiye yakın evde oturan bir kişinin modemiyle bağlanırsınız. Dolayısıyla internete belki de 15-20 kişi üzerinden girersiniz. Bu da sabit bir Internet Protocol (IP- bilgisayarın internette tespit edilmesini sağlayan adres) adresiniz olmasını engeller, yani internette takip edilmenizi engeller.

Meshnet’te sunucuya bağlanan kişileri takip edecek bir sistem, yazılım bulunmuyor. Sunucuların çoğu günlük denen, IP kayıtlarını (log) da tutmuyor. Binaya yetkililer gitse ve bilgi talebinde bulunsalar bile sizin internete giriş bilgilerinizi bulamazlar. Siz internetini paylaşan kişiye her bağlandığınız zaman ayrı bir kimlik numarası ile bağlanıyorsunuz.

<strong>Meshnet’in kullanımının ABD’den Yunanistan’a daha farklı birçok ülkeye yaygın olduğundan bahsetmiştin yazında, biraz açar mısın? Nerelerde kullanılıyor Meshnet, sadece hackerların kullandığı bir sistem mi?</strong>

Meshnet’i sadece hackerlar kullanmıyor. Bugün İspanya’nın Katalonya eyaletinde Meshnet kullanılıyor. Katalonya’da kar amaçlı olmayan bir organizasyon oluşturup, kendi fiber optik hatlarını döşediler ve telekomla internete girmek istemedikleri içim Meshnet’i kullanmaya başladılar. Bugün Katalonya’daki pek çok belediye ve hastane kendi şebekeleriyle internete giriyor. Böylelikle yerel yönetimler özgürleşiyor.

Ayrıca bu sistemler Amerika ve Avrupa’daki birçok üniversitenin kampüsünde kullanılıyor. Afrika’nın yoksul ülkelerinden olan Gana’dan, iflas eden Yunanistan’daki Atina’ya kadar birçok farklı yerde Meshnet kullanılıyor.

<strong>Türkiye’de şu an kullanılan Meshnet hizmeti veren bir şirketin varlığından söz etmiştin yazında. Ve bir başka röportajda da internetin bu şekilde yani ücretsiz bir şekilde başkalarıyla paylaşımını engelleyen bir madde yok diye belirtmiştin. Yani hukuki açıdan bir sıkıntı yok?</strong>

Hukuki durum şu, Türkiye’de internetinizi paylaşmanız suç değildir. Şayet olsaydı internet şirketleri ve telefon operatörleri olmazdı. Başkasının kendi telefonuyla sizin evinizde, sizin internetinize bağlanması suç sayılırdı. Bu suç değil, ancak kanunda şu var: Uydudan interneti paylaşmanız için önce uyduya çıkmanız lazım. Devlet işte bu noktada diyor ki; benden habersiz internete giremezsin o yüzden ruhsat alacaksın, uyduya öyle çıkacaksın, gizli kurumsal internet parası ödeyeceksin, ayda kaç bin dolarsa… Ama pratikte kimin internetin paylaştığını nasıl anlayacaklar? Bugün Türkiye’de üç milyon kişi internetini uydudan paylaşsa, üç milyon kişiyi tek tek takip mi edecekler?

Ancak telekom şirketleri rekabet avantajını korumak için sözleşmelerinde benim internetimi hane dışında başkalarıyla paylaşamazsın diyor. Hane içinde diyemez. Ama bu yasakların yasada bir geçerliliği olmadığı için, bu sözleşmelerin yasal geçerliliği de tartışmalı. Biri çıkıp Telekom şirketlerini mahkemeye verse, davayı kazanabilir bile. Keşke böyle bir hata yapsalar, bir dava açılsa da bir emsal teşkil etse.

<strong>Yazında Meshnet kullanımının dünyanın yoksul bölgeleri için önemli olduğundan bahsetmişsin neden?</strong>

Meshnet Türkiye’nin yoksul bölgelerini şehirlerle birleştiren bağımsız bir iletişim ağı olmaya aday ve Meshnet’in potansiyeli çok büyük. Örneğin İspanya’da kullanılan Meshnet ağı yakında hastaneleri ve belediye binalarını İspanya telekom şebekesinden tümüyle bağımsız olarak döşenen özel fiber optik hatlarla birbirine bağlayacak.

Meshnet şirketlerin kabusu. Bugün Amerika’da bir şirket Federal İletişim Komisyonu’na dava açtı. Diyor ki, ben bir cep telefonu operatörü olarak hangi siteyi yayınlayacağıma ben karar veririm. Siyasi fikirleri bana tersse yayınlamam. Bir kişinin interneti hızlı açılacak, sayfaları hızlı yüklenecekse bana hızlı internet için ek para ödesin diyor. Yani insanlardan sadece internet parası almak istemiyorlar; kişilerden içerik promosyon parası almak istiyorlar, o zaman da internetten sadece zenginlerin sesleri duyuluyor.

Bunu delmenin tek yönü var; sen madem anayasadaki iletişim özgürlüğüne rağmen interneti parayla satıyorsun ve vatandaşın verdiği vergilerle kurulan bir telekom şirketi vatandaşa sormadan başkalarına lisanslanıyor ve sonra da o internet vatandaşı takip ediyor, reklam saldırısına maruz bırakıyor. Bu noktada ben de senin interneti kullanıyorum demek önemli. Burada verilebilecek en iyi cevap, Meshnet kullanmaktır.

<strong>Belediyeler için kullanışlı bir sistem olarak Meshnet’in “demokrasi” ile olan ilişkisi nedir? “Doğrudan internet demokrasi” diye belirttiğin kavramı biraz açar mısın? Bununla ilişkili bir Katalonya örneği var sanırım. Meshnet’in özellikle yerel yönetim ve devlet ilişkisindeki rolünün ne olduğundan bahseder misin?</strong>

Doğrudan internet demokrasisi burayla ilgili. Şirketler hem yavaş interneti satarak paralar kazanıyor, hem de interneti takip ediyor, hem de internet kullanıcılarını fişliyor. Şirketler kullandığınız interneti takip ettikçe, kişisel bilgilerinize-özelliklerinize ulaşırlar ve böylelikle size reklamları da pazarlarlar. Yani hem değerli kişisel bilgilerinizi alırlar hem de bu bilgileri veri simsarları, data brokerlerı aracılığıyla size parayla geri satarlar. Ama Meshnet insanları özgürleştirir. Özgür iletişimin olmadığı bir yerde, demokrasiden bahsedilemez.



