#title Oy ver ya da verme
#subtitle Oy kullanmama üzerine
#author Black Rose Anarchist Federation
#date 31.10.2020
#source 21.01.2023 tarihinde şuradan alındı: [[https://www.yeryuzupostasi.org/2023/01/21/oy-ver-ya-da-verme-oy-kullanmama-uzerine-cameron-a/][yeryuzupostasi.org]]
#lang tr
#pubdate 2023-01-21T17:05:04
#topics oy, seçim, giriş
#notes Çeviri: Yeryüzü Postası <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/black-rose-anarchist-federation-vote-or-don-t][Vote — or Don’t: On Abstention]]



Anarşistlerin seçimlerden tiksindikleri bir sır değil, ancak (anarşizmin de parçası olduğu) devrimci sosyalizmi benimseyenlerin çoğu, bu piyes karşısında nasıl konumlanacaklarına dair basit bir anlayışa sahip olma eğilimindeler. Bu makale, anarşistlerin seçimciliğe cevap niteliğinde bir strateji olarak oy kullanmamaya odaklanmalarının şu an için yetersiz olmakla kalmadığını, aynı zamanda düşmanlarımız tarafından burjuva demokrasisinin yeniden üretimi için kullanılanla aynı ahlakçı mantığa dayandığını iddia ediyor.

*** Devrimci Sol ve Seçimler

Burjuva demokrasisinde sosyalistlerin, temsilcilerin seçimi meselesiyle nasıl ilişki kurması gerektiği meselesi, 150 yılı aşkın bir süredir devam eden esaslı tartışmalardan biri olmuştur. Aslında, Birinci Enternasyonal’in bölünmesinden büyük ölçüde (devlet iktidarı arayışı sorununun doğal sonucu olarak) bu noktadaki anlaşmazlık sorumluydu.

Hiç şüphe yok ki seçimler dikkat çekici olaylardır. Harcanan benzersiz miktarda para, uzmanların bitmek tükenmek bilmeden konuları ele almaları ve size her zaman bir sonraki kışkırtıcı yorumu sunmaya hazır Twitter ile seçimler siyasi olduğu kadar sosyal ve kültüreldir.

Bugün, aynı tartışma sosyalist hareketin her parçasının kendi reçetesini oluşturmasıyla devam ediyor. ABD’deki en büyük sosyalist örgüt, stratejisini büyük ölçüde Demokrat Parti içinde bir taban oluşturmaya ve üyelerinin göreve seçilmesine yardımcı olmaya dayandırıyor.

Devrimci sosyalistler (anarşistler dahil) ise farklı bir taktik izliyorlar. Bu kategorideki bazı gruplar, seçimde başarı ümidi olmadığı açıkça belli olan kendi adayları için, bunları kullanmak yerine, alaycı bir şekilde dikkatleri veya kaynakları kuruluşlarına çekmek için ayrıntılı sahte kampanyalar inşa etmeye başladılar. Diğerleri, özellikle anarşistler, prensip olarak seçim sürecinden tamamen çekilme çağrısı yapma alışkanlığına sürdürdüler.

Bu makalenin odak noktası ikinci kategori olacak.

*** Oy Vermenin Ahlakçılığı, Oy Vermemenin Ahlakçılığı

Anarşistler (ve diğer devrimci sosyalistler) hangi gerekçelerle oy vermemeye çağırıyorlar? Genellikle, bu, bir burjuva seçiminde oy kullanmanın devleti aktif olarak meşrulaştırmak anlamına geldiği, dolayısıyla temel ideolojik ilkelerimizle çelişkili olduğu iddiasına indirgenir.

Bu, ironik bir şekilde, muhalefet partisinin verdiği zarardan sorumlu olmamak için oy kullanılması gerektiğini iddia edenlerin (genellikle liberaller) kullandığı mantığın aynısıdır. Bu, 2016’dan beri ABD’de çeşitli solcuların kafalarında yankılanan tanıdık bir nakarattır.

Bununla birlikte, bu konumların her ikisi de, kökleri siyasi gücün, koşulların ve devletin işleyişinin maddiliğine dayanan soruları bireysel bir ahlaki hesaba indirgemeleri bakımından derinden kusurludur. Liberalden bu beklenebilirken, anarşistler neden aynı referans çerçevesini büyük ölçüde benimsediler?

Bunu biraz daha inceleyelim.

Bu çerçevede, hem devrimci sosyalistler hem de liberaller, özünde merkezi bir sorunun yattığı aynı ahlaki şemadan karşıt sonuçlar çıkarıyorlar: Devletin eylemlerinin meşrulaştırılmasındaki suç ortaklığımı en iyi nasıl azaltabilirim?

Devrimci sosyalistlerden -liberalden farklı olarak- devletin kendisinin kapitalist sınıfın bir aracı olduğunu kabul edecek kadar açıklığa sahip olsa da, ayrık bireyin ve eylemlerinin temel olarak kurucu olduğunu öne süren temel mantıktan sık sık kopamıyor gibiyiz.

Liberallerden farklı olarak, devrimci sosyalistler, devletin kendisinin kapitalist sınıfın bir aracı olduğunu kabul edecek kadar netliğe sahip olsalar da, tek tek bireylerin ve onların eylemlerinin devlete meşruluk sağlayan esas kaynak olduğunu iddia eden temel mantıktan kopabildiklerini çoğu zaman göremiyoruz. Bu, tüm temsili demokrasilerin üzerine dayandığı söylenen ve anarşistlerin tarihsel olarak reddettiği, yönetilenlerin sözde rızasıdır.

Buna karşın, anarşistler, devletin oluşumu, yeniden üretimi ve meşrulaştırılması süreçlerinin baskıcı güçler (ordu, polis, hapishaneler) ve ideolojik koşullandırma (okullar, medya gibi sivil toplum kuruluşları aracılığıyla) yoluyla gerçekleştirildiğini savunan bir devlet teorisi geliştirmişlerdir.

Basitçe söylemek gerekirse: devlet, bırakın en korkunç faaliyetlerini gerçekleştirmek için, var olmak için bile izninize ihtiyaç duymaz.

O halde çoğu anarşistin, yönetilenlerin rızası mantığını kabul ettiği göz önüne alındığında, oy vermeme tutumunu benimsemiş olmaları tuhaf. Karşı gücün geliştirilmesi için ciddi stratejiler inşa etme arayışında anarşist devlet teorisini tamamen benimsemek yerine, boykot ve rızayı geri çekme gibi konforlu, ahlaki bir dil benimsiyoruz.

*** Oy Vermemenin Ötesine Geçmek

Yukarıda gösterildiği gibi, oy vermemek, anarşist devlet teorisiyle bağdaşmayan bir liberal siyaset teorisi varsayımına dayanmaktadır. Buna göre, oy vermemeye güvenmenin ötesine geçmeli ve seçimler ve devlet gücüyle ilgili olarak fiili bir stratejik yönelim geliştirmeliyiz.

Bu yazının bir çözüm olarak seçim sistemiyle aktif biçimde, hevesli veya gerçekten herhangi bir şekilde ilgilenmeyi önermediği yeterince açık. Aslında burada ileri sürülen şey, seçimlere katılım konusunun bizim düşüncelerimizden tamamen çıkarılması gerektiği fikridir. Ne oy vermemek ne de seçimlere dahil olmak aktif bir strateji değildir. Bu soru üzerine kafa yormak için bir andan daha fazla zaman harcamak ya da daha da kötüsü bu konuda ahlak dersi vermek, ciddi bir devrimci için büyük bir zaman kaybıdır.

En acil görevimiz, hem devlet hem de sermaye karşısında irademizi ortaya koyabilecek bir sınıf olarak kendimizi örgütlemektir. Bu, günlük yaşamlarımızda kolektif gücü inşa etmemize ve kullanmamıza imkan veren bağımsız, istikrarlı toplumsal hareket örgütleri inşa etmek veya güçlendirmek anlamına gelmektedir. İşyerinde işçi sendikaları, evde kiracı sendikaları, okulda öğrenci sendikaları ve mahallelerimizdeki halk meclisleri. Kısacası, amacımız halk iktidarı yaratmak olmalıdır.

Anarşistler, özellikle Especifismo stratejisini benimseyenler, görevimizin bu örgütlere dahil olmak ve onların demokratik, mücadeleci ve devrimci karakterlerini geliştirmek için çalışmak olduğunun farkındalar.

Şu anda, bu ülkedeki güçler dengesi kesinlikle sermaye ve devletin lehine. Kitlesel protestolar umut verici olsa da, göstericilerin sokak odaklı eylemin ötesine ve yukarıda bahsedilen ciddi anlamda kök salmış örgütsel biçimler aracılığıyla sürekli hareket inşa etmeye doğru ilerlediklerini gösteren çok az işaret var. Kazanım elde etme yetimizin, sermaye ve devlete, hem savunmasız hem de değerli olarak belirlediğimiz alanlarda ne kadar etkili baskı uygulayabildiğimize bağlı olduğu anlaşılmalıdır. Kazanabiliriz, ancak doğru araçlara sahip olmalıyız.

Tabii ki, bu mücadeleler bir boşlukta gerçekleşmiyor. Dünya dönmeye devam ediyor ve içinde bulunduğumuz koşulları değiştiren ulusal veya uluslararası öneme sahip olaylar olmaya devam edecek. İster bir seçimin, ister bir ekonomik krizin, isterse bir pandeminin (veya üçünün birden) ortasında olalım, ancak içinde bulunduğumuz konjonktürü anlayıp ona göre hareket edebilirsek etkili olabiliriz.

Oy verin ya da vermeyin, ancak gücü inşa etmeye öncelik verin.


