#DELETED Reason for deletion: Machine translation (DeepL: after June 2022, Google Translate: before June 2022) has been detected. - https://mirror.anarsistkutuphane.org/c4ss.org/analyze/
#title Chomsky’nin Augustinusçu Anarşizmi
#author Roderick Long
#date 07.01.2010
#source 23.10.2022 tarihinde şuradan alındı: [[https://c4ss.org/content/57507][c4ss.org]]
#lang tr
#pubdate 2022-10-23T19:35:00
#topics sol piyasa anarşizmi, abd, ekonomi, kapitalizm
#notes Çeviri: Efsa <br>İngilizce Aslı: [[https://c4ss.org/content/1659][Chomsky’s Augustinian Anarchism]]


Noam Chomsky belki de Amerika Birleşik Devletleri’nin en tanınmış anarşistidir. Fakat burada belli bir ironi bulunuyor; tıpkı Aziz Augustine’in bir zamanlar “Bana bekâret ve iffet bahşet, ama henüz değil” diye dua ettiği gibi. Chomsky’nin amacı da aslında anarşidir ama henüz değil.

Chomsky’nin “henüz değil” demesinin nedeni, güçlü bir merkezi hükümetin şu anda şirket seçkinlerinin gücüne karşı bir siper olarak gerekliliğidir; bu nedenle, devleti şirket seçkinlerinin gücünü kırmak için kullanana kadar devleti ortadan kaldırmak veya hatta küçültmek güvenli olmayacaktır:

<quote>
“Uzun vadede, merkezi siyasi gücün ortadan kaldırılması, çözülmesi ve nihayetinde federalizm, birlikler ve benzeri yöntemlerle yerel düzeye indirgenmesi gerektiğini düşünüyorum. Öte yandan, şu anda federal hükümeti güçlendirmek istiyorum. Çünkü biz bu dünyada yaşıyoruz, başka bir dünyada değil. Ve bu dünyada, insanların tasarladığı herhangi bir şey kadar tiranlığa ve totaliterliğe yakın olan devasa özel güç yoğunlaşmaları var.

Bu özel güçler karşısında elde edilmiş hakları savunmanın ya da kapsamlarını genişletmenin tek bir yolu var, o da halka karşı bir şekilde sorumlu olan ve halkın gerçekten etkileyebileceği tek gayrimeşru güç biçimini korumak.”

<right>
[[http://web.archive.org/web/20120305065733/https:/www.findarticles.com/p/articles/mi_m1295/is_n3_v60/ai_18049699/pg_2][You say you want a devolution.]]
</right>
</quote>

Chomsky’nin devleti “özel güç yoğunlaşmalarına” karşı önemli bir siper olarak görmesi, Chomsky’nin kendi araştırmalarının da defalarca doğruladığı gibi, devletin tarihsel olarak bu tür yoğunlaşmaların başlıca destekleyicisi olduğu göz önüne alındığında, başlangıçta şaşırtıcı görünebilir. Ancak Chomsky’nin kastettiği şey, devletin şu anda genellikle bir siper görevi görmesinden ziyade, bunu yapmasının sağlanabileceğidir; eğer kılıcı da (devlet gücü) olan çok daha güçlü bir rakiple (özel güç) karşı karşıyaysanız, kılıcı ele geçirip ona karşı kullanmaya çalışmak, kılıcı yok etmekten daha iyidir.

<quote>
“Hükümet iyi huylu olmaktan çok uzak- bu doğru. Öte yandan, en azından kısmen hesap verebilir ve biz onu ne kadar iyi huylu hale getirirsek o kadar iyi huylu olabilir.

İyi huylu olmayan (aslında son derece zararlı olan) şey, bahsetmediğiniz bir şeydir- oldukça yoğunlaşmış ve şu anda büyük ölçüde ulus ötesi olan iş gücü. İş dünyasının gücü iyi huylu olmaktan çok uzaktır ve tamamen hesap verilemezdir. Hayatlarımız üzerinde muazzam bir etkisi olan totaliter bir sistemdir. Aynı zamanda hükümetin iyi huylu olmamasının da ana nedenidir.”

<right>
[[http://web.archive.org/web/20060927091706/http:/www.spunk.org/texts/writers/chomsky/sp001178.txt][On Gun Control]]
</right>
</quote>

Burada itiraz etmek istediğim iki varsayım var.

Birincisi, Chomsky özel sektörün hükümet üzerindeki etkisinin “hükümetin iyi huylu olmamasının ana nedeni” olduğunu varsayıyor. Buna neden inanıyor ki? Tekel gücü, bu gücü yönlendirenler ister çoğunlukla devlet aygıtının içinde ister dışında olsun, istismara davetiye çıkarma eğilimindedir. Chomsky, ipleri elinde tutan kötü kapitalistler olmadan hükümetin bu kadar zararsız olacağını düşünüyorsa, neden uzun vadede bile onu ortadan kaldırmak istiyor?

İkinci olarak, Chomsky devlet gücünün “kısmen hesap verebilir”, iş gücünün ise “tamamen hesap veremez” olduğunu varsayıyor. Öncelikle, burada devlet gücünün hesap verebilirliği ile gerçekte var olan, devlet tarafından desteklenen iş gücünün hesap verebilirliğinin mi yoksa devlet desteği olmadan iş gücünün hesap verebilirliğinin mi karşılaştırıldığından emin değilim. Ama eğer ilkiyse, o zaman bu karşıtlık, doğru olsa bile, devletin ortadan kaldırılmasına direnmek için hiçbir gerekçe sağlamaz; X + Y’nin tek başına X’ten daha tehlikeli olması, X’i savunmak için iyi bir neden değildir. Bu karşıtlık, ancak devlet desteği olmadan iş dünyası yine de devletten daha az hesap verebilir olacaksa, devletin savunulmasıyla ilgilidir. Ve burada devletin- demokratik bir devlet bile olsa- gerçek anlamda özel bir işletmeden çok daha az hesap verebilir olduğu açıktır.

Sonuçta, bir işletme emeğinizi ve/veya mal varlığınızı ancak bunları vermeyi kabul ederseniz alabilirken, bir hükümet bunları zorla alabilir. Elbette mevcut temsilcilerinizi görevden almaya çalışabilirsiniz, ancak bu sadece birkaç yıllık aralıklarla ve komşularınızın %51’ini aynı şeyi yapmaya ikna etmeniz halinde mümkündür; oysa bir işletmeyle olan ilişkinizi istediğiniz zaman ve başkalarını ikna etmeden sonlandırabilirsiniz. Dahası, her aday bir politika paketi sunuyor, oysa özel teşebbüste örneğin A marketinin sebzelerini ve B marketinin etlerini seçebiliyorum.

David Friedman bu zıtlığı aydınlatmaktadır:

<quote>
“Bir tüketici piyasadan bir ürün satın aldığında alternatif markaları karşılaştırabilir. … Bir politikacıyı seçtiğinizde, vaatlerden başka bir şey satın almazsınız. … 1968 model Ford’ları, Chrysler’ları ve Volkswagen’leri karşılaştırabilirsiniz ama hiç kimse 1968’deki Nixon yönetimini aynı yıldaki Humphrey ve Wallace yönetimleriyle karşılaştıramaz. Sanki 1920’den 1928’e kadar sadece Ford’larımız, 1928’den 1936’ya kadar Chrysler’larımız varmış ve sonraki dört yıl boyunca hangi firmanın daha iyi araba yapacağına karar vermek zorundaymışız gibi….

Bir tüketici sadece bir seçmenden daha iyi bilgiye sahip olmakla kalmaz, aynı zamanda bu bilgi onun daha fazla işine yarar. Eğer alternatif otomobil markalarını araştırır… benim için en iyisinin hangisi olduğuna karar verir ve onu satın alırsam… onu elde ederim. Alternatif politikacıları araştırır ve buna göre oy verirsem, çoğunluğun oy verdiği şeyi alırım. …

Otomobilleri de hükümetleri satın aldığımız gibi satın aldığımızı hayal edin. On bin kişi bir araya gelir ve her biri kendi tercih ettiği araba için oy kullanmayı kabul eder. Hangi araba kazanırsa kazansın, on bin kişinin her biri onu satın almak zorunda kalacaktı. Hangi arabanın en iyisi olduğunu bulmak için ciddi bir çaba sarf etmek hiçbirimize para kazandırmaz; ben neye karar verirsem vereyim, arabam grubun diğer üyeleri tarafından benim için seçilecektir. … Siyasi pazarda ürünleri bu şekilde satın almak zorundayım. Alternatif ürünleri kıyaslayamadığım gibi, kıyaslayabilseydim bile bunu yapmak için harcayacağım zamana değmezdi.”

<right>
[[https://www.amazon.com/Machinery-Freedom-Guide-Radical-Capitalism/dp/0812690699/praxeologynet-20][The Machinery of Freedom]]
</right>
</quote>

Demokratik hükümet tarafından sağlanan “hesap verebilirlik”, piyasa tarafından sağlanan hesap verebilirlikle karşılaştırıldığında gülünç gözükmektedir. Görünüşe göre oy pusulasının başlıca işlevi, halkı bir şekilde yetkili olduklarına ikna ederek daha çekilebilir hale getirmektir.

Bunların hiçbiri Chomsky için yeni bir şey olmayacaktır, ne de olsa kendisi de buna işaret etmiştir:

<quote>
“Şu anki haliyle seçim süreci, halkın arada bir neredeyse birbirinin aynısı olan ticari güç temsilcileri arasından seçim yapmasına izin verilmesinden ibaret. Bu bir diktatöre sahip olmaktan daha iyidir, ancak demokrasinin çok sınırlı bir şeklidir. Nüfusun çoğu bunun farkında ve katılmıyor bile. … Ve tabii ki seçimler neredeyse tamamen satın alınıyor. Son kongre seçimlerinde kazananların yüzde 95’i rakiplerinden daha fazla para harcadı ve kampanyalar ezici bir çoğunlukla şirketler tarafından finanse edildi.”

<right>
[[https://web.archive.org/web/20070206062407/weeklywire.com/ww/02-21-00/alibi_feat.html][Chomsky’s Other Revolution]]
</right>
</quote>

Evet, aynen öyle. Peki Chomsky’nin demokratik devlete olan inancının temeli nedir?

Chomsky, benim piyasa hesap verebilirliğini savunmamın, bu tür bir “hesap verebilirliğin” dolarlarla oy vermeyi içerdiği, böylece zenginlerin yoksullardan daha fazla oya sahip olduğu- oysa demokratik bir devlette herkesin eşit oya sahip olduğu- gerçeğini göz ardı ettiğine itiraz edebilir. Ancak mevcut zenginlik eşitsizliklerinin sistematik devlet müdahalesine olan nedensel bağımlılığını- ve hükümetin sahip olmadığı kaynakların yönünü kontrol ederek zenginlerin gücünü artırdığı gerçeğini- bir kenara bıraksak bile, kişinin ne kadar az doları olursa olsun, bu dolarlarla oy verdiğinde bir şeyler elde ettiği, oy pusulalarıyla oy verdiğinde ise çoğunluk ile birlikte oy vermediği sürece hedeflediği hiçbir şeyi elde edemediği gerçeği hala geçerlidir. Hangisi daha az demokratiktir- kişinin oyunun etkinliğinin kişinin kaynaklarına göre değiştiği bir sistem mi, yoksa nüfusun %49’unun hiçbir etkin oy hakkına sahip olmadığı bir sistem mi?

Chomsky, “iş dünyasının gücü” olarak adlandırdığı şeyin önemli ölçüde hükümet müdahalesine bağlı olduğunun farkında değildir- çünkü bu ilişkiyi belgelemek için herkes kadar çok şey yapmıştır. Kendisinin de belirttiği gibi:

<quote>
“Yoğunlaşmış gücün herhangi bir biçimi, her ne olursa olsun, halkın demokratik denetimine ya da bu bağlamda piyasa disiplinine tabi olmak istemeyecektir. Şirket zenginliği de dahil olmak üzere güçlü kesimler, en azından kendileri için işleyen piyasalara karşı oldukları gibi, işleyen demokrasiye de doğal olarak karşıdırlar.”

<right>
Demokrasi Üzerine Düşünceler (vurgular eklenmiştir)
</right>
</quote>

Madem şirket elitleri serbest piyasadan bu kadar korkuyor, Chomsky neden onları serbest piyasanın içine atmak konusunda bu kadar isteksiz?

Belki de Chomsky’nin görüşü, devletin bu özel güç yoğunlaşmalarını yaratmak için gerekli olmasına rağmen, bunları sürdürmek için gerekli olmadığı, bu nedenle oyunun bu noktasında devleti bastırmanın iş gücünü sağlam bırakacağı yönündedir. Bu çılgınca bir görüş değil, ancak tartışmaya ihtiyacı var. Sonuçta, büyük şirketler adına sistematik devlet müdahalesi sadece Yaldızlı Çağ’da, İlerici Dönem’de ya da Yeni Anlaşma’da olan bir şey değil; kitlesel olarak ve durmaksızın devam ediyor. Özel sektörün gücünün sadece ve sadece devlet desteğine bağlı olduğunu iddia etmiyorum (aslında bunu reddediyorum); ancak tüm bu devlet desteğinin sadece gereksiz olduğuna inanmak zor, eğer bu devlet desteğini kaldırmak iş dünyasının gücünü kayda değer ölçüde zayıflatmayacaksa öyle olmalı.

Chomsky (Answers to Eight Questions on Anarchism eserinde) kendisini “bir dizi konuda kendilerini anarko-kapitalist olarak gören insanlarla önemli ölçüde hemfikir” bulmasına ve “rasyonaliteye bağlılıklarına hayranlık duymasına” rağmen, yine de anarşizmin serbest piyasa versiyonunu “uygulandığı takdirde insanlık tarihinde çok az benzeri olan tiranlık ve baskı biçimlerine yol açacak doktriner bir sistem” olarak gördüğünü söylemiştir. Neden mi? Çünkü “iktidar sahibi ile açlık çeken tebaası arasındaki ‘özgür sözleşme’ fikri hastalıklı bir şakadır.”

Ancak bu argüman açıkça soru işareti yaratmaktadır. Chomsky tam da tartışmalı olan noktayı, yani zenginler adına devlet müdahalesi olmasaydı ekonominin “hükümdarlar” ve “açlık çeken tebaa” olarak ikiye bölüneceğini varsayıyor. Piyasa anarşistlerinin (başka bir yerde açıkladığım nedenlerden dolayı “anarko-kapitalist” teriminden kaçınmayı tercih ediyorum) bazen -bana göre yanlışlıkla- ideal ekonomilerini, sadece devletin olmadığı mevcut ekonomimizdeki zenginlik ve emek rollerinin dağılımına çok benzeyen bir şekilde tanımladıkları doğrudur. Ama Chomsky neden onların sözüne inansın ki? Eğer devlet gerçekten de “iktidar” adına ve “açlık çeken özne “ye karşı kitlesel ve sistematik müdahalelerde bulunuyorsa- ki Chomsky bunu kabul etmelidir, çünkü araştırması bunu açıkça göstermektedir- neden bu müdahale sona erdiğinde güç dengesizliğinin değişmeden kalmasını beklesin ki?

Chomsky sadece anarşinin kaynaklarını küçümsemekle kalmıyor, aynı zamanda devletin hizmet edebilirliğini de abartıyor gibi görünüyor. Sanki devlet şu anda pek çok kötü şey yapıyor olsa da, daha fazla insan doğru oy verirse tüm bunlar değişebilirmiş gibi yazıyor. Şimdi, insanların farklı oy kullanmasının hükümetin ne kadar kötü olduğu konusunda bir fark yaratabileceği yeterince doğru. (1932’de yeterince Alman farklı oy kullansaydı, daha az kötü bir rejime sahip olabilirlerdi). Yine de, günün sonunda, zorlayıcı bir tekelde yanlış olan şey, onu yanlış insanların yönetmesi değil, daha ziyade- içsel adaletsizliğini bir kenara bırakırsak- böyle bir tekelin, kaçınmanın hiçbir yolu olmayan (sorunun kaynağı olan tekeli ortadan kaldırmak dışında, bu durumda sahip olduğunuz şey artık bir devlet değildir) teşvik edici ve bilgi sapkınlıklarını beraberinde getirmesidir.


