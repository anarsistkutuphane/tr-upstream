#DELETED Reason for deletion: Machine translation (DeepL: after June 2022, Google Translate: before June 2022) has been detected. - https://mirror.anarsistkutuphane.org/c4ss.org/analyze/
#title Fikri Mülkiyet Haklarına Karşı Liberteryen Bir Duruş
#author Roderick Long
#SORTtopics sol piyasa anarşizmi, mülkiyet, telif hakkı
#date Sonbahar, 1995
#source 06.09.2022 tarihinde şuradan alındı: [[https://c4ss.org/content/57288][c4ss.org]]
#lang tr
#pubdate 2022-09-06T21:54:29
#notes Çeviri: Faruk Pak <br>İngilizce Aslı: [[https://c4ss.org/content/14857][The Libertarian Case Against Intellectual Property Rights]]



<quote>
İfade kanallarının mevcut konjonktürden çıkar sağlayan kişiler tarafından bu denli büyük ölçüde kontrol edildiği bir toplumda telif hakları kanununun topluma sağladığı sözde faydaları eleştirecek bir görüşün kamuya açık bir şekilde ifade edilme ihtimalini düşünmek bayağı ilginç olur.

<right>
Friedrich A. Hayek, "Entelektüeller ve Sosyalizm"
</right>
</quote>

*** Liberteryenlerin Kendileri Arasındaki Bir Fikir Uyuşmazlığı

Fikri mülkiyet haklarının (telif hakları, patentler vb.) statüsü, liberteryenleri uzun süredir bölmekte olan bir konudur. Herbert Spencer, Lysander Spooner ve Ayn Rand gibi liberteryen aydınlar fikri mülkiyet haklarının güçlü destekçileri olurken, Thomas Jefferson bu konuda kararsız kalmış, geçen yüzyılda Benjamin Tucker ve günümüzde Tom Palmer gibi radikal liberteryenler fikri mülkiyet haklarını bütünüyle reddetmişlerdir.

İlk listedeki türden liberteryenler sözde bir fikri mülkiyet hakkıyla karşı karşıya kaldıklarında, bunu bir bireyin emeğinin ürünü üzerinde hak iddiası olması olarak değerlendirirler. İkinci listedeki türden liberteryenler sözde bir fikri mülkiyet hakkıyla karşılaştıklarında ise devlet tarafından devredilmiş ve hak edilmemiş bir tekel ayrıcalığının bir örneğini görürler.

Eskiden ilk gruptaydım. Şimdi ikinci gruptayım. Fikri mülkiyet haklarının neden haksız olduğunu düşündüğümü ve şu anda fikri mülkiyet hakları yoluyla aranan meşru amaçların başka, gönüllü yollarla nasıl güvence altına alınabileceğini açıklamak istiyorum.

*** Tarihsel Argüman

Fikri mülkiyet haklarının lekeli bir geçmişi var. Başlangıçta hem patentler hem de telif hakları salt ve basit tekel ayrıcalıklarıydı. Bir matbaaya kraliyet emriyle bir “telif hakkı” verilebilirdi, bu da belirli bir bölgede sadece kendisinin kitap veya gazete basmasına izin verildiği anlamına geliyordu; telif hakkının yazardan kaynaklandığına dair bir karine yoktu. Aynı şekilde, siyasi gücü olanlara, icatla bir ilgileri olup olmadığına bakılmaksızın, bir mal üzerinde “patent”, yani münhasır bir tekel verilebilirdi. Fikri mülkiyet hakları, yaratıcıların çabalarının meyveleri üzerindeki haklarını koruma gayretinden değil, hükümet ayrıcalıklarından ve hükümet korumacılığından kaynaklanıyordu. Patentlerin kaldırılması, 17. yüzyıl Leveller’larının (tartışmasız ilk özgürlükçüler) toplanma çığlıklarından biriydi.

Şimdi bu tek başına, bugün bildiğimiz şekliyle fikri mülkiyet haklarında yanlış bir şey olduğunu kanıtlamaz. Kötü bir geçmiş, herhangi bir olguya karşı kesin bir argüman değildir; birçok değerli ve değerli şey şüpheli başlangıçlardan ortaya çıkmıştır. (Nietzsche bir keresinde, geçmişine bakmayı gerektirecek kadar muhteşem hiçbir şey olmadığını söylemiştir). Ancak fikri mülkiyet haklarının devlet baskısından kaynaklandığı gerçeği, en azından onları benimsemeden önce duraksamamıza ve çok dikkatli olmamıza neden olmalıdır.

*** Etik Argüman

Etik olarak, her türlü mülkiyet hakkı, bireylerin kendi hayatlarını kontrol etme hakkının uzantısı olarak gerekçelendirilmelidir. Dolayısıyla, köle sahibi olma “hakkı” gibi bu ahlaki temelle çelişen sözde mülkiyet hakları geçersizdir. Benim görüşüme göre, fikri mülkiyet hakları da bu testi geçememektedir. Telif hakkı yasalarını ve benzerlerini uygulamak, insanların sahip oldukları bilgileri barışçıl bir şekilde kullanmalarını engellemektir. Eğer bilgiyi meşru bir şekilde elde ettiyseniz (örneğin bir kitap satın alarak), o zaman hangi gerekçeyle onu kullanmanız, çoğaltmanız, ticaretini yapmanız engellenebilir? Bu ifade ve basın özgürlüğünün ihlali değil midir?

Bilgiyi üreten kiĢinin bu bilgi üzerinde mülkiyet hakkı olduğu itiraz edilebilir. Ancak bilgi bir bireyin kontrol edebileceği somut bir şey değildir; diğer insanların zihinlerinde ve diğer insanların mülklerinde var olan bir <em>evrenseldir ve</em> bunlar üzerinde kaynak kişinin meşru bir egemenliği yoktur. Diğer insanlara sahip olmadan bilgiye sahip olamazsınız.

Diyelim ki ben bir şiir yazdım ve siz onu okuyup ezberlediniz. Bu şiiri ezberleyerek aslında beyninizde depolanmak üzere şiirin bir “yazılım” kopyasını yaratmış olursunuz. Ancak siz özgür ve özerk bir birey olarak kaldığınız sürece bu kopya üzerinde hiçbir hak iddia edemeyeceğim açıktır. Kafanızdaki bu kopya sizindir, başkasının değil.

Ama şimdi şiirimi yazıya dökmeye, beyninizde depolanan bilginin “basılı bir kopyasını” çıkarmaya başladığınızı varsayalım. Kullandığınız malzemeler – kalem ve mürekkep – sizin kendi malınızdır. Kullandığınız bilgi şablonu – yani şiirin depolanmış hafızası – da sizin mülkiyetinizdedir. Öyleyse bu malzemelerden ürettiğiniz basılı kopya nasıl olur da yayınlamak, satmak, uyarlamak ya da istediğiniz gibi davranmaktan başka bir şey olabilir?

Bir fikri mülkiyet öğesi bir tümeldir. Platonik Formlara inanmadığımız sürece, evrenseller, çok sayıdaki tikel örneklerinde gerçekleştikleri ölçüde var olmazlar. Buna göre, <em>Atlas Shrugged’ın</em> her bir fiziksel kopyasına sahip olma iddiası anlamına gelmediği sürece, herhangi birinin Atlas <em>Shrugged</em>’ın metnine sahip olduğunu nasıl iddia edebileceğini anlamıyorum. Ancak <em>Atlas Silkindi’nin</em> kitaplığımdaki kopyası Ayn Rand’a ya da onun mirasına ait değil. Bana ait. Onu ben satın aldım. Parasını ben ödedim. (Rand muhtemelen satıştan telif ücreti almıştır ve eminim ki onun izni olmadan satılmamıştır!)

Patentlere karşı ahlaki durum daha da nettir. Patent aslında bir doğa kanunu üzerinde sahiplik iddiasıdır. Newton kalkülüsün ya da yerçekimi yasasının sahibi olduğunu iddia etseydi ne olurdu? Keşfettiği ilkelerden birini her kullandığımızda onun mirasına bir ücret ödemek zorunda kalır mıydık?

<quote>
… patent tekeli … mucitleri … hizmetlerinin emek karşılığının çok üzerinde bir ödülü halktan zorla almaya yetecek kadar uzun bir süre rekabete karşı korumaktan ibarettir- başka bir deyişle, belirli kişilere Doğanın yasaları ve olguları üzerinde yıllarca sürecek bir mülkiyet hakkı ve herkese açık olması gereken bu doğal zenginliğin kullanımı için başkalarından haraç alma yetkisi vermektir.

<right>
(Benjamin Tucker, <em>Instead of a Book, By a Man Too Busy to Write One: A Fragmentary Exposition of Philosophical Anarchism</em> (New York: Tucker, 1893), p. 13. -Kitabın Türkçesi bulunmamakta)
</right>
</quote>

<em>Patent savunucuları, patent yasalarının keşiflerin</em> değil, sadece <em>icatların</em> mülkiyetini koruduğunu iddia etmektedir. (Aynı şekilde, telif hakkı savunucuları da telif hakkı yasalarının fikirlerin kendisini değil, sadece fikirlerin <em>uygulamalarını koruduğunu iddia etmektedir</em>). Ancak bu ayrım yapay bir ayrımdır. Doğa kanunları farklı genellik ve özgüllük derecelerine sahiptir; bakırın elektriği iletmesi bir doğa kanunu ise, bu kadar bakırın, bu şekilde düzenlenmiş diğer malzemelerle birlikte çalışabilir bir pil oluşturması da daha az bir doğa kanunu değildir. Ve böyle devam eder.

Bir vadinin dibinde mahsur kaldığınızı varsayalım. Kılıç dişli kaplanlar açlıkla yaklaşıyor. Tek umudunuz yakın zamanda icat ettiğim bir havaya yükselme cihazını hızlıca inşa etmek. Nasıl çalıştığını biliyorsunuz, çünkü bu konuda verdiğim halka açık bir konferansa katıldınız. Ve vadide etrafta gördüğünüz malzemelerle oldukça hızlı bir şekilde inşa etmek çok kolay.

Ama bir sorun var. Havalandırma cihazımın patentini aldım. Sadece yaptığım bireysel modelin değil, evrensel olanın da sahibiyim. Dolayısıyla, benim mülkümü kullanmadan kaçış aracınızı inşa edemezsiniz. Ve ben, yaşlı bir cimri olarak, izin vermeyi reddediyorum. Ve böylece kaplanlar iyi yemek yer.

Bu, fikri mülkiyet kavramıyla ilgili ahlaki sorunu vurgulamaktadır. Havaya kaldırma cihazım üzerinde patent talep ederek, kendi bilginizi amaçlarınız doğrultusunda kullanmanıza izin verilmediğini söylüyorum. Hangi hakla?

Patentlerle ilgili bir başka sorun da, doğa kanunları söz konusu olduğunda, oldukça spesifik olanlar da dahil olmak üzere, bağımsız çalışan ancak aynı araştırma geçmişinden yararlanan iki kişinin aynı buluşu (keşfi) bağımsız olarak ortaya çıkarma ihtimalinin oldukça yüksek olmasıdır. Yine de patent yasası, patent ofisine ilk ulaşan mucide keyfi olarak münhasır haklar tanıyacak; ikinci mucit, fikri kendi başına geliştirmiş olmasına rağmen, buluşunu pazarlaması yasaklanacaktır.

Ayn Rand bu itirazı çürütmeye çalışır:

<quote>
Patent kanunlarına bir itiraz olarak, bazı insanlar iki mucidin bağımsız olarak aynı icat üzerinde yıllarca çalışmasını ele alırlar, fakat birisi bir saat veya bir gün farkla diğerini yenerek kendine ait bir tekel elde ederken, kaybedenin çalışması tama men boşa gitmiş olacaktır. Bu tip itiraz, potansiyel olanı aktüel olanla eşdeğer tutma hatasına dayanmaktadır. Bir kişinin ilk olabileceği gerçeği onun ilk olmadığı gerçeğini değiştirmez. Konu ticari haklardan biri olduğu için, bu tip bir durumda kaybeden kişi, başkalan ile iş yaparken bir rakibin yarışı kazanabileceği gerçeğini kabul etmek zorundadır, bu diğer tüm rekabet tiplerinde de geçerlidir.

<right>
(Ayn Rand, <em>Kapitalizm: Bilinmeyen İdeal)</em>
</right>
</quote>

Ancak bu cevap yeterli olmayacaktır. Rand, patent ofisine ilk ulaşma rekabetinin diğer ticari rekabet türleri gibi olduğunu öne sürüyor. Örneğin, siz ve benim aynı iş için rekabet ettiğimizi ve sırf işverene benden önce ulaştığınız için işe alındığınızı varsayalım. Bu durumda, oraya benden önce gitmiş olmam bana iş üzerinde herhangi bir hak vermez. Çünkü en başta benim iş üzerinde hiçbir <em>hakkım yoktur.</em> İşe girdiğinizde ise, bu iş üzerindeki hak iddianız yalnızca işvereninizin sizi işe almayı seçmiş olmasına bağlıdır.

Ancak patentler söz konusu olduğunda, hikayenin farklı olması gerekir. Bir mucidin X üzerinde patent talebinde bulunmasının temelinde X’i icat etmiş olması yatmaktadır (Aksi takdirde, X’i duyup duymadıklarına bakılmaksızın, patent ofisine tesadüfen gelen herkese neden X üzerinde patent hakkı vermeyelim?) Bir kişinin buluşunu patent ofisine kaydettirmesi, onu <em>yaratmak için</em> değil, hakkını <em>kaydetmek içindir.</em> Dolayısıyla, patent ofisine ikinci gelen kişinin de ilk gelen kişi kadar hakkı olduğu sonucu çıkar – ve bu kesinlikle tüm patent kavramının bir reductio ad absurdum’udur.

*** Ekonomik Argüman

Olağan mülkiyet hakları için ekonomik durum kıtlığa bağlıdır. Ancak bilgi, teknik olarak konuşursak, gerekli anlamda kıt bir kaynak değildir. Eğer A bir maddi kaynağı kullanırsa, bu B’nin elindeki kaynağı azaltır, dolayısıyla kimin neyi ne zaman kullanacağını belirlemek için bazı yasal mekanizmalara ihtiyacımız vardır. Ancak bilgi böyle değildir; A bilgi edindiğinde, bu B’nin payını azaltmaz, bu nedenle mülkiyet haklarına gerek yoktur.

Bazıları, sanatçılara ve mucitlere yaratmaları için mali teşvik sağlamak amacıyla bu tür haklara ihtiyaç duyulduğunu söyleyecektir. Ancak tarihteki büyük yenilikçilerin çoğu telif hakkı yasalarından yararlanmadan faaliyet göstermiştir. Aslında, yeterince sıkı telif hakkı yasaları onların başarılarını imkansız hale getirirdi: Euripides ve Shakespeare gibi büyük oyun yazarları hayatları boyunca hiçbir zaman orijinal bir olay örgüsü yazmamışlardır; başyapıtlarının hepsi başkaları tarafından yazılmış hikayelerin uyarlamaları ve geliştirmeleridir. Bach, Tchaikovsky ve Ives gibi en büyük bestecilerimizin çoğu, eserlerine başkalarının bestelerini dahil etmiştir. Bu tür bir kendine mal etme, uzun zamandır meşru sanatsal özgürlüğün ayrılmaz bir parçası olmuştur.

Telif hakkı koruması sağlanmadığı sürece yazarların yazmaya motive olmayacakları inandırıcı mı? Pek değil. Her gün yazarları tarafından internete yüklenen ve dünyadaki herkesin ücretsiz olarak ulaşabileceği yüz binlerce makaleyi düşünün.

Yayıncıların, rakip bir yayıncının girip tekellerini yıkacağı korkusuyla telif hakkı olmayan eserleri yayınlama zahmetine girmeyecekleri inandırıcı mı? Pek değil. Neredeyse 1900’den önce yazılmış tüm eserler kamu malıdır, ancak 1900 öncesi eserler hala yayınlanmakta ve hala satılmaktadır.

Telif haklarının olmadığı bir dünyada yazarların çalışmalarının karşılığını almaktan mahrum kalacakları inandırıcı mıdır? Yine, pek olası değil. 19. yüzyılda İngiliz yazarların Amerikan yasaları kapsamında telif hakkı koruması yoktu, ancak yine de Amerikalı yayıncılardan telif ücreti alıyorlardı.

Herbert Spencer otobiyografisinde, fikri mülkiyet haklarına duyulan ihtiyacı örneklemesi beklenen bir hikaye anlatır. Spencer yeni bir tür hastane yatağı icat etmişti. Hayırseverlik güdüleriyle, icadının patentini almak yerine bunu insanlığa armağan etmeye karar verdi. Ancak bu cömert plan geri tepti: hiçbir şirket yatağı üretmeye yanaşmadı, çünkü garantili bir tekelin yokluğunda, rekabet tarafından baltalanabilecek herhangi bir ürüne para yatırmayı çok riskli buldular. Bu durum patent yasalarına duyulan ihtiyacı göstermiyor mu?

Ben öyle düşünmüyorum. Öncelikle, Spencer’ın davası abartılmış görünüyor. Ne de olsa, şirketler sürekli olarak kimsenin özel bir patente sahip olmadığı ürünler (yataklar, sandalyeler, <em>vb.</em>) üretmektedir. Ama neyse; Spencer’ın hikayesini tartışmadan kabul edelim. Bu neyi kanıtlıyor?

Spencer’ın yatağını sermayeleri için başka kullanımlar lehine reddeden şirketlerin, tekel olacakları bir meta üretmek ile <em>tekel olmayacakları bir meta üretmek</em> arasında seçim yaptıklarını hatırlayın. Bu seçimle karşı karşıya kaldıklarında, daha az riskli bir seçenek olarak patentli metayı tercih ettiler (özellikle de aynı şekilde tekel sahibi olan diğer şirketlerle rekabet etmek zorunda oldukları gerçeği ışığında). Dolayısıyla patent yasalarının varlığı, diğer korumacı yasalar gibi, patentli mala patentsiz rakibine karşı haksız bir rekabet avantajı sağlamıştır. O halde Spencer’ın tarif ettiği durum, patent yasalarının kendisinin bir eseridir! Patent yasalarının olmadığı bir toplumda, Spencer’ın hayırsever yatağı diğer ürünlere kıyasla hiçbir dezavantaja sahip olmazdı.

*** Bilgi Temelli Argüman

Hiçbir zaman haklı gösterilmese de, telif hakkı yasaları muhtemelen şimdiye kadar topluma çok fazla zarar vermemiştir. Ancak Bilgisayar Çağında, artık insanlığın ilerlemesine giderek daha pahalıya mal olan prangalar haline geliyorlar.

Örneğin, mümkün olduğunca çok sayıda kitabı elektronik formata aktarmak ve internet üzerinden ücretsiz olarak erişilebilir kılmak için kar amacı gütmeyen harika bir gönüllü çabası olan Project Gutenberg’i düşünün. (Project Gutenberg hakkında bilgi için proje direktörü Michael S. Hart ile hart@vmd.cso.uiuc.edu adresinden iletişime geçebilirsiniz.) Ne yazık ki, bugüne kadar yapılan çalışmaların çoğu 20. yüzyıl öncesine aittir – telif hakkı yasasının zorluklarından kaçınmak için. Dolayısıyla, günümüzde telif hakkı yasaları bilginin erişilebilirliğini teşvik etmek yerine kısıtlamak için çalışmaktadır. (Ve Kongre, yayıncılık ve kayıt endüstrilerinin emriyle, şu anda telif hakkı korumasını yaratıcının ölümünden sonra neredeyse bir yüzyıl sürecek şekilde genişletmek için harekete geçiyor, böylece var olan bilginin yalnızca küçük bir kısmının kamuya açık olmasını sağlıyor).

Daha da önemlisi, modern elektronik iletişim, telif hakkı yasalarını uygulanamaz hale getirmeye başlıyor; ya da en azından, hükümetin interneti ele geçirmesi dışında hiçbir yolla uygulanamaz – ve insanlığın geleceğine yönelik böylesi tüyler ürpertici bir tehdit, açıkça hastalıktan çok daha kötü bir tedavi olacaktır. Herhangi bir bireyin bir belgenin binlerce kopyasını anında oluşturabildiği ve bunları tüm gezegene gönderebildiği bir dünyada telif hakkı yasaları, herkesin röntgen görüşüne sahip olduğu bir dünyada röntgencilere ve dikizcilere karşı çıkarılan yasalar kadar demode olacaktır.

*** İlk Tolkien Hikayesi

İşte fikri mülkiyet yasalarının neden olabileceği gereksiz huzursuzluklardan bazılarını gösteren bir hikaye.

Birkaç yıl önce avangart film animatörü Ralph Bakshi, J. R. R. Tolkien’in klasik fantezi üçlemesi <em>Yüzüklerin Efendisi’nin</em> bir filmini yapmaya karar verdi. Daha doğrusu, üçlemeyi iki filme bölmeye karar verdi, çünkü eser gerçekten de tek bir filme kolayca sığmayacak kadar uzun.

Bakshi <em>Yüzüklerin Efendisi</em> (Birinci Bölüm) ile başladı. Bu film üçlemenin ilk cildini ve ikinci cildin bir kısmını kapsıyordu. İkinci film, ikinci cildin geri kalanını ve ardından üçüncü cildin tamamını kapsayacaktı. O halde ilk filmi yapmak için Bakshi’nin ilk iki cildin haklarını satın alması gerekiyordu ve o da (ya da muhtemelen stüdyosu) bunu yaptı.

Ancak Bakshi (muhtemelen ilk filmin finansal açıdan beklenenden daha az başarılı olması nedeniyle) ikinci filmi çekmeye bir türlü fırsat bulamadı. Başka bir stüdyo olan Rankin-Bass devreye girdi. Rankin-Bass, Tolkien’in daha önceki romanı <em>Hobbit’in</em> animasyon TV filmini yapmıştı ve aynı şeyi <em>Yüzüklerin Efendisi’nin</em> Bakshi tarafından çekilmeyen ikinci bölümü için de yapmak istiyorlardı.

Ama bir sorun vardı. Bakshi’nin stüdyosu üçlemenin ilk iki cildinin haklarına sahipti. Sadece üçüncü cildin hakları mevcuttu. Dolayısıyla Rankin-Bass’ın devam filmi (<em>Kralın Dönüşü</em> olarak yayınlandı) zorunlu olarak sadece üçüncü cildi kapsıyordu. Bakshi’nin ikinci ciltte filme alınmadan bıraktığı olaylar basitçe kayboldu. (İlk iki ciltteki olaylara geri dönüşlere bile izin verilmedi – <em>Hobbit’e geri dönüşler sorun değildi,</em> çünkü Rankin-Bass bunun haklarına sahipti).

Video katalogları artık <em>Hobbit</em>, <em>Yüzüklerin Efendisi</em> ve <em>Kralın Dönüşü’nü</em> birleşik bir paket olarak satıyor. Ancak kitaplara aşina olmayan izleyicilerin kafası biraz karışacaktır. Bakshi filminde, kötü büyücü Saruman hesaba katılması gereken bir güçtür; Rankin-Bass’ın devam filminde ise adı bile geçmez. Aynı şekilde, Bakshi filminin sonunda Frodo, Sam ve Gollum birlikte seyahat ederken, Rankin-Bass devam filminin başında onları herhangi bir açıklama olmaksızın ayrılmış halde buluyoruz. Cevaplar, Saruman’ın yenilgisini, Gollum’un Frodo’ya ihanetini, Sam’in Shelob’la savaşını ve Frodo’nun Orklar tarafından yakalanmasını ele alan ikinci cildin filme alınmamış kısmında yatmaktadır. Bunlar önemsiz olaylar değil. Ancak fikri mülkiyet yasaları sayesinde izleyicinin bunları bilmesine izin verilmiyor.

Bu bir felaket mi? Sanırım değil. Bir sanat eserinin estetik bütünlüğü ve sürekliliği, hukukun gerekleri uyarınca zedelenmiştir. Ama bu sadece bir animasyon TV filmiydi. Ne olmuş yani?

Ne olmuş yani, belki de. Ancak benim hikayem, telif hakkının sanatsal ifadenin siperi olduğu fikrine şüpheyle yaklaşılmasına hizmet ediyor. Bir sanat eseri başkaları tarafından yaratılan malzemenin yeniden işlenmesini içeriyorsa (tarihsel olarak çoğu sanat eserinde olduğu gibi), telif hakkı yasaları onu deli gömleğine sokabilir.

*** Fikri Mülkiyet Haklarına Alternatifler: Bazı Formülasyonlar

Şu ana kadar fikri mülkiyet haklarının hiçbir yararlı işlevi olmadığı izlenimini vermiş olabilirim. Benim pozisyonum bu değil. Telif hakları ve patentlerin araç olarak sunulduğu bazı amaçların tamamen meşru olduğunu düşünüyorum. Bununla birlikte, bu amaçlara başka araçlarla daha iyi hizmet edilebileceğine inanıyorum.

Diyelim ki çalışmanızı korsan olarak aldım, üzerine adımı yazdım ve benimmiş gibi pazarladım. Ya da çalışmanızı izniniz olmadan revize ettiğimi ve sizinmiş gibi pazarladığımı varsayalım. Yanlış bir şey yapmış olmaz mıyım?

Aksine, kesinlikle bir hak ihlali gerçekleştirdim. Ancak ihlal ettiğim haklar sizin değil, müşterilerimin haklarıdır. Bir kişinin eserini başka bir kişinin eseriymiş gibi satarak, soya bifteğini dana bifteği olarak sattığımda ya da tam tersini yaptığımda olduğu gibi, bu eseri satın alanları <em>dolandırmış oluyorum.</em> Tek yapmanız gereken bir kopya satın almak (böylece müşteri olduğunuzu iddia edebilirsiniz) ve ardından bana karşı toplu dava açmak.

Fikri ürünlerin yaratıcıları için başka yasal seçenekler de mevcuttur. Örneğin, birçok yazılım üreticisi programlarına kopya koruma önlemleri koyabilir ve koymaktadır ya da satın alanların yazılımı yeniden satmamayı kabul eden sözleşmeler imzalamalarını şart koşmaktadır. Benzer şekilde, ödemeli TV uydu yayıncıları sinyallerini karıştırır ve daha sonra şifre çözücü satarlar.

Elbette bu tekniklerin hiçbiri kusursuz değildir. Yeterince zeki bir korsan genellikle kopya korumalarını nasıl aşacağını veya bir sinyalin şifresini nasıl çözeceğini bulabilir. Ve şartlı satış sözleşmeleri, yazılımı başka bir şekilde elde eden üçüncü taraf kullanıcılara herhangi bir kısıtlama getirmez. Yine de bu tür şirketler, fikri ürünlerinin korsan kullanımını zorlaştırarak toplam korsan kullanım miktarını azaltmayı başarıyor, işlerini sürdürüyor ve kar elde ediyorlar.

Peki ya çalışmanızı izniniz olmadan ve size kârdan herhangi bir pay teklif etmeden pazarlamaya devam edersem? Bunda yanlış bir şey yok mu? Bu konuda hiçbir şey yapılamaz mı?

Açıklanan durumda, yaptığım şeyin <em>adaletsiz</em> olduğunu düşünmüyorum. Yani, kimsenin haklarının ihlali değil. Ama çok <em>adice</em>. Birinin haklarını ihlal etmek yanlış bir şey yapmanın tek yolu değildir; adalet tek erdem değildir.

Ancak adalet, meşru olarak <em>uygulanabilecek</em> tek erdemdir. Eğer eserinizin korsan satışından kazanç elde edersem, bana karşı meşru bir ahlaki talebiniz olur, ancak bu talep bir <em>hak değildir</em>. Bu nedenle, itaati sağlamak için meşru olarak zorlama kullanılamaz. Ancak bu, diğer gönüllü yöntemlerle uygulanamayacağı anlamına gelmez.

Fikri ürünlerin yaratıcılarına yönelik korumanın büyük bir kısmı sadece gönüllü uyum yoluyla sağlanabilir. Yazılım yaratıcılarının ürünlerini gelen herkese ücretsiz olarak sunduğu, ancak programı faydalı bulanların yazara cüzi bir ücret göndermesini talep ettiği shareware olgusunu düşünün. Muhtemelen, paylaşılan yazılım kullanıcılarının sadece küçük bir yüzdesi ödeme yapmaktadır; yine de, bu yüzde paylaşılan yazılım olgusunu devam ettirecek kadar büyük olmalıdır.

Ancak gönüllü uyumu sağlamanın daha organize ve etkili yolları da vardır. Aklımda üreticilerin meşru taleplerine saygı göstermeyenlerin boykot edilmesi stratejisi var. Özgürlükçü akademisyen Tom Palmer tarafından yürütülen araştırma, bu tür organize boykotların çok sayıda başarılı örneğini ortaya çıkarmıştır. Örneğin 1930’larda Moda Yaratıcıları Loncası, hükümetin zorlayıcı gücünden hiçbir yardım almadan elbise stillerini ve benzerlerini diğer tasarımcıların korsanlığına karşı korumayı başarmıştır.

Gönüllü boykot, entelektüel üreticilerin taleplerini korumak için aslında devletten çok daha güvenli bir araçtır, çünkü üreticilerin ekonomik gücü ile tüketicilerin ekonomik gücü arasında pragmatik bir denge kurmaya çalışırken, özel bir çabanın iki grubun meşru ahlaki talepleri – üreticilerin ücretlendirme konusundaki ahlaki talepleri ve tüketicilerin kolay erişilebilir bilgi konusundaki ahlaki talepleri – arasında benzer bir denge kurma olasılığı, piyasa teşviklerinden arındırılmış bir devlet tekelinden daha yüksektir.

Daha resmi bir şey kolayca hayal edilebilir. Orta Çağ’ın sonlarında, devlet tarafından sağlanan ticaret hukukunun yetersizliklerinden dolayı hayal kırıklığına uğrayan tüccarlar tarafından gönüllü bir mahkeme sistemi oluşturulmuştur. Kanun Tüccarı (“kanun” isim, “tüccar” sıfattır) olarak bilinen bu sistem, kararlarını yalnızca boykot yoluyla uyguluyordu ve yine de son derece etkiliydi. Fikri ürün üreticilerinin – yazarlar, sanatçılar, mucitler, yazılım tasarımcıları, vs – telif ve patent haklarını – daha doğrusu telif ve patent taleplerini – korumak için benzer bir mahkeme sistemi kurduklarını varsayalım (çünkü söz konusu manevi talepler, çoğu zaman meşru olsa da, özgürlükçü anlamda haklar değildir). Korsanlıkla suçlanan bireyler ve kuruluşlar gönüllü bir mahkemede davalarını savunma şansına sahip olurlar, ancak suçlu bulunurlarsa boykot cezasına çarptırılmak üzere korsanlıklarını durdurmaları ve vazgeçmeleri ve korsanlıklarının kurbanlarına tazminat ödemeleri gerekir.

Ya bu sistem çok ileri gider ve fikri mülkiyet yasalarının yaptığı gibi bilginin serbest akışını istenmeyen şekillerde kısıtlamaya başlarsa?

Bu kesinlikle bir olasılıktır. Ancak zorlayıcı yaptırımlarda tehlikenin gönüllü yaptırımlara kıyasla çok daha büyük olduğunu düşünüyorum. Rich Hammer’ın da belirtmeyi sevdiği gibi: dışlama gücünü gerçeklikten alır ve gücü gerçeklikle sınırlıdır. Bir boykot çabasının kapsamı arttıkça, boykot nedeniyle istedikleri bir şeyden mahrum bırakılanların hayal kırıklığına uğramış arzularının sayısı ve yoğunluğu da artacaktır. Bu gerçekleştikçe, bu arzuları karşılamanın (ve bunu yapmak için yüklü bir ücret ödemenin) faydalarının boykotu ihlal etmenin maliyetlerinden daha ağır bastığına karar verenlerin sayısında da buna karşılık gelen bir artış olacaktır. Kopya taleplerine yönelik çok sert ve kısıtlayıcı bir savunma, tüketici tercihleri kayasına dayanacaktır; çok gevşek bir savunma ise üretici tercihleri kayasına dayanacaktır.

*** İkinci Tolkien Hikayesi

Tolkien ve ünlü üçlemesi hakkında ikinci bir hikaye ile kapanışı yapayım. <em>Yüzüklerin Efendisi’nin Amerika</em> Birleşik Devletleri’nde basılan ilk baskısı Ace Books’tan korsan bir baskıydı. Şimdi unuttuğum nedenlerden dolayı Tolkien Ace’e karşı yasal işlem başlatamadı. Ancak Ballantine, <em>Yüzüklerin</em> Efendisi’nin yazar onaylı kendi resmi Amerikan baskısını çıkardığında, Tolkien Ace baskısına karşı bir kampanya başlattı. Ballantine baskısı, Tolkien’in arka kapakta yeşil bir kutu içinde bunun tek yetkili baskı olduğunu belirten ve yaşayan yazarlara saygı duyan herhangi bir okuyucuyu başka bir baskı satın almamaya çağıran bir notla piyasaya sürüldü. Dahası, Tolkien Amerikalı bir okurdan gelen hayran mektubuna her cevap verdiğinde, durumu açıklayan bir dipnot ekliyor ve mektubu alan kişiden Tolkien hayranları arasında Ace baskısının boykot edilmesi gerektiğini yaymasını istiyordu.

Ace baskısı Ballantine’dan daha ucuz olmasına rağmen, kısa sürede okuyucu kaybetti ve baskısı tükendi. Boykot başarılı oldu.

Tolkien hayranlarının ortalama okurlardan daha fanatik olma eğiliminde olduğu ve bu nedenle böyle bir boykot stratejisinin genel olarak böyle bir sadakati sağlamada başarılı olmasının beklenemeyeceği itiraz edilebilir. Yeterince doğru. Ama öte yandan, Tolkien’in boykotu tamamen <em>örgütsüzdü</em>; o zamanlar adı sanı duyulmamış bir İngiliz Orta Çağ dili ve edebiyatı profesörünün hayran mektuplarına el yazısıyla cevaplar karalamasından ibaretti. Organize bir boykotun ne kadar etkili olabileceğini bir düşünün!

<br>



