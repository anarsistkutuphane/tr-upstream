#title Sınıf Üzerine Notlar
#author Richard Gunn
#lang tr
#pubdate 2023-05-10T07:47:19
#topics marksizm, sınıf
#notes Çeviri: AIUX <br> İngilizce Aslı: [[https://libcom.org/article/notes-class][Notes on Class]]
#DELETED DeepL ile %89 oranında benzediği için metin reddedildi. https://web.archive.org/web/20230522064516/https://www.diffchecker.com/loxiksBM/


1. Marksizme göre sınıfın ne olmadığını söylemek,ne olduğunu söylemekten çok daha kolaydır. Sınıf, ortak noktalarıyla (gelir düzeyleri ya da yaşam tarzları, 'gelir kaynakları' [1], üretim araçlarıyla ilişkileri vb) belirlenen bir grup birey değildir. Örneğin proletarya, 'sermayeye karşı' bir grup olarak tanımlanamaz. [2] Sınıf, toplumsal ortamda yapısal ve ilişkisel olarak belirlenmiş bir " mekân " da değildir (bireylerin " işgal edebilecekleri " ya da bireyler olarak " enterpolasyona tabi tutulabilecekleri " bir mekân [3] vb.) Sınıfları sırasıyla birey toplulukları ya da " mekânlar" olarak ele alan "ampirist" ve "yapısalcı" Marksizmler arasındaki fark bu bakımdan önemsizdir. Daha uygun bir terim bulamadığım için, sınıfları ya gruplar ya da mekânlar olarak ele alan görüşü "sosyolojik" sınıf anlayışı olarak adlandıracağım.

2. Marksizm, sınıfı, sermayenin kendisi gibi (Marx 1965, s. 766), toplumsal bir ilişki olarak görür. Bir ilişki olan şey, ilişkisel olarak belirlenmiş bir grup bile olsa bir grup olamaz; bir grubun oluşturulabileceği ya da içinde durabileceği bir yer (ilişkisel olarak belirlenmiş bir yer) de olamaz. Bu tür görüşleri bir kenara bırakırsak, sınıfın ilişkinin kendisi (örneğin sermaye-emek ilişkisi) ve daha spesifik olarak bir mücadele ilişkisi olduğunu söyleyebiliriz. 'Sınıf' ve 'sınıf-ilişkisi' terimleri birbirinin yerine kullanılabilir ve 'bir' sınıf, belirli bir türden bir sınıf ilişkisidir.

3. Başka bir deyişle: sınıflar, toplumsal olarak önceden verilmiş olgular olarak mücadeleye girmezler. Aksine sınıf mücadelesi sınıfın temel öncülüdür. Daha da iyisi: sınıf mücadelesi sınıfın kendisidir. (Marx, Komünist Manifesto'nun giriş cümlesinde 'sınıf'ı bu şekilde tanıtır). 'Sınıf savaşımının 'sınıf'a içkin olması, Marx'ın sınıfın 'kendisi için' varoluşun - yani muhalif, mücadele eden varoluşun - sınıfın varoluşuna içkin olduğunu vurgularken işaret ettiği noktadır (Marx 1969, s.173).

4. Bir ilişki (bir mücadele ilişkisi) olarak sınıf anlayışına "Marksist" sınıf anlayışı olarak atıfta bulunacağım: bu noktada terminolojik tercihi kolaylıktan daha fazlası belirlemektedir. Bilindiği üzere, sosyolojik sınıf anlayışı, burjuva toplumundaki tüm bireylerin, 'kapitalistler' ve 'proleterler' olarak etiketlediği gruplara düzenli bir şekilde yerleştirilememesi gibi bir utançla karşı karşıyadır. Bu sıkıntı, sınıfların gruplar ya da yerler olarak kavranmasından kaynaklanır ve sosyolojik Marksizm bu sıkıntıdan kaçmak için 'orta sınıflar', 'orta tabakalar' gibi kategorilere başvurur: bu kategoriler artık ya da her şeyi kapsayan gruplardır ve kısacası, yoksul bir kavramsal şema tarafından üretilen teorik figürlerdir. Marksist sınıf anlayışı ise bu tür zorluklarla karşılaşmaz: sınıf ilişkisini (örneğin sermaye-emek ilişkisi) farklı bireylerin yaşamlarını farklı şekillerde yapılandıran bir ilişki olarak görür. Bu bağlamda Marksist ve sosyolojik sınıf anlayışları arasındaki karşıtlık çok kabaca şu şekilde gösterilebilir:
<br>
(Yukarıdaki resme bakınız.)

En azından bu gösterim kabadır çünkü sermaye-emek ilişkisinin burjuva toplumundaki bireylerin yaşamlarını yapılandırma biçimlerindeki farklılık niceliksel olduğu kadar nitelikseldir de: uzamsal bir diyagram yalnızca niteliksel ayrımlardan değil, aynı zamanda herhangi bir verili durumda sınıf ilişkisini karakterize eden "yaşamın saf huzursuzluğundan" (Hegel 1977, s. 27) -mücadelenin huzursuzluğundan- da soyutlanarak ancak "diyalektik olmayan" olabilir. (Bu tür uzamsal diyagramlar için model, Fiore'li Joachim'in Figurae'sidir ve çağırdıkları tinsel zeka kendi haline geldiğinde artık gerekli değildir: bkz. Reeves 1976 s. 13.)

<br>

5. Yaşamların sermaye-emek (bir kez daha, her zaman mücadele ilişkisi) tarafından yapılandırılması hangi niteliksel biçimleri alabilir? Marx'ın özellikle üzerinde durduğu biçim sömürü/kamulaştırma biçimidir. Diğer biçimler arasında içerme/dışlama (Foucault), el koyma/harcama ve homojenlik/heterojenlik (Bataille) ve dahil etme/reddetme (Marcuse, Tronti) sayılabilir:[4] liste fenomenolojik olarak zengin ve açık uçludur.

<br>

6. Marksist ve sosyolojik görüşler arasındaki bir fark, yukarıda gösterildiği gibi, Marksist görüşe göre, toplumsal varlığı (tüm "ara" figürlerin aksine) hiçbir şekilde kendi içinde ve kendisine karşı bölünmemiş olan "saf" işçinin (en solda yer alan) metodolojik olarak hiçbir şekilde ayrıcalıklı olmamasıdır. 'Saf' kapitalist de öyle. Her ikisi de, daha ziyade, yalnızca sınırlayıcı vakalardır ve bu nedenle, yalnızca farklı şekilde yapılandırılmış bir kalabalığın içinde diğerleriyle bir araya gelen figürler olarak görülürler. Öte yandan sosyolojik görüş, "saf" işçiyi ve "saf" kapitalisti, ara sınıflar ağının aralarında asılı durduğu metodolojik sütunlar olarak ele alır.

7. Bu fark önemlidir çünkü Marx "a göre "salt" işçi diye bir şey yoktur. Bunun nedeni "geleneksel işçi sınıfı"nın (teorik olarak şüpheli olan bu grup nasıl tanımlanırsa tanımlansın) sayısındaki göreceli azalma değildir. Aksine, bunun nedeni ücret ilişkisinin kendisinin burjuva ve gizemli bir biçime sahip olmasıdır (Marx 1965 Bölüm VI): onun işareti altında yaşayan herkes - hatta ve özellikle tam istihdam edilen artı-değer üreticisi - kendi içinde ve kendisine karşı bölünmüş bir hayat yaşar. Ayakları sömürüye saplanıp kalırken, kafası (bu sömürüyü "düşük ücret" terimleriyle, yani gizemli terimlerle yorumlama eğilimindedir) burjuva ideolojik bulutlarında nefes alır. [5] Buna göre, sınıf mücadelesi hattı, artı-değerin üretildiği bireyden geçer (örneğin, yukarıdaki 4. paragrafta yer alan diyagramda soldan ikinci sırada duran figürde olduğu gibi). 4, yukarıda). Burada da, sermaye-emek ilişkisinin belirli yaşamları antagonistik olarak yapılandırdığı belirli yollarla ilgilenen Marksist sınıf anlayışı için bir sorun yoktur. Ancak tüm saflığıyla bir proletaryanın var olmaması, sosyolojik sınıf kavrayışını yalnızca yerle bir edebilir.

8. İki şema arasındaki bir diğer belirgin fark da Marksist şemanın mevcut toplumda tek bir sınıf ilişkisinden (yani sermaye-emek ilişkisinden) söz etmesi, sosyolojik şemanın ise toplumsal mekanlar ya da gruplar arasındaki olası bağlantılar kadar çok sayıda ilişkiyi kabul etmesidir. Bu nedenle 'sosyologlar' 'Marksistleri' indirgemecilikle suçlamaktadır. Aslında indirgemecilik suçlaması sosyologların kendilerine karşı yöneltilebilir. Sosyologlar her bir bireyi kesin olarak ve herhangi bir geri kalanı olmaksızın belirlenmiş gruplardan ya da yerlerden birine ya da diğerine yerleştirmek isterler: sosyologların çizdiği resimde "çapraz kategorili" bir bireyin görünmesine izin verilemez. Sosyologların orta sınıfları, orta tabakaları, yeni küçük burjuvazileri vb. çoğaltmasının amacı, her bir bireyin kesin olarak atanabileceği bir güvercin deliği bulmaktır. Dolayısıyla, tam da sınıfsal açıdan bireylerin kendi içlerinde ve kendilerine karşı bölündüğü yollar -sınıf mücadelesinin jeolojik kırılma hattının yalnızca bireyler arasında değil, bireyler boyunca uzandığı sayısız ve karmaşık yollar- teorik tutulmaya girer. Dolayısıyla sosyologların indirgemeciliği. Buna karşılık Marksist sınıf anlayışı, bu (öz)çelişkili yaşam dokusunun deneyimsel zenginliğini tam anlamıyla teorik ve fenomenolojik ışığa kavuşturur. Marksizmin bireysel öznelliğin yaşanmış deneyimini kişisel olmayan ve salt nesnel "sınıf güçlerinin" [6] bir oyununa indirgediği suçlaması, "sınıf" otantik Marksist anlamıyla anlaşıldığında en az uygulanabilirdir.

<br>

<br>

9. Bununla ilgili bir nokta da Marksist anlayışın, sosyologların aksine, sınıfı şu ya da bu toplumsal rolün taşınması açısından yorumlamamasıdır. Marx, "Yahudi Sorunu Üzerine" adlı ilk denemesinden itibaren, rol tanımlarının (ya da "toplumsal işbölümünün") geçerli olduğu her toplumu yabancılaşmış ve özgür olmayan bir toplum olarak nitelendirir. Rol tanımlarını metodolojik bir ilke olarak benimsemekten çok uzak olan Marx'ın sınıf görüşü, bireyi, bireyselliğin yalnızca "evrensel" (rol üstlenen ve toplumsal olarak homojen) değil, aynı zamanda "tikel" (benzersiz ve toplumsal olarak heterojen) boyutlarını da devreye sokan bir mücadelenin - kendi mücadelesinin - alanı olarak tasvir eder. "Proleter" ya da "burjuva" (ya da aslında "erkek" ya da "kadın" ya da "yurttaş") gibi rol tanımları ne teoride ne de pratikte Marx'ın çözümünü temsil eder; aksine, Marksist tanımıyla "sınıf "ın çözmeyi amaçladığı sorunlardan biri olarak ortaya çıkarlar.

10. Marksist ve sosyolojik sınıf anlayışları arasındaki bir başka farklılık alanı da elbette politiktir. Sosyolojik görüş, sınıflar ve sınıf fraksiyonları arasında bir ittifaklar siyasetinin reklamını yapar: dahası, "salt" işçi sınıfına ayrıcalıklı -öncü ya da hegemonik- bir siyasi rol atfeder. Marksist görüşte bu tür ittifaklar söz konusu değildir. Ne de 'saf' işçi sınıfının (işsizlere karşı istihdam edilenlerin, "dolaylı" üreticilere karşı artı-değerin dolaysız üreticilerinin, lümpenproletaryaya karşı proletaryanın, emeği olmayanların) metodolojik olarak ayrıcalıklı bir yeri olduğu kadar politik olarak da ayrıcalıklı bir yeri vardır: çünkü böyle bir "yer" yoktur. "Yükselen" sınıflara karşı "düşen" sınıflara devrimci ilgi ya da güç tekeli atfetmek de söz konusu değildir: bu tür tanımlamalar ancak sınıflar yer ya da grup olarak görüldüğünde anlamlıdır. Son olarak, "ileri" ve "geri" sınıf unsurları arasındaki ayrım, sınıfın sosyolojik kavranışıyla birlikte ortadan kalktığı için, öncü parti kavramının tamamı (artı sulandırılmış varyantları) altüst olur. Özetle: geleneksel 'Marksist' siyaset olarak geçen şey aslında sosyolojiktir ve gerçek anlamda Marksist siyaset anarşist bir tarzda siyaset anlamına gelir.

<br>

11. Eğer sınıflar gruplar ya da yerler değil de mücadele ilişkileri ise, o zaman devrimci çatışma gruplar arasında bir çatışma şeklini aldığı ölçüde (ama bunu her zaman kusurlu ve saf olmayan bir şekilde yapar), bu bizzat sınıf mücadelesinin sonucu olarak anlaşılmalıdır. Sosyolojik olarak, örneğin önceden verili sınıfların ortaya çıkması olarak anlaşılmamalıdır - nihayet! - teorik ve politik 'hakikatlerine' dönüşmesi olarak anlaşılmamalıdır. Bireyin önündeki soru kimin tarafında değil, hangi tarafta (sınıf ilişkisinin hangi tarafında) durduğudur; ve bu son soru bile toplumsal olarak önceden var olan yerler ya da roller arasında bir seçim olarak anlaşılmamalıdır. Sadece niceliksel olarak değil, niteliksel olarak da sınıf mücadelesi doğası gereği öngörülemez olmaya devam eder. Marx'ın sınıf anlayışı, sınıf mücadelesinin bizi karşı karşıya bıraktığı seçim meselesine keskin bir şekilde odaklanır ve bunu yaparken, (sosyolojiye göre) yapmayı seçtiğimiz öz-belirleyici taahhütten önce zaten içinde bulunduğumuz herhangi bir role, yere veya gruba başvurmaya izin vermez. Buna izin vermemesinin en önemli nedeni, bizi sınıflı bir toplumda her zaman bilinçli ya da bilinçsiz olarak içinde bulunduğumuz sınıf mücadelesinin gücüyle parçalanmış olarak tasvir etmesidir.

<br>

12. Dileyen herkes Marx'ın metinlerinden sosyolojik bilgelik çıkarabilir. Elbette, özellikle de siyasi yazılarında, Marx her zaman Marksist değildi: örneğin Komünist Manifesto'da benimsenen 'iki büyük kamp' sınıf anlayışı, Marksist sınıf anlayışının açık ve net bir sosyolojik anlamda inşa edilmesinden kaynaklanmaktadır. Bununla birlikte, Marksist sınıf anlayışı aslında Marx'a ait olmasaydı, Marx'ın Kapital'i yazmış olması anlaşılmaz olurdu. Eleştirmenlerinden ve revizyonistlerinden çok önce, kapitalizm geliştikçe 'orta sınıfların' sayısının artmasının beklenebileceğine işaret eden Marx'ın kendisiydi (Marx 1968 s. 562, 573); ve yine de tek bir sınıf ilişkisinin (sermaye-emek ilişkisi) ele alınan teorik 'nesne' olduğu Kapital başlıklı bir kitap yazdı. Bu muamma ancak onun orta sınıflar hakkındaki sözlerinin sosyolojik olduğu kabul edilerek ve Kapital'in ana argümanı yukarıda belirtilen anlamda Marksist bir şekilde okunarak çözülebilir.

<br>

<br>

13. Sosyolojik sınıf kavrayışı, ne zaman Marksist referanslar oluşturmak istese, her zaman ekonomik-determinist olur. Bu böyledir çünkü Marx'ın yazılarında sınıf üyeliğinin ("sınıf" burada bir kez daha bir yer ya da grup olarak görülmektedir) tek "göstergesi" üretim araçlarıyla olan ortak ilişkidir. Ancak, üretim araçlarıyla ilişkili olmanın yanı sıra, sınıf üyesi olan (ya da sınıfa dahil olan) bireyler kendilerini devletle ve "ideolojiyle" ilişkili bulurlar, yerel kiliseleri, futbol takımları ya da barları hakkında hiçbir şey söylemezler. Dolayısıyla, sınıfın sosyolojik kavranışı aynı anda ayrı toplumsal 'düzeyler' ya da 'pratikler' ya da 'örnekler' (Althusser) şeması üretir ve bu düzeylerin nasıl ilişkili olduğu sorusunu ele almalıdır. Cevap çok iyi bilinmektedir: son kertede 'ekonomik hareket... kendisini gerekli olarak ileri sürer'. [7] Başka bir deyişle, son kertede sosyolojik Marksizm, kuşkusuz uzun ve karmaşık deterministik dizgeleri olan bir ekonomik determinizm anlamına gelir. Althusser'in yaptığı gibi, böyle bir teorinin (karmaşıklığı nedeniyle) artık ekonomist olmadığını iddia etmek, bir makinenin, motorunu çalıştıran dişli çarkların sayısı nedeniyle artık bir makine olmadığını iddia etmek gibidir.

14. Marksist sınıf anlayışında her şey farklıdır. Marx'ın "kendinde" ve "kendisi için" sınıf arasındaki ayrımı, toplumsal düzeyler arasında değil (bkz. yukarıda Dipnot 5), sınıfın sosyolojik ve Marksist kavranışları arasında bir ayrım olarak alınmalıdır: eğer bir sınıf ancak "kendisi için" olduğunda böyle bir sınıf haline geliyorsa, o zaman tüm öngörülemez dallanmaları, gelişmeleri ve harcamalarıyla siyasi mücadele, sosyolojik Marksizmin ekonomik "temel" olarak ele aldığı şeyin içine zaten yerleştirilmiştir. Sosyolojik Marksizm ayrı olduğunu varsaydığı düzeyleri birleştirmeye çalışırken ve bu başlangıç noktası ve sorun temelinde ancak nedenselci ve ne kadar 'yapısal' (Althusser) olursa olsun dışsal ilişkiler üzerinde durabilirken, Marksist Marksizm ters yönde hareket eder ve ayrımları çelişkili bir bütünlük içinde, yani içsel ve antagonistik olarak ilişkili bir bütün içinde çizer: 'Somut somuttur çünkü birçok belirlenimin yoğunlaşmasıdır, dolayısıyla çeşitliliğin birliğidir (Marx 1973 s. 101). Para. 4'teki şemanın da açıkça ortaya koyduğu gibi 4. paragraftaki şemanın açıkça ortaya koyduğu gibi, örneğin burjuva toplumuna özgü sınıf ilişkisinin bütünlüğü (sermaye-emek ilişkisi), bu toplumun anlarını ya da parçalarını oluşturan bireylerin her birinde - niteliksel olarak farklı şekillerde olsa da - tamamen mevcuttur. Asıl önemli olan şey uzun zaman önce erken dönem Lukács tarafından söylenmiştir: "Marksizm ile burjuva düşüncesi arasındaki belirleyici farkı oluşturan şey, tarihsel açıklamada ekonomik güdülerin önceliği değil, bütünselliğin bakış açısıdır" (Lukács 1971, s. 27).

<br>

15. 'Bütünlüğün bakış açısı' ile birlikte, tamamen yeni bir sınıf siyaseti anlayışı devreye girer. "Siyaset" (sosyologlar tarafından olduğu gibi) ayrı bir toplumsal düzey olarak görüldüğünde, sınıfın "kendisi için" varlığının turnusol testi, az ya da çok geleneksel -yani burjuva- türden bir siyasi partinin kurulması haline gelir. Böyle bakıldığında, öncü bir parti bile burjuva temasının bir varyasyonu anlamına gelir. Ancak siyasi devlet ve sivil toplum düzeyleri arasında ayrım yapan Marx değil, burjuva toplumudur - bkz. 'Yahudi Sorunu Üzerine' - ve birincisini, toplumsal grupların olgunluklarında (yani uygunluklarında) rekabet edebilecekleri bir arena olarak öngörür. Marksist sınıf anlayışı ya da başka bir deyişle "bütünselliğin bakış açısı", sosyolojik sınıf anlayışının gerektirdiği siyaset anlayışının darlığını kesin bir şekilde reddeder. Marksist görüşe göre siyaset kategorisi, sınıf mücadelesinin (ve dolayısıyla sınıfın kendisinin) öngörülemez bir şekilde aldığı biçimler kadar geniş olur. Sadece hiçbir mesele siyasi gündemin dışında bırakılmaz; siyasi gündem kavramının kendisi de dışlanır, çünkü böyle bir gündem teorik olarak önceden belirlenmiş bir siyasi alana girmeyen her şeyi dışlar ve marjinalleştirir.

<br>

16. Yukarıdaki notlar ne tamlık ne de şematik olarak yeniden ifade etmeye çalıştıkları sınıf kavramının her noktada savunusunu sağlama iddiasındadır. Daha ziyade, amaçları Marksist bir sınıf anlayışının neleri gerektirdiğini açıklığa kavuşturmak olmuştur. Bu anlayışın değerlendirilmesine gelince: verimli görünen tek eleştirel sorgulama hattının, sermaye-emek ilişkisinin tüm zenginliğiyle yaşamlarımızı yapılandıran tek mücadele ilişkisi olup olmadığını sormak olduğu öne sürülebilir. Ve burada Marx'ın yerini almak söz konusu olamaz: bu türden diğer ilişkiler (örneğin cinsel ve ırksal ilişkiler) sermaye ilişkisi aracılığıyla dolayımlanır, tıpkı sermaye ilişkisinin kendi payına onlar aracılığıyla dolayımlanmış olarak var olması gibi. Hangi ilişkinin "egemen" olduğuna dair sorgulama, somut olarak politik (yani fenomenolojik) terimlerle başlatılmadığı sürece skolastik kalacaktır. Hem politik hem de metodolojik olarak Marksizmin sosyolojik olana en büyük üstünlüğü, Marx'ın kapitalizmin en öldürücü özellikleri arasında saydığı determinizmin - "ölü" emeğin "canlı" emek üzerindeki ya da başka bir deyişle geçmişin (tüm determinist şemalarda olduğu gibi) şimdiki zaman ve gelecek üzerindeki tiranlığı- her türlü lekesinden Marksizmi kurtarması ve en iyi düşüncesinin baştan sona amansızca karşı durmasıdır. Bu böyledir çünkü Marx'ın "sınıf analizinin" tek teması, Marx'a göre bizzat sınıfın varlığı olan ince dokulu ve sürekli ve kesintisiz olarak gelişen mücadeledir.

Teşekkür

Bu yazı John Holloway ile yaptığım konuşmaya çok şey borçludur. Filio Diamanti tartışmaya bile başlamadan önce “sınıf” anlayışımın netleştirilmesi gerektiğini fark etmemi sağladı.

Notlar

[1] En azından bu kadarı Kapital 3. Cilt’in son, tamamlanmamış bölümünden anlaşılmaktadır(Marx 1971 pp. 885-6).

[2]. Marx (1969) s. 173.

[3] Althusser (1971) s. 160-5.

[4] Foucault (1979) Bölüm Dört, kısım 2; Bataille (1985); Marcuse (1968); Tronti (1979).

[5] Ücret formuna içkin "ideolojik" mistifikasyonun işçinin sınıf saflığını kirletmeden bıraktığı görüşü, üretim ve ideolojiyi ayrı toplumsal " seviyeler" veya örnekler olarak ele almaya dayanır; Marx'ın "içinde" ve "için" sınıf arasındaki ayrımının aşağıda para. 14'te reddedilen okuması da öyle. 14, aşağıda. 'Seviyeler' kavramı hakkında bkz. 13. Geçerken, ideolojinin (nasıl belirtilirse belirtilsin) ayrı bir düzey olarak kavranmasının tamamen gizemli kaldığını belirtmekte fayda var, çünkü geriye kalanı olmayan toplumsal varoluş - örneğin cinsiyet ayrımları, mimari, iş disiplini ve bilimsel bilgi - "ideolojik" bir suçlama taşır.

[6] Bu suçlamanın bir tartışması için Sartre'a (1963) bakınız.

[7] Engels'ten J Bloch'a 21-22 Eylül 1890 (Marx/Engels n.d. s. 498). Althusser'in 'belirleyici' ve 'egemen' örnekler arasındaki ayrımı aynı temanın bir permütasyonunu ifade eder.

Referanslar

Althusser L. (1971) Lenin ve Felsefe (İletişim Yayınları)

Bataille G. (1985) Visions of Excess: Selected Writings 1927-1939 (Manchester University Press)

Foucault M. (1979) Hapishanenin Doğuşu (İmge Kitabevi)

Hegel G.W.F. (1977) Tinin Görüngübilimi (İdea Yayınevi)

Lukacs G. (1971) Tarih ve Sınıf Bilinci (Belge Yayınları)

Marcuse H. (1968) Tek Boyutlu İnsan (İdea Yayınevi)

Marx K. (1965) Kapital Cilt I (Yordam Kitap)

Marx K. (1968) Artı-Değer Teorileri 2. Kitap(Sol Yayınları)

Marx K. (1969) Felsefenin Sefaleti (Sol Yayınları)

Marx K. (1971) Kapital Cilt III (Yordam Kitap)

Marx K. (1973) Grundrisse (İletişim Yayınları)

Marx K./Engels F. (n.d.) Seçme Yazışmalar (Sol Yayınları)

Reeves M. (1976) Joachim of Fiore and the Prophetic Future (SPCK)

Sartre J-P. (1963) Yöntem Araştırmaları (Can Yayınları)

Tronti M. (1979) Ret Stratejisi, İşçi Sınıfı Otonomisi ve Kriz (CSE Books / Red Notes)



