#!/bin/bash
REMOTE="upstream"
UPSTREAM="https://tr.anarchistlibraries.net/git/tr"

git config credential.helper 'cache --timeout=2628000'

if ! git config remote.${REMOTE}.url | grep "${UPSTREAM}"; then
	git remote remove ${REMOTE}
fi

if ! git config remote.${REMOTE}.url; then
	git remote add ${REMOTE} ${UPSTREAM}
fi

git remote -v
git -c http.sslVerify=false fetch ${REMOTE}
git push
git checkout master
git merge ${REMOTE}/master
git push
