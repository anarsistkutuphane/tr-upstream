#!/bin/bash

if [[ -z "${1}" ]]; then
	echo "Usage: $(basename "${BASH_SOURCE[0]}") <date>"
	exit 1
fi

date_set="${1}"
files="$(grep -rl "#pubdate ${date_set}")"

grep -rl "#pubdate ${date_set}" &>/dev/null || { echo "not found: ${date_set}"; exit 1; }

while IFS= read -r file; do
	pub_date="$(grep "#pubdate ${date_set}" "${file}" | awk '{print $2}')"
	first_date="$( date --iso-8601=seconds -d "$(git log --format=%aD "${file}" | head -1)" | sed 's,+.*,,' )"

	echo "changing ${pub_date} to ${first_date} for ${file}.."
	sed -i "s/${pub_date}/${first_date}/g" "${file}"
done <<< "${files}"
