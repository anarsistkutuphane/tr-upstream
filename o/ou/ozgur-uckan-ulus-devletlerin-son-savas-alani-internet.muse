#title Ulus-devletlerin son savaş alanı: İnternet
#author Özgür Uçkan
#date 20 Haziran 2011
#topics pro-internet
#source 09.06.2023 tarihinde şuradan alındı: [[https://web.archive.org/web/20111115105859/http://www.gennaration.com.tr/yazarlar/ulus-devletlerin-son-savas-alani-internet/][gennaration.com.tr]]
#lang tr
#pubdate 2023-06-09T20:23:19



Bir süredir bu köşede aynı konuyu işliyorum, biliyorsunuz: Devletler ve interneti zapt-u rapt altına almak için verdikleri savaş… Ama ne yapayım, konu sürekli gelişiyor.

Daha önce de söylediğim gibi, bu savaş önce Wikileaks ve “Cablegate” skandalıyla kızıştı. Ardından patlayan ve hala dinmemiş olan Arap Baharı ulus-devletleri gafil avladı. “Demokrat” Batılılar, onca zaman kendilerine o kadar iyi hizmet etmiş olan Arap diktatörlerden vazgeçmemekte önce biraz direndiler; ama sonra mecburen kendilerini akıntıya bıraktılar. Bizimki gibi ülkelerde şaşkınlık biraz daha derin oldu ve kendisini normal olarak inkar biçiminde gösterdi.

Bir süredir, ulus-devletler internete yönelik tehdit algılarını karşı saldırılarla ifade ediyor. Wikileaks’e karşı verilen hukuk-dışı tepkilerden, tarihe “Napolyon kompleksi” ile geçmiş ve umarım yakında yolcu olacak Fransa Cumhurbaşkanı Sarkozy’nin büyük hayallerle düzenlediği eG8 Forumu’na geldik. Sarkozy, internetin “daha uygar” olması gerektiğini, bunun için de devletler tarafından yönetilmesinin şart olduğunu söyledi ve oraya topladığı, Murdoch gibi eski ekonomi hantallarından destek istedi. İktidarının son yıllarını, Avrupa Komisyonu tarafından hukuk dışı ilan edilen HADOPI, LOPPSI gibi internete yönelik baskıcı düzenlemelerle ziyan eden Sarkozy, cevabını foruma katılan ya da boykot eden tüm önemli internet aktörlerinden ziyadesiyle aldı; tarihsel sıfatının yanına “Facebook’la savaşmaya kalkan deli”, “20. yüzyıl kafası”, “zararlı” gibi sıfatlar da ekledi ve forum, tam bir fiyaskoyla sonuçlandı.[1]

Tabii, bizim gibi, Google ve Youtube’a karşı resmen savaş ilan etmiş bir Ulaştırma Bakanı’nın bulunduğu, sansürlenen site sayısının 60.000’i aştığı, devlet eliyle interneti toptan filtrelemeye soyunan, internet alan adlarında ayıp kelime avına çıkmış bir ülkede Sarkozy’yi anlamak daha kolay. 8 yıldır iktidarda olan hükümet, interneti tamamen denetlemek hayalini uzun zamandır görüyor ve onunla sürekli savaşıyor zaten. Neyse ki, sansürcü devlet refleksi, 15 Mayıs 2011’de, İstanbul’da 50.000, eşzamanlı olarak diğer illerde de 10.000’i aşkın kişinin katıldığı ve dünya internet tarihine en kitlesel sokak protestosu olarak geçmiş efsanevi bir yürüyüşle aldı. Yürüyüş dünya basınında manşet, başta Araplar olmak üzere internette hit oldu, ama hükümet ve internet tarafından paçavraya çevrilmiş “bir kısım medya” bu cevabı okumamakta direndi; en yetkili ağızlardan gösteriyi “15-20 pornocu yürüdü.”, “bunlar filtre yazılımcılarının rant kışkırtmaları” gibi trajikomik söylemlerle küçümsemeyi tercih etti. Öte yandan da, seçim atmosferinin zoruyla yetkililer sivil toplum kuruluşlarıyla masaya oturdu; ama sonucunu henüz görmedik. Ancak, şunu rahatlıkla ifade edebilirim ki otoriteler bu anlamsız ve hukuken gayrimeşru savaşı sürdürmekte inat ettikleri sürece, demokratik tepkinin de yükseldiğini göreceğiz.

Ulus-devletlerin krizde olduğu ve krizin özellikle 2008 küresel ekonomik krizinden bu yana daha da derinleştiği, bunun aynı zamanda, giderek katılımsızlığın kısır döngüsünde işlevsizleşen temsili demokrasinin de krizi olduğu artık daha yüksek sesle dile getiriliyor. İnternetin daha doğrudan demokrasi deneyimlerine kapı açtığı da… Arap Baharı bu saptamayı muhteşem bir biçimde taçlandırdı.

Ulus-devletleri egemen kılan iki işlev vardır: vergi salmak ve ordu kurmak. 20. yüzyılın ikinci yarısında, başta Avrupa Birliği gibi bir ağ-devleti olmak üzere, ulus-devletlerin bu iki ayrıcalığı aşamalı olarak kaybetmekte olduklarını gördük. AB tarzı bölgesel devletler, Asya-Pasifik’ten Latin-Amerika’ya, Akdeniz’den Orta-Asya’ya yavaş yavaş kuruluyor. Sonuçta, en fazla dört yüz yıllık tarihi olan bir yapıdan bahsediyoruz. Her ne kadar bize ezeli gibi gelse de dün, ulus-devlet diye bir şey yoktu. Sanayi Devrimi nasıl bittiyse, bu devrimin ürettiği yapıların da ortadan kalkması normal.

Birileri, ulus-devletleri “demokrasinin teminatı” olarak sunmaya çalışıyor olabilir. Ama başkaları da onları demokrasiye yönelik tehdit olarak niteliyor. Hangi görüşün baskın çıkacağını zaman gösterecek; ama ulus-devletlerle demokrasi arasındaki ilişkiyi, bu sonuncuların internete, özellikle de temsil ettiği demokratik imkanlara karşı açtıkları savaş üzerinden okursak, zamanın neyi göstereceği konusunda bir fikrimiz olabilir. Hele, ulus-devletlerin (Fransa ya da Türkiye örneğinde olduğu gibi) devlet politikalarını, halkın değil; artık kontrol sanayilerini elinden kaçırmakta olan eski ekonomi aktörlerinin çıkarlarını korumak üzerine kurduklarını gördükçe…

Sanayi Devrimi’nin, aynı imkansız direnci gösteren iktidarlara burjuva devrimleriyle öğrettiği dersi hatırlamak gerek: Ne halkın ne de ekonominin önünde durabilirsiniz.

<right>
Dr. Özgür Uçkan

İstanbul Bilgi Üniversitesi İletişim Fakültesi
</right>

[1] http://ozguruckanzone.blogspot.com/2011/07/oyunun-kural-eg8-forumu-ve-devletlerin.html?m=1

