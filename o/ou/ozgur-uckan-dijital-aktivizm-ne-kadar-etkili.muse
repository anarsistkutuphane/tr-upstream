#title Dijital aktivizm ne kadar etkili?
#author Özgür Uçkan
#date 21 Mayıs 2010
#source 09.10.2023 tarihinde şuradan alındı: [[https://web.archive.org/web/20120113015535/http://www.gennaration.com.tr/yazarlar/dijital-aktivizm-ne-kadar-etkili/][www.gennaration.com.tr]]
#lang tr
#pubdate 2023-06-09T20:13:28
#topics dijital kapitalizm, aktivizm, dijital aktivizm, pro-internet



Ağ ekonomisi, ağ toplumu, ağ kültürü derken, kaçınılmaz olarak “ağ siyaseti” de diyoruz aslında. Çünkü küresel ağların hayatımıza gömülü hale gelmesi, sadece ekonomiyi, sosyal ilişkileri ve kültürü değil siyaseti de dönüştürüyor.

Dijital ağlar ve siyaset arasındaki ilişki, ağın tarihi kadar eski. Çünkü ağın olduğu yerde iletişim, iletişimin olduğu yerde de topluluk var; topluluğun olduğu her yer ise siyasetin alanı. İnternet henüz Pentagon’un ARPANET’inde koza halindeyken, projede çalışan sivil bilim adamları bu ağı aralarında geyik yapmak, bilim kurgu paylaşmak ve siyaset tartışmak için de kullanıyorlardı. Ağ dünyaya açılır açılmaz, etkileşimli bir kamusal alan olmasının getirdiği dinamiklerle topluluklara ve elbette siyasete de açıldı.

Siyaset yapmak dönüşüyor. Bireylerin siyasete katılımı geleneksel temsiliyet mekanizmalarını aşındırıyor. Bilgi ve iletişim teknolojilerinin ağ etkisi ana akım siyaset için de etkili kanallar yaratıyor. Geçen yıl Almanya’da yapılan genel seçime damgasını vuran internet oldu. Japonya’da yılların muhafazakar iktidarını düşürüp değişimin yolunu açan, internet sayesinde siyasetle ilgilenmeye başlayan kadınların oy kullanmasıydı. Türkiye’de de özellikle son yerel seçimlerde internet kullanımının sonuç üzerinde etkili olduğu görüldü. Meslekten siyasetçilerin hoşuna gitmese de siyasette bir inovasyon gerçekleşiyor. Müzik yayıncıları, gazeteler ya da reklam ajansları da bu işten hoşlanmamıştı, ama sonuç değişmedi. Geleneksel siyasetçileri zor günler bekliyor…

Ana akım siyasetin dışında, tabanda da durum farklı değil. Ağ teknolojilerinin ve internetin bugün ulaştığı kapsam ve derinlik, sunduğu katılım araçları ve küresel etkileşim imkanı, onu aktivistler için verimli bir ortam haline getiriyor. Sosyal ve ekonomik hayatımıza gömülü hale gelmiş ve dünya ile aramızda bir “arayüz”e dönüşmüş olan internet, doğal olarak aktivizmin de ayrıcalıklı alanlarından biri. Bilgi ve iletişim teknolojilerinin ve bu teknolojiler aracılığıyla yaratılmış ağların farklı aktivist amaçlar için kullanımı çok geniş bir yelpazeye yayılıyor. Ama kullanıcı deneyimini öne çıkaran ve etkileşim boyutunu derinleştiren Web 2.0 dönemi ile birlikte dijital aktivizmde de bir patlama yaşanıyor. Sosyal ağlar bu aktivizm türünün gözde mecrası.

İnternetin toplumsal muhalefet hareketleriyle ilişkisi konusunda çok sayıda yaklaşım var. Ama bu ilişkiyi kabaca üç kategoriye ayırabiliriz:

 1. İnternetin katılımcı, şeffaf ve doğrudan demokrasiye ulaşmak için öncelikli yol olduğunu savunanlar;

 1. İnternetin neoliberal sömürü politikalarının yeni bir aracı olduğunu, sadece ayrıcalıklı kesimlerin kullanabildiği bu aracın toplumsal muhalefet için değersiz olduğunu düşünenler;

 1. İnternetin iktidar sahipleri tarafından yönlendirilmesine ve geniş kesimlerin dışlanmasına rağmen, sunduğu imkanlar nedeniyle etkili bir mücadele aracı olabileceğini kabul edenler… (Bkz. Gamze Göker, “Yeni Toplumsal Hareketler ve İnternet İlişkisi”, Evrensel Kültür, Aralık 2009)[1]

Dijital aktivizmin en önemli sorunu, “dijital fanus etkisi” diye adlandırdığımız bir dezavantajdan kaynaklanıyor: İnternetle sosyalleşen insanlar, bir banner’a tıklayarak Afrika’daki açları doyurduğunu, yağmur ormanlarını kurtardığını veya ozon deliğini kapattığını düşünmeye başlıyor! Üçüncü gruptakiler ağlarla gerçekliği birleştirerek bu fanus etkisini kırabiliyorlar. Dijital aktivizm, dijital olmayan hedeflere yönelik olarak ancak “hibrid” yapılarda işe yarıyor: Yani sokakla buluştuğu zaman…

Elbette öncelikle dijital hedeflere yönelik dijital aktivizm türleri de var: İnternet sansürüyle mücadele, çevrimiçi mahremiyetin korunması, net yansızlığının savunulması gibi alanlar, dijital aktivizmi baskın hale getiriyor. Ama örgütlenmelerini özellikle ağlar üzerinde gerçekleştirmelerine rağmen, bu tür eylemler bile bir şekilde sokağa, salonlara, meclislere, alanlara çıktıklarında daha etkili olabiliyorlar.

Dijital aktivizmin ve ağ örgütlenmesinin en etkili olduğu boyutlardan biri de, paradoksal bir biçimde yerel ölçekteki eylemler. Dijital aktivizmin ağ etkisi bir yandan muhalefeti küresel çoğulluklarla etkileşime sokup beslerken, öte yandan yerel tekillikleri küresel bağlama taşıyarak etkilerini büyütüyor. Özellikle kent, hatta mahalle ölçeğinde örgütlenen yerel toplulukların eylemleri, ağ etkisiyle bölgesel, ulusal, küresel ölçeklerde ses getirerek etkilerini artırabiliyor. Mutenalaştırma projeleriyle dışlanmaya direnen, sağlık, eğitim, ulaşım, hijyen gibi büyük kentsel sorunlar etrafında örgütlenen toplulukların kurduğu iş birliği ağlarından yayılan dijital aktivizm, farklı coğrafyalardaki benzerleriyle ağlar oluşturarak kritik kütle etkisi yaratabiliyor.

Dijital aktivizmin etkili olabilmesi için gerçekliğe genişlemesi gerekiyor…

[1] Ekleyenin notu: Atıf yapılan makalenin tam hali: https://kaosgl.org/haber/yeni-toplumsal-hareketler-ve-internet-iliskisi



