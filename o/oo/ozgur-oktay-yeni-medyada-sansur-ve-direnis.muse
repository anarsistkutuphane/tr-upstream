#title Yeni Medyada Sansür ve Direniş
#author Özgür Oktay
#SORTtopics internet, bilişim, cypherpunk, kripto anarşizm
#date 19.06.2013
#source Meydan Gazetesi
#lang tr
#pubdate 2020-04-21T20:29:53


Yeni medya, yeni sansür olanaklarını ortaya çıkardığı gibi, daha geniş oranda yeni direniş olanaklarını da birlikte getiriyor. Reyhanlı’daki patlama sonrasında ilan edilen yayın yasağı ve sonrasında Redhack’in yayınladığı gizli belgeler, sansür ve bu sansüre karşı direniş hakkında yeniden düşünmemizi sağladı. Daha sonra Taksim direnişi sırasında ana akım medyanın suskunluğu düşüncelerimizi pekiştirdi.

Ana akım medyanın çoğu olaylara duyarsız kalırken, geriye kalanlar da devlet baskısı karşısında pısırıp kaldılar. İnsanlar sosyal medya ve internet üzerinden haberleri alırken, suskun kalan medyayı vicdansızlıkla suçladılar. Türkiye’de yıllardır artarak süregelen baskı ve sansürün tarihine bakarsak gazetecilerin üzerindeki baskıyı görebiliriz. Üstelik bu durum, batı medyası için de çok farklı değil. Wikileaks’in kurucusu Assange, sansürün en önemli kısmının otosansür olduğunu ve bunun boyutlarını şöyle açıklıyor[1]:

Yayıncılığın bütün dünyada problemli olduğunu açıkça gördüm, gerek otosansür, gerek açık sansür olsun. Çoğunlukla otosansür var. Aslında en önemlisi bu diyebilirim. Eskiden en önemlisi ekonomik sansürdü. Basitçe, bir şeyi yayınlamak karlı değildi. Pazarı yoktu. Bugünkü yapıyı ise bir sansür piramidi diye tanımlıyorum. İlginç bir yapı. Piramidin tepesinde gazeteci ve yayıncı cinayetleri var. Hemen altında gazeteci ve yayıncılara yapılan politik saldırılar. Peki, hukuki saldırının ne olduğunu zannediyorsunuz? Hukuki saldırı basitçe, geciktirilmiş zorlayıcı güçtür. Cinayetle sonuçlanmaz ama hapis ya da araçlarınıza el konulmasıyla sonuçlanabilir. Böylece bir alt seviyeye iniyoruz ve hacmi hatırlayın… Piramidin hacmini! Tepeden aşağı indikçe piramidin hacmi önemli ölçüde artar. Bu örnek için, piramitte aşağı indikçe sansür vakalarının artması anlamına geliyor. Öldürülen birkaç gazeteci var. Kişilere ve kurumlara birkaç kamu davası saldırısı var. Ve bir sonraki seviyede inanılmaz ölçüde otosansür var. Ve bu otosansürün nedeni kısmen, insanların piramidin daha üst katmanlarına yükselmek istememeleri. Hukuki saldırılarla ya da zorlayıcı güçle karşılaşmak istemiyorlar. Ve öldürülmek de istemiyorlar. Böylece doğru davranmak isteyenlerin cesareti kırılıyor. Sonra diğer otosansür biçimleri var: iş olanaklarını kaybetmek, terfi edilmemek ve bunlar daha da önemli çünkü piramidin daha aşağısındalar. En altta ise -ki en büyük hacim burada- okuma bilmeyenler, gazete alamayanlar, hızlı iletişim araçlarına ulaşamayanlar ya da bulunduğu yerde bunu sağlayan karlı bir endüstri olmayan insanlar var.

Teoride demokratik devlete inananlara göre, demokrasiyi sağlamak için otoritenin baskısına karşı basının “dördüncü kuvvet” olması, “bekçi köpeği” görevini yapabilmesi gerekir. Wikileaks’e açılan davalar ve benzerleri, tüm dünyada devletlerin gerçek yüzünü görmemizi sağlıyor. Wikileaks’in barınma hizmetlerini sağlayan ve aynı zamanda dünyanın en büyük dosya paylaşım sitesi olan thepiratebay.org’un kurucularını, yeni vizyona giren belgeselde[2] izledik. Belgeselde onları önce ifade özgürlüğünün kalesi zannettikleri İsveç devletine övgüler düzerken, ardından polis baskınıyla sunucu bilgisayarları gasp edilirken ve en sonunda cezaevine giderken izledik.

Taksim direnişi, tam da devletin bu baskıyı artırıp tüm topluma yöneltmesi nedeniyle büyüyerek bir halk isyanına dönüştü. Zamanın ruhunu kavrayıp isyana katılmak için geç kalan ana akım medya çalışanları halkın öfkesinden nasibini aldı. Taksim eylemcilerin kontrolüne geçtikten sonra gelen NTV yayın aracı devrilip yakıldı. Halk Doğuş grubu ve ATV/Sabah binalarının önünde eylem yaptı.




*** Direniş ve Twitter Belası

Pratikte medya, devletin, otoritesini meşru kılmak için kullandığı bir aygıta dönüştü[3]. Fakat bilgi teknolojileri ve internet bu aygıtın arada bir teklemesine sebep olabiliyor. İnternet üzerinde kimliğini gizleyerek iletişim kurma imkanı sayesinde, muhbirler ve yayıncılar devletin baskı araçlarından bir yere kadar kurtulabiliyorlar. Wikileaks’in devletlerin gizlilik duvarlarında delikler açtığını gördük. Ancak tek adama (karizmatik Assange) bağlı yapısı, devasa finans desteği (güç çürütür) ve kendi leaks (sızıntı) haberleri, wikileaks’i güvenilir bir bilgi kaynağı olarak kabul etmemizi zorlaştırıyor.

Sosyal medya sayesinde devlet sansürüne takılmadan haber alabiliyoruz. Başbakan “twitter belası” diyerek bu ortamdan ne kadar korktuğunu itiraf etmiştir. Fakat iktidar mücadelesi sosyal medyada da devam ettiği için, tonlarca dezenformasyonu da birlikte alıyoruz. Sosyal medyada manipülasyon yapan devlet güçlerine ek olarak, hızlı gelişen politizasyondan faydalanmaya çalışan başta Kemalistler olmak üzere muhalefet partileri de var. Üstelik gelişmeler çok hızlı olduğu için paylaşılan çatışma ve durum bilgileri kısa sürede geçerliliğini yitirebiliyor. Bu durumu geç de olsa fark eden direnişçiler polis araçlarının, çatışma noktalarının ve sağlık kuruluşlarının anlık konumları yayımlayan occupygezimap.com gibi inisiyatifler aldılar.

Wikileaks’in ilham aldığı siberpunk düşüncesinin ilk izini, Timothy C. May’in bilgi teknolojilerine değindiği 1992’de yayınlanan “Kripto Anarşist Manifesto” [4] da buluyoruz:

…Bu gelişmeler hükümet düzenlemelerinin yapısını, ekonomik işlemleri vergilendirme, yönetme ve bilgiyi gizli tutma gücünü tamamıyla değiştirecek; hatta güven ve itibarın doğasını bile değiştirecek…

…Tıpkı baskı teknolojisinin ortaçağ loncalarının iktidarını ve toplumsal iktidar yapısını değiştirmesi ve azaltması gibi, kriptolojik yöntemler de kurumların ve ekonomik işlemlerdeki devlet müdahalesinin yapısını köklü olarak değiştirecek…

Devletler her alanda olduğu gibi iletişim alanında da otorite olmak ister ama bilgi teknolojilerini elde etmek kolay olduğu için bunu tümüyle başaramaz. Örneğin, T.C. 2813 nolu telsiz kanunu “Kamu kurum ve kuruluşları dışında kalan tüzelkişiler ile gerçek kişiler telsiz sistemleri üzerinden kodlu veya kriptolu haberleşme yapamazlar.” der. Ancak bugün wifi mesh teknolojileri ile şehirlerde bu yasağı delmek çok kolay. 22 Kasım 2011 de başlayan internet filtre düzenlemesi ile tüm servis sağlayıcılar hem sansür hem de izlemeye yarayan sistemler kurdular. Standart filtreyi seçseniz bile, bazı sitelere ulaşamıyorsunuz ve size ait tüm haberleşme bir izleme sisteminden geçiyor. Ama otoritenin baskısından, izleme sistemlerinden kaçmak için Tor (anonim ağı) kullanabilirsiniz. İnternet üzerinde bir hedefle (örneğin bilgi alacağınız yasaklı bir site, gizli belgeleri yükleyeceğiniz wikileaks sunucusu ya da saldırı yapacağınız bir hükümet sitesi) kurduğunuz bağlantı Tor üzerinde binlerce noktadan atlayarak dolaylı olarak kurulur. Kullandığınız bilgisayar güvenli değilse bile bir USB anahtarda saklayacağınız Tor Tarayıcıyı kullanarak izleme sistemlerini atlatabilirsiniz.

Casusluk faaliyetlerinde genel olarak kimlerin, ne konuştuğu, ne yazıştığı dinlenir, bazen de sahte mesajlar atılır. Bilgi teknolojileri gizlilik ve kimlik doğrulama imkanları ile sizi bu faaliyetlerden korur.

Bilgi teknolojileri, bugünün devrimcilerine hem yayıncılık hem de iletişim alanında devlet ile boy ölçüşebilecek güçte olanaklar sağlıyor. Bugün isyanın hızla yayılmasını sağlayan şey tüm kötü yanlarına karşın sosyal medya üzerindeki hızlı ve sansürsüz iletişim olanağıdır.

[1] 23 Haziran 2011’de İngiltere’de ev hapsinde tutulan Julian Assange ile Google CEO’su Eric Schmidt’in, yeni kitabı “Yeni Dijital Dünya” için yaptığı toplantı. (http://wikileaks.org/Transcript-Meeting-Assange- Schmidt.html)
[2] TPB AFK: Korsan Koyu Klavyeden Uzakta (http://youtu.be/eTOKXCEwo_8)
[3] “İdeoloji ve Devletin İdeolojik Aygıtları” – Althusser
[4] http://www.activism.net/cypherpunk/crypto-anarchy.html http://acikhavasiir.blogspot.com/2012/01/kripto-anarsist-manifesto-bir-hayalet.html



