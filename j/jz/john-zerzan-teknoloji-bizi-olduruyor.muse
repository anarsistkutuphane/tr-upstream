#title Teknoloji Bizi Öldürüyor
#author John Zerzan
#SORTtopics teknoloji, ilkelcilik
#date 28.03.2012
#source 21.01.2022 tarihinde şuradan alındı: [[https://karaisyan.blogspot.com/2012/06/teknoloji-bizi-olduruyor_18.html][karaisyan.blogspot.com]]
#lang tr
#pubdate 2022-01-21T10:15:06
#notes Çeviri: Tijen Tansel <br> İngilizce Aslı: [[https://www.nydailynews.com/opinion/technology-killing-article-1.1052015][Technology is killing us]]


Dijital dünyaya olan bağımlılığımız bir materyal pahasına ortaya çıkıyor.

Her defasında Apple yeni bir iPhone ya da iPad halka sunduğunda dünya çıldırıyor. Fakat ne fark eder? Neden ‘smartphone’ (Akıllı Telefon) ve tabletler bu kadar önemli oldular?

CNN’de Şubat’ın sonlarında, Andrew Keen, Cep Telefonu Dünya Kongresinde (Mobile World Congress) rapor verirken, raporun “Cep Telefonlarımız Nasıl Frankenstein’in Canavarı Oldu” isimli bir bölümünü “cep telefonlarına olan bağımlılığımızda bir artış” olarak andı.

SecureEnvoy, bir İngiliz güvenlik firması, cep telefonu yokluğu korkusu ya da cep telefonu kaybetme korkusu olarak adlandırılan yaygın bir durumu açıkladı. SecureEnvoy tarafından yapılan ankete katılanların üçte ikisi cep telefonlarını kaybetmekten çok korktuklarını – bu korku 4 sene önce %53’ten yükseldi – belirterek bu korkunun titreme, terleme ve mide bulantısı gibi belirtileri olduğunu kaydettiler.

Bu garip gelişme bazı yönlerden yenidir ve ayrıca çok yeni değil. Hızlanan bir süratle ve yeni teknolojinin çığırtkan vaatleriyle şüpheler ortaya çıkmaya başlar. iPhone gibi cihazlar yüksek teknolojinin bizi güçlendirdiği ve iletişim sağladığımız iddiasını somutlaştırır. Ve bir kat daha biz her zamankinden daha güçsüzleşmiş ve daha izole edilmiş olmadık mı?

Bir mezar kinizm noktasında güçsüzleştirilmiş ve hesap verilebilirlik ya da sorumluluk duygusu kaybıdır. Sosyologlara göre izole edilmiş bir toplumda daha az arkadaşlarımız var ve onları daha az ziyaret ediyoruz. 1980’lerin ortalarından itibaren arkadaşları olmayanların sayısı 3 katına çıktı.

İzole edilmiş ve aralıksız bir şekilde devam eden daha ve daha çok teknolojik kültürde dayanışma erozyonuna, bağların yıpranmasına şahit oluyoruz. Teknolojinin tek faktör olduğu söylenemez, fakat yüksek düzeyde bunalan ve dağınık duyguların toplumun koşulu olarak yükselişine eşlik etmesi tesadüf değildir.

Fenomenin hayatımızı teknikleştirmekle ilgisi olduğu için ben şu anın kronik öfkeli alanlarına kadar gidebilirim. İnsan topluluklarını cihazlarıyla birlikte belirlediğimizde herşey olabilir. Sosyal ilişkilerde giderek artan çatlaklar herşeyin olabileceği ve olurluğu demektir. Hiçbir yerden hiçbir yere anında bağlantı bizim aşırılığımıza bir çözüm değildir.

Okullardaki, işyerlerindeki ve alışveriş merkezlerindeki alanlar bilinçli olarak incelenmediler ve bilinmez olarak kaldılar. Bu yönelimin toplum hakkında ne söylediği tartışılmıyor. Bu arada son versiyonunda daha da kötüleşiyor. Baba (ya da anne) bütün ailede katliam yapıyor.

Toplum da giderek zayıflamakta iken gerçek; ekranın arkasında kaybolduğunda ve direkt deneyim zayıfladığında tekno-meditasyon yeni zirvelere ulaşır. Sanal gerçeklik, herhangi biri? Gerçekten hemen hemen hiçbir topluluğun kalmaması üzücü bir durumdur. Bu nedenle siyasetçiler ve geliştiriciler (programcılar) bu sözü sıklıkla kullanıyorlar. Günümüzde toplum ne süreklidir ne de dolaysızdır (doğrudandır). Dijital dünyaya gerçekten ev denir mi?

Nasıl yapılırlığın problemi olarak bu kadar çok hayat teknolojik terimlerde inceleniyor. Bizim dünya ile, birbirimiz ile, insan olarak içgüdülerimiz ile olan doğal bağımıza ne oldu? Bu bir gecede olmadı. 1968’de Bilgisayar öncüsü J.C.R Licklider; “Gelecekte yüz yüze iletişim kurmaktansa makina yoluyla iletişim kurmak daha etkin biçimde olabilecek.” dedi. İnancını yitirmiş teknolojik alan yüz yüze iletişimi ısrarla tüketerek bu durumu başardı. Hangi yüksek fiyatla? Cep telefonlarının yerleşik gözetim fonksiyonunu ve beyin kanseri riskini bir yana bırakın diğer teknolojik gruplar gibi onlar doğal dünyanın sistematik yıkımı üzerine inşa edilmektedir. Ölü sayısı ne kadar ve böyle “harika” şeylere düşkünlük için alternatif olabilir mi?

Bireysel ve toplumsal yabancılaşma kitle toplumunun doğasında birleşiyor. Seri üretim, kitle kültürü, kitle tüketimi ne kadar sağlıklı? Bir zaman önce, W. H. Auden; “bir aldatıcı suç gibi zamanımızın koşullarının etrafı sarılıyor” sonucuna vardı. Fakat bu sadece aldatıcı bir ölçüde çünkü belirlenmiş olarak çağımızın temel özelliklerini kabul etmeye devam ediyoruz – sorunlaştırılmamaya ya da politize olmamaya, soruya açık değil.

Muazzam bir teknolojik yabancılaşma aileye de uzanıyor. Hiçbir yer bundan muaf değil. 1800’den itibaren küresel ısınma küresel sanayileşmenin artan seviyesine cevap verdi. Aletlere karşı olan modern sistemlerin teknolojisi endüstri olmadan varlığını sürdüremez.

iPhone'lar ve diğerleri bu bütünün bir parçası. Bir çözüm tüm parçaları sorgulamayı gerektirir. Teknolojik gelecek, gelecek değildir.
