#DELETED Reason for deletion: Machine translation (DeepL: after June 2022, Google Translate: before June 2022) has been detected. - https://mirror.anarsistkutuphane.org/c4ss.org/analyze/
#title Anarşizm: Nedir ve Ne Değildir?
#author Joseph Labadie
#SORTtopics giriş, anarşizm
#date Kış, 1979
#source 04.06.2022 tarihinde şuradan alındı: [[https://c4ss.org/content/56743][c4ss.org]]
#lang tr
#pubdate 2022-06-04T18:56:30
#notes Çeviri: Faruk Pak <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/joseph-labadie-anarchism-what-it-is-and-what-it-is-not][Anarchism: What It Is and What It Is Not]]


Şimdi size anarşizmin ne olduğunu anlatmamı istiyorsunuz değil mi? En azından deneyebilirim ve kendi açımdan en azından bilgisiz kapitalist gazetelerin, yalancıların, aptalların ve kötü niyetlilerin genel olarak servis ettiği anarşizm gibi bir şey olmadığını anlamanıza yardımcı olmak isterim.

Öncelikle Anarşizm hakkında gerçekleri öğrenmek isteyen herkesi, anarşizmin düşmanlarıyla konuşmaya değil de anarşistlerin kendileriyle konuşmaya ve anarşist literatürü okumaya teşvik etmek isterim. Bir de kendilerini anarşist olarak adlandırsalar bile birkaç hatta bir düzine kişinin bu konuda söyleyecekleriyle hareket etmek de her zaman güvenli değildir. Çoğunluğunun söylediği şeyleri alın ve aralarında uyuşamadıkları kısımları boşverin. Geriye kalanlar da muhtemelen doğrudur. Örneğin, Hristiyanlık nedir? Bir düzine veya daha fazla kişiye bu soruyu yöneltin ve muhtemelen cevaplarında her açıdan hemfikir olmayacaklar. Bazı temel önermeler üzerinde anlaşabilirler. Hristiyanlığın doğru anlatımı, aralarından birinin söylediklerinin aksine muhtemelen bu anlaştıkları noktalar olur. Bir felsefenin ne olduğunu bulmanın en iyi yolu bu ayıklama sürecidir. Bunu anarşizmin ne olduğunu belirlerken uyguladım ve gerçeğe makul şekilde yaklaştığımı söylemek adil bir karine olur.

Anarşizm, Benjamin R. Tucker ağzında, “insanoğlunun tüm hususlarının bireyler ve gönüllü dernekler tarafından yönetilmesi ve devletin ortadan kaldırılması gerektiği” doktrini olarak tanımlanabilir.

Devlet, “işgal ilkesinin, kendilerini belirli bir bölgedeki tüm halkın temsilcisi veya efendisi olarak gören bir birey ya da bireylerde ete kemiğe bürünmüş halidir” dir.

Hükümet, “invaziv olmayan bireyin harici bir iradeye tabi kılınmasıdır.”

Şimdi bu tanımları aklınızdan çıkarmayın ve “devlet”, “hükümet” veya “Anarşi” kelimelerini Anarşistin kendisinin kullandığı anlamdan başka anlamda kullanmayın. Bay Tucker’ın tanımları genel olarak, Anarşistler tarafından her yerde kabul edilmektedir.

Herbert Spencer ve diğerlerine göre devlet, savaştan, saldırgan savaştan, şiddetten doğdu ve her zaman şiddetle varlığını sürdürdü. Devletin işlevi her zaman yönetmek, egemen sınıfların yapılmasını istedikleri şeyleri egemen olmayan sınıflara yaptırmak olmuştur. Devlet bir monarşide kraldır, sınırlı bir monarşide kral ve parlamentodur, Amerika Birleşik Devletleri gibi bir cumhuriyette seçilmiş temsilciler ve İsviçre’de olduğu gibi bir demokraside seçmenlerin çoğunluğudur. Tarih gösteriyor ki devletin bireyler üzerindeki gücü azaldıkça, kitlelerin zihinsel, ahlaki ve maddi koşulları her zaman daha iyiye gitmiştir. İnsan, bireysel ve kolektif çıkarları hususunda daha çok aydınlandıkça, kendisi ve davranışlarına dayatılan otoritenin ortadan kaldırılmasında ısrar eder hale gelir. Kilise maddi konuda iyiye gitmiş, bağış toplayabilmiştir çünkü insanlar öğretileri kabul etmeye, desteklemeye yahut sapkın ilan edilip direğe bağlandıktan sonra yakılmaya yahut türlü kötü muameleye zorlanmamıştır; insanlar daha iyi giyinmeye başlamıştır çünkü devlet kılık kıyafet yasağını kaldırmıştır; insanlar kendi eşlerini seçebildikleri için mutlu olmuşlardır; kişinin, saçını kestirmesini, seyahat etmesini, ticaretini, evinin pencere sayısını, Pazar günleri tütün kullanıp kullanmayacağını veya öpüşebilip öpüşemeyeceğini vb. düzenleyen kanunlar ortadan kalktığından beri insanlar her açıdan daha iyiye gitmiştir. Rusya’da ve diğer bazı ülkelerde yasal izin olmadan ülkeye girmenize veya ülkeden çıkmanıza, yasaların onayladığı durumlar dışında kitap yahut gazete çıkarmanıza veya okumanıza, polise haber vermeden gece boyunca yabancı birini evinizde barındırmanıza izin verilmez ve binlerce başka şekilde bireyin hareketleri kısıtlanır. En özgür ülkelerde bile birey, vergi tahsildarları tarafından soyulmakta, polis tarafından dövülmekte, mahkemeler tarafından cezalara çarptırılmakta ve hapse atılmaktadır; bireyin davranışları saldırgan olmadığında yahut eşit özgürlüğü ihmal etmediğinde de otoriteler tarafından gözdağı verilmektedir.

Anarşizmin mutlak özgürlüğü tesis etmeyi amaçladığını öne sürmek, bazı Anarşistler tarafından bile sıklıkla düşülen bir hatadan ibaret. Anarşizm pratik bir felsefedir ve imkansızı başarmaya çabalamaz. Bunu söylemekle beraber Anarşizmin yapmayı amaçladığı şey, eşit özgürlüğü her insan evladına uygulanabilir kılmaktır. Buna göre çoğunluğun azınlıktan daha fazla hakkı olamaz, milyonların birden fazla hakkı yoktur. Bu, er insanın doğanın tüm ürünleri üzerinde parasız ve fiyatsız eşit haklara sahip olması gerektiğini; birinin ürettiği şeyin kendisine ait olacağını ve ya kanun kaçağı ya devlet olarak karşımıza çıkacak birey ya da birey grubunun, üretenin rızası olmadan herhangi bir pay almaması gerektiğini; herkesin kendi ürünlerini dilediği noktada takas edebilmesine izin verilmesi gerektiğini; isterse hemcinsleriyle işbirliği yapmasına ya da isterse onlarla herhangi bir alanda rekabet edebilmesine izin verilmesi gerektiğini; hemcinslerinin eşit haklarını ihmal etmediği sürece, çıkarttığı, okuduğu, yediği içtiği veya yaptığı şeylerde hiçbir kısıtlamaya tabi olmaması gerektiğini ima eder.

Anarşizmin Amerika Birleşik Devletleri’ne ithal edilen, pratik olmayan bir teori olduğu, birçok cahil yabancı tarafından sıklıkla belirtilir. Elbette bu sözü söyleyenler, önermenin yanlışlığının bilincinde olarak yapmışlar gibi yanılıyorlar. Kişisel özgürlük doktrini uygulamaya koyulması açısından bir Amerikan doktrinidir; Paine, Franklin, Jefferson ve diğerleri bunu çok iyi anlamış ve göstermişlerdir. Buraya dini konularda özel yargı hakkını kullanmak için gelen Püritenler bile bu konuda az çok bir fikre sahiptiler. Dinde özel hüküm verme hakkı, dinde anarşidir. Bireysel egemenlik doktrinini ilk formüle eden kişi de zaten aşağılık bir yanki idi, sonuçta Josiah Warren, Devrimci General Warren’ın soyundan geliyordu. Bu ülkede eyaletler arasında yapılan ticarette bir anarşiden söz edilebilir, sonuçta serbest ticaret anarşinin gözler önündeki halidir.

Suç işleyen hiç kimse Anarşist olamaz çünkü suç, agresyon yoluyla bir başkasına zarar vermektir – bu da anarşizmin tam tersidir.

Nefsi müdafaa durumları dışında bir başkasını öldüren hiç kimse Anarşist olamaz çünkü bu, bir başkasının eşit yaşama hakkını elinden almaktır – bu da Anarşizmin antitezidir.

Anarşizmin kınadığı şeyleri yaparak Anarşist olamazsınız.

Anarşizm bir arazi bulur ve toprak üzerinde tek mülkiyeti sağlardı, akabilinde de toprak rantı ortadan kaldırılmış olurdu.

Her bir bireye veya birliğe bir takas aracı olan parayı basma hakkı garanti edilecek ve akabilinde para üzerindeki faiz, işbirliği ve rekabetin yapabileceği ölçüde ortadan kaldırılmış olurdu.

Anarşizm patent ve telif haklarını reddeder ve bu konudaki tekelden kurtulmak için patent haklarını ortadan kaldırır.

Rastgele bir grup insanın bir bireyi istemediği bir şey için vergilendirme hakkını reddeder, vergilendirmenin ancak gönüllü olması gerektiğini öne sürer, örneğin kiliselerin, sendikaların, sigorta topluluklarının ve diğer tüm gönüllü derneklerin yaptığı gibi.

İnsan ırkının daha mutlu koşullara çıkabilmesinin en iyi yolunun hayatın her anında özgürlük olduğuna inanır.

Anarşizmin sosyalizm olmadığı söylenmekte. Bu da bir hata. Anarşizm gönüllü sosyalizmdir. İki çeşit sosyalizm mevcuttur, devletçi ve anarşist, otoriter ve liberter, devlet ve özgürlük. Gerçekten de toplumsal iyileşmeye yeğin her önerme, birey üzerindeki dış iradenin ve güçlerin birey üzerindeki etkisini artırmak yahut azaltmaktadır. Artırıyorlarsa devletçi; azaltıyorlarsa anarşistlerdir.

Anarşi; liberte, özgürlük, bağımsızlık, serbestlik, özyönetim, gayri-müdahalecilik, kendi işine bak ve komşunu rahat bırak, laissez faire, yönetilemezlik, özerklik vb. gibi anlayışlarla eşanlamlıdır.

Size, Anarşizmin ne olduğu ve ne olmadığı konusunda yalnızca yarım yamalak bir taslak verildiğini görüyorum. Konuyu daha fazla araştırmak isteyen entelektüel yetişkinler şunlarda aradıklarını bulabilir: Tucker’ın Instead of a Book; Proudhon’dan What is Property? ve Economical Contradictions; Tandy’nin Voluntary Socialism’i; Mackay’ın The Anarchists; Auberon Herbert’den Free Life; The Demonstrator; Lucifer ve daha nicesi.

