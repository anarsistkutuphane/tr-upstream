#title Lucy Parsons
#author Zeynep Ülger
#SORTtopics abd, anarko sendikalizm, anarşist, anarşist feminizm, haymarket, işçi mücadelesi, kadın mücadelesi, lucy parsons, tarih
#date 19.05.2022
#source 19.05.2022 tarihinde şuradan alındı: [[https://karala.org/dergi/portre-4-lucy-parsons-zeynep-ulger/][karala.org]]
#lang tr
#pubdate 2022-05-19T20:35:13


1 Mayıs’ı yaratan “günde 8 saat” mücadelesinin örgütleyicilerinden, Kara Enternasyonal IWPA’nın ve anarşist sendika IWW’nin kurucularından, Amerika polisinin “bin isyancıdan daha tehlikeli” olarak nitelendirdiği, ırkçılığa ve ataerkiye karşı mücadelenin önemli isimlerinden Lucy Eldine Gonzales, bilinen adıyla Lucy Parsons…

Hem Meksikalı hem Amerika yerlisi olan Lucy 1851 yılında Teksas’ta bir köle olarak doğdu. Ten rengi ve yerli kökeninden dolayı ırkçılık ve sömürüyle çok erken yaşlarda tanıştı. Amcasının çiftliğinde yaşayan Lucy kendisi gibi hayatı mücadeleyle geçecek olan, beraber anarşizm ve toplumsal devrim mücadelesini örgütleyeceği Albert Parsons’la 1871 yılında tanıştı. ABD’nin bir siyahın bir beyazla evlenmesini yasakladığı bir dönemde birlikte oldular.

Siyahların özgürlük mücadelesini savunan bir yayın olan “Spectator” isimli gazeteyi çıkardılar. Yürüttükleri ırkçılık karşıtı mücadeleden dolayı ABD’nin hedefi haline geldiler. Her şeye rağmen mücadeleye devam ettiler. Albert bacağından vuruldu ve Lucy ölüm tehditleri aldı. Siyahların verdiği mücadeleye karşı kurulan faşist çetelerden oluşan Ku Klux Klan tarafından da tehdit edilen Lucy ve Albert 1873 yılında Chicago’ya taşınarak mücadeleye orada devam etti.

Aktif bir şekilde mücadele yürüten Lucy sendikalarda ve işçi örgütlerinde de rol aldı. Uluslararası Kadın Konfeksiyon İşçileri Sendikası’nda (International Ladies Garment Union Workers), Emekçi Kadınlar Birliği’nin kuruluşunda ve Emek Şövalyeleri’nde (Knights Of Labor) yer aldı. Kara Enternasyonal IWPA’nın Chicago kolunun kuruluşunda örgütleyici olarak rol aldı.

Lucy Parsons, 1877 yılında sömürü koşullarına karşı ABD tarihinin en büyük genel grevlerinden birini örgütleyenler arasındaydı. Bu dönemde mücadelesini ve fikirlerini yazılarına da taşıdı ve “The Socialist”, “The Alarm” gibi anarşist yayınlar için yazılar yazdı.

1 Mayıs 1886’da anarşistler tarafından toplumsal devrim mücadelesinin bir basamağı olarak görülen “günde 8 saat” talebi çerçevesinde bir genel grev örgütlendi. Lucy de bu süreç içerisinde aktif bir rol oynamıştı. 3 Mayıs’ta grev kırıcıları engellemeye çalışan grevci işçilere polis saldırdı ve 4 işçi katledildi. Bu saldırıya tepki vermek için anarşistlerin örgütlediği 4 Mayıs’ta Haymarket Meydanı’nda düzenlenen mitinge yapılan polis saldırısı ve bu saldırı sonrasında patlayan bombanın sonucu olarak devlet anarşizme savaş açtı. 8 anarşist tutuklanarak idamla yargılandı.

Lucy Parsons bu süreçte hem idamları engellemek hem de devletin ve patronların sözcülüğünü yaparak anarşistleri hedef gösteren medyaya karşı gerçekleri haykırmak için şehir şehir gezerek konuşmalar yaptı. Lucy yoldaşlarını kaybetmesine ve devletin anarşizmi ezmeye çalışmaktaki kararlı tutumuna rağmen mücadeleyi bırakmadı.

1891’de Lizzy Holmes ile birlikte “Freedom: A Revolution Anarchist Communist Monthly” dergisini çıkarmaya başladı. Aynı zamanda Dünya Endüstri İşçileri IWW’nin kurucu üyelerindendi. Lucy dönemin baskılarına karşı ezilenlerin kendini özgürce ifade edebilmesi ve siyasi tutsakların özgürlüğü için de mücadele etti. 1905 yılında “Liberator” dergisinde yazılar yazmaya başladı. Lucy, hayatının sonuna kadar mücadeleyi sürdürdü ve 7 Mart 1942’de evinde çıkan bir yangında yaşamını yitirdi.

ABD’de anarşizmin örgütlenmesinde büyük rol oynayan Lucy’in anarşizme ve toplumsal mücadeleye bakışını kendi sözleriyle dinleyelim. 1884 yılında The Alarm gazetesinde yazdığı “To Trumps” (Serserilere) yazısında ezilenlere şöyle sesleniyor:

“’İyi patronla’ ‘kötü patron’ arasında hiçbir fark olmadığını görmüyor musunuz? Her ikisi için de sadece birer av olduğumuzu ve onların tek görevinin bizi soymak olduğunu; değiştirilmesi gerekenin patronlar değil endüstriyel sistem olduğunu göremiyor musunuz?”

Lucy Parsons, tüm yaşamı boyunca ezilenleri bu soyguna, sömürüye karşı kendilerini savunmaları için mücadeleye çağırdı. Lucy devlet ve kapitalizmin baskı, sömürü ve zor gücüne karşı pasifist yöntemlere sonuna kadar karşı çıktı ve “şiddetli bir doğrudan eylemin veya böyle bir eylemin tehdidinin, işçilerin taleplerini kazanmasını sağlayacağını” savundu.

“The Principles of Anarchism” (Anarşizmin İlkeleri) makalesinde oy vermeye ve devletli siyasete karşı durmuş ve düşüncelerini şu şekilde ifade etmişti:

“Anarşistler, toplumda büyük ve köklü herhangi bir değişiklikten önce uzun bir örgütlenme gerektiğini bilirler, bu nedenle seçim sandıklarına veya siyasi kampanyalara değil bireylerin kendi örgütlenmelerini yaratmasına inanırlar. Yardım için hükümetten yüz çeviririz, çünkü yasallaştırılmış gücün insanın kişisel özgürlüğünü istila ettiğini, doğal olanı ele geçirdiğini ve insan ile doğa yasaları arasına müdahale ettiğini biliyoruz. Toplumda var olan neredeyse tüm sefalet, yoksulluk, suç ve karışıklık hükümetler aracılığıyla uygulanan bu müdahaleden kaynaklanır.”

Kanunların ve yasaların sadece devletlerin işine yaradığını ve “suçu” önlemediğini bilen Lucy bu durumu şu şekilde açıklamıştır:

“Yasalar, hapishaneler, mahkemeler, ordular, silahlar ve cephanelikler suçun gerçek önleyicileriyseler; bunlara hepimizi aziz yapacak kadar sahibiz. Ama biliyoruz ki bunlar suçu önlemezler. Bunların tümü, sınıflar arasındaki mücadeleyi daha şiddetli hale getirerek; zenginleri daha zengin, yoksulları daha yoksul ve çaresiz hale getirerek kötülüğü ve ahlaksızlığı var ederler.”

“Suçun ancak insanlara iyilik yapmayı öğrettiğinde sona ereceğini biliyoruz, çünkü yanlıştan çok doğru yapmak onları daha mutlu ediyor. Biliyoruz ki, yasalar çıkarmak suçu önleyebilseydi veya insanları daha iyi yapsaydı, şimdiye kadar hepimiz melek olurduk.”

Lucy Parsons, anarşizm ve özgürlük için şu sözleri kullanmıştır:

“Özgürlük kelimesi anarşizmin felsefesine doğrudan dahildir. Ancak anarşizm ilerlemeye yardımcı olan diğer her şeyi içerecek kadar da kapsamlıdır. Anarşizm, insanın ilerlemesine, düşünceye veya araştırmaya hiçbir engel koymaz. Hiçbir şey kesin olarak ya da mutlak doğru olarak kabul edilemez ki gelecekteki keşifler onun yanlışlığını kanıtlamayabilir. Bu nedenle, tek bir yanılmaz, değişmez slogan vardır; o da özgürlüktür. Herhangi bir gerçeği keşfetme özgürlüğü, gelişme özgürlüğü, doğal ve eksiksiz yaşama özgürlüğü.”

Bir köle olarak doğan ve hayatı boyunca özgürlük mücadelesinin bir parçası olan Lucy, mücadeleyi bütünlüklü kavramış; işçi sınıfının özgürlüğü, ezilen halkların özgürlüğü ve kadınların özgürlüğü arasında hiçbir fark koymamıştır. Kadınların sınıf mücadelesindeki pozisyonunun kimilerince görmezden gelindiği tarihlerde bir kadın olarak mücadelenin en ön saflarında yer almayı başarmıştır.

Bütün yaşamı bir özgürlük manifestosudur Lucy’nin. Toplumsal mücadelelerin ekonomik taleplere sıkıştırılmaya çalışıldığı bir dönemde, doğum kontrolüne erişim hakkı gibi birçok konuda mücadele yürüten Lucy IWW’de yaptığı bir konuşmada şöyle anlatıyor kadın olmayı:

“Bizler kölelerin de kölesiyiz. Erkeklerden daha acımasızca sömürülüyoruz. Ne zaman ücretler düşürülecekse, kapitalist sınıf onları azaltmak için kadınları kullanıyor.”

Kadının sosyal hayattaki varlığının bile kabul edilmediği bir dönemde yaptığı bu konuşmaya, seks işçilerini “gece sokağa çıktığımda görebildiğim kardeşlerim” diyerek selamladığını da özellikle belirtmek gerekiyor. Lucy’nin 20. yüzyılın başında savunduğu ve uğruna mücadele ettiği talepler, dünyadaki birçok toplumsal hareketin gündemine 100 yıl sonra ancak girebilmiştir. Dönemin anarşist kadınlarında görülen bu değişmez özelliği, Lucy oldukça karakteristik olarak taşımıştır.

Kimdir Lucy? 19. yüzyılın ortasında Amerika’da bir siyahtır. Bir köledir. Bir kadındır. Bir anarşisttir. Onlarca işçi örgütünün, dünyanın gelmiş geçmiş en yaygın işçi örgütlerinden IWW’nin kurucusu, onlarca grevin örgütleyicisidir. Onlarca anarşist yayının yaratıcısı, onlarca özgürlük konferansının konuşmacısı, ABD tarihinin en büyük hukuk kumpaslarından birine karşı bir adalet savaşçısıdır. Dünya kadınlarının özgürlük mücadelesinin en erken militanlarından biridir. ABD polisinin tanımıyla “bin isyancıdan daha tehlikeli”dir. Hepimizden daha köle doğmuş, hepimiz için yaşamıştır!



