#title İletişim
#lang tr
#pubdate 2020-05-05T18:48:37




**=kutuphane{et} riseup {nokta}net=**

[[https://keys.mailvelope.com/pks/lookup?op=get&search=0xD6B7841BF67A6B05][GnuPG Public Key]] |
 2614 F37D B394 B98C D244 D7B7 D6B7 841B F67A 6B05

<quote>

İsteğe bağlı olarak bizimle PGP ile basit bir şekilde nasıl şifreli iletişime geçebileceğini öğrenmek için şu rehberdeki yönergeleri takip et:

[[https://guvenlik.oyd.org.tr/yazisma_guvenligi/mailvelope.html][Mailvelope ile Webmail Üzerinde Şifreleme]] (Unutma! [[https://guvenlik.oyd.org.tr/yazisma_guvenligi/index.html][Yazışma Güvenliği]] adındaki üst rehberi de öğrenmelisin.)
</quote>

-----------


*** Anti-Sosyal

IRC | [[/special/webchat][#kutuphane]]

Telegram | [[https://t.me/anarsistkutuphane][@anarsistkutuphane]]

Mastodon | [[https://kolektiva.social/@kutuphane][@kutuphane@kolektiva.social]] | [[/library/ozgur-yazilim-dernegi-neden-mastodon][Neden Mastodon?]]
