#title Noretorp Noretsyh
#author Kenneth Rexroth
#SORTtopics şiir, anarşist
#date 1956
#source 30.04.2022 tarihinde şuradan alındı: [[https://karala.org/dergi/noretorp-noretsyh-kenneth-rexroth-ceviri-burak-aktas/][karala.org]]
#lang tr
#pubdate 2022-04-30T11:01:56
#notes Çeviri: Burak Aktaş <br>Çevirenin Notu: Başlıkta tersten “Hysteron Proteron” yazılmıştır. Hysteron Proteron, mantıken sonra söylenmesi gereken şeyi önceden söylemek anlamına gelen bir söz sanatıdır.



Yağmurlu, sisli Sonbahar,
<br>
Bulutlardan kuleler
<br>
Parlak Pasifik gökyüzünde.

Golden Gate Parkı’nda,
<br>
Tavus kuşları,
<br>
Bağırıyorlar,
<br>
Düşen yaprakların
<br>
Arasında dolaşarak.

Duman karasındaki
<br>
Kan donduran gecelerde
<br>
<strong>Kronştad</strong> denizcileri yürüyor,
<br>
Budapeşte’nin sokaklarında.

Barikatın taşları,
<br>
Yükseliyor, parlıyor,
<br>
Şekilleniyor.
<br>
Şeklini alıyor,
<br>
<strong>Mahno</strong>’nun köylü ordularının.

Sokaklar meşalelerle aydınlanıyor.
<br>
Benzinle ıslanmış bedenleri,
<br>
<strong>Solovetski anarşistleri</strong>nin
<br>
Her köşede yanıyor.

<strong>Kropotkin</strong>’in açlıktan ölmüş bedeni doğuyor,
<br>
Devlet ofislerinin yanında,
<br>
Korkak bürokratların.

Bütün Polisolatörlerinde
<br>
Sibirya’nın,
<br>
Ölü partizanlar
<br>
Savaşa gidiyor.

<strong>Berneri</strong>, Andreas Nin,
<br>
İspanya’dan bir lejyonla geliyor.
<br>
<strong>Carlo Tresca</strong> geçiyor
<br>
Atlantik’i,
<br>
<strong>Berkman</strong> Taburuyla.

Buharin Acil Ekonomik Konsey’e katıldı.
<br>
Yirmi milyon
<br>
Ölü Ukrayna köylüsü
<br>
Buğday yolluyor.

Julia Poyntz,
<br>
Amerikan hemşirelerini örgütlüyor.

Gorki bir manifesto yazdı,
<br>
“Dünyanın bütün entelektüellerine!”

Mayakovski ve Essenin
<br>
Bir mısra üzerinde anlaştılar,
<br>
“Bırakalım İNTİHAR etsinler.”

Macar gecesinde bütün ölüler,
<br>
Tek bir sesle konuşuyor.

Bisikletle geçerken yeşilinden
<br>
Güneş lekeli
<br>
Kaliforniya Kasımı’nın,
<br>
O sesi duyabiliyorum
<br>
Tavus kuşlarının bağırtısından daha net,
<br>
Düşen öğleden sonra.

Tıpkı boyalı kanatları gibi rengi,
<br>
Sonbaharın bütün yapraklarının,
<br>
Yuvarlak batik etek
<br>
Senin için yaptığım,
<br>
Rüzgârda ışıldıyor
<br>
Eşsiz uylukların üzerinde.

Ah!
<br>
Hayal gücümün
<br>
Muhteşem kelebeği,
<br>
Gerçekliğe uçuyor,
<br>
Daha gerçek bütün hayallerden.
<br>
Kötülüğü dünyanın,
<br>
Senin yaşayan etine göz dikiyor.



