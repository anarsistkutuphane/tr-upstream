#title Hapishaneler: Suç Üniversiteleri
#author Pyotr Kropotkin
#SORTtopics suç, ceza, hapishane, tutsaklık
#date Ekim 1913
#source 09.01.2022 tarihinde şuradan alındı: [[https://barikathaber.org/article/sucun-yetistirme-yurtlari-hapishaneler-kropotkin/][barikathaber.org]]
#lang tr
#pubdate 2022-01-09T08:14:04
#notes İngilizce Aslı: [[https://theanarchistlibrary.org/library/petr-kropotkin-prisons-universities-of-crime][Prisons: Universities of Crime]]



Son zamanlarda birçok tanınmış avukat ve sosyoloğun gündemini meşgul eden, büyük “suç ve ceza” sorusunu şimdilik bir kenara bırakacak ve elimden geldiğince şu sorunun cevabını arayacağım: “Hapishaneler toplumdaki anti-sosyal davranışların sayısında azalma sağlayabiliyorlar mı?”

Bu soruya; hapishanelerde neler olup bittiğini bilen, önyargısız her insan güçlü bir ‘Hayır’ yanıtını verecektir. Aksi takdirde, konu ile ilgili ciddi bir çalışma yapılacak olursa, hapishanelerin -en iyisinin bile- “suç”un yetiştirme yurtları olduğuna ve anti-sosyal davranışların daha kötü bir hale gelmesine sebep olduklarına; sözgelimi suç olarak bilinen her ne varsa onun liseleri, üniversiteleri oldukları sonucuna varılacaktır. Tabi ki bir zamanlar hapsedilmiş kim varsa hapishaneye geri döneceği iddiasında değilim. Her yıl binlerce kişi yanlışlıkla hapse atılıyor. Ancak hapishanede geçmiş birkaç yılın -buranın bir hapishane olmasından dolayı- bireyi yargı önüne çıkaran kusurları artırdığını savunuyorum.

Bu nedenler; risk alma isteği, çalışmaya karşı duyulan antipati, (büyük oranda yapılacak işin iyi bir uzmanlaşma gerektirmesi sebebiyle) adaletsizlik ve ikiyüzlülükten dolayı toplumu hor görme, fiziksel enerji isteği ve bütün bu sebepler hapishanede tutsak edilerek pekiştirilir.

Yirmi beş yıl önce bu fikri geliştirdiğim ve şimdilerde baskısı tükenen kitabımda (Rus ve Fransız Hapishanelerinde) Fransa’da ikinci kez hapsedilen tutukluların sayılarıyla ilgili yürütülen bir soruşturmayla açığa çıkarılan gerçekleri inceleyip bu düşünceyi destekledim. Bu incelemenin sonuçlarına göre mahkemeye çıkanların neredeyse yarısı yargı önüne çıkarılmadan önce, beşte ikilik kısmı ise polis sorgusuna çıkarılmadan önce zaten bir ya da iki kez hapsedilmiş oluyor. Fransa’da ortaya çıkan bu yüzde kırklık korkunç oranın yanı sıra, Michael Davitt’e[1] göre kürek cezasına çarptırılmış tutsakların yüzde doksan beşi daha önce hapishane ‘eğitimi’ görmüş kişilerden oluşuyor.

Küçük bir düşünme süreci, meselenin başka türlü olamayacağını gösterecektir. Bir hapishane, tutsaklar üzerinde yozlaştırıcı bir etkiye sahiptir ve hep öyle olacaktır. Birini ilk kez hapse atmış olun. Binaya girdiği andan itibaren bütün insanlığını kaybeder, o artık sadece “Numara XY” dir. Kendi iradesiyle hiçbir şey yapamayacaktır. Onu aşağılık bir duruma düşürmek için aptal bir giysinin içine koyarlar. Onu bağlı olduğu bütün ilişkilerden mahrum bırakırlar ve böylece üzerinde olumlu etkisi olabilecek her bireyin eylemini dışarıda bırakırlar.

Sonra emeğini koyar ortaya, ancak bu onun ahlaki gelişimine yardımcı olabilecek bir emek değildir. Hapishane emeği, temelinde bir intikam aracıdır. Tutsak, uyguladığı cezaları “reform” yaparmış gibi gösteren bu “toplumun ileri gelenleri”nin zekası hakkında ne düşünmelidir?

Fransız hapishanelerinde tutsaklara bir çeşit faydalı ve maaşlı bir iş verildi. Ancak bu iş için bile saçma bir şekilde düşük ücret uygulandı ve hapishane otoritelerine göre başka türlü olması mümkün değildi. Hapishane emeği, diyorlar; değersiz köle emeğidir. Bunun sonucunda tutsak, çalışmaktan nefret etmeye başlar ve şöyle söyleyerek yaptığı işe son verir; “Gerçek hırsızlar biz değil, bizi burada zorla tutanlardır.”

Böylece tutsağın beyni, dolandırıcı şirket yöneticilerine saygı duyan, onu ise yeterince kurnaz olmadığı için kötü bir şekilde cezalandıran toplumun adaletsiz olduğu fikriyle tekrar tekrar dolup taşar. Ve dışarı çıktığı an çoğunlukla intikamını ilkinden daha ağır bir şekilde alır. İntikam, intikam doğurur.

Ondan alınan intikam üzerine, o da toplumdan intikam alır. Her hapishane, bir hapishane olduğu için, tutsakların fiziksel enerjilerini yok eder. Onlara kutup soğuklarından bile kötü davranır. Geçtiğimiz günlerde İngiliz Tabipler Birliği Kongresi’ndeki konuşmasında Miss Allen’ın gayet açık bir şekilde ortaya koyduğu gibi; temiz havaya duyulan istek, varoluşun monotonluğu, özellikle izlenim edinme isteği, tutsağın bütün enerjisini alır ve uyarıcılara (alkol, kahve) yönelik bir arzu üretir. Ve nihayet, anti-sosyal eylemlerin çoğu irade zayıflığına dayandırılabilir, hapishane eğitimi ise tamamen, iradeyi her ortaya çıktığı yerde öldürmeye yöneliktir.

Daha da kötüsü. Ben hapishane reformcularına ciddi anlamda, Amerikan hapishanelerinde 14 yıl tutulmuş ve deneyimlerini büyük bir samimiyetle kitabında anlatmış olan Alexander Berkman’ın “Hapishane Anıları”nı (Prison Memoirs)[2] okumalarını tavsiye ediyorum. Okuyan, eğer bu cehennemden kurtulmaya karar vermezse, dürüst duyguların nasıl baskılanması gerektiğini görecektir. 5-6 yıllık böylesi bir eğitimden sonra bireyin iradesi ve iyi niyetini arttıracak ne olabilir?

Ve serbest bırakıldıktan sonra, onlarla birlikteliği yüzünden hapse girdiği dostlarının yanına dönmecekse nereye gidebilir? Onu kendileriyle eşit görenler yalnızca o dostlarıdır. Ama eğer onlara katılırsa mutlaka birkaç hafta içinde geldiği yere geri dönecektir. Ve sonunda döner. Gardiyanlar bunu iyi bilir.

Bana sıkça “hapishaneler için nasıl reformlar öneriyorsunuz” diye soruluyor; şimdi, 25 yıl önce olduğu gibi, hapishanelerin nasıl düzeltilebileceğini gerçekten bilmiyorum. Onların temeline kadar yıkılması gerekir. Şöyle de denebilir ya da şöyle bir yol çizilebilir; her ne yapıyorsanız daha az zalim, daha fazla düşünceli olun. Ancak bu, şunu beraberinde getirecektir: Her hapishaneye müdür olarak bir Pestalozzi[3] ve gardiyan olarak da 60 Pestalozzi’yi görevlendirelim, ne kadar saçma olurdu. Ama bunun dışında hiçbir şey işe yaramaz.

Oldukça iyi niyetli Massachusetts hapishane görevlileri, önerilerimi sormak için yanıma geldiklerinde onlara tek söyleyebildiğim şuydu: Eğer hapishane sistemini tamamen ortadan kaldıramıyorsanız, o zaman kesinlikle çocukları ya da gençleri hapishanelerinize almayın. Eğer bunu yaparsanız, bu bir cinayettir. Ve sonra, hapishanelerin ne olduğunu deneyimleyerek öğrendikten sonra, gardiyan olmayı reddedin ve suçla mücadele etmenin tek doğru yolunun onu önlemek olduğunu anlatmaktan asla yorulmayın. Maliyetine, sağlıklı belediye konutlarında, okulda veya ailede, hem ebeveynlerin, hem çocukların; her kızın ve erkeğin bir meslek öğrendiği, komünal ve mesleki iş birliği, her türden uşraş için topluluklar ve her şeyden önemlisi gençlerde, insan doğasını ahlaki duyarlılığa taşıyabilecek bir özlemin, idealizmin gelişmesi. Bunlar cezalandırnanın asla yapamayacağı şeyleri başaracaktır.

<br>

[1] Michael Davitt (1846-1906), IRA’nın öncüsü olan İrlandalı Fenian Kardeşliği’nin üyesi, toprak ağalarına karşı köylü hareketini örgütleyenlerdendi.

[2] Bu metnin Türkçesi, Kavram Yayınları’ndan yayınlanan “Anarşistin Yaşamı-Alexander Berkman’ın Yazıları” isimli kitabın birinci kısmında yer almaktadır.

[3] Johann Heinrich Pestalozzi (1746-1827), Toplumun eğitimle düzeltilebileceğine, her insanın iyiliğe elverişli olduğuna inanan, bireylerin toplumdaki yaρıcı rolünü yerine getirebilmesi için, ahlaki eğitimin, hayati bir değeri olduğunu öne süren pedagog.



