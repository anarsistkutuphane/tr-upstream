#title Lenin'e Mektup
#author Pyotr Kropotkin
#SORTtopics mektup, rus devrimi, eleştiri
#date 21.12.1920
#source 07.09.2022 tarihinde şuradan alındı: [[https://www.marxists.org/turkce/kropotkin/1920/aralik-21-1920.htm][marxists.org/turkce]]
#lang tr
#pubdate 2022-09-07T20:10:48
#notes Çeviri: Deniz Muratli <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/petr-kropotkin-letter-to-lenin][Letter To Lenin]]


<quote>
Dmitrov (Moskova ili)
<br>21 Aralık 1920
</quote>

Saygıdeğer Vladimir Illich,

Izvestiia ve Pravda’da, Sovyet hükûmetinin, Savinkov’un gruplarındaki SR’ları [Sosyalist Devrimci Parti üyeleri], milliyetçi ve stratejik merkezden Beyaz Muhafızları ve Wrangel’in subaylarını rehin olarak tutacağına; sovyet liderlerinden herhangi birine karşı suikast girişiminde bulunulması durumunda bu rehineleri “acımadan katledeceğine” dair bir duyuru yayımlandı.

Etrafında, böyle tedbirlerin Ortaçağ’ın ve din savaşlarının en kötü dönemlerine dönüş anlamına geleceğini, ve komünist ilkeler üzerine kurulu bir toplum yaratma görevini üstlenmiş bir halkın bunu hak etmediğini yoldaşlarına hatırlatabilecek ve açıklayabilecek tek bir kişi bile yok mu? Komünizmin geleceğini önemseyen kimse böyle tedbirler almaya yanaşamaz.

Rehinin ne olduğunu sana kimsenin açıklamamış olması mümkün müdür? Bir rehine, herhangi bir suçtan dolayı hapsedilmez. Düşmana şantaj yapabilmek için rehin tutulur. “Bizden birini öldürürseniz, biz de sizden birini öldürürüz.” Ama bu bir adamı her sabah darağacına götürüp geri döndürmek, “Biraz daha bekle, bugün değil...” demek değil midir?

Yoldaşların, bunun rehineler ve aileleri için işkencenin geri getirilmesi olduğunu anlamıyorlar mı?

Birinin çıkıp bana iktidardaki insanların da kolay hayatlar sürmediklerini söylemesini bekliyorum. Günümüzde, kralların arasında bile suikast olasılığını “meslekî bir tehlike” sayan kimseler var.

Ve devrimciler, hayatlarını tehdit eden mahkemelere karşı kendilerini savunma sorumluluğunu üstleniyorlar. Louise Michel bu yolu seçti. Ya da Malatesta ve Voltairine de Cleyre’nin yaptığı gibi, işkence görmeyi reddediyorlar.

Krallar ve papalar bile rehin almak gibi barbarca öz savunma yöntemlerini reddediyorlar. Yeni bir hayatın havarisleri ve yeni bir toplumsal düzenin mimarları nasıl olur da düşmanlarına karşı böyle bir öz savunma yöntemini benimseyebilirler?

Bu, komünist deneyi başarısız bulduğunuz ve değerli gördüğünüz sistemi değil, kendinizi kurtarmayı amaçladığınız anlamına gelmez mi?

Yoldaşların, sizin, komünistlerin, (hatalarınıza rağmen) gelecek için çalıştığınızın farkında değiller mi? Ve bu yüzden ilkel teröre çok yakın eylemlerde bulunarak çalışmanızı lekelememeniz gerektiğinin? Geçmişteki devrimciler tam da bu eylemleri gerçekleştirdikleri için yeni komünist çalışmalar çok zorlaşıyor.

En iyilerinizin gözünde, komünizmin geleceğinin, hayatlarınızdan daha değerli olduğuna inanıyorum. Bu gelecek hakkındaki hayaller, sizi bu tedbirlerden vazgeçmeye itmeli.

Ciddi eksikliklerine rağmen (ve ben, bildiğin gibi, onların çok iyi farkındayım), Ekim Devrimi muazzam bir gelişmeyi sağladı. Batı Avrupa’daki insanların düşünmeye başladıklarının aksine, toplumsal devrimin imkânsız olmadığını gösterdi. Ve tüm kusurlarına rağmen, eşitliğe doğru, geçmişe dönüş girişimleriyle aşındırılamayacak bir şekilde ilerleme kaydediyor.

O zaman neden devrim, sosyalizme ya da komünizme özgü olmayan, eski düzen ve kargaşayı, sınırsız, her şeyi yiyen bir otoriteyi temsil eden kusurlarla, onu yıkıma sürükleyen bir yola itiliyor?

<right>
P. Kropotkin
</right>





