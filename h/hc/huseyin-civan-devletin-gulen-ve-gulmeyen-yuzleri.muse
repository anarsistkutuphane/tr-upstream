#title Devletin Gülen ve Gülmeyen Yüzleri
#author Hüseyin Civan
#SORTtopics devlet, yolsuzluk, akp
#date 08.01.2014
#source Meydan Gazetesi
#lang tr
#pubdate 2020-04-21T20:35:59


Yaşanan gerilimin sinyalleri, aslında Oslo Görüşmeleri’nin sızdırılması ve MİT müsteşarı Hakan Fidan’ın 7 Şubat 2012’de KCK kapsamıyla ifadeye çağırılmasıyla fark edilmişti. 11 yıldır TC iktidarında bulunan AKP ile onun yakın bir zamana dek, iktidarındaki paydaşlarından olan Gülen Cemaati ile aralarında var olan, ”dershanelerin kapatılması tartışması” üzerinden hissedilmeye başlanan gerilim, 17 Aralık sabahı gerçekleşen polis operasyonlarıyla iyice açığa çıktı ve gündemin en önemli konusunu oluşturdu.

Kamuoyunda cumhuriyet tarihinin en büyük rüşvet ve yolsuzluk operasyonu olarak adlandırılan bu operasyonda açığa çıkanlarsa bakan oğulları, belediye başkanı oğulları, inşaat şirketleri patronları, banka genel müdürleri, kara para ticareti yapanlar, rüşvet, ihalelerde yolsuzluk, uluslararası altın ticareti, para sayma makineleri, ayakkabı kutusuna saklanan milyon dolarlar oldu.

Hükümet ve Hizmet Hareket’i arasındaki bu gerilim sonucunda, 11 yıllık “istikrarın” nasıl sağlandığı kısmen ortaya çıkmış oldu. 11 yıllık aklık, pürü paklık yalanlarına sığdırılanlar para çeşmelerinin başını tutmuş olanlardı. Emlak sektörü ile ilgili bir bakanlık, finans sektörü ile ilgili bir bakanlık ve tüm bunlar arasındaki güvenliği sağlayacak bürokratik merkez konumunda bulunan İçişleri Bakanlığı…

Hükümetin ağzından eksik etmediği ancak işçilerin, emekçilerin bir türlü farkına varamadığı istikrarlı büyümenin, aslında kimlerin cebini büyüttüğü, kimler için istikrar olduğu ortaya çıktı. Halkın maruz kaldığı ekonomik sömürünün, kimleri zengin ettiği ayan beyan ortaya çıkmış oldu. Madenlerde, inşaatlarda, fabrikalarda yaşamlarını üç kuruş için riske atanların emekleri üzerine konan bu zat-ı muhteremlerin bulunduğu konumlar aslında birer rastlantıdan mı ibaretti?

Hükümet açığa çıkan durumdan sadece yargı süreciyle işleyen ve bakanların istifalarıyla sonuçlanan hukuksal bir kayıp yaşamadı. Aynı zamanda hükümetin siyasi karizması, özellikle partiden istifa eden milletvekilleriyle sarsıldı. Sayıca fazla mitinglerle, başbakana destek eylemleriyle durum düzeltilmeye girişildiyse de bu siyasi karizma yitimi engellenemedi. Ölçüt olarak özellikle uluslararası medya kuruluşlarının sürecin hemen başından itibaren AKP politikalarının otoriterliğine vurgu yapan yazılarını (AKP tarafından her ne kadar faiz lobileri ile ilintili gösterilse gösterilsin) düşünmek gerek. Son süreçte varılan nokta, ortaya çıkan yolsuzluğun, bu siyasi karizma yitiminde etkili olacağıdır. Hem de batının İhvan’dan vazgeçtiği bir süreçte buna benzer bir durumun yaşanmasının rastlantısallığı da özellikle tartışmalı.

AKP, süreci belli kurumların başında bulunan ve operasyonu yürüten kişileri bulundukları konumdan alarak karşıladı. Tayyip Erdoğan’ın söylemi, gittikçe artan bir şekilde sertleşti. Partisinin yolsuzluğunu aklamaya çabasıyla, ayrılanlar için “biz ayıkladık” ifadesini kullandı. Hükümetin çevresindeki medya kuruluşları da aynı sertliğe geçmekte sıkıntı yaşamadı. Cemaatle ilişkili olduğu bilinilen kanallar ve gazeteler topa tutuldu. Çıkarılan yönetmeliklerle yargıya müdahale edildi. AKP’nin süreci yönetmekte uyguladığı politikalar “darbe” olarak nitelendirildi.

Yolsuzluk ve rüşvetin bu kadar ayan beyan ortada olmasına rağmen, hükümetin otoriterliği kullanarak durumu saklama gayretleri, gözlerden birkaç şeyi ya uzak tuttu ya da bu birkaç şeyin normalleşmesine izin verdi.

AKP hükümeti karşısında güç odağı olarak beliren yargı-yürütme-yasama etkisi, tüm kabineyi değiştirtecek olan cemaatti. Cemaatin bu etkisi, AKP iktidarının en başından bu yana biliniyordu. Ancak işin şaşırtıcı kısmı bu etkiye sahip olan cemaatin siyasi bir yapı olarak meşruiyetidir. Cemaatin siyasi meşruiyeti sorgulanmadan, gazetelerden, televizyon kanallarından, internet sitelerinden bu iktidar çatışması gündemleştirildi. Cemaat herhangi bir siyasi kurum değildi. Gülen ve cemaatinin, ta Pensilvanya’dan buraya bu kadar etki edebileceği, en azından ilgili okullar, dershaneler ya da STK’lar üzerinden tahmin edilemezdi. Hükümet için Gülen’in her bedduasında, bu bedduayı emir telaki eden ne kadar çok bürokrat varmış meğer!

Devletin içinde kurumsallaşmış bu yapının hangi seçimde, bu konumu kazandığı, hangi yetkililer tarafından atandığı meçhul! Cemaat tüm bu yolsuzluk bilgilerine sahipken, gerilimin tırmanması sonucunda bir koz gibi oynadığı 17 Aralık operasyonu, cemaatin niyetinin ne denli “saf” olduğunu göstermek açısından önem taşıyor. Cemaat, operasyonu artan gerilim sonucunda siyasi bir manevra olarak kullanmıştır; AKP’nin yolsuzluklarını ortaya çıkarmak için değil, onun üzerinde bir baskı unsuru oluşturabilmek için kullanmıştır.

Hükümetin ortaya çıkan yolsuzluğunda, bu ayrıntının kaybolmaması önemlidir. Keza ezenler arasındaki savaşta bize düşen, ezenlerden birini desteklemek değil, yaşamlarımızı çalanlara karşı mücadele etmektir.

Yaşanılan tüm bu süreçler için hem hükümetin hem de muhaliflerin (hatta cemaatin de), çözüm olarak gösterdiği seçimlerin neye denk düştüğünü bu açıdan tekrar düşünmek gerek. Siyasi iradelerimizi sandıklarla teslim alanların “paralel evrendeki” izdüşümlerini düşünmek gerek. Mücadele de en etkili çözüm, bu kriz anlarında siyasi irademizi sokaklarda, meydanlarda KAZANARAK, seçim aldatmacasının tuzağına ve oyalamasına düşmeden Taksim İsyanı’nda olduğu gibi devrimi şimdi şu anda yaşayabilmektir.



