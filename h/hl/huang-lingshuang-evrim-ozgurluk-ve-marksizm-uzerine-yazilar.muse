#title Evrim, Özgürlük ve Marksizm Üzerine Yazılar
#author Huang Lingshuang
#SORTtopics çin, evrim, anarşizm
#date 1917-1929
#source ANARŞİZM: Özgürlükçü Düşüncelerin Belgesel Bir Tarihi
#lang tr
#pubdate 2020-05-06T12:00:51
#notes Çeviri: Nil Erdoğan, Mustafa Erata <br> Çevirenin Notu: Huang Lingshuang (1898-1982), Shifu’nun erkek kardeşi Liu Shixin’in Guangzhou’da oluşturduğu anarşist komünist grubun bir üyesiydi. Daha sonra Beijing’deki ilk anarşist grubun kurulmasına yardım etti. Rus Devrimi’nden sonra Marksizm’e eleştiri getiren ilk Çinli anarşistler arasındaydı. İlk pasaj, Huang Lingshuan’ın, ilk olarak Ocak 1917’de yayımlanan anarşist mecmua Evrim’in açılışına yazdığı ‘Deklarasyon’ dan alınmıştır. İkinci pasaj, ilk olarak Temmuz 1917’de yayımlanan, Beijing’deki anarşist Gerçek Derneği’nin yayın organı Özgürlük Belgeleri’ne yazdığı önsözdendir. Üçüncü pasaj, Huang Lingshuang’ın, ilk olarak Mayıs 1919’da Yeni Gençlik’te yayımlanan, ‘Marksizmin Eleştirisi’ adlı makalesinden alınmıştır. Bu yazılar Kropotkin’in Çinli anarşistleri arasındaki süregiden etksini göstermektedir. Son pasaj, Huang Lingshuang’ın Social Evolution (Şangay: World Books, 1929) kitabındandır, bu kitapta, kültürel evrimi biyolojik ve toplumsal evrimden ayırmış (bu ayrım Kropotkin tarafından asla açıkça yapılmamıştır.) ve önceki Marksizm eleştirisine uygun olarak, evrensel tarihsel gelişim aşamaları kavramlarını eleştirmiştir. İngilizceye çeviriler Montgomery Üniversitesi, Tarih ve Siyaset Bilimi bölümünden tarih doçenti Shuping Wan tarafından yapılmıştır.





*** Evrim için Deklarasyon <sup>1917</sup>

TOPLUM, ORGANİZMALAR bütünlüğüdür. Her organizmanın, her birinden yeteneğine göre, her birine ihtiyacı kadar, kendi grubundan yardım aramasıyla, insanoğlu daha fazla mutluluk için daha az zaman harcayabilmelidir. Böylelikle insan toplumunun gelişmesinin yolunun ahlakın gelişmesinden geçtiğini anlayabiliriz. Bu amaçtır: Dünya üzerindeki tüm insanlara mutluluğu getirmek. Bu yüzden, toplumsal evrim tartışmalarında soru, hangi tür sistemin topluma en iyi şekilde uyacağı, insan mutluluğunu nasıl arttırabileceğimiz ve bu özellikleri nasıl geliştirebileceğimizdir… Modern anarşizm bu gerçeği göstermek anlamına gelir. Şimdi bu gerçek açığa kavuştuğundan, bizim için zaman bir devrim başlatma ve bu gerçeği gerçekleştirme zamanıdır. Devrim ne anlama gelir? Çin dilindeki “geming” kelimesi Batı dilinde “revolution”(devrim) demektir. “Re”, “geng” (more-daha fazla) demektir; “evolution” (evrim), “jinhua” demektir. Eğer “re” ile “evolution”u birleştirirsek, “revolution” (devrim)kelimesini elde ederiz ki, bu basitçe “daha fazla evrim” anlamına gelir. Fakat, savunduğumuz devrim çeşidi yüzeysel devrim çeşitlerinden bir hayli farklıdır. Örneğin, İtilaf Devletleri, Almanya ve Avusturya-Macaristan’ı yendiğinde, birçok insan bunu adaletin güç üzerindeki zaferi olarak anlar. Zihinlerimizdeki otoriteryen iktidar çeşidi, Almaya ve Avusturya-Macaristan militarizminden ve Nietzsche’nin “üstün insan”ından fazlasına gönderme yapar. Toplumumuzda tüm insanların özgürlüğe ve mutluluğa ulaşmasını engelleyen her şey-politika, din, yasa, kapitalizm gibi- otoriteryen bir iktidardır. Daha fazla ilerlemeli ve bunları bütünüyle ortadan kaldırmalıyız. Halkın dünya devrimi, onların yaşamını “karşılıklı yardımlaşma”ya (herkesten yeteneğine göre, herkese ihtiyacı kadar) yönlendirecektir. Bu, evrimin gerçekliğidir (ne hükümet ne de özel mülkiyet) ve bu, gerçeğin iktidar üzerindeki nihai zaferidir! Bu, mecmuamızın taraftarı olduğu şeydir.

<right>
Evrim, cilt.1/ 10 Ocak 1917
</right>

*** Özgürlük Belgelerine Önsöz <sup>1917</sup>

Dünyanın evrimi çok yavaştır. Binlerce yıllık karmaşanın ardından, cumhuriyete henüz ulaşılmıştır. Halkın öncekinden sadece biraz daha fazla mutlu olduğu görülüyor. Buna rağmen iktidar sahipleri insanların kendileri için özgürlük aradıklarını keşfettiklerinde, bu yeni yükselen eğilimi engellemek için her korkunç aracı kullanırlar. İdeal toplum hala uzaktadır. Bizler günümüz toplumundaki eşitsizliklerden ve sayısız insanın mutsuzluğundan etkilenerek, toplumu dönüştürme gerekliliğinin farkına vardık. Amacımız, politik örgütlenme olarak anarşizmi, ekonomik ideal olarak komünizmi gerçekleştirmek. Madem ki amaç belirlendi, bunu gerçekleştirmek için tüm güçlüklerin üstesinden gelmeye azimliyiz. Zorluklara ve acılara dayanmak bizim için bir şey değil. Anarşizmin güzelliğinin ve komünizmin iyiliğinin değerinin biliyoruz, idealizme bir gecede ulaşılamayacağını da anlıyoruz. Önceliğimiz, anarşist komünizmi halk arasında yaymak ve öz bilinci yükseltmektir. Buna ulaşmanın iki yolu vardır; şiddet yolu bombalar ve tabancalar kullanmaktır… Barışçıl yol, çoğunluğun ahlakını ve bilgisini geliştirmek için eğitimi ve iknayı kullanmaktır, bu yol kalbe uygun düşer. Bu iki yol çok farklıymış gibi görünür, fakat bunlar hiçbir surette birbiriyle çelişki içinde değillerdir. Kişisel olarak ben barışçıl yolun daha etkili olduğunu düşünüyorum.

Ah, günümüz sosyal sisteminin ne denli kötü olduğunu görmüyor musunuz? Bunu görmezden mi geleceksiniz? Sistemi dönüştürmek amacıyla, tüm dünya üzerindeki ilerici insanlara yetişmek ve onlarla birlikte çalışabilmek için elimizden gelen her şeyi yapmak zorundayız. Şiir klasiklerinde şöyle bir mısra vardır: “Yağmur ve rüzgar karanlığı kasveti getirir, fakat horoz, ötüşünü asla durduramaz.” Yeteneğimizin sınırlılığına rağmen, bu eseri kolaylaştırmak ve bir dalga yaratmak için kendi payımıza düşeni yapacağız. Toplumdaki aydınlanmış insanların kaygımızı paylaşması en büyük beklentimizdir.

<right>
Özgürlük belgeleri/1 Temmuz 1917
</right>

*** Marksizm Eleştirisi <sup>1919</sup>

Marx’ın politik fikirleri, Engels ile birlikte yazdıkları “Komünist Manifesto” da bulunabilir. (Marx ve anarşist Bakunin uluslararası işçi birliğinde yüzleştiler. Aslında, Marx’ın komünizm fikri şimdi kolektivizm düşüncesiyken, Bakunin’in kolektivizm fikri şimdiki komünizm düşüncesidir) Manifesto’da sosyal demokrasinin politikası olarak görülebilecek on kıstas vardır. Peki bu politikalar ne ile alakalıdır?

 1. Mülkiyetin kaldırılması;
 2. Ulaşım sisteminin devlet tarafından yönetilmesi;
 3. Fabrikaların ve üretim araçlarının devletin elinde toplanması;
 4. Özellikle tarım için endüstriyel orduların kurulması.

Bu politikalara en ciddi eleştiriler, komünizm anlayışları Marx’ın kolektivizm bir hayli farklı olan anarşistlerden geldi. Anarşistler, tarihsel açıdan devletin sadece azınlığın ayrıcalıklarını ve mülkiyetlerini korumak için örgütlendiğine inanırlar. Şimdiye kadar bütün eğitim, devlet dini ve ulusal savunma iktidarı, devletin ellerinde toplanmıştır. Devlete, toprağın, madenlerin, demir yollarının, bankaların, sigortaların denetimi gibi daha fazla iktidar verirsek, devletin tiranlığı daha da merhametsiz olmaz mı? (bunlar Kropotkin’ in Encyclopedia Britannica’ daki sözleridir.) Liderlerimizin bir Napolyon veya bir Yuan Shikai [Çinli güç sahibi bir adam] olmayacaklarının güvencesini verebilir miyiz? Dahası, sosyalizm birey özgürlüğünün bastırılması olmamalıdır. Sosyal Demokrat Parti hükümeti, endüstriyel ordular ve tarımsal orduların kurulmasını istiyor. Bu bireyin baskılanması değil mi? Ayrıca dağıtım ilkelerinde de bazı sorunlar var. Toplum bireylerden farklı değildir. Sosyalizmin ışığı altında, toplumun mülkiyeti bireyden ziyade halka ait olmalıdır. Marx’ın kolektivizm düşüncesine göre, konutlar ve kıyafetler gibi şeylere özel olarak sahip olunabilir. Ben mülkün, özel mülkiyetinin sosyalizm ilkesiyle çelişkili olduğuna inanıyorum. Sığır ahırı kamu mülkiyetiyken, aynı evdeki yatak odalarının özel mülkiyet olması sorun değil mi? Ayrıca, Marksistler ‘herkese yeteneğine göre’ yi savunuyor. Öyle olursa, zayıf yeteneklere sahip kişi yaşam araçlarını yitirirken, güçlü yeteneklere sahip olan kişi ödüllerin zevkini çıkaracaktır. Zayıf beceri, birinin fiziksel durumundan kaynaklanır; onun tembelliğinden değil. Böylesi bir dağıtım yönetiminin insan mutluluğuna verecek bir şeyi yoktur. Anarşist komünistler devlet örgütlenmesini altüst etmeyi ve halkın girişimlerde bulunmak üzere eğitim birlikleri ve zirai birlikler gibi çeşitli birlikleri kurmaları için olanak sağlamayı isterler. Bu birlikler adım adım, her çeşit otoriteryen iktidarı ortadan kaldırmak ve her bireye eşitliği ve mutluluğu getirmek maksadıyla, toplum içerisindeki tüm işlerle başa çıkmak açısından yeterince karmaşık hale geleceklerdir. Emek ilkeleri “herkesten yeteneğine göre” ve dağıtım ilkeleri “herkese ihtiyacına göre” dir. Anarşistler ve Marksistler arasındaki farkın odak noktası budur.

<right>
Yeni Gençlik, Cilt.6, No:5/ Mayıs 1919
</right>

*** Toplumsal Evrim <sup>1929</sup>

İnsanın toplumsal yaşamının derecesi, kültürel gelişmişlik derecesi ile değerlendirilir. Kültür insan toplumunun eşsiz bir niteliğidir ve insan yaşamıyla birlikte var olur… Toplumsal evrim insanlığın ortaya çıkmasından önce başlamıştır, kültürel evrim ise tam da insan yaşamının mutlak başlangıcıyla başlamıştır. Kültürün ve insan yaşamının gelişimi eş zamanlı olarak başlamıştır. Toplumsal evrim organik evrimden, kültürel evrim toplumsal evrimden doğmuştur.

Toplumsal evrimin ve kültürel evrimin dünya evrim sürecindeki yeri nedir? Dünya evriminin doğal bir sonuç olduğunu biliyoruz. En başta, dünyada sadece fiziksel ve kimyasal olgular vardı; uzun bir gelişim döneminden sonra hayat ve toplum meydana çıktı. Aşamalı evrimin son ve en yüksek ürünü kültürdür…

Toplumsal evrim gerçeğini anlamak istiyorsak, organik evrimi incelemek için kullanılan yöntemlerin ve kavramların, bizim toplumsal evrim çalışmamıza yeterli yardımı sağlayamayacağının farkında olmamız gerekiyor.

İlk olarak, organik bilimin konuları (bitkiler ve hayvanlar) kalıtımla alakalıdır, ancak kültür kalıtım yoluyla geçmez… Ve ırkla hiçbir alakası yoktur. Kültür süper organiktir [superorganıc] ve biyoloji alanının ötesindedir; ikinci olarak, insan fizyolojisi buzul çağının sonundan beri çok değişmemiştir. İnsanların ilk atası olarak düşünülen Neanderthal kafatası çok büyüktü; modern insana benzer en gelişmiş Cro-Magnon’un muazzam bir beyin kapasitesine sahip olduğu aşikardı. Antropolojiye göre, fiziksel güçleri ve beyin kapasiteleri modern insanınkinden çok az farklıydı, ancak onların taş devri kültürüyle modern kültür arasındaki fark devasadır.

Özetle, toplumsal evrimin kültürün gelişimine bağlı olduğu ve kültürel evrimin toplumsal evrimin yönünü belirlediği emin bir biçimde söylenebilir…

On dokuzuncu yüzyılda, jeologlar, paleontologlar ve biyologlar gelişim aşamalarına dair birçok teori önerdiler. İstisnasız her organik yaşamın, ilk jeolojik devirden modern devire [bu aşamalardan] geçtiğini öne sürdüler. Bu, toplumun gelişim aşamaları fikrine yol açtı. [Herbert) Spencer, toplumun politik ve sosyal sisteminin, basit sistemlerden karmaşık sistemlere doğru belirli bir dizide ilerleyen birçok değişimi tecrübe ettiğini ileri sürdü. [Lewis] Morgan’ın Eski Toplum’u bu türden bir evrim kavramının “kapsamlı” bir modeli olarak görülebilir…

Günümüz antropologları böylesi bir argümanı kabul etmiyor. Geniş kapsamlı bir genelleme yapmak yerine, belli grupların kültürel özelliklerini çalışmaya çok önem veriyorlar, bu da onları tüm ulusların istisnasız aynı sosyal ve kültürel aşamalardan geçmek zorunda olduğunu reddetmeye götürmekte…

Ekonomik ilerlemenin zorunlu aşamaları olduğu önerisini de reddediyoruz. İlk olarak, teknoloji veya ekonomi, örgütlenme veya alım satım tipi, veya değişim araçları gibi unsurları ölçmek için neredeyse hiçbir standart yol yoktur, bunlardan hiçbiri bir ulusun ekonomik aşamasının temeli olarak görülemez. Bu unsurların arasında, özel öneme layık olan tek bir faktör bulamayız. Ekonomi çok karmakarışıktır ve kültürel bir karmaşıklık sadece ekonomik faktörü değil, psikolojik ve sosyal faktörleri de içerir. Aynı ekonomik ve teknolojik koşullar illa ki aynı kültüre yol açmaz. Ekonomik yaşamın kültürü etkilediği doğrudur, fakat ekonomik yaşam tüm sosyal faktörleri değiştirebilen bir faktör değildir; ikinci olarak, tarihsel değişim gibi ekonomik değişimin de güçlü bir devamlılık sistemi vardır, bir ayrım yaratmak çok da kolay değildir, ve ekonominin farklı görünümleri aynı oranda ortaya çıkmaz. Dünyada varsayılan bütün aşamaları hiç atlamadan geçmiş olan çok az topluluk vardır. Ekonomik aşamalar teorisi, bu yüzden, en az Morgan’ın kültürel aşaması teorisi kadar akla yatkın olmaktan uzaktır.



