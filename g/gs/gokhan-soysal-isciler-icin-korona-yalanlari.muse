#title İşçiler İçin Korona Yalanları
#author Gökhan Soysal
#SORTtopics koranavirüs, işçi
#date 03.05.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-05-03T17:08:24



17.04.2020 tarihli Resmi Gazete’de yayınlanan 7244 sayılı Kanun ile patronlar işçiyi işçinin herhangi bir rızasını aramadan isterse -3 ayı geçmemek üzere- ücretsiz izne çıkartabilecek. Patronun 3 ay sonra işçiyi işten çıkarmasının önündeyse yasal hiçbir engel yok. Ayrıca devletin patronları kurtarmaya yönelik bu hamlesinde patronun ücret ödeme yükümlülüğü de bulunmuyor. İşçiye -kendisinden işsizlik sigortası adı altında yıldan yıla kesilen paralardan alınarak oluşturulan- işsizlik sigortasından verilecek ücret ise günlük 39,24 liradan aylık 1.177 lira!

Patronun, işçiyi istediği zaman ücretsiz izne çıkarabilmesi için ilk etapta 17.07.2020 tarihine kadar vakti var. Ancak süre, bu kadarla sınırlı değil. Devlet başkanı istediği takdirde bu süreyi 3 ay daha uzatabilecek. Son kanun değişikliklerinde patronun, işçiyi ücretsiz izne çıkarmasında herhangi bir kısıtlamaya gidilmedi. Patronlar, “ihtiyaçları doğrultusunda” istediği sayıda ve istediği özellikteki işçiyi ücretsiz izne çıkarabilir.

Kanun çıkana kadar işçinin korona krizi nedeniyle oluşacak durumda haklı nedene dayanarak işten ayrılma hakkı vardı, işçinin bu yola gittiğinde de kıdem tazminatını almasının önünde herhangi bir engel bulunmuyordu. Ancak son değişiklikle patronun işçiyi istediği zaman ücretsiz izne ayırması durumunda, işçinin haklı nedene dayanarak sözleşmeyi fesih hakkı elinden alınmış durumda. Bu düzenlemeyle birlikte işçinin, işten ayrılarak kıdem tazminatı almasının önüne geçilmiş oluyor. İşçi bu durumda ayrıca işsizlik ödeneğine de başvuramayacak.

Son kanun değişikliklerinin en çok gündem olan maddelerinden biri de işten çıkarmalara ilişkin. Buna göre her türlü iş sözleşmesi, 17.04.2020 tarihinden itibaren üç ay süreyle sona erdirilemeyecek; sona erdiren patronlara aylık brüt asgari ücret tutarında idari para cezası kesilecek. Ancak bunun istinasıysa <em>“ahlak ve iyi niyet kurallarına uymayan haller ve benzeri sebepler.”</em> Yani uygulamada sıklıkla gördüğümüz üzere patronlar yalan yanlış bahanelerle işçiyi ahlak ve iyi niyet kurallarına uymadıkları gerekçesiyle herhangi bir tazminat ödemeden işten çıkarmaya devam edebilecek. Karşılarına arabuluculuklar, dertlerini anlatamayacakları avukatlar ve mahkeme kapıları çıkarılan işçilerse alacaklarını hukuk yoluyla -eğer ortada şirket kaldıysa- iyimser bir tahminle 2-3 yıla ancak alabilecek.

Ahlak ve iyi niyet kuralları bahanesine uymak istemeyen patronlar da 3 bin lira civarı bir para vererek işçileri işten çıkarabilecek. Yani işçiyi işten çıkarmak yasak değil. Bugün değilse yarın, bu bahaneyle olmazsa şu bahaneyle, en olmadı parasını vererek işçiler işten çıkartılmaya devam edilecek. Gerekli koşulları sağlama şansına sahip işçiler işsizlik ödeneğine başvurabilecek. Ücretsiz izne ayrılan işçilerse başvursa bile asgari ücretin yarısına dahi ulaşamayan bir miktarla yani açlıkla karşı karşıya kalacak. O da hijyenik olmayan ortamlarda çalıştırılırken eğer Covid-19 kapıp öldürülmediyse.

<em>“Hepimiz aynı gemideyiz, biz bize yeteriz”</em> denilerek sürdürülen propaganda çalışmalarında devletin, geçmediğimiz köprüler bahanesiyle elini cebimize atarak ödediği miktarların yanından geçmeyen komik rakamlar dönüp duruyor ekranlarda. Bu kampanyaya destek verdiklerini söyleyerek reklamlarını yapan ve iktidara yakın kalarak pastadan pay kapmaya çalışan patronlar, arka kapıdan da işçileri kovmaya devam ediyor. İşçiyi işten çıkarmak yasak değil, sadece geçici bir süreliğine yasakmış gibi davranılacak!



