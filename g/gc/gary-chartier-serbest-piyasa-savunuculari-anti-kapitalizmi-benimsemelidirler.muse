#DELETED Reason for deletion: Machine translation (DeepL: after June 2022, Google Translate: before June 2022) has been detected. - https://mirror.anarsistkutuphane.org/c4ss.org/analyze/
#title Serbest Piyasa Savunucuları “Anti-Kapitalizmi” Benimsemelidirler
#author Gary Chartier
#SORTtopics sol piyasa anarşizmi, kapitalizm, anti kapitalizm
#date 19.01.2010
#source 12.05.2022 tarihinde şuradan alındı: [[https://c4ss.org/content/56699][c4ss.org]]
#lang tr
#pubdate 2022-05-12T18:55:09
#notes Çeviri: Faruk Pak <br>İngilizce Aslı: [[https://c4ss.org/content/1738][Advocates of Freed Markets Should Embrace “Anti-Capitalism”]]




*** I. Giriş

Serbest piyasa savunucularının konumlarını bir “anti-kapitalizm” türü olarak tanımlamaları için iyi nedenleri var. Neden böyle olduğunu açıklamak için, “kapitalizmin” üç muhtemel tanımını ortaya koyuyor ve kendini serbest piyasalara adamış insanların iki ve üç numaralı kapitalizm tanımlarım bağlamında kapitalizme karşı çıkmaları gerektiğini öneriyorum. Akabilinde, serbest piyasa savunucularının itiraz etmeleri gereken bazı sosyal düzenlemeleri “kapitalizm” olarak adlandırmaları için nedenler sunuyorum.

*** II. “Kapitalizmin” Üç Anlamı

“Kapitalizmin” en az üç ayırt edilebilir anlamı var:

<strong>Kapitalizm-1</strong>: Mülkiyet haklarını ve gönüllü mal ve hizmet mübadelesini içeren bir ekonomik sistem.

<strong>Kapitalizm-2</strong>: Büyük şirketler ve hükümet arasında simbiyotik bir ilişki olmasına yol açan bir ekonomik sistem.

<strong>Kapitalizm-3</strong>: İşyerlerinin, toplumun ve (eğer varsa) devletin, kapitalistler tarafından (yani, yatırılabilir net serveti ve üretim araçlarını kontrol edecek oldukça az sayıda kişi tarafından) yönetilmesi.

Kapitalizm-1 serbest bir piyasadan ibarettir; o yüzden, eğer “kapitalizm karşıtlığı” kapitalizm-1’e muhalefet etmek olsaydı, “serbest piyasa anti-kapitalizmi” oksimoron olurdu. Ancak serbest piyasa anti-kapitalizminin savunucuları kapitalizm-1’e karşı değiller; bunun yerine ya kapitalizm-2’ye ya da hem kapitalizm-2’ye hem de kapitalizm-3’e itiraz ediyorlar.

Pek çok insan, bu farklı “kapitalizm” anlamlarından çıkarılan unsurları birleştiren tanımlarla hareket ediyor gibi görünüyor. Kapitalizmin hem tutkunları hem de eleştirmenleri, terimle “kişisel mülkiyet haklarını ve gönüllü mal ve hizmet mübadelesini içeren ve dolayısıyla öngörülebileceği gibi kapitalistler tarafından yönetilen bir ekonomik sistem” gibi bir şeyi kastediyorlar gibi görünüyorlar. Az sayıda varlıklı insanın egemenliğinin herhangi bir anlamda serbest piyasanın olası bir özelliği olduğu varsayımına meydan okumak için iyi bir nedene sahip olduğumuzu düşünüyorum. Böyle bir hakimiyetin ancak güç ve hile ekonomik özgürlüğü engellediğinde ortaya çıkabileceğini öne sürüyorum.

*** III. Kapitalizm-2 ve Kapitalizm-3 Neden Serbest Piyasa İlkeleriyle Bağdaşmaz

**** <em>A. Giri</em>ş

Kapitalizm-2 de Kapitalizm-3 de serbest piyasa ilkeleri ile çelişmektedir: Kapitalizm-2 piyasa özgürlüğüne doğrudan müdahaleyi içerdiği için, kapitalizm-3 ise bu müdahaleye bağlı olduğu için – hem geçmiş hem de süregelen – ve piyasa özgürlüğüne desteğinin arkasındaki genel özgürlük taahhüdünün karşısında olduğu için.

**** <em>B. Kapitalizm-2 Piyasa Özgürlüğüne Doğrudan Müdahale İçerir</em>

Kapitalizm-2, kapitalizm-1 ile ve dolayısıyla serbest piyasa ile çelişmektedir. Kapitalizm-2’de, politikacılar kendilerini ve yardımcılarını zenginleştirmek uğruna kişisel mülkiyet haklarına ve gönüllü mal ve hizmet mübadelesine müdahale eder ve büyük işletmeler, kendilerini ve müttefiklerini zenginleştirmek için kişisel mülkiyet haklarına ve gönüllü mübadelelere müdahaleyi teşvik etmek için politikacıları ayartır.

**** <em>C. Kapitalizm-3 Piyasa Özgürlüğüne Yapılmı</em>ş <em>Geçmi</em>ş <em>ve Süregelen Müdahalelere Bağlıdır</em>

Kapitalizm-3’ün kapitalizm-1 ile ve takiben serbest piyasa ile çeliştiğini göstermenin üç yolu vardır. İlki, tartışılabilir olsa da makul bir bakış açısı olan piyasaların işleyişine ilişkindir. Bu görüşe Piyasalar Ayrıcalıkları Sarsıyor (PAS) diyelim. PAS’a göre serbest bırakılmış bir piyasada, kapitalizm-2 altında devlet gücünden yararlananlara tanınan ayrıcalıklar olmayacağı için servet geniş çapta dağıtılacak ve dev, hiyerarşik işletmeler verimsizleşecek ve ayakta kalamayacaktı.

Hem çoğu insan hiyerarşik çalışma ortamlarında görev almaktan hoşlanmadığından hem de daha basit ve çevik organizasyonlar, devlet desteği alamayacak olan büyük ve tıknaz kuruluşlara kıyasla çok daha sürdürülebilir olacağından, serbest piyasada çoğu insan bağımsız yükleniciler olarak çalışabilir veya ortaklıklar ve kooperatifler oluşturabilirdi. Çok daha az sayıda büyük işletme olurdu, hala var olanlar da yüksek ihtimalle bugünün kurumsal devleri kadar büyük olmayacaktı ve toplumsal zenginlik çok sayıda küçük firma arasında geniş çapta dağılacaktı.

Siyasi olarak iyi bağlantıları olanların insanları yoksullaştırmaya ve yoksul tutmaya yardım eden diğer ayrıcalıkları – örneğin mesleki ruhsatlandırma ve imar kanunları – serbest bir piyasada bulunmaz. Bu nedenle, sıradan insanların, ekonomik statüsü en düşük olanlar bile, büyük işletmeler de dahil olmak üzere hoş olmayan çalışma ortamlarında istihdamdan ayrılabilmelerini mümkün kılacak bir ekonomik güvenlik seviyesinden yararlanma olasılıkları daha yüksek olacaktır. Ve özgür bir toplum, kişisel mülkiyet haklarına ve gönüllü mübadelelere müdahale etme kapasitesi bir yana, böyle bir sözde hakka sahip bir hükümete sahip olmayacağı için, kapitalizm-3’te sosyal statüsü en yüksek olanlar, serbest piyasada zenginlik ve güç kazanmak ve güçlerini sürdürmek için politikacıları manipüle edemezdi, böylece üretim araçlarının sahipliği birkaç elde toplanamazdı.

Piyasa özgürlüğüne süregelen müdahaleye ek olarak, PAS, kapitalizm-3’ün geçmişte yaşanmış büyük ölçekte adaletsizlik eylemleri olmasaydı mümkün olamayacağını öne sürüyor. Mülkiyet haklarına ve piyasa özgürlüğüne yapılmış ve İngiltere’de, Amerika Birleşik Devletleri’nde ve başka ülkelerde çok sayıda insanın yoksullaşmasına yol açmış müdahaleye dair kapsamlı kanıtlar var. Serbest piyasa savunucuları bu nedenle kapitalizm-3’e itiraz etmelidirler çünkü kapitalistler yalnızca büyük ölçekli ve devlet onaylı meşru mülkiyet haklarının ihlalleri sayesinde sistemi yönetebilirler.

**** <em>D. Kapitalizm-3’e Destek, Özgürlüğü Desteklemenin Temel Mantığı ile Çeli</em>ş<em>ir</em>

Kapitalizm-3’ün, serbest piyasaları desteklemenin temel mantığının ışığında kapitalizm-1 ile tutarsız olduğu görülebilir. Kuşkusuz bazı insanlar, kapitalizm-1 desteklerini daha geniş çaplı bir insan yaşamı ve sosyal etkileşim anlayışına entegre etmeye çalışmadan, kişisel mülkiyet haklarını ve gönüllü mübadeleleri kendi iyilikleri için tercih ediyorlar. Ancak diğerleri için kapitalizm-1’e verilen destek, kişisel özerklik ve haysiyete saygının altında yatan bir ilkenin yansımasıdır. Bu görüşü -Kapsamlı Özgürlük (KÖ) olarak adlandırdığım görüş- benimseyenler insanların kendi tercihlerine göre (başkalarına zarar vermemek şartıyla) istedikleri gibi ilerleme ve gelişme özgürlüğüne sahip olduklarını görmek isterler. KÖ savunucuları sadece saldırganlıktan arınmış özgürlüğe değil, aynı zamanda insanların kendileri veya başkaları saldırganlığa karıştıkları veya saldırganlıktan yararlandıkları için uygulayabilecekleri türlü türlü sosyal baskılardan arınmaya ve ayrıca saldırgan olmayan ancak mantıksız – belki keyfi – özgürlüğe değer verirler. İnsanların yaşamlarını istedikleri gibi şekillendirebilme seçeneklerini önemserler ve kapasiteleri kısıtlayan sosyal baskıya da karşı çıkarlar.

Farklı özgürlük türlerine kesin değerler vermek, bu farklı özgürlük türlerinde meydana gelmiş saldırganlıklar için aynı tür çözümleri onaylamak demek değildir. KÖ savunucuları çoğu pasifist olmasa da tartışmaların silah zoruyla çözülmesini istemezler; saldırgan şiddete kesinlikle karşı çıkıyorlar. Bu nedenle, küçük hakaretlerin şiddetli tepkileri gerektirdiğini düşünmüyorlar. Bununla birlikte, aynı zamanda insanların özgürlüğüne yönelik şiddet içermeyen saldırıları önemsiz olarak değerlendirirken özgürlüğü genel bir değer olarak desteklemenin hiçbir anlamı olmadığını kabul ediyorlar. (Dolayısıyla, bu tür saldırılara, alenen utandırma, kara listeye alma, grev, protesto, gönüllü sertifikaları vermeme ve boykot etme dahil bir dizi şiddet içermeyen tepkiyi tercih ediyorlar.)

KÖ, o halde, kapitalizm-3’e karşı çıkmak için başka bir neden verir bize. KÖ’ye bağlı çoğu insan PAS’i gayet makul buluyor ve bu nedenle kapitalizm-3’ü kapitalizm-2’nin bir ürünü olarak düşünmeye meyilliler. Ancak özgürlüğün hem şiddetli hem de şiddet içermeyen saldırılara maruz kalabilecek çok boyutlu bir değer olarak anlaşılması, kapitalizm-3’e karşı çıkmak için iyi bir neden sağlar- kapitalizm-2’den tamamen izole edilmiş halde ortaya çıksa bile-.

*** IV. Serbest Piyasa Savunucuları Karşı Çıktıkları Sistemi Neden “Kapitalizm” Olarak Adlandırmalıdır

Serbest piyasaların ve dolayısıyla kapitalizm-1’in savunucuları, açıkça kapitalizm-2’ye, en azından “devlet kapitalizmi” veya “şirket kapitalizmi” ya da “korporatizm” demelidir. Ancak “sözcükler onlara yüklenilen anlamlardan bilinir”, bu nedenle serbest piyasa savunucularının, özellikle KÖ’ye bağlı olanların karşı çıktıkları sistemi salt “kapitalizm” olarak adlandırmaları için birkaç neden vardır.

**** <em>1. Kapitalizm-3’ün Özgül İstenmezliğini Vurgulamak</em>

“Devlet Kapitalizmi” ve “Korporatizm” gibi adlandırmalar, kapitalizm-2’de neyin yanlış olduğunu görmüş olurlar ama kapitalizm-3’teki soruna tam olarak değinemezler. Kapitalistlerin yönetimi, makul görünen de budur, politik bir açıklamaya ihtiyaç duyuyor -politikacıların bağımsız kötü davranışları ve iş liderleri tarafından manipüle edilmelerini ele alacak bir açıklama- İş dünyası-hükümet simbiyozuna karşı çıkmanın yanı sıra büyük şirketlerin sistemi yönetmesi de karşı çıkılmaya değer. Kapitalizm-3’e getirilen eleştiriler büyük işletmelere sahip olan ve onları yönetenlerin bol bol “kapitalist” olarak etiketlendiği ve özgürlük savunucularının karşı çıktığı şeye “kapitalizm” dendiği ölçüde kolaylaşır.

**** <em>2. Serbest Piyasa Savunucuları ile Kaba Piyasa Tutkunları Arasına Bir Çizgi Çekmek</em>

“Kapitalist” pankartı, genelde serbest piyasalara verilen desteği kapitalizm-2 ve kapitalizm-3’e verilmiş bir destekmiş gibi gören insanlar tarafından hevesle sallanıyor -belki her ikisinin de gerçeğini veya sorunlu doğasını görmezden gelerek, hatta belki de iş dünyası devlerinin takdire şayan karakterler olduğunu düşünüp kapitalizm-3’ü pohpohluyorlar. “Kapitalizme” karşı çıkmak, serbest piyasa savunucularının, iktidar seçkinlerinin özgürlüğünden yana olanlarla karıştırılmamasını sağlamaya yardımcı olur.

**** <em>3. Serbest Piyasa Radikallerinin “Sosyalizm” Terimini Geri Alması</em>

“Kapitalizm” ve “sosyalizm” karakteristik olarak karşıt bir çift oluşturuyor olarak görülüyor. Ancak, serbest piyasaların radikal bir savunucusu olan Benjamin Tucker’ın, bu terimlerin tutkuyla tartışıldığı ve tanımlandığı dönemde sahip olduğu etiket “sosyalist” idi. Tucker, serbest piyasalara olan ciddi bağlılığı ile Birinci Enternasyonal üyeliği arasında bir çelişki görmedi. Bunun nedeni, sosyalizmi, işçileri aristokratlar ve şirket yöneticilerinin baskısından kurtarma meselesinden ibaret görmesi ve- makul bir şekilde- devletin ekonomik seçkinlere verdiği ayrıcalıklara son vermenin, sosyalizmin özgürleştirme planının uygulanmasının en etkili – ve en güvenli – yolu olduğuna inanmasıdır. Kapitalizme karşı çıkmak, Tucker gibi radikallerin çağdaş özgürlük hareketinin silsilesindeki önemli yerini görmeye ve günümüzün özgürlük savunucularına sosyalist etiketini devlet sosyalistlerinden geri almaları için ikna edici bir gerekçe sağlar. (Bu cidden yapılmalıdır çünkü özgürlük savunucuları, insan sorunlarını çözmenin kaynağı olarak devleti değil toplumu-özgür ve gönüllü olarak iş birliği yapan birbirine bağlı insanlar- görürler. Bu yüzden de doğal olarak sosyalizmi devletçiliğin bir türü değil de ona bir alternatif olarak kabul ettikleri söylenebilir.) Anti-Kapitalizmi benimsemek, serbest piyasaların sosyalist hedeflere ulaşmak için bir yol sunduğu gerçeğini gözler önüne serer -piyasa araçlarını kullanarak işçilerin güç kazanması ve üretim araçları üzerindeki sahiplik ve kontrolün geniş çapta dağıtımını teşvik etmek.

**** <em>4. İ</em>ş<em>çilerle Dayanı</em>ş<em>ma İçinde Olunduğunu Göstermek</em>

PAS doğruysa, büyük şirketlerin- “sermayenin”- tercihlerinin memnuniyetini, işçilerin kendi memnuniyetlerine ulaşabilmelerinden daha eksiksiz bir şekilde maksimize etme yeteneği, serbest piyasa ilkeleriyle çelişen iş dünyası-devlet simbiyozunun bir sonucudur. Ve KÖ’ye destek olarak, doğrudan saldırgan olmayan bir şekilde bile olsa itilip kakılan işçilerden yana olmak için daha fazla nedenimiz de var. İşçilerin karşı çıktığı patronlara genelde “kapitalist” deniyor, bu nedenle “anti-kapitalizm” doğal olarak bu patronlara muhalefetin etiketi oluyor. Ve serbest piyasaların -kapitalizm-2 ve kapitalizm-3’ün aksine- tüm işçilerin kendi hayatlarını şekillendirme ve önemli ölçüde daha fazla refah ve ekonomik güvenlik deneyimleme fırsatlarını çarpıcı biçimde artıracağı için “anti-kapitalizmi” benimsemek, işçilerle dayanışma içinde olunduğunu belirtmenin bir yoludur.

**** <em>5. Küresel Anti-Kapitalist Hareketin Geçerli Kaygılarıyla Özde</em>ş<em>le</em>ş<em>mek</em>

“Anti-Kapitalizmi” benimsemek, aynı zamanda ve daha geniş ölçekte, emperyalizmin, çokuluslu şirketlerin hayatlarına yaptıkları müdahalelerin ve ilerleyen ekonomik kırılganlıklarının kaynağı olarak gördükleri düşmanlarını “kapitalizm” olarak adlandıran dünyanın dört bir yanındaki sıradan insanlarla özdeşleşmenin bir yoludur. Belki bazıları, karşı çıktıkları sistemin arkasında yatan şeyin serbest piyasa — kapitalizm-1 – olduğunu düşünerek yanlış teorik açıklamaları benimsiyorlar. Tabii birçoğu için “kapitalizme” karşı olmak, serbest piyasalara karşı çıkmak anlamına gelmiyor; bu, -özgürlük savunucularının çoğu zaman ne yazık ki yapmayı reddettikleri gibi- kendilerinin ve başkalarının hayatlarını kötü etkilemeye kararlı görünen güçlere meydan okuma yolunda yanımızda olmaya hazır sosyal eleştirmenler tarafından sağlanan uygun bir etiket kullanmak demek. Özgürlük savunucuları, bu insanlarla ortak bir zemin oluşturmak için altın bir fırsata sahiptir, karşılaştıkları birçok durumun yanlışlığı konusunda onlarla hemfikir olurken, bu durumların özgürlük temelli bir açıklamasını ve söz konusu sorunlara özgürlük temelli çözümler sunmak bunu sağlayacaktır.

*** V. Sonuç

Bundan otuz beş yıl önce, büyük liberter kahraman Karl Hess, “kapitalizm dininden döndüğünü” söyleyerek şunları yazdı: “Kapitalizme olan inancımı kaybettim” ve “Bu kapitalist ulus devlete direniyorum”. “Kapitalizm”in üç anlamını -piyasa düzeni, iş dünyası-hükümet ortaklığı ve sistemi kapitalistlerin yönetmesi- ortaya koymak, Hess gibi birinin neden “kapitalizm” denen şeye muhalif olduğunu ve sürekli olarak özgürlüğe bağlı olduğunu anlamamıza yardım eder. Serbest piyasa savunucularının hem politikacılar ve iş dünyası liderlerinin piyasa özgürlüğüne müdahalesine hem de iş dünyası liderlerinin (saldırgan ya da değil) sosyal egemenliğine karşı çıkmaları mantıklı olandır. Karşı oldukları şeyi “kapitalizm” olarak adlandırmaları da mantıklıdır. Bunu yapmak, dikkatleri özgürlük hareketinin radikal köklerine çeker, toplumu devlete bir alternatif olarak anlamanın neden önemli olduğunu gösterir, özgürlük savunucularının özgürlüğe getirilen saldırgan müdahalelere olduğu kadar saldırgan olmayan kısıtlamalara da itiraz ettiğinin altını çizer, özgürlük savunucularının, adaletsiz statükoyu desteklemek için piyasa retoriği kullanan insanlarla karıştırılmaması gerektiğini gösterir ve serbest piyasa savunucuları ile işçiler arasındaki dayanışmayı gözler önüne serer. -İşçilerle olduğu gibi “kapitalizmi” özgürlüklerini kısıtlayan ve yaşamlarını bir noktada engelleyen dünya sistemi için bir kısaltma olarak kullanan dünyanın dört bir yanındaki sıradan insanlar ile de dayanışmadır bu. Serbest piyasa savunucuları, özgürlüğe tam anlamıyla bağlılıklarını göstermek ve insanları tabi altına almayı, dışlamayı onaylamayı ve mahrumiyetlerini gizlemek için serbest piyasa retoriğini kullanan uyduruk alternatiflerini reddetmek adına “anti-kapitalizmi” benimsemelidirler.

