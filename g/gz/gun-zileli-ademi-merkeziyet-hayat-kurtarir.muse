#title Ademi Merkeziyet Hayat Kurtarır...
#author Gün Zileli
#date 26.02.2023
#source 03.03.2023 tarihinde şuradan alındı: [[https://www.gunzileli.net/2023/03/03/ademi-merkeziyet-hayat-kurtarir/][gunzileli.net]]
#lang tr
#pubdate 2023-03-03T17:39:31
#topics afet, devlet, eleştiri, merkeziyetçilik


Belki de genç kuşaklar bu kelimeyi bilmezler ya da tam anlamıyla bilmezler. “Merkezkaç” desek daha iyi anlayabilirler ama bu sefer de gerçek anlam biraz sapmış olur. “Merkez”den “kaçmak” gibi anlaşılabilir. Oysa ademi merkeziyetçilik, hayatın gerçek gücünün, canlı dokusunun, ölü ve kabuksu merkezlerde değil, hayatiyet dolu yerelde olduğuna işaret eder. Belki de “yerel yönetim” ya da “yerinden yönetim” demek en doğrusudur.

Türkiye toplumu, kültürüyle ve yönetim alışkanlıklarıyla merkeziyetçidir. Eski evler bile ona göre yapılmıştır. Baba, merkezî sofada oturur ve bütün odaları gözetimi altında tutar. Baba, aslında her şeyi biliyor olmasa da biliyormuş gibi görünmeyi tercih eder. Zaten merkeziyetçilik, aslında “yapmak” değil, “yapar gibi görünmek”ten ibarettir. Son büyük depremde gördüğümüz gibi.

**** Prens Sabahattin

Ademi merkeziyetçilikten söz edildiği zaman tarihimizdeki, kenara iteklenmiş, görmezden gelinmiş, hatta horlanmış önemli bir düşünce insanını, Prens Sabahattin’i hatırlamamak olmaz. Merkezî devlet ve onun merkeziyetçi ideolojisi, Prens Sabahattin’i ve onun ademi merkeziyetçi önerilerini her zaman marjinalize etmiştir. Merkeziyetçi devletten daha da merkeziyetçi olan solumuz da öyle. “Liberal” deyip bir köşeye atmışızdır onu. Oysa Prens Sabahattin de, ademi merkeziyetçilik de öyle kolayca köşeye atılacak, görmezden gelinecek bir düşünür ve düşünce değildir. Vikipedi’de şöyle deniyor: “İmparatorluğun geniş ve hantal yapısı nedeniyle gereğince gerçekleştirilmeyen yerel yönetimin bölgenin yaşayanları tarafından üstlenilmesi gerektiği fikrini savundu. ‘<em>Adem-i merkeziyetçilik</em>’ olarak adlandırılan bu görüşü etnik unsurlara prim verme olarak algılandı.”

Düşünce dünyamız için pek bir şey söyleyemeyeceğim ama edebiyatımızda Prens Sabahattin’in ve düşüncesinin hakkını veren edebiyatçı Demir Özlü’dür. Demir Özlü, bütün öykülerinin yer aldığı <em>Sürgün Küçük Bulutlar</em> (YKY, 2012) kitabındaki “Prens Sabahattin Bey” öyküsünde, oğluyla birlikte gezmeye geldiği Londra’daki otel odasında, gece yarısı, Prens Sabahattin’i, yatağının ayakucuna gelip oturmuş, kendisiyle sohbet eden haliyle canlandırmıştır.

Öyküde, kâh Demir Özlü’nün, kâh Prens Sabahattin’in ağzından, onun hayatını ve düşüncelerini izleriz. Prens Sabahattin Bey ve kardeşi Prens Lütfullah Bey, 1899 yılının Aralık ayında, Abdülhamit’in kız kardeşi Seniha Sultan’la evli olan babaları Damat Mahmut Celalettin Paşa’yla birlikte sürgün edilmiş ve Marsilya limanına çıkmışlardır. Prens Sabahattin, bu öyküde, “Dayım sultanın despotizmine başkaldırmıştım” der, Demir Özlü’ye, “Yirmi iki yaşındaydım o zaman ve o günden bitmeyen sürgünüm başladı.”

Prens Sabahattin, 1902’de, Paris’te, Osmanlı Liberalleri Kongresi’ni topladığını anlatır. “Evet, Sultan’ın despotizmine karşı olan muhalefeti, her ulustan birleştirmek için. Osmanlı halklarının kurtuluş ve yükseliş sorununa çare bulmak için. Bir Osmanlı Konfederasyonu yaratabilmek için… Milliyetçilik, ilkel bir hırstan başka nedir ki… Hürriyetçilik, bir siyasal olgunluğa ulaşamaz idiyse, kuru bir sözden başka ne olabilirdi… bunları göremeyenler, 1908’den sonra, on yıl içinde imparatorluğu büyük bir hızla çökerttiler. Otokrasidir imparatorluğu çökerten…”

1908 Meşrutiyet Devrimi ile Abdülhamit devrilmiş, İttihat ve Terakki, yeni iktidarın yönlendirici gücü olmuştur. Prens Sabahattin yurda döner. “Seçimler yapılacak, Millet Meclisi açılacaktı. Milliyetçi olan, bu yüzden de otokrat eğilimleri içlerinde taşıyan ittihatçılar, seçimlere girmemi, seçilmemi önleyeceklerdi… İstibdat dirilmişti… İstibdatın dirilmiş olması, hatta, dayımın istibdadını da geçmesi. İstibdat istibdattan başka bir şey doğurmaz. Türkiye’yi mahveden de budur… O zaman… <em>Adem-i Merkeziyet ve Teşebbüs-ü Şahsi Cemiyeti</em> içinde fikirlerimizi olgunlaştırmayı, yaymayı tercih ettik… <em>Otokrasiden sadece yöneticiler değil, yapılan her şeye sessizce boyun eğen halk da sorumludur…</em> Özgürlük adına yola çıkanlar, tarihte eşine rastlanması çok güç otokratlar olabilirler.”

İttihat Terakki de Abdülhamit gibi despotik yolu tutunca, Prens Sabahattin 1913 yılında ülkeden bir kez daha kaçmak zorunda kalır. 1918 yılında, İttihat iktidarının yıkılmasından sonra yurda döner. Fakat, Cumhuriyet yönetimi zamanında, 1924 yılında, Saltanat ailesine mensup olduğu gerekçesiyle, 1948 yılında İsviçre’de ölene kadar üçüncü sürgüne mahkûm edilir (s. 670-683).

Demir Özlü’nün öyküsünde Prens Sabahattin’in ademi merkeziyetçi fikirlerini etraflı bir şekilde girilmemiş. Özetle söyleyecek olursam, bu düşünce, işlerin merkezden değil, hayatın gerçek alanlarından, yerelden, bizzat o yerel bölgelerde yaşayan insanların inisiyatifi ve katılımı ile özerk bir şekilde yürütülmesidir ve ABD’nin federatif, İsviçre’nin kantonal yapısı ya da Bakunin’in “özgür komünler federasyonu” gibi bir yönetsel yapıyı gerektirir. Buna, Tito zamanındaki Yugoslavya’da bir dönem uygulanılmaya çalışılan “özyönetim”i de ekleyebiliriz. Bu yapı, merkezî hükümetlerin müdahalesine en az, yerel bölgelerdeki insanların inisiyatifine en çok olanağı tanır. Böylece, normal günlük işleyişte büyük bir toplumsal inisiyatife olanak tanıdığı gibi, olağanüstü bunalım anlarında ve büyük doğasal veya toplumsal altüst oluşlarda, yerel inisiyatif yükün çoğunu kaldırır.

**** Türkiye Her Zaman Merkeziyetçilikle Yönetildi

Türkiye, böylesi bir ademi merkeziyetçi yapıya ve özerkliğe her zaman uzak durmuştur. Kadim Osmanlı İmparatorluğu da, ondan sonra kurulan Türkiye Cumhuriyeti de her zaman merkeziyetçi bir devlet yapısına dayanmıştır. Türkiye Cumhuriyeti, kadim Osmanlı merkeziyetçiliği ile Jakoben türdeki üniter devlet merkeziyetçiliğinin bir bileşimidir. Ulus-devlet, bu temel üzerinde inşa edilir. Her yeni iktidar, iş başına geçer geçmez bu merkezî devleti devralıp geliştirmiş, yerel inisiyatiflere kesinlikle izin vermemiştir.

20 yıllık son AKP iktidarı da merkezî devlete çökmüş, merkeziyetçi yapıya yerleşmiş, gerçek, yaşayan sivil topluma giden kan damarlarını tıkamış ya da doğrudan kopartmıştır. Her ulusal ya da doğal felakette “devlet nerede?” feryatlarının yükselmesinin nedeni budur. Çünkü merkezî devlet yönetimi, yerel inisiyatifi, ulusal vb. her türlü farklılığı bastırırken yerelde sadece yerel soyguncularla (mütegallibe) işbirliği yapmıştır. Merkezî despotizmlerin vazgeçemediği bir yönetim tarzıdır bu. Aslında merkezî yapı, felçli bir ihtiyar kadar hareket etmekten acizdir. “Kendisi himmete muhtaç bir dede, nerde kaldı gayrıya himmet ede” diye boşa denmemiştir. Acziyet ve despotizm, merkezî devlet madalyonunun iki yüzüdür.

**** İki Dallı Bir Yerel Yönetim Modeli

Bundan sonra günümüzdeki ademi merkeziyetçi yönetim tarzının unsurlarına geçebiliriz.

Bu ademi merkeziyetçiliğin iki dalı vardır:

Birincisi, normal yönetsel tasarrufların yanı sıra, başta depremler olmak üzere her türlü felakete karşı, tam yetkili ve merkezî müdahaleden masun, özerk <strong>yerel yönetim organları</strong>nın oluşturulmasıdır.

İkincisi, yine depremler başta olmak üzere her türlü felakete (deprem, yangın, sel, toprak kayması, kasırga vb.) karşı, tam yetkili ve merkezi müdahaleden masun, sürekli ve özerk <strong>yerel acil müdahale organları</strong>nın oluşturulmasıdır.

<strong>Yerel yönetim konseyi:</strong> Her mahallî birimde, her ilde, her ilçede, hatta daha alt birimlerde de oluşturulacak tam yetkili yerel yönetim konseyi (bugün de kent konseyleri var, iyi bir fikirdir ama pratikte ne kadar yetki sahibi olduğu ve işlediği meşkuk), bütün ilgili oda temsilcilerinden (Mimar ve Mühendis odaları, İnşaat Mühendisleri odaları, Türk Tabibler Birliği, Eczacılar Odası vb), Türkiye Barolar Birliği, hukuk derneklerinin (ÇHD, Sosyal Haklar Derneği vb) temsilcilerinden, İnsan Hakları Derneği, Hayvan hakları dernek ve temsilcilerinden, kadın dernekleri ve inisiyatiflerinden, gençlik ve öğrenci dernek ve inisiyatiflerinin, yerel STK’ların temsilcilerinden, basın temsilcilerinden, yerbilimcilerden, Belediye temsilcilerinden, yerel parti teşkilatlarının temsilcilerinden, bölge sendikalarının temsilcilerinden, bölge köylerinin muhtarlarından, Çevre ve Şehircilik Müdürlüğü görevlilerinden, İtfaiye ve ilk yardım temsilcilerinden, AFAD temsilcilerinden ve merkezî devletin mülki amirlerinden vb. vb. oluşmalıdır.

Birincisi, bu kurul, il ya da ilçedeki veya mahallî bölgedeki bütün tayin edici işlere karar verme yetkisine sahip olmalıdır. Son deprem örneğinden hareket edecek olursak, örneğin, yapıların sağlamlığı veya çürüklüğüne, yeni yapılacak yapının zemininin ve bizatihi yapının sağlamlığına karar verecek organ bu olmalıdır. Bu organın onayı olmadan hiçbir yapıya sağlam ruhsatı verilmemelidir. Hiçbir merkezî ya da yerel hükümet müdahalesi bu organın kararlarının önüne geçmemelidir.

Böyle bir yapılanma, felaketler karşısındaki her türlü zaafı asgari boyutlara indirecektir. Kısacası, tek bir binanın bile yıkılmadığı Erzin belediye başkanının tek başına yaptığını bu kurullar ya da organlar rahatlıkla yerine getirebileceklerdir.

<strong>Yerel Acil Müdahale Konseyi:</strong> Keza, bu konsey de yerel madencilerin temsilcilerinden, itfaiyecilerin, dağcıların, spor kuruluşlarının, okulların, STK’ların, siyasi partilerin ve belediyenin temsilcilerinden, Kızılay temsilcilerinden (1868 yılında Hilal-i Ahmer adıyla kurulan ve 1935 yılında Kızılay adını alan, AKP iktidarı tarafından “bitkisel hayata” sokulan bu kuruluşun yeniden hayata döndüğünü farz edelim), gönüllü kurtarma ekiplerinden, doktor ve sağlıkçı temsilcilerinden vb. oluşmalıdır.

Bu konsey açısından kritik olan nokta, acil müdahale ekiplerinin ve araçlarının olay olduktan sonra değil, olmadan önce hazır ve nâzır olmasıdır.

Her şeyden önce bu konseyin elinin altında yerel bölgenin büyüklüğüyle orantılı bir deposu olmalıdır. Bu depoda; olay anında müdahale etmeye yarayan araçlar, kesme aletleri, buldozerler, vinçler, aydınlatma araçları, jeneratörler vb. vb. hazır tutulmalıdır.

Ayrıca yeterli ölçüde çadır, yatak, battaniye, ısıtma aracı, ecza malzemesi, hazır yiyecek ve konserve, sahra hastanesi ve sağlık personeli vb vb de hazır tutulmalıdır.

Keza, acil müdahale için bir ekip, maaşları ödenerek hazırda bekletilmelidir (her an yangın olmadığı halde itfaiye ekipleri nasıl hazır bekletilirse, onun gibi). Tamamen gönüllü çalışan ve dağ sporcularından oluşan Munzur Arama Kurtarma Derneği (MUDAK) bu konuda iyi bir örnektir. Bu arkadaşlar, depremi haber alır almaz, kimseden emir ya da talimat veya izin beklemeden, Dersim’den Adıyaman’a acilen hareket etmiş ve bu bölgede 170 kadar insanı (belki birçok hayvanı da) kurtarmışlardır. Dinamik ekiplerin yerelden anında acilen harekete geçmeleri hayatların kurtarılmasında belirleyicidir.

Bu durumda, AFAD gibi, merkezî hükümetin emriyle kurulmuş, bürokratik tutumlarla yardımları engelleyen, madencilerin bölgeye gitmesini geciktiren, hatta gönüllü kuruluşların yardımlarına el koyan merkezî “ilk yardım” kuruluşlarına “gölge etme başka ihsan istemez” demek ve ortak yerel çalışmada mütevazı bir şekilde yer almalarını sağlamak kaçınılmaz olmaktadır.

Merkeziyetçilik ve tekelcilik öldürür. Ademi merkeziyet ve çoğulculuk hayat kurtarır.



