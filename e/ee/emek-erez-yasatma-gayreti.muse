#title Yaşatma gayreti
#author Emek Erez
#date 26.02.2023
#source 08.03.2023 tarihinde şuradan alındı: [[https://kisadalga.net/yazar/yasatma-gayreti_58446][kisadalga.net]]
#lang tr
#pubdate 2023-03-08T15:02:16
#topics afet, iktidar, dayanışma



Yan Lianke’nin, “Günler, Aylar, Yıllar” adlı kitabında, kuraklık nedeniyle tüm köyün göç etmek zorunda kaldığı bir hikâye anlatılır. Bu hikâyede ihtiyar bir insan, büyük çaba vererek yeşerttiği mısır fidesini ve yoldaşı kör köpeği bırakmak istemediği için göç etmez. Metin boyunca bu üç farklı türden varlığın hayatta kalma, birbirini yaşatma çabasına tanık oluruz. İhtiyarın, kendi hayatının önüne koyduğu mısır fidesini ve köpeği yaşatma gayreti bir direnme biçimine dönüşür ve bize yaşatmanın, şartlar ne olursa olsun hayatta tutmanın, o arzunun önemini hatırlatır.

**** Utancın asıl sahipleri

Son günlerde yaşadığımız deprem felaketiyle birlikte hayatta tutma çabasının önemini bir kere daha fark ediyoruz. Çünkü yıllardır bize sadece ölüm dayatılıyor. Yaşam ekranda akıp giden tabutların sayısına indirgeniyor. Felaket ve salgın gibi durumlarda ölüm bir an önce ortadan kaldırılması, görünmez kılınması gereken olarak şeyleştiriliyor. Sayı sayı akanın birinin hayatı olduğunu unutmamız, ölüm normalmiş gibi benimsememiz isteniyor. Pandemi, iş cinayetleri, yangınlar, katledilen kadınlar, translar hepsi yaşamın içine yerleştirilen bir ölüm imgesine dönüştürülüyor. Sadece bunlar da değil tabii yaşatmayan siyaset hayvanın, doğanın bedeninde kısacası, yeryüzünün tüm parçalarında kendini gösteriyor. Yaşamın üstüne çökmüş bir ölüm bulutuyla yaşamaya zorlanıyoruz.

Bu durum tüm bunlara tanık olarak yaşayanın da eyleme kudretini aşındırıyor. Çünkü dilin hep ölüme çıktığı yerde, hayatta kalan için sonrası “ontolojik utanç”la varolmaya dönüşüyor. Bradatan’ın tanımıyla bunun anlamı; “varoluşumuzda saygısızca bir şey olduğunu düşünmektir.” Bu durum insan için kendi varlığından, çaresizliğinden, ölümün yönünü yaşama çevirememekten duyulan bir utancı imler.

İnsani olana yabancılaşmamış herkesin duyabileceği bir duygudur utanç. Ancak kapitalizmin krizleri çağında yaşarken kişisel olarak utanç duymanın ötesinde, yaşananın sorumluluğunun şirketleşmiş devletlerin, yaşama geçirdiği dişliler olduğunu akılda tutmak gerekiyor. Bize devamlı ölümü hak görenlerin, cenazelerimiz buz gibi betonun altında beklerken acılarımızı şova dönüştürenlerin, inşaat şirketleriyle danışıklı dövüş halinde havada para uçuranların, insanlar bir yaşam daha nasıl kurtulur diye çırpınırken hiçbir şey yapma gayreti göstermeyenlerin utancıdır bu. Ama maalesef yaşama dair her şeyi değersizleştirenlerin, ölenin beden bütünlüğüyle toprağa kavuşmasını bile konuşmak zorunda bırakanların, bizi ölüme dönük bir yüzle yaşamaya mahkûm edenlerin, böyle bir duyguya sahip olmadıkları da ortada. Şu koşullarda bizim de payımıza düşenler var elbette: Ölüler bizim, yas bizim, dayanışma bizim ama kriz sizin, felaketten kalan o beton bile olmayan malzeme sizin, bu inşaat düzeni sizin… Fikrimce, bunu iktidar sahiplerine sürekli hatırlatmak, utancı asıl sahiplerine iade etmek gerekiyor.

**** Yaşam değil, şirketler önemli

Pandemi deneyiminin bize gösterdiği en önemli şey devletlerin şirketlerle iç içe geçmiş kapitalist pratikleri olmuştu. Fabrikaların, iş yerlerinin neredeyse “kamp” gibi işlediğini, güvenliğin yaşam için değil tam tersine yaşamı tahakküm etmek için işe koşulduğuna tanık olmuştuk. Deprem felaketiyle birlikte bir kere daha bu durumu yaşıyoruz. Yaşatmayan siyasetin üzerimize düşürdüğü ölüm gölgesinde, insanlar işe çağrılıyor, işten çıkarılıyor veya ücretsiz izne gönderiliyor. Burada bir kere daha görüyoruz ki kapitalist pratiklerin işlediği yerde yaşamın değil, şirketlerin devamlılığı esas alınıyor. Bizden, acısız makineler gibi işe devam etmemiz bekleniyor. Sistemin çarkları dönsün diye bir kere daha insani olana, acıya, yasa sırtımızı dönmemiz talep ediliyor çünkü ölümün sadece veri veya bürokratik bir mesele haline getirildiği bir yaşamda kedere yer yok.

Böylece şu bir kere daha ortaya çıkıyor, kapitalist çalışma düzeninde yaşam ve beden sağlığı sadece şirketlerin devamlılığı söz konusu olduğunda bir anlam ifade ediyor. Bize dayatılan “pozitif ol”, “bardağın dolu tarafını gör”, iyileş”, “sağlıklı ol”, “acıdan biraz uzaklaş” gibi kişisel gelişim soslu cümlelerin anlamı da burada açığa çıkıyor. Çünkü bu bir an önce “iyi ol”, rutine dön ki sistemin çarkları işlemeye devam etsin demek. Kısacası, bize devamlı dayatılan sağlıklılık bedenlerimiz ancak işe koşulabiliyorsa, verimli olabiliyorsa önemli oluyor.

**** Güvenlik söylemi

Ayrıca, tıpkı Pandemi döneminde olduğu gibi, güvenlik söyleminin yaşatmak için değil, deprem bölgelerinde “istisna hali” olarak işlevselleştirildiğini, dayanışmanın, yaşatma telaşının, önüne bir set gibi çekildiğini görüyoruz. Çünkü devlet iktidarı yıllardır ölüm siyasetiyle var ediyor kendisini. Her sabah güne ölümle başlatıyor bizi. İnşaat hâline getirdiği bir coğrafyada dağın, toprağın, hayvanın, denizin ve nihayetinde denetimsiz, kâğıt gibi binaların altında insanın-insandışının yaşam hakkının gasp edildiğine tanık oluyoruz.

Yaşatmayan siyaset öldürüyor ve sonrasında “enkazın” sahipleri, yasın sahiplerine güç gösterisi yapabilmek için güvenlik söylemini işlevselleştiriyor. Bu aslında militarist güvenlik söyleminin de amacını gösteriyor çünkü konu gerçekten halkların emniyeti olsaydı, felaket anında yaşam güvenliği için her kurum seferber edilirdi ama öyle olmadığını bir kere daha gördük. Bu da yaratılan “istisna” koşullarının, felaketin asıl sahiplerinin kendi sorumluluklarını ötelemek, güç kullanarak, zor siyasetiyle bizi disipline etmek dışında bir anlam ifade etmediğini gösteriyor.

Şirketleşmiş devletlerin kapitalizmin krizler çağında felaketleri kendi yararına işlevselleştirdiğine çok kez tanık olduk. Yaşadığımız yangınlar sonrası şirketlerin kalan ağaçları yıkmak için nasıl hemen ortaya çıktığını, sel felaketi sonrası onca yaşam yok olmuşken, HES’ler zarar görmedi açıklamaları yapanları duyduk. Şimdilerde de yaşamı kurtarmak dururken banka kasalarını kurtarmaya çalışanları, şirketlerle kol kola girerek depremi yaşayan bölgeleri, inşaat sahası yapma gayretlerini görüyoruz.

Kapitalist devletler için yaşamın tek anlamının kâr olduğu, kendi coğrafyamızın iktidar sahiplerinin icraatlarında daha açık hâle geliyor ki coğrafyamızda son yıllarda yaşadığımız tüm felaket anlarında, bu daha da yüzeye çıktı. Bu durumda yüzünü yaşama dönen bir siyaset için bu görünür olanı devamlı hatırlamak ve hatırlatmak elzem. Çünkü bu artık sistemin sürdürülemez hale geldiğinin de göstergesi, bu böyle devam ettikçe şirketleşmiş devletler kendi krizlerinin bedelini halklara ödetmeye devam ediyorlar ve edecekler.

**** Yaşatan siyasetin göründüğü yer

Şimdi en başa Yan Lianke’nin, “Günler, Aylar, Yıllar” kitabına dönelim. Bahsettiğimiz gibi, kuraklık nedeniyle tüm köy göç ediyor; ihtiyar, kör köpek ve mısır filizi kalıyor geriye. Bu üç farklı varlık için devam etmek oldukça zor çünkü tek bir mısır tanesinin, bir damla suyun bile hayatta kalma çabasının parçası olduğu koşullar var hikâyede. İhtiyar bu şartlarda köpeği ve mısır filizini yaşatmak için her şeyi yapıyor. Çünkü onun için özellikle mısır filizini yaşatmanın şöyle bir anlamı var: Kendisi hayatta kalamasa bile yağmur bir gün yağacak, köyden göçenler geri dönecek ve döndüklerinde mısır filizinden kalan tohumları ekecekler. Yani aslında onun amacı yaşamı sürdürmek bir tek fidenin ona verdiği yaşatma kudretinin nedeni, her şeye rağmen yeni bir hayat kurulabileceğine olan inanç.

Tıpkı bahsettiğimiz hikâyede olduğu gibi, deprem felaketi her şeye rağmen bize dayanışmanın, kimseyi dışarıda bırakmadan; hayatta tutmanın, bunun için eyleme kudretinin ne kadar önemli olduğunu bir kez daha hatırlattı. Sadece bunu da değil, baştan beri bahsettiğimiz yaşatmayan siyasete karşı hayatta tutan siyasetin nerede olduğunu da gösterdi. Kriz nedeniyle yoksullaştırılmış Türkiyeli halkların ellerindeki iki ne varsa birini gönderme gayretine tanık olduk. Sahada her şeyini ortaya koyan gönüllülerin felaketi yaşayan insanlar için nasıl seferber olduğunu, o tek mısır filizini yaşatmaya çalışan ihtiyar gibi, yarını kurmak için canla başla nasıl çalıştıklarının şahidiyiz. Tek bir canı bile dışarıda bırakmadan gösterilmeye çalışılan bu çaba bana kalırsa yaşatan siyaseti nerede bulacağımızı da gösteriyor. Çünkü sistemi içeriden geçersiz kılmanın, kuruyanı yeniden yeşertmenin, armağan etmenin, diğerkâm olmanın, bizi yok sayanlara karşı varız demenin anlamını da hatırlıyoruz. Rancière’nin işaret ettiği gibi, “hâlihazırda gerçekleşen gelecek, daima belirsiz, kırılgan bir şimdide gerçekleşir.” Tüm bu yaşatılanlara karşı gelişen tepkinin ve dayanışmanın, neden böyle bir anlamı olmasın?

**** Kaynaklar

 - Lianke., Y., (2020), “Günler Aylar Yıllar”, (Çev. Erdem Kurtuldu), İstanbul: Jaguar Kitap.

 - Bradatan,C., (2018), “Fikirler İçin Ölmek ‘Filozofların Tehlikeli Hayatları”, s. 146 (Çev. Kübra Oğuz), İstanbul: Can Yayınları.

 - Jacques Rancière’nin yazısı için bknz. [[https://textumdergi.net/degismekte-olan-bir-dunyada-ozgurlesme-ve-esitlik/][https://textumdergi.net/degismekte-olan-bir-dunyada-ozgurlesme-ve-esitlik/]]



