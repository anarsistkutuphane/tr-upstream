#title Anarşizm ve Cinsiyet Sorunu
#author Emma Goldman
#SORTtopics cinsiyet, kadın, anarşizm, evlilik, anarşist feminizm
#date 1896
#source 23.03.2020 tarihinde şuradan alındı: [[https://www.geocities.ws/anarsistbakis/makaleler/goldman-anarsivecinsiyet.html][www.geocities.ws/anarsistbakis]]
#lang tr
#pubdate 2020-03-23T23:00:00
#notes Çeviri: Anarşist Bakış <br> İngilizce Aslı: [[https://theanarchistlibrary.org/library/emma-goldman-anarchy-and-the-sex-question][Anarchy and The Sex Question]], The Alarm, 27 Eylül 1896, s. 3 <br> Çevirenin Notu: Çevirenin metine yaptığı eklemeler, açıklamalar vb, […] ile gösterilmiştir.

Kuvveti ve kasları solgun, çelimsiz zengin bebeler tarafından oldukça beğenilen, ancak emeği açlık kurdunu kapısından uzak tutmaya ancak yeten işçi sınıfı erkekleri, onlara sabahtan akşama kadar kölelik yapmak ve harcamaları düşük tutmak üzere her türlü çabayı sarf etmek zorunda kalan eşlere veya ev-hizmetçilerine sahip olmak için evlenirler yanlızca. [Kadının] kocasının acınacak ücretinin ikisinin de yaşamını sürdürmesini sağlamak için devamlı çaba sarfetmekten sinirleri o kadar gerilir ki, giderek asabi hale gelir ve –yazık ki kısa zamanda umut ve planlarının kül olup uçtuğu sonucuna ulaşacak ve evliliğin gerçekten de başarısız olduğunu düşünmeye başlayacak olan– lordunun ve efendisinin sevgi talebini yerine getirmekte artık başarılı olamaz.

*** ZİNCİRLER GİDEREK AĞIRLAŞIR

Harcamalar azalmak yerine çoğaldıkça, evliliğinde çok az kuvveti olan, benzer şekilde kendini ihanete uğramış hisseden eş bu kuvvetinin tümünü de yitirir, ve devamlı açlık sıkıntısı ve korkusu, güzelliğini evliliğinin ardından kısa zaman içinde tüketir. Giderek ümitsizleşir, ev işlerini ihmal etmeye başlar; ve kendisiyle kocası arasında, onlara yaşamlarındaki keder ve yoksullukları göğüsleme kuvveti verecek hiçbir sevgi ve sempati bağı da olmadığı için, birbirlerine daha da sıkıca kenetlenmek yerine birbirlerine giderek daha fazla yabancılaşırlar ve hatalarına karşı giderek daha tahammülsüz olurlar.

Milyonerlerin yaptığı gibi klübe gidemeyen erkek meyhaneye gider, ve kederini bira veya viski bardaklarında boğmaya çalışır. İhmal edilişini bir sevgilinin kollarında unutmaya çalışamayacak kadar onurlu ve herhangi bir meşru eğlence ve uğraştan faydalanmak için fazlasıyla yoksul olan erkeğin kederinin talihsiz ortağı ise, ev diye adlandırdığı sefil ve derme çatma yerin içine sıkışıp kalır, ve kendini bu zavallı adamın eşi yapan ahmaklığa kötü kötü lanet eder.
<br>
Ancak onların birbirlerinden ayrılmalarının hiçbir yolu yoktur.

*** ANCAK ONU TAŞIMAK ZORUNDADIRLAR

Ancak kanun ve Kilise tarafından boyunlarına vurulan zincir ne kadar canlarını yakarsa yaksın, bu iki kişi onun kırılmasına müsade etmedikçe kırılamaz.

Yasanın onlara özgürlük bahşedecek kadar merhametli olması için, özel yaşamlarının tüm detayları gözler önüne serilmelidir. Kadın, kamu oyunda ayıplanır ve tüm yaşamı alt üst olur. Bu utancın korkusu, kendisini ve pekçok kızkardeşini ezen insafsız sisteme karşı tek bir protestoya dahi girişmeksizin, onun evlilik yaşamının ağır yükü altında ezilmesine yol açar.

Zengin olan skandaldan sakınmak için buna katlanır –fakir ise çocukları uğruna ve kamu oyunun korkusuyla. Onların yaşamları ikiyüzlülük ve yalanın uzun bir sürekliliğidir.

Cinselliğini satan bir kadın onu satın alan adamları terk etme özgürlüğüne her zaman sahipken, “namuslu eş” ise onun canını yakan birlikten [evlilikten] kendisini kurtaramaz.

Kilise ya da toplum öyle kabul etsin ya da etmesin, aşkla kutsanmamış, doğal olmayan tüm birlikler fahişeliktir. Bu gibi birliklerin toplumun ahlakını ve sağlığını azaltmaktan başka bir yanı olamaz.

*** SUÇLU SİSTEMDİR

Kadınları kadınlıklarını ve bağımsızlıklarını en yüksek fiyat teklif eden alıcıya satmaya zorlayan sistem, az sayıdaki kişiye arkadaşları [aynı toplumun üyeleri olan kişiler] tarafından üretilen refahla yaşama hakkı sunan sistemin bir dalıdır. Emeklerinin meyveleri, refahın satın alabileceği her türlü lüksün içinde yüzen bir avuç aylak vampir tarafından emilirken, [toplumun] yüzde 99’u ise ruh ve bedenlerini ancak birarada tutacak kadar [bir şey için] sabahtan akşama kadar güç koşullarda çalışmak ve kölelik yapmak zorundadır.

Bir anlığına yirminci yüzyıl toplumsal sisteminin şu iki resmine bakın.

Pahalı iç döşemeleri fakir erkek ve kadınların rahat koşullarda yaşamasını sağlayabilecek o muhteşem saraylara, refah sahiplerinin evlerine bir bakın. Refah içindekilerin oğullarının ve kızlarının akşam yemeği partilerine bir bakın; tek bir tanesi suyla kuru ekmeğe talim eden açlık içindeki yüzlerceyi doyuracak olan bunlar lükstür. Günlerini kendi kendilerini tatmin etmeye yarayan yeni uğraşlar –tiyatrolar, balolar, konserler, yatçılık, süs ve gösteriş ile sefahat için delicesine bir arayışla dünyanın bir yerinden başka bir yerine koşuşturma– icat etmekle geçiren o moda düşkünlerine bir bakın.

*** ÖTEKİ RESİM

Hiçkimsenin sevgi dolu bir tek sözcük ve şefkat dolu bir ilgi sarfetmediği çocukları çıplak ve aç bir şekilde sokaklarda koşuşturan, cehalet ve hurafelerle büyüyen, doğdukları güne lanet yağdıran; asla bir parça temiz hava dahi soluyamadıkları karanlık, rutubetli bodrumlarda [hayvan sürüsü gibi] kümelenen, yırtık pırtık elbiseler giyinen, sefaletin tüm yükünü beşikten mezara sırtında taşıyanlara bir bakın.

Siz ahlakçılar, siz hayırseverler, bu iki resmin ürkütücü karşıtlığına bir bakın ve bana bunun suçlusunun kim olduğunu söyleyin! Yasal yollarla veya başka şekillerde fahişelik yapmaya itilenler mi, yoksa kurbanlarını böylesi bir ahlaki bozulmaya itenler mi?

Sebep fahişelikte değil, toplumun kendisindedir; özel mülkiyetin eşitsizliği[ne dayanan] sistemde, Devlet ve Kilise’dedir. Suçsuz kadınların ve çaresiz çocukların soyulmasını, katledilmesini ve onlara şiddet uygulanmasını yasallaştıran bu sistemdedir.

*** KÖTÜLÜĞÜN TEDAVİSİ

Bu canavar yok edilmedikçe, Senato’da, tüm kamu dairelerinde, zenginlerin evlerinde olduğu gibi keza yoksulların sefil barakalarında da var olan bu hastalıktan kurtulamayız. İnsanoğlu kendi kuvvetinin ve yeteneklerinin bilincine ulaşmalıdır; daha yeni bir yaşam, daha iyi ve onurlu bir yaşam başlatmak için onların özgür olması gereklidir.

Fahişelik, Saygıdeğer Dr. Parkhurst ve diğer reformcuların kullandıkları araçlarla önlenemez. [Fahişelik] onu besleyen sistem var oldukça sürecektir.

Tüm bu reformcular çabalarını, her türlü suçun babası olan sistemi yıkmak için mücadele edenlerinkiyle birleştirdikleri ve tam bir eşitliğe dayanan bir [sistem] –her bir üyesine, erkeğe, kadına ve çocuğa emeğinin ürününün tamamını veren, doğanın hediyelerinden faydalanmakta ve en yüksek bilgiye erişmekte tamamıyle eşit haklar sunan bir sistem– inşa ettiklerinde, kadınlar kendi başlarına var olabilecek [ing. self-supporting] ve bağımsız olacaklardır. Erkek artık sağlıksız, doğal olmayan tutkulara ve kötü huylara sahip olmazken, kadının sağlığı da artık bitip tükenmeyen zorlu çalışma ve kölelikle ezilmeyecek, o artık erkeğin kurbanı olmayacaktır.

*** ANARŞİSTİN HAYALİ

Her biri, fiziki kuvvet ve karşısındakine ahlaki güvenle evlilik haline girecektir. Her biri diğerini sevecek ve saygı gösterecektir; ve sadece kendi refahları için değil, kendilerini mutlu edecek işlere yardım edecektir; aynı zamanda insanlığın evrensel mutluluğunu arzu edeceklerdir. Bu tip [bir] birliğin ürünü zihinsel ve bedeni olarak güçlü ve sağlıklı olacaktır; ebeveynlerini onurlandıracak ve saygı göstereceklerdir; bunu görevleri olduğu için değil, ebeveynleri bunu hak ettiği için yapacaklardır. Onlar bütün bir topluluk tarafından eğtilecek ve bakılacaklardır, ve kendi isteklerinin peşinden gitmekte serbest olacaklardır; ve onlara dalkavukluğu ve kendi arkadaşlarını [toplum üyelerini] soyma temel sanatını öğretmenin gereği kalmayacaktır. Onların yaşamdaki amacı kardeşleri üzerinde iktidar kurmak değil, topluluğun her üyesinin saygı ve takdirini kazanmak olacaktır.

*** ANARŞİST BOŞANMA

Erkek ve kadın arasındaki birliğin onlar için tatminkar ve hoş olmadığı ortaya çıkarsa, sessizce ve arkadaşça ayrılacaklar ve sevimsiz bir birliği devam ettirerek evliliğin pekçok ilişkisini bayağılaştırmayacaklardır.

Günün reformistleri eğer kurbanlara eziyet etmek yerine, çabalarını sebebi ortadan kaldırmak için birleştirirlerse, fahişelik artık insanlığın yüz karası olmayacaktır.

Bir sınıfı baskı altına almak ve bir diğerini ise korumak ahmaklıktan daha kötü bir şeydir. Bu bir suçtur. Siz ahlak sahibi erkek ve kadınlar, başlarınızı öte tarafa çevirmeyin.

Önyargılarınızın sizi etki altına almasına izin vermeyin: soruna önyargısız bir açıdan bakın.

Kuvvetinizi heba etmek yerine, el ele verin ve bu bozuk, hastalıklı sistemin yıkılmasına yardım edin.

Eğer evlilik yaşamı onurunuzu ve kendinize saygınızı çalmadıysa, eğer çocuklarım dediklerinizi seviyorsanız, onlar için olduğu kadar kendiniz için de kurtuluşu hedeflemeli ve özgürlüğü kurmalısınız. O zaman, ancak bundan [bu gerçekleştikten] sonra evlilik kurumunun [ing. matrimony] kötülükleri son bulacaktır.



