#title Anarşist Kuir Vegan Manifesto
#author İç-mihrak
#SORTtopics vegan, queer
#date 27.04.2015
#source 17.09.2022 tarihinde şuradan alındı: [[https://web.archive.org/web/20220922093837/https://picsur.org/i/923b3f2e-653a-46f3-9b2e-0424785a062f.jpg][facebook.com/iç-mihrak-171345101826]]
#lang tr
#pubdate 2022-09-17T17:37:54

Anarşinin içindeki insanmerkezci, türcü, cinsiyetçi, homofobik/transfobik ve örgüt fetişisti tüm algı ve eğilimleri reddediyoruz.

Kurduğu ilk yalan cümleyle konuşmaya başlayarak dili peyderpey kurumsal bir tahakküm haline getiren uygar insana karşı susarak ve dansederek çok şey anlatan bir mimci, hüzünlü ve asi bir palyaçodur bize göre anarşi.

Bitmeyen hastalıklar, uzayan sınırlar, giderek keskinleşen dikenli teller, hızla yükselen duvarlar, boğazlanan hayvanlar, artan tecavüzler, normalleştirilen nefret cinayetleri ve umursamazlıkla yıkanan bu dünyanın bir sonu olmalı: Anarşi!

Anarşi tüm sosyal mücadelelere destek veren tekçi olmayan bir genel özgürlük talebi ve farkındalığı yaratmayı kendine ilke edinir. Bu amaçla doğrudan eylem, boykot, dayanışma, sabotaj, saldırı ve reddediş eylemlerini de benimser.

Anarşi için devrim; duvarları hiçbir dayatma olmaksızın molotofla ateşe vererek yıkmaktır ya da nöronları arasındaki öfke dolu sinapslarıyla asimetrik, sanal, sosyal yaşamsal algılar kurgulayarak sistemi çökertmektir.

Anarşist devrim tutkularımızı, bize öğretilmiş düşünceleri sürekli eleştirerek ve gerçekte ne istediğimizi durmadan sorgulayıp özgür bırakırken sömürünün izlerini yok ederek yepyeni, taze ve doğaçlama umutlar inşa eder.

İnsan olmayan dünyaya karşı gösterilen şefkatle zulme karşı verilen TÜM mücadeleleri kendi kendine kucaklayan tavır, anlam ve felsefe anarşist devrimin özüdür.

Anarşistler; devlet, hiyerarşi ve rekabete dayalı tüm örgütlü otoriter kurum ve şirketlerle ilişkisini nihai olarak sıfırlamaya doğru giderken, yıkım pratiklerinde uzlaşmasız bir mücadeleyi içselleştirerek kuralsızlıklar, dayanışma ve aşkla örülmüş özgür ve yaratıcı başkaldırı yöntemlerinden faydalanır.

Acıların, çürümüşlüğün, umursamazlığın din adıyla kodlarımıza işlendiği hile olan kültüralizmin ve uygarlığın batağında mistizm bir çıkış değil kaçıştır; öyleyse kağıttan, betondan, aşağılama ve teslimiyetten müteşekkil tüm sefil Tanrılara karşı mücadelemiz esastır.

Anarşi hakim kültüre, doğanın tüketilmesine, iktidarsal, sömürgen, otoriter şiddete meydan okur. Yaşamı ve doğal hayatları kutsar, yeniden dirilmeye, cennet/huri rüşvetine değil özgürleşmiş insanlara bel bağlar.

Güdülen, acı çeken kitleler bir şeylerin yolunda gitmediğini anlıyorlar ve bir şeye muhtaç olduklarını biliyorlar. Muhtaç oldukları şey ne bir önder ne de Tanrı: Anarşi!

Anarşi, insanın diğer türlere üstünlüğünü temel alarak dünyayı hızla totalitarizme ve ekolojik faşizme sürükleyen endüstrizasyon ve hayvan sömürüsünü ortadan kaldırılmadıkça özgürlük hayaldir.

Anarşi; erkek egemen toplumun saldırısı altındaki kadınların, tüm insanlar tarafından sömürülen hayvanlarla duygudaş bir ittifak kurması gerekliliğidir.

Doğaya karşı kurduğu tüm tuzaklarını uygarlıkla inşa ederken, hayvanlardan miras aldığı bedensel varlığını dahi unuturak enerjisini ve birikimini yine hayvanlara karşı acımasızca kullanan, onları tüketen ve iktidarını onlar üzerine kuran insan saldırganlığını ve şiddetini çökertmek anarşinin işidir.

Her yıl milyarlarca canlının katledilmesini meşrulaştırmak için kullandığımız o çürümüş ve yanlış “insan” kavramını artık anarşiyle terk etmek zorundayız.

Hayvanların asaleti ve yaşamın masumiyeti biz onları yedikçe tükeniyor. Veganizm ve anarşi aynı iktidarsal sorunların mağdur ettiği bireyleri temsil eder.

İsyan olduğu ilan edilsin ya da edilmesin hayvan özgürlüğü ve quir hareket; hakim kültüre karşı defakto bir isyandır.

Anarşi; heteroseksizmin baskı ve saldırısı altındaki quirlerin, tüm insanlar tarafından sömürülen hayvanlarla duygudaş bir ittifak kurması gerekliliğidir.

Türler ve cinsler arası eşitsizlik ve sömürü ilişkileriyle mücadele anarşiden bağımsız düşünülemez. Trans kadınlar kadındır, inek sütü tecavüzdür ve feminizm anarşinin tutarlı bir ifadesidir.

Anarşi; zenginlerin sömürdüğü emekçi yoksulların, tüm insanlar tarafından sömürülen hayvanlarla duygudaş bir ittifak kurması gerekliliğidir.

Hayvanlarla kadınları aynı bilboardlarda sömürerek tecavüzü meşrulaştıran ve hazları için öldüren uygarlığın erkek doğası anarşist mücadelenin hedeflerindendir.

Her canlı cinsiyetsiz doğar ve büyür, toplum denen dayatmaları kanıksatma zorbalığı cinsiyeti belirler ve rolleri dağıtır; cinsiyete dair tüm sapkın ahlaki yargıları anarşiyle itham ediyoruz.

Anarşi; kültür tarafından egemenlik altına alınan doğanın, erkek tarafından hükmedilen kadının, akıl tarafından esir edilen duygunun, insan tarafından sömürülen hayvanın ve heteroseksizm tarafından bastırılan özgür yönelimin garantisidir.
