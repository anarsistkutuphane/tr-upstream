#title Politika?
#author İlyas Seyrek
#SORTtopics politika, devlet, hükümet, siyaset, anarşizm, devrim, mikhail bakunin
#date 07.04.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-04-18T21:04:01


Politika, tarihsel süreç içerisinde toplumsal ilişkilerin ve mekanizmaların değişimiyle birlikte dönüşüme uğramış, oldukça farklı biçimlerde tanımlanmıştır.

Belirli bir yerleşim alanında nüfusları giderek artan topluluklar farklı istek, arzu ve ihtiyaçları yaratmış ve politikanın, toplumsal ihtiyaçların ortaklaşa giderilebilmesinin yolu olduğu giderek ortaya çıkmıştır.

İnsanın toplumsal yapısının işleyiş yolu olarak politikanın önemi kimi düşünür ve filozoflar tarafından dile getirilmiştir. Aristoteles’in insanı “zoon politikon/politik hayvan” olarak tariflemesi de politikanın insan toplumsallığına ilişkin olduğuna dair bir görüşün yansımasıdır.

Politika temel olarak, zihinsel ve fiziksel alanlarda çeşitlenmiş ihtiyaçların toplumsal çapta giderilmesi için kararlar alma, kararların uygulanmasını sağlama çabası olarak tanımlanabilir.

Toplumsal yaşam biçimlerinin hemen hepsi için geçerli olabilecek bu işleyiş, iktidarlı yapılar ile birlikte yozlaşmış ve dönüşüme uğramıştır. Toplumsal yapıyı ve onun ürettiği avantajları kontrol altına almaya çalışan iktidarlar, baskı ve manipülasyon yoluyla politika yapma biçimini değiştirmiştir. Bu biçim bir kişi, grup ya da sınıfın/zümrenin hakimiyetine geçmiştir. Politika toplumsal bir uğraş olmaktan çıkıp ya tamamen iktidarların hakimiyetinde kalarak sadece “yönetim” olarak anlaşılmış ya da iktidarların izin verdiği ölçüde, sınırlarını iktidarın çizdiği bir çerçeve içerisine hapsedilmiştir.


*** Siyaset Yapmak

Toplumsal bir sorumluluk içerisinde tek tek bireylerin düşlediklerini eyleyebilmeleri olarak tanımlayabileceğimiz özgürlük halinin yolu toplumsal yaşamı sürdürmekten ve dolayısıyla politika yapmaktan geçerken iktidar ve mülkiyet ilişkileriyle sarılı otoriter mekanizmaların varlığı ile birlikte yeni politika yapma yolu köleliğe neden olmuştur.

Politika sözcüğü Türkçe’de “siyaset” olarak karşılık bulmaktadır. Arapça kökenli bir kelime olarak siyasetin “hayvan ve reayayı keyfi yönetme” anlamına sahip olması da yukarıda bahsedildiği gibi politikanın yozlaştırılmış biçiminin günümüzde kabul gören anlamını işaret etmektedir.

Günümüzde devlet, politika sahnesini tamamıyla kendisi doldurmaya çalışmaktadır. Politika toplumsal yaşamı tümüyle düzenleme iddiasına sahip devletli sistemin sınırlarına sıkıştırılmıştır. Günümüzde politika denildiğinde akıllara partiler, seçimler, anayasa vs. gelmesinin nedeni de iktidarlı mekanizmaların son üç yüz elli yılda politika alanını domine etmiş olması ve devletli bir işleyişin konusu dışındakileri politikanın dışına itmiş olmasıyla alakalıdır.


*** Politika ve Anarşizm

Bilimde, teknolojide ve ekonomide büyük gelişmelerin yaşandığı ve aynı zamanda büyük toplumsal sorunların ve dönüşümlerin yaşanmaya başlandığı modern dönemle birlikte toplumsal yaşamın kim tarafından ve hangi temellerle inşa edileceğine dair de önemli düşünce ve hareketler gelişmiştir.

Politikanın sınırlarının tekrar tartışıldığı, politika yapma hakkının tek bir kişi ya da zümrenin elinden alınmak istendiği dönemle birlikte siyasi hareketler ve ideolojiler “politik” arenaya etkide bulunmuştur.

Toplumsal dönüşümü öngören hareket ve ideolojilerin politikanın alanının genişlemesine dair talep ve mücadeleleri, politikanın tanımı ve uygulanmasına dair önemli tartışmaları ve çatışmaları yaratmıştır. Halkın, temsilcileri yoluyla politikaya dahil olmasını isteyen teori ve ideolojilerin yanında toplumsal üretimi sağlayan ve halkın büyük çoğunluğunu oluşturan işçi sınıfının devleti kontrol ederek toplumsal yaşamı düzenlemesi gerektiğini düşünen teori ve ideolojiler ortaya çıkmıştır.

Politikayı devletli arenada konumlandıran, yönetsel bağlama sokan düşüncelerin karşısında ise anarşizm belirmiştir.

Toplumsal yaşamı düzenlemek politikanın konusu ise anarşizm de toplumun iktidarsız bir biçimde yeniden organize edilmesini savunduğu için politik bir ideoloji olarak ortaya çıkmıştır. Hem tek bir kişi ya da grubun uğraşı olan eski iktidarlı politika yapma biçimine hem de halkın temsilcileri yoluyla icra edilen politika yapma biçimine karşı çıkarak halkın tamamının toplumsal yaşamı düzenleyebilmesi için etkin birer özne olacağı politika yapma biçimini amaçlamıştır.

Toplumsal yaşamın radikal dönüşümünü, bir devrimi amaçlayan ve buna göre politika yapma biçiminin değişmesini isteyen anarşizmin bu güçlü iddiası ile birlikte, Birinci Enternasyonal, Paris Komünü, 1917 Rus Devrimi, 1921 Mahnovşçina deneyimi ve 1936 İberya Devrimi başta olmak üzere, dünyadaki toplumsal hareketlerde önemli etkileri bulunan bir hareket ve ideoloji olduğunu görmek mümkündür.

Anarşizm, toplumsal anlamda etkisi yüksek bir hareket ve ideoloji olarak politika yapma biçimlerinin tartışılmasında da oldukça büyük katkılarda bulunmuştur. Liberal politika yapma biçiminde deneyimlenen, temsili demokrasinin çıkmaza girmesi durumu ve işçi sınıfını otoriter araçlardan ayrı düşünemedikleri için politikanın tek partinin çıkar ve söylemlerine terk edildiği örnekleri yerel, bölgesel veya küresel çapta açlık, göç, savaş, sömürü gibi toplumsal adaletsizlik ve baskı pratiklerine neden olmuştur.

Tüm bu toplumsal sorunların çözümü olarak da liberal veya marksist teori ve hareketler özgürlükçü düşüncelerden beslenmeyi tercih etmektedir. Devletli toplumsal yaşama itiraz, birey ve toplum analizleri gelişmiş olan anarşist teori ve pratikten yeni politika yapma araçları ödünç alınmaya çalışılmıştır. Özörgütlenme, özyönetim gibi kavramlar bu yöntem ve araçlardan sadece birkaçıdır.

Günümüzde liberal ve marksist teorilerin etkilendiği bu kavramların kökenleri pek tabi ki klasik anarşist döneme kadar gitmektedir. Ayrıca bugün bu kavramların kökenlerini ve bir ideoloji olarak anarşizmin politikadan ne anladığını klasik anarşist düşünürlerin politikaya bakış açısına bakarsak anlayabiliriz.


*** Bakunin ve Anarşist Politika

Klasik, devrimci anarşizmin politika ile ilişkisini kurabilmek için yüzümüzü dönmemiz gereken kişilerden biri Bakunin’dir. Bakunin’in devrimci mücadele, anarşizm ve politika konusundaki düşüncelerini en net anlayabileceğimiz metinler ise I. Enternasyonal’e dair yazdıklarında ortaya çıkmaktadır.

Bakunin, dönemindeki politika yapma biçimini analiz etmiş ve politikayı “bugüne kadar, gerçek bir halk politikası olmadı. Bugüne kadar var olan biricik politika, sonu gelmez üstünlük mücadelesinde birbirlerini devirip birbirlerinin yerine geçmek için işçilerin fiziksel yiğitliğini kullanan ayrıcalıklı sınıfların politikası oldu.” sözleriyle eleştirmiştir.

Fransız Devrimi’nde olduğu gibi bir önceki döneme göre politikanın çerçevesinde büyük değişikliklerin meydana geldiği zamanları da yetersiz ve eksik bulmuştur: “Fransız Devrimi bile halkın konumunu esaslı bir şekilde değiştirmedi. Yalnızca yerine burjuvaziyi geçirmek üzere, soylular sınıfını ortadan kaldırdı”.

Bakunin’in, bu anlayışla birlikte, I. Enternasyonal esnasında yürütülen “Enternasyonal’deki işçilerin politik mücadele verip vermemesi” tartışmasında politika karşıtlığı yapmasını iyi anlamak gerekmektedir: “Kurtuluş söz konusu olduğunda, her türlü politika, gerici unsurlar tarafından belirlendiği için, Enternasyonal’in öncelikle kendisini bir bütün olarak politikadan arındırması ve ardından da burjuva toplumsal düzenin yıkıntıları üzerinde Enternasyonal’in yeni politikasını oluşturması gerekiyordu”.

Enternasyonal örgütlenme Bakunin’e göre sadece ekonomik nitelikte de değildir. Bu kurtuluş aynı zamanda toplumsal, felsefi ve ahlakidir. Ayrıca tüm devletleri ve sınırlarını ortadan kaldırma anlamında da negatif bir politiklik taşımaktadır. Mevcut politik düzeni yıkmak oldukça politik bir niteliktir.


*** Malatesta ve Politik Mücadele

Anarşist mücadele içerisinde pek çok teorik ve pratik tartışmada yer almış, anarşizmi örgütlemeye çalışmış ve politik örgütlenmelerin içerisinde bulunmuş biri olan Errico Malatesta’nın politika hakkındaki düşüncelerine bakmak da politika yapmanın anarşizm içerisinde nasıl anlaşıldığını görmek açısından önemlidir.

Malatesta politik mücadele sözüyle, devlete karşı mücadeleyi kastetmektedir ve ona göre devlet, yasaları yoluyla politika yapma meşruluğunu kendi elinde tutan bir yapıdır. Herkesi ilgilendiren tüm konularda, herkesin onayı alınmalıdır; bu nedenle Malatesta insanları yeni bir politika tarzına çağırmaktadır.

“Politik özgürlük sağlanmadan ekonomik özgürlüğe, ekonomik özgürlük sağlanmadan da politik özgürlüğe ulaşılamaz” derken de politik özgürlükten kastı -toplumun kendi yaşamını organize edebilmesi için- devletin politik arenadan silinmesidir.

Politika alanını kendi çıkarları doğrultusunda domine eden devletin karşısında halkın yaşamsal acil ihtiyaçlarının karşılanması doğrultusunda verilen hak mücadelelerini de “mücadele ederken de bir şeyler öğrenileceğine ve biraz olsun özgür olmak için başlanan mücadelenin, özgürlüğün tadına varıldığında tamamen özgür olmak için verilen bir mücadeleye dönüşeceğine inanıyoruz.” ifadesiyle politik mücadele kapsamında değerlendirmiştir.


*** Politik Mücadele ve Devrimci Anarşizm

Bugün her türlü bireysel ve toplumsal özgürlüğün karşısında, toplumsal yaşamın tamamını kontrol etmeye çalışan iktidar mekanizmaları tarafından icra edilen “politika” tarzı, devrimci anarşizmde kullanılacak bir yöntem değildir. Aksine devrimci anarşistlerin, toplumların bütünlüklü özgürlüğü için devletlerin politikasına karşı kendi yaşam alanlarını kurup büyütme ve nihai olarak tüm iktidar mekanizmalarını yok etme mücadelesi kendi politikalarının özünü oluşturmaktadır.



