#title Deprem ve "halkın şok doktrini"
#author Foti Benlisoy
#date 13.02.2023
#source 15.02.2023 tarihinde şuradan alındı: [[https://fotibenlisoy.tumblr.com/post/709162657226391552/deprem-ve-halk%C4%B1n-%C5%9Fok-doktrini][fotibenlisoy.tumblr.com]]
#lang tr
#pubdate 2023-02-15T18:06:01
#topics afet, iktidar, devlet



19 Eylül 1985’te, yerel saatle 07.17’de Mexico City, merkez üssü şehre 350 km. mesafede olan 8 şiddetinde bir depremle sarsılır. Şehrin büyük bir bölümünde çok ciddi bir hasara neden olan deprem sonucunda kaç kişinin hayatını kaybettiği bugün dahi tartışma konusudur. Resmi rakamlar beş bini aşkın kişinin öldüğü yönündeyken çeşitli yurttaş inisiyatiflerinin verileri bu sayıyı 30.000’e kadar yükseltmektedir.

Ölü sayısının tartışmalı olmasının nedeni, Meksika hükümetinin deprem sonrasında aldığı tutumla alakalıdır. Hükümet depremin hemen ardından ülkede bir yayın yasağı getirir. Deprem sonrasında hükümete bağlı kurtarma ve yardım kuruluşlarının afet bölgesine erişimi çok yavaş gerçekleşir. Üstelik hükümet yurtdışından gelen yardımların önüne geçer. Kurtarma ve yardım faaliyetinin ağırlıklı bölümü halkın oluşturduğu inisiyatif ve örgütlenmeler aracılığıyla gerçekleşir.

Depremin hemen ardından sermaye ve devlet, gecekondu mahallelerinde neticede yoksulların yerinden edilmesi anlamına gelecek bir kentsel yenilenme ve dönüşüm programı başlatır. Ancak gecekondu mahalleleri, özellikle de belli bir mücadele ve özörgütlenme geleneği olan mahalleler, sadece deprem sonrası yardım faaliyetlerini örgütlemekle kalmaz, bu planlara karşı da başarıyla direnir. Halkın yeniden inşa faaliyetinin aktif bir bileşeni olması, demokratik ve halkçı bir yeniden inşa sürecinin işletilmesi için büyük bir mücadele yürütülür.

Bu mücadeleleri incelemiş Harry Cleaver’ın ifadesiyle, “Mexico City’nin çok sayıda yoksul gecekondu mahallesinde yeryüzünün hareketlenmesi, binalardaki tahribatı ve siyasal iktidar yapılarında oluşan çatlakları baskıcı toplumsal ilişkilerden sıyrılmak ve yaşam koşullarını geliştirmek için kullanan halk hareketlerini ateşledi. (…) Depremin yarattığı tehlikeler kadar ortaya çıkan yeni olanaklar da muazzam ölçüde karmaşıktı.”[1]

Aslında her felaket, toplumsal ve siyasal güç ilişkilerinde bir kırılmayı gündeme getiren bir mücadele alanıdır. Dean Spade’in dediği gibi, “felaketler siyasal programlar arasındaki rekabette, çok şeyin kaybedilip kazanabileceği merkezi momentlerdir.”[2] Felaket, Naomi Klein’ın Porto Riko’yu 2017 yılının sonbaharında vuran “Maria” kasırgasının ardından yazdığı gibi bir “ütopyalar mücadelesini” kışkırtır. Bu “ütopyalardan” biri, özelleştirmeler ve deregülasyon yoluyla Porto Riko’yu “Karayiplerin Hong Kong’una” çevirecek bir distopyadır aslında. Diğeriyse gıda ve enerji egemenliğini, kamusal sağlık ve eğitim hakkını esas alan, ABD sömürgeciliğini karşısına alan bir ütopya. “Cennet” için verilen mücadele, felaketin müsebbibi olduğu cehennemde cereyan edecektir.[3]

Felaketler iktidar ve sermayenin gücünü pekiştirebileceği gibi, onların gücüne karşı potansiyel direnişin açığa çıkabileceği zeminleri de yaratabilir. Felaketin normalliği kısa devreye uğratması, kısa bir süre için, daha önce bastırılmış direniş potansiyellerinin açığa çıktığı, daha önce düşünülemez olanın düşünülebilir hale geldiği koşullara yol açar. Bu kısa aralığın değerlendirilmesi, inisiyatifin muktedirlere bırakılmaması hayati önemdedir. Çünkü egemen sınıf için felaketi fırsata çevirecek “şok doktrini” hazırdır, ilk bocalamanın hemen ardından devreye sokulacaktır.

“Şok doktrini”, yani felaketin travmatik etkisinin “normalde” gerçekleştirilmesi mümkün olmayacak değişiklikleri gündeme getirmek için bir fırsata dönüştürülmesi devlet ve sermayenin oldukça mahir olduğu bir alan. OHAL ilanı ve belki de önümüzdeki günlerde seçimin ertelenmesi, saray rejiminin deprem felaketini bir şok doktriniyle karşılamaya hazır olduğunu gösteriyor. Felaketi siyasal güç ilişkilerini kendi lehine değiştirecek radikal adımları atmak için bir fırsata dönüştürmek bu iktidarın zaten deneyimli olduğu bir husus.

Ancak bu tekel pekâlâ kırılabilir, felaket toplumsal ve ekonomik eşitsizlikleri pekiştirmek ya da iktidarın gücünü artırmak için değil, dayanışma, eşitlik ve özgürlüğü güçlendirmek, güçler dengesini aşağıdakiler lehine değiştirmek için bir olanağa dönüştürülebilir. Felaketin yarattığı şok daha önce düşünülemez olanı düşünmeyi mümkün kılıyorsa bunun ille sağa doğru bir radikalleşme anlamını taşıması gerekmiyor. Solun da tıpkı neoliberal sağ gibi kendine has bir “şok doktrini” olması gerektiğini savunan Graham Jones, tepkisel seferberliklerin ötesine geçmek için “solu şoklara hazırlık etrafında hizalayabilir ve böyle anlarda hızlı, geri döndürülemez değişimler gerçekleştirebiliriz” diye yazıyor.[4]

Halkın deprem sonrasında sergilediği teyakkuz, ortaya koyduğu o muazzam dayanışma ve karşılıklı yardımlaşma seferberliği bu yönde çok ciddi imkânların olduğunu gösteriyor. Siyasal iktidar da zaten deprem sonrasında oluşan bu toplumsal dayanışma seferberliğinin kendisini siyaseten sıkıştıracağını, hareket alanını daraltıp savunmada kalmaya zorlayacağını biliyor. O nedenle de bu dayanışmacı mobilizasyonu zapturapt altına almanın yollarını arıyor.

Felaketin ortasında biz istesek de istemesek de bir mücadele şekilleniyor. İktidar bizi, felaketin bizzat nedeni olmuş o eski normale geri taşımak istiyor, her şeyin yıkıldığı bir anda hiçbir şey değişmesin istiyor. Bu yüzden bugün muktedirlerin şok doktrinine karşı halkın şok doktriniyle çıkmak hepimiz için kelimenin gerçek anlamıyla yaşamsal bir görev.

[1] Harry Cleaver, “The uses of an earthquake”, [[https://href.li/?https://libcom.org/library/uses-of-earthquake-cleaver][https://libcom.org/library/uses-of-earthquake-cleaver]]
[2] Dean Spade, <em>Mutual Aid Building Solidarity During This Crisis(and the Next)</em>, Verso, Londra, 2022, s. 30.
[3] Naomi Klein, <em>The Battle for Paradise Puerto Rico Takes on Disaster Capitalists</em>, Haymarket Books, Chicago, 2018.
[4] Graham Jones, <em>The Shock Doctrine of the Left</em>, Polity Press, Cambridge, 2018, s.12-3.
