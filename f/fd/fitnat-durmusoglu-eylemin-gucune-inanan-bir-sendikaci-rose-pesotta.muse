#title Eylemin gücüne inanan bir sendikacı: Rose Pesotta
#author Fitnat Durmuşoğlu
#SORTtopics anarko sendikalizm, anarşist, anarşist feminizm, işçi mücadelesi, kadın mücadelesi, rose pesotta, tarih, abd
#date 21.09.2021
#source 18.05.2022 tarihinde şuradan alındı: [[https://www.kadinisci.org/2021/09/21/eylemin-gucune-inanan-bir-sendikaci-rose-pesotta/][kadinisci.org]]
#lang tr
#pubdate 2022-05-18T20:46:47


<quote>

Pesotta, yaşamı boyunca, merkezi otoriteye karşı işçilerin özgür birliğini savundu. Anarşist ideallerin tutkulu bir savunucusuydu. Kadınlara asgari ücret için patronlarla, sendikalarda kadınlara eşit temsil hakkı için de sendikacı erkeklerle mücadele etti.

</quote>

Rose Pesotta 1896 yılında Ukrayna’nın demiryolu kasabası olan Derazhnya’da orta sınıf bir ailenin sekiz çocuğundan ikincisi olarak dünyaya geldi. Ailesi adını Rakhel koydu. Babası Itsaak Peisoty bir tahıl tüccarıydı ve annesi Masya aile işinde aktifti. Pesotta, Rus müfredatı, Yahudi tarihi ve İbranice dersleri öğrenmek için Rosalia Davidovna’nın kızlar okuluna gitti. Okulda iki yıl geçirdikten sonra, kardeşlerine bakması için evde ona ihtiyaç duyuldu ve okulu bıraktı, evde özel ders aldı. Ablasıyla birlikte illegal anarşist siyasi eğitimlere katıldı. Bakunin, Kropotkin ve Proudhon gibi anarşist düşünürlerin teorilerini okudu ve onlardan etkilendi.

Ailesinin onu evlendirme planı yapması üzerine Amerika’daki kız kardeşinin yanına gitme kararı aldı. 1913 yılında New York’a gelen Rakhel Peisoty’un adı Ellis Adası’ndaki göçmenlik bürolarının karmaşasında Rose Pesotta oldu. Kız kardeşi, ona <em>International Ladies Garment Workers Union’un</em> <strong>(</strong>Uluslararası Kadın Konfeksiyon İşçileri Sendikası) (ILGWU) örgütlü olduğu bir gömlek fabrikasında iş buldu. 1914 yılında Pesotta, kadınların önderlik ettiği aktivizm ve terzilerin eğitimine yoğun bir şekilde dâhil olan ILGWU Local 25’e katıldı. Çok geçmeden sendikanın organizatörü oldu. Birçok greve katıldı. Ayrıca, anarşist Emma Goldman tarafından verilen bir konuşmayı dinlendikten sonra New York’un Yahudi işçileri arasında anarşist siyasete dâhil oldu.

*** Kadın işçi sendikasında örgütçü

1915 yılında ILGWU’da ilk eğitim departmanının oluşturmasına yardımcı oldu. Pesotta, 1919 yılında Palmer baskınlarında tutuklandı ve sınır dışı edilmek üzere tutuldu. Nişanlısı Sibirya’ya sınır dışı edildi ve ondan bir daha haber alamadı. Kendisi Amerika vatandaşı oldu. 1920 yılında Local 25’in yönetim kuruluna seçildi. 1922 yılında eğitimine <em>Bryn Mawr School for Workers in Industry</em>, <em>Rand School of Social Science</em> ve <em>Wisconsin Summer School for Workers’</em>da devam etti. İngilizceyi gece okulunda öğrendi. 1926 yılında, işçi aktivistleri eğiten <em>Brookwood Labor College</em>’den (Brookwood İşçi Koleji) mezun oldu. 1927 yılında Bir soygun sırasında işlenen cinayetle ilgili suçlanan iki İtalyan anarşist Nicola Sacco ve Bartolomeo Vanzetti davasında yerel halk adına aktif olarak yer aldı. Sacco ve Vanzetti davasını araştırdı. Pesotta onlar için yapılan protesto ve gösterilere katıldı. Aynı yıl Sacco ve Vanzetti’nin idamını protesto ettiği için tutuklandı.

Pesotta, sendika ve anarşist yayınlar için Yidiş ve İngilizce dilinde düzenli olarak yazdı. Anna Sosnovsky, Fanny Breslaw ve Clara Rothberg Larsen ile birlikte, 1923 ve 1927 yılları arasında <em>Der Yunyon Arbeter</em>‘i (<em>The Union Worker</em>) (Sendika İşçisi) yayınladı. 1925 yılından 1929 yılına kadar, anarşist yayın <em>Road to Freedom’ın</em> genel sekreteri olarak çalıştı. Etkili bir işçi örgütleyicisi olan Pesotta, 1930’lı yılları Atlantic City, Buffalo, Milwaukee, San Francisco ve Seattle’daki kampanyaları örgütleyerek geçirdi.

New York dışındaki işçileri örgütlemek için düzenli olarak seyahat etti. 1933 yılında ILGWU, hazır giyim işçilerini örgütlemesi için onu Los Angeles’a gönderdi. 1933 Los Angeles Konfeksiyon İşçileri Grevi’ni örgütleyen Meksikalı göçmen hazır giyim işçileri ile çalıştı.

*** Kadınlar için asgari ücret

1933 yılında Los Angeles hazır giyim endüstrisi, yarısı şehir merkezinde olmak üzere tahmini 200 küçük atölyeye dağılmıştı ve yaklaşık 7.500 işçi istihdam ediliyordu. Bu işçilerin yaklaşık yüzde75’ini Latinler, geri kalanı ise İtalyan, Rus ve Amerika’lılar oluşturuyordu. Kadın terzilerin yaklaşık yarısı haftada beş dolardan az kazanıyor, bu da Kaliforniya’da kadın işçiler için haftada 16 dolar olarak belirlenen asgari ücret ortalamasının ve endüstride standartları belirleyen <em>Ulusal Endüstriyel İyileştirme Yasası’</em>nın (NRA) Kıyafet açık bir ihlaliydi.

Örgütlenmeye çalışan işçiler, işverenler tarafından sürekli işten atılıyor ve kara listeye alınıyordu. Çoğunluğu beyaz erkeklerden oluşan ILGWU’nun yerel liderliği, kadın terzileri örgütlemekle ilgilenmedi, kadınların ailelerini genişletmek için sektörü terk ettiğini ya da ilk etapta çalışmaması gerektiğini düşünüyordu. Ancak Rose Pesotta, bu tavrı kabul etmeyi reddetti. ILGWU International’ın onayı ile yeni bir Local 96 için çalışmaya başladı. İki dilli bir radyo programı ve <em>The Organizatör</em> adlı haftalık bir gazete aracılığıyla Latin topluluğuna ulaştı. 28 Eylül’e kadar Pesotta, Los Angeles terzilerini sendikanın tanınması ve koşulların iyileştirilmesi için grev yapmaya hazır oldukları bir noktaya getirdi. Terziler, sendikanın tanınması, haftada 35 saat çalışma, NRA yasasına göre asgari ücret, eve iş götürmeme ve zaman çizelgesi düzenlemesi gibi taleplerini belirlemek için bir toplantı yaptı. İşçiler ayrıca gelecekteki anlaşmazlıkların tahkim yoluyla çözülmesini istedi. İşverenler, sendikayı tanımayı reddederek, işçilerin toplu iş sözleşmesine sahip olduklarını ve bir şirket sendikası aracılığıyla temsil edildiğini belirterek yanıt verdi. İşverenin direnişine yanıt olarak, işçiler 12 Ekim 1933 yılında greve gitti. İlk günün sonunda 4.000’den fazla işçi greve katılmıştı.

Tüccar ve İmalatçılar Derneği, Los Angeles Polis Departmanından destek talep ederek greve tepki gösterdi. Polis, grevcileri tutuklamaya başladı. Mahkemeler, çoğu vahşetin şirket adamları tarafından yapıldığı gerçeğini göz ardı ederek sendikaya karşı ihtiyati tedbir kararı verdi. Grev sırasında 50’ye yakın kadın gözcü, barışı bozmak ve küfür etmekten darp etmeye kadar çeşitli suçlamalarla tutuklandı. İşverenler polisin yeterince ileri gittiğine inanmadılar. Grev sırasında, üreticilerden birinin ajanının bir polis memuruna rüşvet vermeye çalıştığı ve fabrikaların önündeki tüm gözcüleri kilitlerse 800 dolar teklif ettiği ortaya çıktı. Ajan ve işvereni rüşvetle suçlandı ve tutuklandı. 6 Kasım’da grev, hakemli bir anlaşmayla sona erdi. Terziler haftada otuz beş saatlik ücret almaya başladılar. Haftalık ücretler kurallara uygun olacaktı. Ancak sendika, işçilere şikâyetleri konusunda yalnızca atanmış hakemler aracılığıyla yardımcı olabildi, işverenlerle sözleşmeleri yoktu. Sendika isteksizce şartları kabul etti, ancak mücadeleye devam etme kararı aldı.

*** Sendika başkan yardımcısı oldu

Pesotta 1934 yılında ILGWU’na başkan yardımcısı olarak seçildi. 1934 ve 1944 yılları arasında Pesotta, Amerika Birleşik Devletleri’ndeki en başarılı örgütçülerden biri olarak görülüyordu. Sendika politikasını Porto Riko, Detroit, Montreal, Cleveland, Buffalo, Boston, Salt Lake City ve Los Angeles’taki işçilere taşıdı.

Pesotta, ILGWU adına, yeni kurulan <em>Congress of Industrial Organizations</em>’a (Sanayi Örgütleri Kongresi)(CIO) katıldı. Şubat 1936 yılında, kauçuk işçilerinin Ohio, Akron’daki Goodyear oturma grevlerinde, <em>United Auto Workers</em>’ın (Birleşik Otomobil İşçileri) Aralık 1936 yılından Ocak 1937 yılına kadar Michigan Flint’daki General Motors tesisindeki oturma grevlerinde öncü bir rol oynadı. Müzakerelere katıldı ve grevcileri destekledi. Bu grev sırasında sendika karşıtları tarafından ciddi bir şekilde dövüldü ve ömür boyu işitme kaybı yaşadı. Grev sona erdiğinde, Pesotta, işçileri dışarı çıkaran CIO ve UAW liderlerinden biriydi.

Los Angeles’ta bir spor giyim üreticisine karşı yapılan grev sırasında, genç kadın işçilerden bazılarına gece elbiseleri giydirdi ve pankartlarıyla gösterişli bir otelin önünde yürümelerini sağladı. Basının orada olduğunu biliyordu. Bu eylem güzellik kraliçelerini sömüren biri olarak görülmek istemeyen işverenin tutumunu yumuşattı. Grev, ILGWU’nun zaferiyle sonuçlandı.

Pesotta, Kasım 1939 yılında sendikanın uzun süredir başkanı olan Dubinsky’e <em>“Organizasyonumuzdaki işlerin durumu ve saflarımızdaki mevcut ahlaki parçalanma”</em> başlıklı bir mektup kaleme alarak bilgilendirme yaptı. David Dubinsky’nin yönetim kurulundaki tek kadın olmakla ilgili şikâyetlerine yanıt vermemesi üzerine hayal kırıklığına uğradı. ILGWU liderliğinin 305.000 üyeliğinin en az yüzde 85’ini oluşturan kadınlara yönelik cinsiyetçi davranışını, kadın tabanına herhangi bir yükümlülük vermeyen politikalarını kınadı. Dubinsky ve diğer erkek ILGWU liderleri, kadınları ikincil olarak görüyorlardı.

*** Savaş düşüncelerini değiştirdi

ILGWU liderliğinin cinsiyetçi bakış açısının değişme ihtimalinin olmadığını düşünerek sendika yönetiminden, kendisine yetki verilmesini istedi. Çünkü iktidarının hakiki olmaktan çok sembolik olarak görüldüğünü fark etmişti. 1944 yılında sendikanın Genel Yönetim Kurulu üyeliğinden istifa etti<em>. “Bir kadın başkan yardımcısı, şu anda Enternasyonal’in 305.000 kişilik üyeliğinin yüzde 85’ini oluşturan kadınları yeterince temsil edemez”</em> şeklinde kısa bir eleştirel açıklama yaptı. Manhattan’daki bir giyim fabrikasında dikiş işine geri döndü.

Pesotta’nın hayatı, II. Dünya Savaşı’nın başlamasıyla farklı bir hal aldı. Akrabalarının birçoğu da dâhil olmak üzere Avrupa Yahudileri’nin katledildiği haberiyle sarsıldı. 1945 yılında <em>B’nai B’rith</em> <em>İftiraya Karşı Mücadele Birliği</em>‘ne katıldı. Birlik için bağış toplama görevini kabul etti. Ülke çapında anti-Semitizm ve ırkçılığa karşı konuşmalar yaptı. Savaştan sonra Polonya’ya gitti ve hayatta kalanlarla tanıştığı Majdenek toplama kampını ziyaret etti. Onları ABD’ye yerleştirmek için yorulmadan çalıştı. Holokost’un etkisiyle Siyonist oldu. İsrail’in işçi örgütü <em>Histadrut</em> için Amerikan Sendika Konseyi’nin Midwest Direktörü oldu ve o andan itibaren İşçi Siyonist hareketiyle özdeşleşti.

Hayatı boyunca, Ukrayna’daki özel bir öğretmenden öğrendiği derse sadık kaldı: “<em>Önce halk gelir. Toplumda değişiklikler meydana getiren onların eylemleridir.”</em>

Pesotta’nın kitap çalışmaları oldu. Devrimci bir Yeshiva öğrencisi hakkında bir roman ve <em>Bread</em> <em>Upon the Waters</em> (1944) ve <em>Days of Our Lives</em> (1958) adlı iki anı kitabı yayınladı. <em></em> İkinci cildi, Naziler tarafından yok edilen Yahudi medeniyetinin anısına adadı. 1965 sonbaharında Pesotta’ya dalak kanseri teşhisi kondu. Sessizce işinden istifa etti ve Miami’ye “güneşte iyileşmek” için gitti. 4 Aralık 1965 yılında Miami’deki bir hastanede tek başına öldü.

*** Kaynak

 - https://libcom.org/history/pesotta-rose-1896-1965

 - Rose Pesotta (1896-1965) – Find A Grave Memorial

 - https://www.findagrave.com

 - Pesotta, Rose (1896–1965) | Encyclopedia.com

 - https://www.encyclopedia.com

 - https://jwa.org/encyclopedia/article/pesotta-rose

 - Rose Pesotta | Jewish Women’s Archive

 - https://ufcw324.org/rose-pesotta-knowing-a-womans-place/

 - Woman’s History Month: Rose Pesotta – UFCW Local 227

 - https://www.ufcw227.org

 - Rose Pesotta – Wikipedia



