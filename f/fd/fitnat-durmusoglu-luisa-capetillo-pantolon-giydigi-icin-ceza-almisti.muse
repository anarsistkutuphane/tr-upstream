#title Luisa Capetillo: Pantolon giydiği için ceza almıştı
#author Fitnat Durmuşoğlu
#SORTtopics anarko sendikalizm, anarşist, anarşist feminizm, işçi mücadelesi, kadın mücadelesi, luisa capetillo, tarih
#source 18.05.2022 tarihinde şuradan alındı: [[https://www.kadinisci.org/2021/08/03/luisa-capetillo-pantolon-giydigi-icin-ceza-almisti/][kadinisci.org]]
#lang tr
#date 03.08.2021
#pubdate 2022-05-18T20:38:48


<quote>

Porto Rikolu sendika lideri, feminist, anarşist ve gazeteci. Tütün işçileri ile çalıştı, kadınların evlilik bağı olmaksızın kimi seveceklerini özgürce seçebileceklerini savundu. Kendi de öyle yaptı. Ülke tarihindeki en önemli grevleri örgütledi.

</quote>

Luisa, 28 Ekim 1879 yılında Porto Riko, Arecibo’da doğdu. Annesi Luisa Margarita Perone Fransa’dan, babası Luis Capetillo Echevarria İspanya’dan Porto Riko’ya geldi. Anne varlıklı ailelerin çocukları için mürebbiye olarak çalışmak istiyordu ancak ev hizmetlerinde iş buldu. Baba hayatını çeşitli mesleklerde kazanan bir işçiydi. Luisa’nın ebeveynleri aynı felsefi ve politik ideolojiyi paylaşıyordu. Birlikte yaşadılar tek çocukları olan Luisa’yı büyüttüler ve hiç evlenmediler. Resmi bir eğitim almamış olmasına rağmen, evde eğitim gördü. Anne ve babasından özellikle felsefe ve politika konusunda eğitim aldı. Fransızca bilgisi Fransız yazarların eserlerini okumasını sağladı. Gustave Flaubert, Emile Zola ve George Sand gibi Avrupalı ​​yazarların yazılarından etkilendi. Daha sonra Peter Kropotkin, Georges Sorel, Errico Malatesta ve Leo Tolstoy gibi düşünürler onun siyasi görüşlerini din, anarşizm ve sosyal adalet üzerine yoğunlaştırmasını sağladı.

*** İşçilere kitap okuyordu

19 yaşında Arecibo’nun önde gelen ailelerinden bir genç olan Manuel Ledesma ile tanıştı. Sevgili oldular ve 1898 yılında ilk çocukları Manuela, iki yıl sonra Gregorio doğdu. Luisa ve Manuel hiç evlenmediler ve üç yıl sonra birliktelikleri sona erdi. Luisa, tekstil endüstrisinde bir işe girdi, çocuklara annesi bakıyordu. Daha sonra <em>American Tobacco Company</em>‘e ait bir puro fabrikasında okuyucu olarak bir iş buldu. Tütün fabrikalarında yaprak sıyırıcı ve puro silindiricisi olarak çalışan kadın ve erkek işçilere yükseltilmiş bir platformdan seslenen Luisa Capetillo ulusal ve uluslararası gazeteler ve romanlar okuyordu. Çoğu zaman, belirli edebi eserlerin veya politik denemelerin belirli pasajları, işçilerin onları hafızaya alabilmesi için birkaç kez tekrarlanırdı. Tütün fabrikalarında, çalışmayı kesintiye uğratmadan belirli dersler üzerinde açık tartışma ve münazara yapmak da bir gelenekti. İşçiler ayrıca her gün hangi eserlerin okunacağını tartışır ve oylardı.

Tütün fabrikaları, işçilerin çalıştığı önemli kültürel mekânlardı. Çoğu işçiler alternatif bir eğitim aldı ve Porto Riko’daki ilk işçi sınıfı aydınlarını oluşturdular. Luisa’nın fabrikada okuyucu olması işçi lideri olarak gelişmesinde önemli rol oynadı. Burada sendikal hareketle tanıştı. <em>La Federacion Libre de Trabajadores</em>‘e (Özgür İşçi Federasyonu) (FLT) bağlı olan <em>La</em> <em>Federacion de Torcedores de Tabaco</em> (Tütün Silindirleri Federasyonu) ile ilk temasını tütün fabrikasında yaptı ve daha sonra <em>La</em> <em>Federacion Libre de Trabajadores’e</em> katıldı<em>.</em>

Capetillo’nun işçi hareketine katılımı, 1905 yılında FLT tarafından yönetilen çiftlik işçilerinin grevi sırasında başladı. Grevde önemli bir rol oynadı ve kısa sürede sendikada lider oldu. İşçileri eğitmek ve örgütlemek için Porto Riko’da seyahat ederken FLT gazetesi <em>Unión Obrera</em>’yı sattı ve gazete için yazılar yazdı. <em>“İşçiler, haysiyet ve eşitlik haklarını savunmak için bir bayrak altında birleşmelidir “</em>dedi. O kadar etkili bir örgütleyiciydi ki memleketi Arecibo, ülkedeki en fazla sendikalaşan bölge haline geldi. FLT’nin üyesi olarak, adanın her yerinde bir gazeteci ve sendika organizatörü olarak yorulmadan çalıştı.

*** Çocuklarını vaftiz ettirmedi

1907 yılında sendikalar ve işçi kooperatifleri aracılığıyla oluşacak yeni bir eşitlikçi toplum tasavvur eden makalelerinin derlemesi olan <em>“</em><em>Ensayos Libertarios”</em> u (Özgürlükçü denemeler) yayınladı. Luisa çocukken vaftiz edilmiş olmasına rağmen, bir yetişkin olarak Katolik Kilisesi’ni reddetti ve rahipleri ikiyüzlü olarak nitelendirdi. <strong>Ensayos Libertarios’te</strong> <em>“Çocuklarınızı vaftiz etmeyin. Bir düşünün. Bu kadar gerekli olsaydı, inanmayan milyonlarca insanın olması aptallık olurdu”</em> diye yazdı. Bir anne olarak çocuklarını hiçbir zaman vaftiz ettirmedi ve kızına yazdığı bir mektupta <em>“Sana dua etmeyi öğretmedim, bu hissetmen gereken bir şey. Herhangi bir dini ayinle vaftiz edilmedin”</em> dedi. Organize dini, insanlar için bir hapishane biçimi olarak gördü. Ayrıca, kadınların çocuk yetiştirmekten sorumlu olmaları durumunda, dürüst vatandaşlar oluşturmak için gerekli entelektüel araçlarla donatılmaları gerektiğini savundu.

<em>“Influencias de las ideas modernas”</em> (Modern Fikirlerin Etkisi) adlı bir oyun yazdı. Luisa, kadınları hakları konusunda harekete geçirmek amacıyla Porto Riko’da seyahat etti. 1908 yılında FLT nin 5. Kongresinde, kadınların oy kullanma hakkı için mücadele edecek bir politikayı savunmaları konusunda ısrar etti. Sadece zengin ya da okuryazar değil, tüm kadınların erkeklerle aynı oy hakkına sahip olması gerektiğini savundu. Porto Riko’nun zengin oy hakkı savunucuları, “eğitimli” kadınlara oy hakkı vermeye odaklanırken, Luisa, kadınlar ve erkekler için evrensel oy hakkı olması gerektiğini savundu.

1909 yılında FLT, <em>“la Cruzada del Ideal”</em> (İdeal Haçlı Seferi) adını verdikleri iddialı bir örgütlenme kampanyasına girişti. Luisa işçi sınıfının diğer liderleriyle birlikte işçileri sendikalaşma konusunda eğitmek için ülkeyi gezdi. Porto Riko’daki ilk süfrajet olarak kabul edildi. Faaliyetleri, işçi hareketine ve kadın hakları için kampanya yürüttüğü sendikaya odaklandı. Çok sayıda işçi, talepleri ve faaliyetleriyle sendikaya üye oldu. Ona göre proleter bir kadın hareketi, genel oy hakkı, daha iyi çalışma koşulları ve daha yüksek ücretler için mücadele ederken örgütlü işçi hareketinin ayrılmaz bir parçası olmalıydı. Sendikalar yoksul, çalışan kadınların adalet ve eşitlik elde etmelerinin aracı olmalıydı.

*** Kadınlar eşitlik için ileri

1910 yılında, federasyon gazetesi için muhabir oldu. Kadın sorunlarını ele alan <em>La Mujer</em> gazetesini kurdu. Aynı yıl, <em>La Humanidad En El Futuro</em> (Gelecekte İnsanlık ) kitabında ütopik bir toplumu ayrıntılı olarak ve geniş bir perspektifle ele aldı. Kilisenin ve devletin gücünü, evlilik kurumunu ve özel mülkiyeti tartıştı. 1911 yılında Luisa Capetillo üçüncü çocuğu Luis Capetillo’yu doğurdu. <em>Mi Opinion</em> (Benim Görüşüm<em>)</em> adlı kitabını yayınladı. Bu çalışmasında kadınları toplumsal eşitlik için savaşmaya çağırdı. “<em>Ey kadın! Adalet tohumunu yaymak için yetenekli ve istekli olun; tereddüt etmeyin, üzülmeyin, kaçmayın, ilerleyin! Ve gelecek nesillerin yararına, toplumsal eşitliğin inşası için ilk taşı sakin ama sağlam bir şekilde, size ait olan tüm haklarla, aşağı bakmadan yerleştirin, çünkü artık eskisi gibi maddi veya entelektüel köle değilsiniz”</em> dedi.

1911 yılında <em>Mi</em> <em>Opinión Sobre las Libertades, Derechos y Deberes de la Muj’u</em> (Kadınların Özgürlükleri, Hakları ve Görevleri Üzerine Benim Görüşüm) yayınladı. Bu çalışmasında, toplumda kadınların durumunu analiz etti, kadınların ezilmesi ve kölelik olarak gördüklerine odaklandı ve eğitimin özgürlüğün anahtarı olduğunu vurguladı. Varlıklı kadınların çalışan kadınları etkileyen sorunlardan hiçbir zaman etkilenmediğine, bunun temel nedeni ailelerini desteklemek için ev dışında iş bulmak zorunda olmadıklarına ve çocuklarına bakması için her zaman başka bir kadın tuttuklarına dikkat çekti. Bu çalışma feminist bir araştırma olarak kabul edildi. Kendisini feminist olarak görse de, o dönemde ortaya çıkan feminist örgütlerin hiçbirine katılmadı.

*** Şeker kamışı grevinde en öndeydi

1912-1916 yılları arasında şeker kamışı işçilerini örgütleyen <em>la Federación Anarquista de Cuba</em> (Küba Anarşistleri Federasyonu) ile çalışmak için Küba’ya gitti. Orada kooperatiflerin nasıl kurulacağı konusunda dersler verdi. 1913 yılında Florida Tampa’ya taşındı, sendika işçileriyle birlikte çalışmalar yaptı. <em>Mi Opinion</em>‘un ikinci baskısını yayınladı. Halka açık yerlerde <em>pantalone</em> (pantolon) giyerek “kamuoyunu rahatsız etmek” ten Temmuz 1915’te Havana’da tutuklandı ve sınır dışı edildi. Mahkemeye, “erkek kıyafeti” giymesini yasaklayan bir yasa olmadığını savunarak itiraz etti. Hâkim kabul etti ve suçlamalar düştü. Bu haberler Küba ve Porto Riko’daki tüm büyük gazetelerde yayınlandı.

Luisa, 1916 yılında Porto Riko’ya geri döndü. 32 belediyede 40 binden fazla işçinin katıldığı beş ay süren Şeker Kamışı Grevine katıldı, grev sonucu ortalama maaş artışı yüzde 13 oldu. 1916-1918 dönemi, Porto Riko tarihinde grev faaliyetleri açısından en yoğun dönemdi. 1916 yılında <em>“Influencias de las idea modernas”</em> (Modern fikirlerin etkisi) çalışmasını yayınladı.

Luisa, 1919 yılında Porto Riko’daki kadınlardan pantolon giymelerini istedi. O zamanlar bu, “suç” anlamına geliyordu ve tekrar tutuklandı. Kadınların istedikleri gibi giyinme, kendilerini özgürce ifade etmesi konusunda ısrarcıydı. Aynı yıl, diğer işçi aktivistlerle birlikte, Porto Riko Yasama Meclisinde asgari ücret yasasının çıkarılmasına yardım etti. New York’ta sosyalist tartışma grupları için bir buluşma yeri haline gelen vejetaryen restoran açtı. Grevci işçileri desteklemek için Dominik Cumhuriyeti’ne gitti. Burada mitinglere, grevlere ve sendika ile ilgili faaliyetlere katıldı. ABD’li yetkililer tarafından işçi ve feminist örgütlere katılımı nedeniyle Dominik Cumhuriyeti’nden sınır dışı edildi. Yolda, tarım işçilerinin çocuklarına Pan-Karayip okul sistemi kurmak için birçok farklı grup ve aktivistle bağlantı kurdu.

Bu süre zarfında, <em>Partido Socialista</em> (Sosyalist Parti) FLT’nin siyasi kolu olarak kuruldu. Bir anarşist olarak ideallerine aykırı olmasına rağmen, Luisa Capetillo Sosyalist Parti için siyasi bir kampanyaya katıldı.

10 Ekim 1922 yılında Porto Riko, Rio Piedras’ta tüberkülozdan öldü. Arecibo Belediye Mezarlığı’na gömüldü.

*** Kaynak

 - https://libcom.org/history/biography-luisa-capetillo

 - https://prmeencanta.com/2021.03.08/luisa-capetillo-una-mujer-con-pantalones/

 - Luisa Capetillo: Puerto Rican Changemaker

 - https://blogs. loc. gov

 - Ekmek ve gül- Porto Riko’lu bir işçi ve sendikacı-luisa capetillo

 - Çatlakzemin.com-28 Ekim 1879 kamusal alanda pantolon giydiği için tutuklanan feminist işçi lideri- luisa capetillo

 - https://www.valorycambio.org/luisacapetillo

 - https://www.peoplesworld.org/article/today-in-labor-history-puerto-rican-labor-organizer-and-feminist-luisa-capetillo-born/

 - NAWSA Suffragists Biyografik Veritabanı, 1890-1920 Luisa Capetillo’nun Biyografisi, 1879-1922 Nancy Hewitt, Emekli Profesör, Rutgers Üniversitesi

 - https://biography.yourdictionary.com Luisa Capetillo

 - Biographical Sketch of Luisa Capetillo https://documents.alexanderstreet.com



