#title Melankoli isyancıdır
#author Fazıl Tar
#SORTtopics isyancı
#date 15.02.2015
#source 21.07.2022 tarihinde şuradan alındı: [[https://hayattabirleri.blogspot.com/2015/02/melankoli-isyancdr.html][hayattabirleri.blogspot.com]]
#lang tr
#pubdate 2022-07-21T20:40:37



Olağanüstü yaşamlarımız yok. Normalin, sıradanlığın tuzağına çoktan beri düşmüşüz. Anlayıştan uzak, birbirinden kopmuş ve her geçen gün biraz daha şeyleşen değersizleşen yaşamlarımıza inancımızı ve umudumuzu yitirdik. Gözlerimiz yaşararak, tüylerimiz dikenleşerek sarılmıyoruz artık birbirimize. Yakınlarda bir yerlerde bize dair olup bitenlere yüreğimizi çoktan kapattık. Teşekkür etmek, takdir etmek, özür dilemek, empati kurmak, anlamaya çalışmak hayatlarımızdan çıkmış. Sevdiğimiz insanlara bunu tereddüt etmeden söyleyebilmek, dost edinmek, paylaşmanın koşulsuz sadakatini unutmuşuz. Şikayet etmek, söylenmek, günü birlik hazların ve avunmaların peşinde kendimize yabancılaşmak en iyi yaptığımız şey olmuş. Yaşadığımız sinkaflı gerçekliğimizi izlediğimiz filmlerle, siber uzayın duvarlarıyla, makinelerin basitleştirdiği sözde hayatlarımıza acımasızca uyguluyoruz. Artık estetize edilmiş planlı yaşamlardan başka bir amaç için var olabileceğimizi tamamen unuttuk. İçimizde bir yerde ormanı, denizi, toprağı özleyen yanlarımız rasyonalize edilmiş bilincin esiri artık. Süperegolarımızı def etmenin yolunun bizden geçtiği tahayyül edemiyoruz bu çağda.

Sığındığımız bir yerde hırslardan, kârdan, egolardan, kötülükten ve düşmanlıktan uzak bir isyan içimizi güzelleştirmeli artık. Yalnızlıktan ve ses çıkarmaktan çekinmeyen itirazlar büyümeli gürültüyle. Bizleri kuşatan bu acizlik duygularının kaynağı otoriter şiddeti parçalamalıyız. Aşkı hediye paketlerine, sevgiyi evlilik cüzdanlarına, dostluğu kredi kartlarına tevil eden modernizmin aynalarını kırmalıyız. Bizi okulla eğiten, cezaeviyle tehdit eden, dinle korkutan, parayla esir eden, fabrikayla hasta eden uygarlığın tüm kurumlarını yok etmeliyiz.

Kozmosun ücra bir köşesinde yıldızlarla oynayaşan, gezegenlerle sevişen yaşam düşmanlarına karşı özgürlüğü ve aşkı savunan anarşistleriz biz.

<strong>Tüm devletlerle ve yarattığı örgütlü şiddet yok olana dek isyan, aşk, anarşi!</strong>

<br>



