#title Eylem
#author Carlo Cafiero
#SORTtopics doğrudan eylem, eylem, çeviri, devrim
#date 25.12.1880
#source 30.12.2021 tarihinde şuradan alındı: [[https://www.yeryuzupostasi.org/2021/12/30/eylem-carlo-cafiero/][www.yeryuzupostasi.org]]
#lang tr
#pubdate 2021-12-30T19:56:36
#notes Çeviri: İbrahim İpek, [[https://www.yeryuzupostasi.org][Yeryüzü Postası]] <br> Çevirenin Notu: L’Action, 25 Aralık 1880’de Le Révolté’de yayınlandı. Ara sıra Fransızca olarak yeniden basıldı ve diğer dillere çevrildi, ancak genellikle yanlış bir şekilde Kropotkin’e atfedildi. <br> İngilizce Aslı: [[http://www.revoltlib.com/anarchism/action-carlo-cafiero/][Action]]



Eğitim almış insanların sanki tüm dünyanın yükünü taşımak zorundaymış gibi omuz silkmeleri için hiçbir neden yok: devrimci düşünceyi bulanlar onlar değillerdi. Çoğun zaman bilinçsiz çabalarıyla zalimlerin boyunduruğundan sıyrılma girişimleriyle eğitim almış insanların dikkatini toplumsal ahlaka çekenler ezilen insanlardı; ve bir süre sonra birkaç ender düşünür bunu yetersiz bulmayı başardı ve daha sonra ise birileri bunu tamamen bunun tamamen yanlış olduğu konusunda hemfikir oldular.

Evet, eğitim almış bu insanların kafasındaki düşüncelerin oluşmasına neden olan, insanların akıttığı kandır. Carlo Pisacane siyasi vasiyetinde “idealler eylemlerden doğar, tersi olmaz” dedi ve haklıydı. Devrim kadar ilerleme kaydeden de insanlardır: aynı sürecin yapıcı ve yıkıcı yönleri. Evrensel üretimi sürdürmek için her gün feda edilen halktır ve insan kaderini aydınlatan meşaleyi kanlarıyla besleyen yine halktır.

İnsanlığın ıstırapları kitabını [gerçekliğini] dikkatle inceleyen bir düşünür, yaygın özlemin formülünü tanımladığında -dünyanın her yerindeki muhafazakarlar ve her türden gericiler, yüksek sesle bağırmaya başlarlar: “Bu bir skandal!”

Evet, bu bir skandal: ve skandallara ihtiyacımız var; çünkü devrimci fikir, yolunu skandalın gücüyle yapar. Proudhon, “Mülkiyet hırsızlıktır!” diye haykırdığında ne büyük bir skandala yol açmıştı. Ama bugün, kapitalistin hırsızlar arasında en kötüsü, en alçak olanı olduğunu düşünmeyen hiçbir sağduyulu ya da duyarlı insan yoktur; bundan daha fazlası -onun tek gerçek hırsız olduğudur. En korkunç işkence aleti olan açlıkla donanmış olarak kurbanına bir an için değil, bir ömür boyu işkence eder: sadece kurbanına değil, aynı zamanda elinde tuttuğu adamın karısına ve çocuklarına da işkence eder. Hırsız özgürlüğünü ve çoğu zaman yaşamını riske atar, ama kapitalist, gerçek hırsız, hiçbir şeyi riske etmez ve çaldığında işçinin servetinin yalnızca bir kısmını değil bütününü alır.

Ancak teorik bir formül bulmak yeterli değil. Eylemin devrimci düşünceyi doğurduğu gibi, devrimci düşüncenin uygulamaya konulması da yine eylemle olur.

Enternayonal’in ilk kongresinde, Fransız proletaryasından sadece birkaç işçi kolektif mülkiyet düşüncesini onaylıyordu. Devrimci düşünceyi hayata geçirmek, yaymak ve bizi Lahey Kongresi’ne getirmek için Komün’ün kışkırtıcısı -halkı coşkulandıran- olan 48 Fransız temsilci işçiydi, ki onlar özgür komünizmi hedef olarak tanıdılar. Yine de, ciddiyet ve bilgelik dolu bazı otoriter dogmacıların, Komün’ün en feci tepkilere yol açarak sosyalist hareketi [kötü yönde] kontrol ettiğini [ve tanıttığını] yalnızca birkaç yıl önce tekrarladıklarını hala hatırlıyoruz. Gerçekler, sosyalistler arasında iyi bilinen “sonuç siyasetini” yaymaya çalışan bu “bilimsel sosyalistlerin” (çoğu bilimden habersiz) görüşlerinin doğruluğunu göstermiştir.

Bu yüzden gerekli olan eylemdir, eylem ve tekrar eylem. Eylemde bulunurken, aynı zamanda hem teori hem de pratik için çalışıyoruz, çünkü fikirleri ortaya çıkaran ve aynı zamanda onları dünyaya yayma sorumluluğu olan eylemdir.

Fakat ne tür bir faaliyet sürdürmeliyiz?

Parlamentoya, hatta belediye meclislerine bizim adımıza başkalarını göndermeli miyiz?

Hayır, bin kere hayır! Burjuvazinin entrikalarıyla hiçbir ilgimiz yok. Onların zulmüne katılmak istemediğimiz sürece, zalimlerin oyunlarına karışmaya ihtiyacımız yok. “Parlamentoya gitmek, müzakere etmektir; müzakere etmek, barış yapmaktır”, demişti, ondan sonra pek çok müzakere yapan eski bir Alman devrimci.

Eylemimiz, sözle, yazıyla, hançerle, silahla, dinamitle, hatta Blanqui veya Trinquet gibi uygun bir adaya oy vermek [anarşistler ilkesel olarak oy kullanmazlar, burada farklı bir örnek kastedilmiş olabilir] söz konusu olduğunda, oylamayla bile kalıcı bir isyan olmalıdır. Tutarlıyız ve isyan için kullanılabilecek her silahı kullanacağız. Yasal olmayan her şey bizim için doğrudur.

“Fakat ne zaman harekete geçip hücumumuzu başlatmalıyız?” diye soruyor arkadaşlar bazen. “Gücümüzün organize olmasını beklememiz gerekmez mi? Hazır olmadan saldırmak, kendinizi ifşa etmek ve başarısızlık riskini almaktır.”

Arkadaşlar, saldırmadan önce yeterince güçlü olana kadar beklemeye devam edersek, asla saldırmayacağız ve yüzmeyi öğrenene kadar denize girmeyeceğine ant içmiş iyi adam gibi olacağız. Tıpkı egzersizin kaslarımızın gücünü geliştirmesi gibi, gücümüzü geliştiren de devrim niteliğindeki eylemdir. Doğru, ilk başta darbelerimiz ölümcül olmayacak; belki ciddi ve bilge sosyalistleri bile güldürebiliriz, ama her zaman cevap verebiliriz: “Bize gülüyorsunuz çünkü yürümeyi öğrenirken düşen bir çocuğa gülenler kadar aptalsınız. Bize çocuk demek sizi eğlendiriyor mu? Pekala, o zaman biz çocuğuz, çünkü gücümüzün gelişimi henüz emekleme aşamasında. Ama yürümeye çalışarak, insan olmaya, yani eksiksiz,
<br>
sağlıklı ve güçlü, devrim yapabilecek organizmalar olmaya çalıştığımızı gösteriyoruz ve zamanından önce yaşlanan, karalamalar yapmayan, asla sindiremeyecekleri bir bilimi sürekli çiğnemeyen ve sonsuz uzay ve zamanda her zaman bulutlarda kaybolan bir devrim hazırlamayan editörler olmadığımızı [kanıtlıyoruz].

Eylemimize nasıl başlayalım?

Sadece bir fırsata bakın, yakında ortaya çıkacaktır. Bu isyanın hissedilebildiği ve savaşın sesinin duyulabildiği her yerde, olmamız gereken yer orası. Üzerinde resmi sosyalizm etiketi bulunan bir hareketin içinde yer almak için beklemeyin. Her halk hareketi, devrimci sosyalizmin tohumlarını zaten beraberinde taşır: büyümesini sağlamak için onun içinde yer almalıyız. Açık ve kesin bir devrim ideali ancak sonsuz küçük bir azınlık tarafından formüle edilir ve tam olarak zihnimizde hayal ettiğimiz gibi görünen bir mücadelede yer almak için beklersek, sonsuza kadar bekleriz. Her şeyden önce formülü soran dogmatistleri taklit etmeyin: Halk, yaşayan devrimi kalbinde taşıyor ve biz de onlarla birlikte savaşmalı ve ölmeliyiz.

Ve yasal ya da parlamenter eylemin destekçileri gelip, halk oy kullanırken onlarla [o eyleme yönelik] bir ilgimiz olmadığı için bizi eleştirdiklerinde, onlara şu yanıtı vereceğiz: “Elbette, tanrıları, kralları ya da efendileri önünde diz çökmüş olan halkla hiçbir işimizi yapmayı reddediyoruz; ama güçlü düşmanlarına karşı dik durduklarında her zaman yanlarında olacağız. Bizim için siyasetten kaçınmak, devrimden kaçınmak değildir; herhangi bir parlamenter, yasal veya gerici eyleme katılmayı reddetmemiz, şiddetli ve anarşist bir devrime, sömürülenlerin ve yoksulların devrimine olan bağlılığımızın ölçüsüdür.”



