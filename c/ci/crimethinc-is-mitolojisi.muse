#cover c-i-crimethinc-is-mitolojisi-8.jpg
#title İş Mitolojisi
#subtitle  Gözlerinizi saatlerde tutturacak ve dirsek çürüttürecek sekiz mit
#author CrimethInc.
#SORTtopics çalışma karşıtlığı, giriş
#date 03.09.2018
#source 30.04.2022 tarihinde şuradan alındı: [[https://lite.crimethinc.com/2018/09/03/is-mitolojisi-gozlerinizi-saatlerde-tutturacak-ve-dirsek-curutturecek-sekiz-mitc][lite.crimethinc.com]]
#lang tr
#pubdate 2022-04-30T16:13:56
#notes İngilizce Aslı: [[https://theanarchistlibrary.org/library/crimethinc-the-mythology-of-work][The Mythology of Work]]


*** İş Gereklidir

Bu durum “iş” demekle neyi kastettiğinize göre değişir.

Ne kadar çok insanın kendileri için bahçıvanlık, balıkçılık, marangozluk, aşçılık ve hatta bilgisayar programcılığı yapıp, bundan zevk aldığını düşünün. Peki ya böyle bir aktivite bütün ihtiyaçlarımızı karşılayabilseydi ne olurdu?

İnsanlar yüzyıllarca, teknolojik gelişmelerin insanlığı çalışma ihtiyacından kurtaracağını iddia etti. Bugün atalarımızın hayal dahi edemeyeceği yeteneklere sahibiz ama bu tahminler hâlâ gerçekleşmedi. ABD’de, aslında birkaç jenerasyon öncesinden daha uzun saatler çalışıyoruz – fakirler hayatta kalabilmek için, zenginler rekabet edebilmek için. Diğerleri umutsuzca iş arıyor, tüm bu gelişmenin sağlaması gereken rahat boş zamanların tadını zar zor çıkarıyor. Kemer sıkma yöntemlerine ve durgunluk mevzusuna rağmen şirketler kayıtlı kazançları rapor ediyor, zenginler her zamankinden daha da zenginleşiyor ve muazzam miktarda mal sadece atılmak için üretiliyor. Bir sürü zenginlik var ama bu zenginlikler insanlığı özgürlüğüne kavuşturmak için kullanılmıyor.

Nasıl bir sistem aynı anda hem refah üretir hem de işin çoğunu yapmamızı önler? Serbest piyasanın savunucuları toplumumuz bu şekilde organize olduğu müddetçe başka bir seçeneğin olmadığını iddia eder.

[[c-i-crimethinc-is-mitolojisi-1.jpg f]]

Buna rağmen bir zamanlar, zaman çizelgeleri ve güç yemeklerinden önce, her şey iş olmadan yapılıyordu. İhtiyaçlarımızı karşılayan doğal dünya henüz paylaştırılmamış ve özelleştirilmemişti.

Bilgi ve yetenekler lisanslı uzmanların harici tutulan çalışma alanı değildi, zengin kuruluşlarca rehin tutulan zaman verimli çalışma ve tüketilecek boş zaman olarak ayrılmamıştı. Bunu biliyoruz çünkü iş sadece birkaç bin yıl önce icat edildi, ama insanlar yüz binlerce yıldır buradalar. Bize hayatın “ıssız, fakir, pis, yabani ve kısa” olduğu söylendi – ama bu öykü bize hayat tarzının kökünü kazıyanlardan geliyor; bu tarzı uygulayanlardan değil.

Bu durum, işleri eskisi gibi yapabileceğimizi ya da yapmamız gerektiğini söylemiyor; sadece şu anki hâlde olmak zorunda olmadığını söylüyor. Eğer en eski atalarımız bugünkü bizi görebilselerdi, büyük ihtimalle bazı icatlarımız için heyecanlanacak ve bazılarındansa korkacaklardı. Ama kesinlikle onları nasıl uyguladığımızı görünce şoka uğrayacaklardı. Biz bu dünyayı emeğimizle bu hâle getirdik ve belirli bir engel olmadan elbette daha iyi hâle getirebiliriz. Bu durum, öğrendiğimiz her şeyden vazgeçmeliyiz anlamına gelmiyor. Sadece işe yaramadığını öğrendiğimiz her şey- den vazgeçmek anlamına geliyor.

*** İş Verimlidir

Birisi işin verimli olduğunu güçlükle inkâr edebilir. Sadece birkaç yüz yıl önce çarpıcı bir biçimde bütün dünya yüzeyini kapladı.

Ama bu tam olarak neyi üretiyor? Milyonlar tarafından kullanılan tek seferlik yemek çubukları, bir kaç sene içinde modası geçmiş olacak laptoplar ve cep telefonları. Kilometrelerce çöp atıkları ve tonlarca kloroflorokarbonlar. Başka bir yerde iş gücü ucuzlar ucuzlamaz paslanacak fabrikalar. Milyonlar yetersiz beslenmeden muzdaripken stok fazlalarıyla dolu olan çöplükler, sadece zenginlerin karşılayabileceği ilaç tedavileri; arzuları kâr amacına, ihtiyaçları da mülkiyet haklarına bağlayan bir toplumda çoğumuzun romanlara, felsefelere ve sanat akımlarına ayıracak zamanımız yok.

Peki bütün bu üretim için kaynaklar nereden geliyor? Yağmalanmış ve sömürülmüş ekosistemlere ve topluluklara ne oluyor? Eğer iş verimliyse, bir o kadar da yıkıcıdır. İş, malları havadan sudan üretmiyor; bu bir hokkabazlık hareketi değil. Bunun yerine, biyosferden ( bütün canlıların ortak malı olan hazineden) hammaddeyi alır ve onları piyasa mantığıyla canlandırılmış ürünlere dönüştürür. Dünyayı bilanço tablosu açısından görebilenler için bu bir gelişmedir ama geri kalanlarımız onların sözlerine inanmamalıyız.

Kapitalistler ve sosyalistler her zaman işin değer üreteceğine güvenmişlerdir. İşçiler başka bir olasılığı da göz önünde bulundurmalıdır; iş, değeri tüketir. İşte bu yüzden ormanlar ve kutuplardaki buzullar, hayatımızın saatleriyle yan yana tüketiliyor: işten eve geldiğimizde vücudumuzda olan ağrılarla aynı doğrultuda olan küresel boyuttaki hasarlar.

Eğer bütün bu şeyler olmasaydı ne üretmemiz gerekecekti? Mutluluğun kendisine ne dersiniz? Yaptığımız şeylerin öncelikli amacının servet toplamak veya rekabette üstünlük sağlamak yerine, hayatta en iyisini yapmak ve onun gizemlerini keşfetmek olduğu bir toplumu hayal edebilir miyiz? Tabii ki böyle bir toplumda da maddi mallar yapabiliriz ama çıkar mücadelesi olmaması şartıyla. Festivaller, ziyafetler, felsefe, romantizm, yaratıcı meşguliyet, çocuk yetiştirme, arkadaşlık, macera gibi kavramları boş zamanlarımıza sıkıştırdığımız şeyler olarak değil de hayatımızın merkezi olarak görebilir miyiz?

Bugün işler tam tersinde gidiyor - bizim mutluluk fikrimiz üretimi teşvik etmek üzerine kurulmuş. Şaşılmayacak ürünler bizi dünya dışına sıkıştırıyor.

*** İş Servet Yaratır

İş; basitçe daha önceden sadece fakirlik olan yerde zenginlik yaratmaz. Tam tersine, başkalarının masraflarını zenginleştirdiği sürece, iş; yoksulluk yaratır, daha da fazlası, buna doğrudan orantılı olarak kâr sağlar. Yoksulluk nesnel bir durum değildir, kaynakların eşit olmayan bir şekilde dağıtımından kaynaklanan bir ilişkidir.

İnsanların her şeyi eşit bir şekilde paylaştıkları toplumlarda yoksulluk diye bir şey yoktur. Kıtlık olabilir ama başkaları ne yapacaklarını bilemeyecek kadar çok şeye sahipken, kimse hiçbir şeyi olmadan gitmeye saygısızca mecbur bırakılmamıştır. Kâr biriktirildiğinden ve toplumda etki yaratmak için gerekli olan en düşük servet eşiği gittikçe yükseldiğinden, yoksulluk gittikçe zayıflatıcı bir hâl alıyor. Bu sürgün; dışlandığın hâlde bir toplumun içinde kalmaya devam ettiğin için sürgünün en acımasız türü. Ne dahil olabilirsin ne de başka bir yere gidebilirsin.

İş sadece zenginliğin yanında fakirlik yaratmaz, yoksulluğu enine boyuna yayarken bir kaçının elinde olan zenginliğe yoğunlaşır. Her bir Bill Gates için bir milyon insan yoksulluk sınırının altında yaşamalıdır, her bir Shell Oill için bir Nijerya olmalıdır. Ne kadar fazla çalışırsak emeğimizden o kadar kâr biriktiriliyor ve sömürücülerimize kıyasla daha da fakirleşiyoruz.

Böylece servet yaratmaya ek olarak, iş; insanları fakirleştiriyor. Bizi fakir yapan bütün diğer yolları çok önceden bile hesaba katabildiğimiz çok açıktır; özdeğerlendirmemizden, boş zamanımızdan, sağlıktan, kendi kariyerimizden ve banka hesaplarımızdan öte bir yoksunluk: ruh yoksunluğu.


[[c-i-crimethinc-is-mitolojisi-2.jpg f]]


*** Geçinmek İçin Çalışmalısın

“Yaşam maliyeti” değerlendirmeleri yanıltıcıdır - zaten ortada çok küçük bir geçinme olayı var! “Çalışmanın maliyeti” demek daha uygun olur ve çalışmanın maliyeti ucuz değil.

Hepimiz ev temizlikçilerinin ve bulaşıkçıların ekonomimizin omurgası olmak için ne ödediğini biliyoruz. Yoksulluğun bütün belaları - bağımlılık, parçalanan aileler, sağlık yoksunluğu vb. şeyler -beklenenden farklı değil; bunlardan kurtularak bir şekilde zamanında emek sarf edenler; çalışan mucizelerdir. Bu gücü kendi işçileri için kâr sağlamaktan başka bir şeye uygulamakta özgür olsalardı, neleri başarabileceklerini düşünün.

Piramitte yükselebilecek kadar bahtı açık çalışanları için ne dersiniz? Daha yüksek bir maaşın daha fazla para böylece daha fazla özgürlük demek olduğunu düşünürdünüz ama bu kadar kolay değil. Her iş saklı bir maliyet gerektirir; tıpkı bir bulaşıkçının her gün işten eve otobüs bileti ücreti ödemek zorunda olması gibi, kurumsal bir avukatın birdenbire bir yerlere uçabilmek zorunda olması, resmi olmayan iş toplantıları için bir şehir kulübünde üyeliğini sürdürmesi ve içinde misafirlerini eğlendirebileceği küçük bir köşke sahip olması gerektiği gibi. Orta sınıf çalışanlar için para biriktirip, hazır öndeyken işten ayrılıp bu hengameden kurtulmanın bu kadar zor olmasının sebebi işte bu, yani basitçe ekonomide öne geçmeye çalışmak yerinde saymaktan ibaret. En iyi ihtimalle, daha süslü bir koşu bandına terfi edebilirsiniz ama üzerinde kalabilmek için daha hızlı koşmaya başlamanız gerekecektir.

Ve bu çalışmaların finansal maliyetleri en ucuz hâlleridir. Bir ankette yoldan geçen insanlara hayallerindeki hayatı yaşamak için ne kadar paraya ihtiyaçları olduğu sorulmuş ve hepsi yaklaşık olarak şu anki gelirlerinin iki katını söylemiş. Yani para (bağımlılık yapan bir uyuşturucu gibi) sadece elde etmesi maliyetli bir şey değil ayrıca tatmin ediciliği git gide azalan bir şey. Ve hiyerarşide ne kadar yukarı çıkarsanız aynı yerde kalabilmek için o kadar fazla savaşmanız gerek. Zengin yönetici başa çıkılmaz tutkularından ve vicdanından vazgeçmeli, emeği onun rahatını sağlayan talihsizlerin daha fazlasını hak ettiğine kendini ikna etmeli; sorgulama, paylaşma, kendini başkalarının yerinde görme gibi dürtülerini kontrol altına almalıdır -eğer almazsa öyle ya da böyle daha acımasız bir rakip onun yerini alır. Mavi yakalı işçiler de beyaz yakalı işçiler de kendilerini hayatta tutan işlerini ellerinde tutabilmek için kendilerini öldürmek zorundadır, bu sadece bir fiziksel ve zihinsel tahrip sorusudur.

Bunlar birey olarak ödediğimiz maliyetler ama ayrıca bütün bu çalışmayı ödeyecek küresel bir ücret de var. Çevresel maliyetin yanında işle alâkalı hastalıklar, yaralanmalar ve ölümler var; her yıl binlerce insanı öldürmek, ölmeyenleri sağlık kulüplerine üye yapmak için hamburger satıyoruz. AB Emek Departmanı 11 Eylül saldırısında hayatını kaybedenlerin iki katının 2001 yılında işle alakalı yaralanmaya maruz kaldığını rapor etti ve bu işle alâkalı hastalanmaların hesaba katılmamış hâli. Her şeyden önce, bütün diğer bedellerden daha fazla olan, asla kendi hayatımızı nasıl yönetebileceğimizi öğrenememe, eğer bize kalmış olsaydı, bu gezegendeki zamanımızla ne yapacağımız sorusunu cevaplama şansının asla verilmemiş olmasının bedeli var. İçinde insanların fazla meşgul, fazla fakir ya da fazla bastırılmış olduğu bir dünya için durularak ne kadar şeyden vazgeçtiğimizi asla bilemeyeceğiz.

Bu kadar pahalıysa neden çalışıyoruz? Herkes sebebini biliyor, hayatta kalabilmek için ihtiyacımız olan kaynakları elde etmek için başka bir yol yok ya da bu konuda katılacak bir toplum bile yok. Başka tarz bir hayatı mümkün kılan eski sosyal formların hepsinin kökü kurutulmuş, onlar fatihler, köle ticaretçileri ve ne dokunulmamış ekosistem ne gelenek ne de kabile bırakmamış kuruluşlar tarafından damgalanmışlar. Kapitalist propagandanın tam tersine, özgür insanoğlu başka bir seçeneği varsa çok az bir ücret için fabrikalara doluşmuyor, ad duyulmuş markalı yazılım ve ayakkabılar için bile.

Çalışmada, alışverişte ve fatura ödemede her birimiz bu aktiviteleri gerektiren koşulları idame ediyoruz. Kapitalizm biz pazaryerindeki bütün enerjimizi ve ustalığımızı, marketteki ve borsadaki bütün kaynaklarımızı ve medyadaki bütün dikkatimizi ona yatırdığımız için var. Daha kesin olmak gerekirse, kapitalizm bizim günlük aktivitelerimiz o olduğu için var. Ama eğer başka seçeneğimiz olduğunu hissetseydik onu yeniden üretmeye devam eder miydik?


[[c-i-crimethinc-is-mitolojisi-3.jpg f]]


*** İş Bir Tatmin Yoludur

Tam tersine, insanların mutluluğa ulaşmasını engellemektense, işe teşvik ediciler en kötü şekilde kendilerini inkâr ediyor.

Öğretmenlere, patronlara, piyasanın taleplerine boyun eğmeye, - hukuk kurallarından, ebeveyn beklentilerinden, dinsel yazıtlardan, sosyal kurallardan bahsetmiyoruz bile- bebeklikten itibaren isteklerimizi beklemeye almaya şartlandık. Emirlere uymak bilinçsiz bir reflekse dönüştü, en üstün yararlarınıza olsun ya da olmasın; işi uzmanına bırakmak ikinci doğamız olmuş durumda.

İşleri kendimiz için yapmak yerine zamanımızı satmaya, hayatımızı nasıl değerlendireceğimize göre değil, karşılığında ne alacağımıza göre yaşamaya alıştık. Hayatlarımızı saat saat satan serbest çalışan köleler olarak, kendimizi hepimizin bir kıymeti olduğuna inandırdık ve bu kıymet bizim değer ölçümüz oldu. Bu anlamda alınıp satılan şeyler olduk, aynı tuvalet kâğıdı ve diş macunu gibi. Bir zamanlar insanoğlu olan şeyler şimdi işçiler, aynı önceden domuz olan şeyin şimdilerde domuz pirzolası olması gibi. Hayatlarımız yok oldu, onları parayı harcadığımız gibi harcadık.

<quote>

Çok çalışmak bu kadar harika bir şey olsaydı, kesinlikle zenginler her şeyi kendilerine saklardı.


Lane Kirkland

</quote>


Genellikle bizim için değerli olan şeylerden vazgeçmeye o kadar alışırız ki feda etmek, bir şeyi gerçekten önemsediğimizi göstermenin tek yolu olur. Kendimizi fikirlere, sebeplere ve başkasına olan aşka, kurban ederiz -bunların bize mutluluğu bulmamızda yardımcı olması gerekirken.

Aileleri örnek alalım, aileden biri en çok kim bir şeylerden vazgeçecek diye hastalıklı bir yarışmaya giriyor. Memnuniyet sadece ertelenmiyor, bir sonraki jenerasyona aktarılıyor. Sonunda, büyük olasılıkla boşa zahmetle, yıllar boyunca biriktirilen mutluluğu yaşamanın sorumluluğu çocuklara boyun eğiyor, yine de reşit olduklarında, eğer sorumluluk sahibi yetişkinler olarak görülüyorlarsa, onlar da etten kemiğe kadar çalışmaya başlayabiliyor.

Ama bu sorumluluk bir yerde durmalı.

*** İş İnisiyatif Aşılar

İnsanlar bugünlerde çok çalışıyor, orası kesin. Piyasadaki performans için kaynaklara erişim bağlantısı, eşi benzeri görülmemiş üretime ve teknolojik gelişmelere sebep oldu. Doğrusunu söylemek gerekirse; bizim kendi yaratıcı kapasitemize erişimi o kadar tekelleştirmiş ki çoğu insan sadece hayatta kalmak için değil aynı zamanda sadece yapacak bir şeyleri olsun diye çalışıyor. Ama bu nasıl bir inisiyatif aşılar?

Hadi gezegenimizin yüzleştiği en ciddi krizlerinden biri olan küresel ısınmaya geri dönelim. Onlarca yıl reddedişin ardından, politikacılar ve iş adamları sonunda bu konuyla alâkalı harekete geçiyorlar. Peki ne yapıyorlar? Bunu paraya çevirmenin yollarını arıyorlar! Karbon emisyon kredileri, ‘temiz’ kömür, ‘yeşil’ yatırım firmaları -sera gazlarını önlemenin en etkili yolunun bunlar olduğuna inanarak mı? Kapitalist tüketiciliğin sebep olduğu felaketin daha çok tüketime teşvik edebilmesi çok ironik ancak işin ne tarz bir inisiyatif aşıladığı ile ilgili fazla şeyi açığa vurur. Dünyadaki yaşamın sonunun gelmesini engellemekle yüzleşen nasıl bir insan bundan çıkarının ne olacağını sorabilir?

Eğer toplumumuzdaki her şey başarı kârı güdüsüyle yürütülseydi, bu zaten en başından inisiyatif değil başka bir şey olurdu. Gerçekten inisiyatiften konuşunca, yeni davranış değerleri ve modları başlatmak en uyuşuk işçisi için bile girişimci iş adamları tarafından düşünülemez bir şey. Peki eğer sizin yaratıcı güçlerinizi kiralayan iş, inisiyatifinizi sarsıyorsa?

Bunun kanıtı işyeri sınırlarının da ötesinde, işe bir gün bile geç kalmayan insanlardan kaçı müzik grubu alıştırmalarına geç kalır? Okul için bütün ödevleri zamanında yetiştirebiliyorken, kitap kulübü için okunması gereken kitabı zamanında okuyamıyoruz; hayatımızda gerçekten yapmak istediğimiz her şey eninde sonunda yapılacaklar listesinin sonunda yerini alıyor. Taahhütler aracılığıyla takip etme yeteneği, kendimiz dışında, harici ödüller ya da cezalarla ilişkilendirilen bir şeye dönüşüyor.

Bir dünya düşünün ki insanların yaptıkları her şeyi kişisel olarak gerçekleştirmek istedikleri için yaptığı bir dünya olsun. İlgisiz elemanlarını motive etmekle uğraşan her bir işveren için, aynı projeler üzerine eşit bir şekilde bağlanmış insanlarla çalışmak ütopik bir düşünce. Ama bu patronsuz ve maaşsız hiçbir işin yapılamayacağı anlamına gelmiyor -sadece işin inisiyatifimizi nasıl baltaladığını gösteriyor.

*** İş Güvenlik Sağlar

Hadi işinizin sizi asla yaralamadığını, hastalandırmadığını ve zehirlemediğini varsayalım. Hatta ekonominin senin işini ve birikimlerini parçalamayıp elinden almayacağını ve senden daha kötü durumda olan herhangi birinin seni incitemeyeceğini ve soyamayacağını varsayalım. Hâlâ işten çıkarılamaz olduğunuzdan emin olamazsınız. Günümüzde kimse hayatı boyunca aynı işveren için çalışmıyor; bir yerde, yerinize daha genç, daha ucuza çalışan ya da yurtdışından destek alan biri gelene kadar, bir kaç yıl çalışırsınız. İşinizde en iyisi olduğunuzu kanıtlamak için belinizi kırarsınız ancak yine de dışarıda kurumaya bırakılmış olarak bulursunuz kendinizi.

İşvereninize kurnazca kararlar vermesi konusunda güvenmek zorundasınız ki böylece maaş çeklerinizi yazabilsin, çünkü onlar sadece etrafa para saçıp, size maaş veremezler. Ama asla bu kurnazlığı ne zaman sizin aleyhinize kullanacaklarını bilemezsiniz; şu anda hayatınızı yaşayabilmek için güvendiğiniz bu insanlar, şuan da bulundukları yere duygularıyla hareket ederek gelmediler. Eğer kendi işinizde çalışıyorsanız, piyasanın ne kadar vefasız olabileceğini biliyorsunuzdur.



[[c-i-crimethinc-is-mitolojisi-4.jpg f]]



Gerçek güvenliği ne sağlayabilir? Büyük ihtimalle insanların birbirini kolladığı uzun ömürlü bir topluluğun, mali teşviktense karşılıklı yardımlaşma odaklı bir topluluğun parçası olmak. Ve günümüzde bu tarz bir topluluk oluşturmanın baş engeli nedir?

İş.


[[c-i-crimethinc-is-mitolojisi-5.jpg f][“Kariyer” kelimesinin İngilizcedeki fiil anlamı: Belirli bir yönde yüksek hızla ve kontrolsüz bir şekilde hareket etmek: Araba karayolunda kontrolsüzce ilerledi ve bir çitten geçti.]]



*** İş Sorumluluk Öğretir

Tarih boyunca en büyük adaletsizliğe kim maruz kaldı? İşçiler. Mutlaka bu durumdan, ilk söyleyenler onlar olduğu için, onların sorumlu olduğunu söylemek şart değil.

Bir maaş almak, sizi kendi yaptıklarınızın sorumluluğunu almaktan alıkoyar mı? Çalışmak böyleymiş hissi vermeyi güçlendiriyormuş gibi gözüküyor. “Sadece emirleri uyguluyordum” savunması, milyonlarca işçinin marşı ve mazereti olmuş durumda. İşyerinde birinin vicdanını kontrol etmedeki gönülsüzlüğü (aslında bir nevi paralı asker olmak), türümüzün başının etini yiyen belaların çoğunun kökünde yatıyor.

İnsanlar emir almadan da bir çok berbat şey yaptı. Sadece kendisi için hareket eden bir kişiyi ikna edebilirsiniz, o kişiye kendi kararlarından sorumlu olduğunu kabul ettirebilirsiniz. Diğer bir yandan işçiler, sonuçlarını düşünmeyi reddederek hayal edilemeyecek kadar aptalca ve yıkıcı şeyler yapabilir.

Tabii ki asıl sorun işçilerin yaptıkları şeylerin sorumluluğunu almayı reddetmesi değil, asıl sorun sorumluluk almayı yasaklayıcı bir şekilde pahalılaştıran ekonomik sistemde.


[[c-i-crimethinc-is-mitolojisi-6.jpg f]]


Çalışanlar zehirli atıkları nehirlere ve okyanuslara atıyor. Çalışanlar yemek için inekleri öldürür ve maymunlar üzerinde deneyler yapar. Çalışanlar kamyon dolusu yiyeceği çöpe atıyorlar. Çalışanlar ozon tabakasına zarar veriyorlar. Sizin her hareketinizi güvenlik kameralarından seyrediyorlar. Kira ödemediğinizde sizi tahliye ediyorlar. Vergilerinizi ödemediğinizde hapse atıyorlar. Ev ödevinizi yapmadığınızda veya zamanında çalışmaya gelmediğinizde sizi küçük düşürüyorlar. Özel hayatınızla ilgili bilgileri kredi raporlarına ve FBI dosyalarına giriyorlar. Size hız cezası fişleri veriyorlar ve arabanızı çekiyorlar. Standartlaştırılmış sınavlar, çocuk gözaltı merkezleri ve ölümcül enjeksiyonlar uyguluyorlar. İnsanları gaz odalarına götüren askerler de çalışandı, Tıpkı Irak ve Afganistan’ı işgal eden askerler gibi, ıpkı kendilerini hedef alan intihar bombacıları gibi -cennette maaşını almayı uman Tanrı’nın çalışanlarıdırlar.


[[c-i-crimethinc-is-mitolojisi-7.jpg f]]


Bu Kadarı Yeterli!

Güven fonuna sahip şımarık bir velet olmadığınızı kanıtlamak için

—Bu diğer herkesin harcaması pahasına yapması demek olsa bile!

Bunun dışında kalan her şey sorumsuz, intihara meyilli,

tanrı aleyhine bir günah, fakir ebeveynlerine bir ihanet,

başka bir seçeneği olmayan piçlerin suratına bir tokat,

ve gözetim altında kalma şartlarını ihlâl etmektir-

kendi yolunuzu karşılamak zorundasınız!

Şimdi

oraya geri git

ve

çalışmaya başla!

Şu konuda açık olalım; işi eleştirmek, emeği, çabayı, hırsı ve bağlılığı reddetmek demek değildir. Her şeyin eğlenceli ve kolay olmasını talep etmek anlamına da gelmiyor. Bizi çalışmaya mecbur bırakan güçlere karşı savaşmak zor bir iş. Tembellik; işin bir alternatifi değildir, ancak yan ürünü olabilir.

Sonuç basit; hepimiz kendimize uygun olduğunu düşündüğümüz potansiyelimizden en iyi şekilde yararlanmayı ve kendi kaderimizin efendisi olmayı hak ediyoruz. Hayatta kalmak için bu şeyleri kendimizden uzaklaştırmaya zorlanmak çok trajik ve küçük düşürücü. Bu şekilde yaşamak zorunda değiliz.

Peki eğer hiç kimse çalışmasaydı? Tersaneler boşalır ve montaj hatları bir bocalamaya gülümserdi, en azından kimsenin gönüllü olarak üretmediği şeyleri yapanlar. Televizyonla pazarlama son bulurdu. Zenginlik ve ünvan adına diğerleri üzerinde hakimiyet kuran değersiz bireyler daha iyi sosyal yetenekler geliştirmek zorunda kalırdı. Petrol yayıntısı ve trafik karışıklığı son bulurdu. Kağıt paralar ve iş başvuruları, insanlar tekrar paylaşmaya ve takasa geri döndüğü için, yangın başlatıcı olarak görülürlerdi. Çimenler ve çiçekler kaldırım çatlakları arasından yükselirdi ve en sonunda meyve ağaçlarına dönüşürlerdi.

Ve hepimiz açlıktan ölürdük. Ama biz gerçekten kağıt işleriyle ve performans evrimleriyle geçinmiyoruz değil mi? Para için yaptığımız ve ürettiğimiz çoğu şeyin hayatta kalmakla bir alakası yok ve hayatımıza bir anlam dahi katmıyor.



