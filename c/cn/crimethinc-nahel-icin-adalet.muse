#title Nahel için Adalet
#subtitle Fransa’daki Ayaklanmanın Kökleri
#author CrimethInc.
#date 02.07.2023
#source 06.07.2023 tarihinde şuradan alındı: [[https://tr.crimethinc.com/2023/07/02/nahel-icin-adalet-fransadaki-ayaklanmanin-kokleri][tr.crimethinc.com]]
#lang tr
#pubdate 2023-07-06T13:22:32
#topics fransa, isyan
#notes Çeviri: Yeryüzü Postası <br> İngilizce Aslı: [[https://tr.crimethinc.com/2023/07/02/justice-for-nahel-the-roots-of-the-uprising-in-france][Justice for Nahel: The Roots of the Uprising in France]]


<quote>
Aşağıdaki metin, Paris’in bir banliyösü olan Nanterre kentinde Nahel Merzouk adlı gencin Fransız polisi tarafından öldürülmesinin ardından yaşanan olayların üçüncü gününde Fransız yoldaşlar tarafından bize gönderilmiştir. Rapor, durumun bir analizini ve Fransa’da 1970’lerde başlayan polis şiddetine karşı mücadeleye genel bir bakış ortaya koymaktadır.
</quote>

Bugün bu hareket sokaklarda, medyada ve mahkemelerde yoğun bir baskıyla karşı karşıya. Şu an itibariyle Nahel’in yanı sıra en az üç kişi öldürülmüş durumda. Ülke genelinde konuşlandırılan özel askeri polislere odaklanmak yerine, Nahel ve kendileri için hayatlarını tehlikeye atan gençlerin çabalarıyla başlamayı tercih ediyoruz.

Sokaklarda pek çok kişi öfke duygularının ve mücadelenin yoğunluğunun 2005 yılındaki isyanları anımsattığını söylüyor. Tıpkı 2005’teki öğrenci hareketinin ardından gerçekleşen o ayaklanmalar gibi, bu gerçek ayaklanma da Cumhurbaşkanı Emmanuel Macron tarafından dayatılan emeklilik reformuna karşı ilkbaharda eşi benzeri görülmemiş bir baskıyla karşılaşan güçlü hareketi takip etti. Fransa’da polis, muazzam kaynak aktarımına ve sahip olduğu gerçek anlamda yasal dokunulmazlığa rağmen, hem meşruiyet algısını hem de halkın geniş kesimlerini sindirerek pasifleştirebilme gücünü kaybediyor gibi görünüyor.

[[c-n-crimethinc-nahel-icin-adalet-1.jpg f]]

*** Nahel İçin Adalet

27 Haziran 2023 tarihinde, 17 yaşındaki Nahel Merzouk Nanterre’de araba kullanırken motosikletli polisler tarafından yol kontrolü için durduruldu ve ardından soğukkanlılıkla öldürüldü. Yolculardan birinin daha sonra anlattığı üzere, polislerden biri Nahel’i “Kıpırdama yoksa kafana bir kurşun sıkarım” diye tehdit etti. Ardından her iki memur da arabanın açık penceresinden Nahel’e vurdu. Aldığı darbelerle sersemleyen Nahel yanlışlıkla freni bırakıp gaza bastı ve bunun üzerine memurlardan biri onu silahla vurarak öldürdü. Tüm bunları biliyoruz çünkü neredeyse tüm sahne filme alındı.

Nahel’in öldürülmesine ilişkin video kısa sürede sosyal medyada viral hale geldi ve bu da ardından gelen huzursuzlukta önemli bir rol oynadı. İnsanlar sokaklarda hızla tepki gösterdi.

İlk gece olan 27 Haziran’dan itibaren Nanterre ve Paris’in diğer banliyölerinde (Mantes-la-Jolie, Boulogne-Billancourt, Clichy-sous-Bois, Colombes, Asnières, Montfermeil) ve Fransa genelinde (Roubaix, Lille, Bordeaux…) göçmenlerin çoğunlukta olduğu mahallelerde şiddetli çatışmalar patlak verdi. 28 Haziran’da, politikacıların bu cinayetin iğrenç karakterini kabul etmelerine, hükümetin ve solun ılımlı kanatlarının barış çağrıları yapmalarına rağmen, isyan diğer şehirlere de yayıldı (Neuilly sur Marne, Clamart, Wattrelos, Bagnolet, Montreuil, Saint Denis, Dammarie les Lys, Toulouse, Marsilya…). Bu arada Nahel’in ailesi, kardeşi 2016 yılında polis tarafından vahşice öldürülen Assa Traoré ve eski “Mouvement de l’Immigration et des Banlieues” (MIB) [Göç ve Banliyöler Hareketi] militanlarının yardımıyla bir “Hakikat ve Adalet Komitesi” (“Comité Vérité et Justice”) kurdu. Nahel’in onur ve cesaret örneği olan annesi, 29 Haziran öğleden sonra Nanterre’de büyük bir marche blanche (“beyaz yürüyüş”) çağrısında bulundu.

Hükümet 29 Haziran sabahı, Nahel’i öldüren polis memurunun kasıtlı adam öldürme suçu işleyip işlemediğine dair bir soruşturma başlattığını açıkladı. Görünüşe göre bu durum insanları yürüyüşe katılmaktan caydırmadı.

Bu büyük yürüyüş tahminen 15,000 kişiyi bir araya getirdi. “Herkes polisten nefret ediyor”, “Polis, tecavüzcü, katil” ve “Nahel için adalet” gibi sloganlar eşliğinde Nahel’i son yolculuğunun uğurladılar. Bir pankartta “Başka kaç Nahel kameraya çekilmedi?” yazıyordu.

O andan itibaren, Nahel’in ölümünün büyük bir şok yarattığı ve protestocuların çoğunun kurbanın ailesiyle dayanışma amacıyla yürüdüğü ortadaydı. Ancak talepler aynı zamanda çok daha genel bir şeyle ilgiliydi: polisin toplumumuzdaki rolü. Sanki bunun farkındalarmış gibi, domuzlar bu barışçıl yürüyüşü Nanterre’deki Préfecture’e (merkezi hükümetin bölgesel birimi) ulaştığında gaza boğmaya karar verdiler ve La Défense’ın şık ticari bölgesine kadar yayılan yeni bir çatışma dalgası başlattılar. Genç isyancılar arasından yükselen mesaj “Eğer yürüyüş yapmamıza izin vermezlerse, her şeyi sikip atarız” oldu.

[[c-n-crimethinc-nahel-icin-adalet-2.jpg f][Nanterre’deki yürüyüşün ardından, 29 Haziran]]

Sayıları çok fazla olduğu için 29 Haziran akşamı harekete katılan mahalle ve kasabaları tek tek saymak mümkün değil. Hükümetin cinayeti soruşturacağını açıklamasına rağmen durulmayan bu üçüncü huzursuzluk gecesi, harekete daha önce görülmemiş bir boyut kazandırdı. Jeunes de quartiers (medya ve politikacıların sık sık kullandığı tabirle ” kenar mahalle çocukları”) arabaları, motosikletleri ve scooter’ları, yerel ve ulusal polis karakolları, okullar, belediye kütüphaneleri, valilik ve belediye binaları gibi kamu binalarını ateşe verdi. Polisle çatışmak için havai fişek kullanmanın yanı sıra, sokaktaki araç gereçleri parçaladılar, süpermarketleri yağmaladılar ve inşaat malzemelerini ateşe verdiler. Geçtiğimiz birkaç yıl içinde bu silahlar, her gün tacize ve keyfi polis operasyonlarına maruz kalan gençler arasında tercih edilen öz savunma silahları haline geldi.

Ülke çapındaki bu isyan durup dururken ortaya çıkmadı. Büyük ölçüde yatay, öngörülemez ve onu harekete geçiren isteklere uygun olarak sürekli yeni direniş biçimleri icat etmesi anlamında spontaneydi. Ancak bu isyan aynı zamanda devletin post-kolonyal göçü ele alma biçimine bir yanıt olarak ortaya çıktı.

[[c-n-crimethinc-nahel-icin-adalet-3.jpg f]]

*** Ayaklanmanın Arka Planı

1960’lardan bu yana Fransız devleti, Kuzey ve Batı Afrika’daki eski sömürgelerinden “ithal ettiği” işgücünden yararlandı. Başlangıçtaki plan bu işçilerin Fransa’da bir hayat kurup yerleşmeleri değildi. Belirli bölgelere yerleştirildiler: önce gecekondu mahallelerine, sonra da büyük kent merkezlerinin çevresindeki projelere – “cités”-. Bu bölgeler “banliyöler” olarak anılmaya başlandı.

1970’lerde, Siyahi ve Arap işçilerin Fransa nüfusunun kalıcı bir parçası olduğu ortaya çıktığında, bu durum siyasi bir sorun haline geldi. Birbiri ardına iktidara gelen siyasi partiler bir istisna politikası benimsedi. Amaç, ırksal sınırları korumak ve sürekli olarak incelenen ve sosyal düzen için bir tehdit olarak tanımlanan bir insan grubunu kontrol etmekti. Sonuç olarak, göçmen işçi sınıfı mahalleleri esas olarak polisiye tedbirlerle yönetilmiştir. Polis (ve yerel polisin bağlı olduğu valilikler), Fransa’nın kendi polislik anlayışının deney alanları haline gelen “cités “lerdeki günlük işlerin yönetimi ve kontrolünden neredeyse tümüyle sorumludur.

Bu mahallelerde yaşayanlar her gün polis tarafından aşağılanmaya, korkutulmaya ve cezalandırılmaya maruz bırakılmaktadır. Göçmen kökenli gençler, ülkenin siyasi yaşamından dışlanmanın yanı sıra sürekli kontrol altında tutuklanmakta, hakarete uğramakta ve tutuklanmaktadır. Aynı şekilde, en güvencesiz olanların hayatta kalmak için bağımlı oldukları tüm etkinlikler ve ticari faaliyetler ağır bir şekilde kriminalize edilmektedir.

İsyanlar, Fransa’da ırkçı saiklerle işlenen polis cinayetlerinin uzun tarihi bağlamında da ele alınmalıdır. Amerika Birleşik Devletleri’nde olduğu gibi Fransa’da da, egemen insanlık anlayışından dışlanan bireylere karşı ölçüsüz şiddet kullanımı, ırksal kategorileri üreten ve devam ettiren mekanizmalardan biridir. Polis 1970’lerden bu yana yüzlerce Siyahi ve Arap genci öldürmüştür. Bu kısmen, göçmen mahallelerindeki yoğun ve sürekli polis varlığının bir sonucudur. Ancak daha genel olarak, Fransız devleti ile aileleri 1960’lardan sonra Fransız sömürge imparatorluğunun kademeli olarak parçalanması sırasında Fransa’ya göç eden gençler arasındaki ilişkiyi tanımlayan yapısal ırkçılığın somut bir sonucudur.

[[c-n-crimethinc-nahel-icin-adalet-4.jpg f]]

Quartier’lerdeki (kelime anlamıyla “mahalleler”[1]) insanlar onlarca yıldır polis şiddetine karşı açık siyasi tutum almışlardır. 1983 yılında Lyon ve Marsilya banliyölerinde işlenen bir dizi polis cinayetine tepki olarak “Marche pour l’Egalité” (Eşitlik Yürüyüşü) düzenlenmiştir. Beyaz olmayan gençlere yönelik devlet güdümlü polis şiddetinin bir sembolü olan Vaulx-en-Velin kentinde 1979’dan bu yana her on yılda bir kitlesel ayaklanmalar meydana gelmiştir. 1995 yılında kurulan “Mouvement Immigration Banlieue”, “polisin hatalarının” (bu ifade özürcülerin polisin aşırı şiddet eylemlerini tanımlamak için kullandıkları kılıf) kurbanlarının aileleri için “hakikat ve adalet” (vérité et justice) için mücadele etti. Bu ana akım siyasi partilerin söylemlerini reddeden, bağımsız örgütlenmiş ve otonom bir örgüttü. 2000 yılında Paris’teki mekânından tahliye edildi.

2005 yılında Zyed Benna ve Bouna Traoré adlı iki gencin Paris’in kuzeyindeki Clichy-sous-Bois’de polis tarafından kovalanıp tartaklanarak öldürülmesinin ardından bir ayaklanma patlak vermişti. Diğerlerinin yanı sıra, 2005 yılında polis tarafından öldürülen Lamine Dieng’i; 2016 yılında polis tarafından öldürülen Adama Traoré’yi; 2017 yılında polis tarafından tecavüze uğrayan Théo Luhaka’yı; 2019 yılında polis tarafından öldürülen Ibrahima Bah’ı hatırlıyoruz.

Her seferinde aynı senaryo: polis cinayet işliyor, sonra da kendini korumak için yalan söylüyor. Bazen bir video ya da bir protesto polisin anlatısına karşı çıkıyor ve yetkilileri katile karşı dava açmaya zorlayacak yeterli kanıt sağlıyor. Ancak polislere karşı yürütülen yasal süreçler neredeyse hiçbir zaman mahkumiyetle sonuçlanmıyor. Fransa’da yasalar devletin çıkarlarına hizmet eder; uygulamada ise polisler için serbestlik ve yasal dokunulmazlık sağlanır.

Geçtiğimiz birkaç gün içinde, devletin kendisini koruyanları himaye ettiğini bir kez daha gördük. Göğsünden vurulduktan sonra Nahel’i tedavi eden sağlık görevlisi, onu öldüren polis memurunun adını medyaya ifşa edince, hemen 18 ay hapis cezasına çarptırıldı.[2]

[[c-n-crimethinc-nahel-icin-adalet-5.jpg f]]

*** Yoğunlaşan Toplumsal Çatışmalar Bağlamında

Bu ayaklanmaları anlamak için, onları Fransa’daki çağdaş sınıf mücadelesi bağlamında da görmeliyiz. Fransa 2016’dan bu yana neredeyse her yıl ülke çapında bir toplumsal hareket ya da huzursuzluk dalgası yaşadı. İsyanlar Fransız siyasi dilinin ayrılmaz bir parçası haline geldi ve 2023’te gördüklerimiz bunun bugüne kadarki en radikal ifadesi olabilir.

Başka bir deyişle, 2016’dan bu yana Fransa’da zorla uygulanan neoliberal politikaların ne kadar sevimsiz olduğu göz önüne alındığında, François Hollande ve Emmanuel Macron hükümetleri ancak polis şiddeti sayesinde iktidarda kalabildiler. Sağcı ve faşist polis sendikaları, devleti, hükümeti, polisi ve halkı birbirine bağlayan yapısal iktidar ilişkilerini anladıkları için, giderek daha fazla sosyal yardımı ve diğer herkese şiddet uygulamak için teknolojik ve yasal araçları kendi ellerinde toplamak için sistematik olarak örgütlendiler.

Örneğin 2017 yılında çıkarılan bir yasa ile polise, kendileriyle işbirliği yapmayı reddeden bireylere karşı ateşli silah kullanma hakkı (ve dolayısıyla teşviği) verildi. Bu yasanın doğrudan sonucu, yıllık polis cinayeti sayısında dramatik bir artış oldu. 2017’den önce polis (resmi olarak) her yıl 15 ila 20 genç Siyahi ve Arap erkeği öldürürken, bu sayı 2021’de 51’e yükseldi ve o zamandan beri ortalama 40 oldu.

Daha genel olarak, her yıl ellerinde daha fazla ekipman bulunan daha fazla yeni polis memuru işe alındı. Militarize edilmiş polis, toplumsal hareketlere karşı sistematik baskı uygulamaktadır; polisin giderek hızlanan militarizasyonu, Fransa’daki bazı solcuları etkileyen güçsüzlük hissini açıklayan faktörlerden biridir. Somut olarak bu durum, özellikle göçmen mahallelerinde yaşayan kadınlar başta olmak üzere pek çok kişi için gergin ve güvencesiz yaşam koşulları yaratmaktadır. Annelerimiz.

[[c-n-crimethinc-nahel-icin-adalet-6.jpg f][Nanterre’deki beyaz yürüyüşün neticesi, 29 Haziran]]

*** Huzursuzluk

Süregelen huzursuzluk dalgasıyla ilgili olarak, Paris’e yakın banliyölerde yaşadığım şehirde gördüklerimi anlatarak sadece kendi konumumdan konuşabilirim.

Hareket, hepsi de çok etkili olan üç temel taktik kullandı: polisle şiddetli çatışmalar, Cumhuriyet’in “sembollerinin” tahrip edilmesi ve yağmalama.

Polisle çatışmalar çoğunlukla “quartiers” adı verilen mahallelerde meydana geldi. “Yak onları!” Bu görüntüleri herkes görmüştür: polislere havai fişeklerle, molotof kokteylleriyle, taşlarla ve genellikle çok genç olan kara blok kıyafetli kişiler tarafından dış mekan mobilyalarıyla saldırılmaktadır. Geceleri meydana gelen saldırgan eylemlerden bazıları, özellikle Nahel ile dayanışmadan ziyade, her gün insanları kontrol eden, aşağılayan ve dövenlerden intikam almak için daha genel bir arzudan kaynaklanıyor olabilir. Sanki iktidar dengesi geçici olarak yön değiştirmiş gibi.

Çatışma anında ne sloganlar ne de solcu mesajlar var, sadece radikal bir karşılık verme iradesi var. Katılan ekiplerin çoğu birbirlerini uzun zamandır tanıyan, çoğunluğu erkek olan gençlerden oluşuyor. Bu taktikleri uygulayan kişilerin uzlaşma gibi bir istekleri yok.

Paris yakınlarındaki bir bölgede belediye binasında çatışmalar: [[https://vimeo.com/841547915]]

Birçoğu ergenlik çağında olan genç eylemciler sistematik bir şekilde hareket ediyor. Belli nedenlerden dolayı ilçe ofislerine, belediye binalarına ve yürütme gücünün bulunduğu yerlere saldırdılar. Ama aynı zamanda insanları ayrıştıran, dışlayan ve kapitalist sisteme zorlayan okullara; polislerin arkadaşlarını yakalayıp dövdüğü polis karakollarına; hareketlerini izleyen güvenlik kameralarına; “mahallelerde” nadir bulunan ve genellikle soylulaştırıcıları yeni çevrilmiş banliyö evlerine taşımak için yeni inşa edilen toplu taşıma altyapısına; ve banliyölerin soylulaştırılmasında önemli bir rol oynayan Olimpiyat Oyunları için yeni ve anında eskimiş altyapı inşa eden şantiyelere de saldırıyorlar.

Son olarak, hareket yaratıcı gücünü yağma alanında, özellikle de araba ve scooterların oynadığı rolle ortaya koymuştur. Arabalar kapıları ve çitleri zorlamak için kullanılırken, scooterlar daha sonra hızlı bir çıkış imkanı sağlamaktadır. Scooterlar polisle çatışmalarda da önemli bir rol oynuyor. Çok fazla ayrıntıya girmeden, hareket kabiliyeti geceleri gerçekleşen çatışmalar için kritik bir öneme sahip.

Ne yağmalanıyor? Neredeyse her şey, ancak kurumsal medya anlatısının aksine, yağmalamanın çoğu şenlikli ya da eğlenceli değil: alınanların büyük çoğunluğu sadece temel mallar ve ilaçlar. Bu da Nahel’in ölümüyle ateşlenen hareketin aynı zamanda güvencesizliğin ve hayat pahalılığının temelden anti-kapitalist bir reddini ifade ettiğini gösteriyor.

Sabahın dördünde mahalledeki süpermarkette duydum: “Bunların hepsini annem için alıyorum.”

[[c-n-crimethinc-nahel-icin-adalet-9.jpg f][Nanterre’deki beyaz yürüyüşün neticesi, 29 Haziran]]

Huzursuzluğun temelindeki siyasi duygunun son derece evrensel olmasına ve polis şiddetine karşı mücadelenin (en azından) 2016’dan bu yana toplumsal hareketlerdeki merkeziliğine rağmen, sol ile genç isyancılar arasında bir ittifak olasılığı zayıf görünüyor. Solcu siyasetçiler daha ziyade barış ve uzlaşma çağrısı yapıyor, “polis ile halk arasındaki diyaloğu yeniden başlatacak” (“refonder une police républicaine” [cumhuri̇yetçi̇ bi̇r poli̇s gücünün yeni̇den i̇nşası] ve “rétablir le dialogue entre la police et sa population”[polis ve halk arasındaki diyaloğun yeniden tesis edilmesi]) “cumhuriyetçi bir polis reformu” projeleri hayal ediyorlar.

Devrimci sol (Fransa’da ağırlıklı olarak Troçkistlerden oluşuyor), “Comité Vérité et Justice pour Adama” [Adama için Hakikat ve Adalet Komitesi] ve Traoré ailesi örnek alınarak aile üyeleri ve yakın destekçiler tarafından oluşturulan “Comité Vérité et Justice pour Nahel”i [Nahel için Hakikat ve Adalet Komitesi] destekliyor, ancak mevcut ayaklanmaya ilişkin herhangi bir kamusal tutum almadılar. Anarşistlere ve diğer otonom gruplara gelince, bazılarımız ayaklanmalara aktif olarak katılsak da, çoğunlukla gözlem, yasal ve lojistik destek rolleri üstlenerek hala yerlerini bulmaya çalışıyorlar.

Sonuçta hareket her şeye rağmen devam ediyor ve katılan gençler kendilerini bir parçası olarak hissetmedikleri gruplarla pek ilgilenmiyorlar.

[[c-n-crimethinc-nahel-icin-adalet-10.jpg f]]

[1] Burada quartiers, banliyö şehirlerindeki göçmen mahallelerinde yaşayan insanların kendi topluluklarına atıfta bulunmak için kullandıkları gündelik bir terimdir.
[2] Bu kaynak, durumun ayrıntılarının biraz daha karmaşık olduğunu öne sürüyor; Nahel’in öldürülmesiyle ilgili olarak bir sağlık görevlisinin cezasının ertelendiğini, katil hakkında bilgi verdiği iddia edilen bir başka kişinin ise 12 ayı ertelenmiş olmak üzere 18 ay hapis cezasına çarptırıldığını doğruluyor.
